## tdspw
TD Securities Public Website

### Build
mvn clean install

- pom.jar will build a war
- TDSPublicApplication.java is the main class for war deployment
- The startup class is configured in the pom.xml

- use pom-jar.xml to build as a Spring Boot jar
- TDSPublicJarApplication.java is the main class for Spring Boot jar deployment
- Running as Spring Boot: mvn spring-boot:run

### Maven Repo
The TD Nexus repo should be used as the main repository. This can be configured in your Maven settings.xml file. The following is the sample configuration.

```
    <mirrors>
        <mirror>
            <id>td-nexus</id>
            <name>Public</name>
            <url>https://repo.td.com/repository/maven-public/</url>
            <mirrorOf>central</mirrorOf>
        </mirror>
    </mirrors>
```