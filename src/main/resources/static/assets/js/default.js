/********************** DON'T MODIFY THIS DIRECTLY! ***********************/
/*jslint white: true */
(function loadModules(global, modules) {
    'use strict';

	// Store modules and global to global variables to trigger jquery modules manually
	window.modules_ext = modules;
	window.global_ext = global;	
	
    // Loads required modules in the page.
    // First filter the module list, then executes each of the parts.

    var id, funcId, tag, parts,
        doc = global.document,
        modulesToLoad = ["td_rq_common"],
        tags = doc.querySelectorAll('[class^="td_rq"]');

    for (id = 0; id < tags.length; id += 1) {
        tag = tags[id].className.split(" ")[0];
        tag = tag.replace(/-/g, '_'); // replace all '-' with '_';
        if (modulesToLoad.indexOf(tag) === -1) {
            modulesToLoad.push(tag);
        }
    }

    for (id = 0; id < modulesToLoad.length; id += 1) {
        tag = modulesToLoad[id];
        if (modules.hasOwnProperty(tag)) {
            if (global.console) {
                global.console.log(tag);
            }
            parts = modules[tag];
            for (funcId = 0; funcId < parts.length; funcId += 1) {
                parts[funcId](global);
            }
        }
    }
}(this, {
td_rq_common: [function (global) {/* Source: src/js/td-common.js */
"use strict";
// Dylan Chan (dylan.chan@td.com) - 2016/08/10
// - Fixed bug with equaliseHeight not working with lazy load image because the 'complete' event was being triggered before the image actually finished loading.
//   This is fixed by adding the .onload event to the image element (actual dom element) before firing the complete() function.
// - (dylan.chan@td.com) 2016/10/07 - Added 'checkLazyLoad' on the Tab Carousel "afterTabActiveChanged" event to trigger the image load when the tab changes.
// - (dylan.chan@td.com) 2016/10/11 - Increased tolerance to 400 in the method to detect if an element is visible, this allows lazy load to trigger earlier
// - (dylan.chan@td.com) 2016/10/13 - In Tab Carousel "afterTabActiveChanged" event added check that lazy load images exists before triggering the check
//                                  - In checkLazyImages(), make sure scroll event exists before destroying it...this prevents a situation where the event doesn't exists and then all window events are removed
/**
 * 2018-03-14 by ayeon.chung@td.com
 * - added expand interaction(g.events.afterExpandForOthers) to re-render when the open state of Expand/collapse component gets triggered (afterExpandForOthers)
 */
(function (window) {

var bp, g, $imagesToLoad, nbImagesToLoad, lazyImages, nbLazyImages, evScroll, scrollTimer, resizeTimer,
        resizeActions = [],
        nbImagesLoaded = 0,
        evNS = '.tdG',

        $ = window.jQuery,
        $window = $(window),
        doc = window.document,
        Modernizr = window.Modernizr,
        clearTimeout = window.clearTimeout,
        setTimeout = window.setTimeout,

        hasTransitions = Modernizr.csstransitions,
        TRANSITION_END_EV,

    // -----------------
    // Utilities

        ua = window.navigator.userAgent,
        android = false,
        iOS = false;

    if (/Android/.test(ua)) {
        android = ua.substr(ua.indexOf('Android') + 8, 3);
    } else if (/(iPhone|iPod|iPad)/.test(ua)) {
        iOS = ua.substr(ua.indexOf('OS ') + 3, 3).replace('_', '.');
    }

    // Add helper class for older versions of Android & iOS.
    if ((android && android < 3) || (iOS && iOS < 5)) {
        $('html').addClass('static');
    }

    window.android = android;
    window.iOS = iOS;

    // -----------------
    // PolyFills
    /*global jQuery */
    /*!
     /* * FitText.js 1.2
     *
     * Copyright 2011, Dave Rupert http://daverupert.com
     * Released under the WTFPL license
     * http://sam.zoy.org/wtfpl/
     *
     * Date: Thu May 05 14:23:00 2011 -0600
     *//*

     (function( $ ){

     $.fn.fitText = function( kompressor, options ) {

     // Setup options
     var compressor = kompressor || 1,
     settings = $.extend({
     'minFontSize' : Number.NEGATIVE_INFINITY,
     'maxFontSize' : Number.POSITIVE_INFINITY
     }, options);

     return this.each(function(){

     // Store the object
     var $this = $(this);

     // Resizer() resizes items based on the object width divided by the compressor * 10
     var resizer = function () {
     $this.css('font-size', Math.max(Math.min($this.width() / (compressor*1), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
     };

     // Call once to set.
     resizer();

     // Call on resize. Opera debounces their resize by default.
     $(window).on('resize.fittext orientationchange.fittext', resizer);

     });

     };

     })( jQuery );
     $(".td-icon-wrapper").fitText();*/


    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function (find, i) {
            var n;
            if (i === undefined) {
                i = 0;
            }
            if (i < 0) {
                i += this.length;
            }
            if (i < 0) {
                i = 0;
            }
            for (n = this.length; i < n; i += 1) {
                if (this.hasOwnProperty(i) && this[i] === find) {
                    return i;
                }
            }
            return -1;
        };
    }

    if (!String.prototype.trim) {
        (function () {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function () {
                return this.replace(rtrim, "");
            };
        }());
    }

    if (hasTransitions) {
        TRANSITION_END_EV = (function () {
            var name,
                el = doc.createElement('div'),
                transEndEventNames = {
                    WebkitTransition: 'webkitTransitionEnd',
                    MozTransition: 'transitionend',
                    OTransition: 'oTransitionEnd otransitionend',
                    transition: 'transitionend'
                };
            for (name in transEndEventNames) {
                if (transEndEventNames.hasOwnProperty(name)) {
                    if (el.style[name] !== undefined) {
                        return transEndEventNames[name];
                    }
                }
            }
            return false; // explicit for ie8
        }());

        $.event.special.transitionEnd = {
            bindType: TRANSITION_END_EV,
            delegateType: TRANSITION_END_EV,
            handle: function (e) {
                if ($(e.target).is(this)) {
                    return e.handleObj.handler.apply(this, arguments);
                }
            }
        };

        $.fn.emulateTransitionEnd = function (duration) {
            var callback,
                $el = $(this),
                called = false;

            $el.one('transitionEnd', function () {
                called = true;
            });

            callback = function () {
                if (!called) {
                    $el.trigger(TRANSITION_END_EV);
                }
            };

            setTimeout(function () {
                callback();
            }, duration);
            return this;
        };
    }

    // -----------------
    // Global Helpers

    bp = {
        // breakpoints in px
        xs: 480,
        sm: 768,
        md: 1024,
        lg: 1200,

        // Page width at breakpoints
        smw: 770,
        mdw: 980,
        lgw: 1032,
        ie8w: 960,

        // Space between grid columns
        gutter: 30
    };

    window.g = g = {

        hasTransitions: hasTransitions,

        // Events objects
        ev: $({}),

        events: {
            afterTabActiveChanged: "tdActiveTabChanged",
            afterModalShown: "tdModalShown",
            afterModalHidden: "tdModalHidden",
            afterImagesPreloaded: "tdImagesPreloaded",
            afterLazyImagesLoaded: "tdLazyImagesLoaded",
            afterBackToTop: "tdBackToTop",
            afterHeaderHeightChanged: 'tdHeaderHeightChanged',
            afterProdInfo: "tdProdInfo",
            afterCompareTable: "tdCompareTable",
            afterLargeModalOverlay: "tdLargeModalOverlay",
            afterExpand: "tdExpand",
            afterCatalogueIndicator: "tdCatalogueIndicator",
            afterOperationalMessageDelay: "tdOperationalMessageDelay",
            afterOperationalMessage: "tdOperationalMessage",
            afterABanner: "tdABanner",
            afterAnchorDown: "tdAnchorDown",
            afterExpandForOthers: "tdExpandForOthers",
            afterTabActiveChangedForExternal: "tdActiveTabChangedExternal"
        },
        classes: {
            container: "td-container",
            heroWrapper: "hero-wrapper",
            heroDarkGreen: "td-hero-dark-green",
            heroLightGreen: "td-hero-light-green",
            heroAction: "td-hero-button"
        },

        queryCallout: ".td-callout",
        queryClickable: ".td-makeclickable",
        queryClickableAnchor: "a.td-makeclickable-target",
        queryEqualHeight: ".td-equalcalloutheight",
        queryLazyImages: ".td-lazy [data-src]",
        queryBack: ".td-link-back",
        queryLinks: ".td-link-green, .td-link-dark-green, .td-link-black, .td-link-white, .td-link-gray, .td-link-red, .td-link-standalone, .td-headerlink, .td-list-links li a, .td-rte-link-standalone, .td-rte-link-standalone-14, .td-rte-link-standalone-16, .td-rte-link-standalone-18, .td-rte-link-standalone-22, .td-rte-link-standalone-29",
        queryLinksExceptions: "<sup>",

        dot: function (klass) {
            return "." + klass;
        },

        bpSet: function (meta) {
            if (bp[meta] !== undefined) {
                return bp[meta];
            }
            return bp.lg;
        },

        getPluginDef: function (constructor, name) {
            return function (options) {
                var plugin;
                return this.each(function () {
                    var $this = $(this);
                    if (typeof options === "object" || !options) {
                        if (!$this.data(name)) {
                            $this.data(name, constructor($this, options));
                        }
                    } else {
                        plugin = $this.data(name);
                        if (plugin && plugin[options]) {
                            return plugin[options].apply(plugin, Array.prototype.slice.call(arguments, 1));
                        }
                    }
                });
            };
        },

        getEqualHeight: function ($elements) {
            var $docElement,
                height = 0;

            $elements.each(function () {
                $docElement = $(this);
                $docElement.css({height: 'auto'});
                height = Math.max(height, $docElement.outerHeight());
            });

            return height;
        },

        equaliseHeight: function ($wrapper, query) {
            var $elements = $wrapper.find(query),
                height = g.getEqualHeight($elements);

            $elements.animate({height: height}, 50);

            return height;
        },

        doEqualiseHeight: function () {
            var $wrappers = $(g.queryEqualHeight);

            if ($window.innerWidth() >= bp.sm) {
                $wrappers.each(function () {
                    var $wrap = $(this);
                    if ($wrap.is(":visible")) {
                        g.equaliseHeight($wrap, g.queryCallout);
                    }
                });
            } else {
                $wrappers.find(g.queryCallout).css({height: ''});
            }
        },

        makeClickable: function ($elements, anchorQuery) {

            // Binds the a click event on the whole element

            $elements.on('click', function (event) {
                var $el = $(this),
                    $anchor = $el.find(anchorQuery),
                    url = $anchor.attr("href"),
                    name = $anchor.attr("target"),
                    $target = $(event.target);

                if (!$target.is("a") && !$target.parent().is("a")) {
                    if (name) {
                        window.open(url, name);
                    } else {
                        window.open(url, "_self");
                    }
                    return false;
                }
            });
        },

        isElementVisible: function (element) {
            var TOLERANCE = 400,
                screenWidth = $window.innerWidth(),
                screenHeight = $window.innerHeight(),
                rect = element.getBoundingClientRect(),
                bottomLimit = screenHeight + TOLERANCE,
                isHorVisible = rect.left >= 0 && rect.right <= screenWidth + TOLERANCE,
                isBelowTop = rect.top >= 0 && rect.top <= bottomLimit,
                isAboveBottom = rect.bottom <= bottomLimit && rect.bottom >= -TOLERANCE,
                isDisplayed = element.offsetWidth > 0 && element.offsetHeight > 0;

            return isHorVisible && (isBelowTop || isAboveBottom) && isDisplayed;
        },

        loadImage: function (element, complete) {

            var img,

                setLoaded = function (src) {
                    (g.checkSrcset(src)) ? element.srcset = src : element.src = src;
                    element.className = (element.className + ' loaded').trim();
                    element.onload = function() {complete(); }; // trigger complete event only after the element has finished loading the new image, this ensures the image is fully on the page so any calculations dependent on it will be correct
                };

            img = new window.Image();

            img.onerror = function () {
                element.style.height = '100%';
                (this.srcset == "" || typeof this.srcset === "undefined") ? setLoaded(this.src) : setLoaded(this.srcset);
            };
            img.onload = function () {
                (this.srcset == "" || typeof this.srcset === "undefined") ? setLoaded(this.src) : setLoaded(this.srcset);
            };

            if(g.checkSrcset(element.getAttribute('data-src'))){
                if(typeof img.srcset === "undefined") {
                    var imgId=element.getAttribute('id'),
                        imgClass=element.getAttribute('class'),
                        imgSrc=element.getAttribute('src'),
                        imgSrcset=element.getAttribute('data-src'),
                        imgDataSrc=element.getAttribute('data-src');

                    if(!imgId) {imgId='';}
                    else{imgId="id='"+imgId+"'";}

                    if(!imgClass) {imgClass='';}
                    else{imgClass="class='"+imgClass+" loaded'";}

                    $(element).parent().append('<img '+imgId+' '+imgClass+' src="'+element.getAttribute('src')+'" srcset="'+element.getAttribute('data-src')+'" style="display:none;"/>');
                    $(element).parent().find('img').fadeIn();
                    $(element).remove();
                    $('.td-lazy img[data-src]').animate({opacity: 1}, 100);

                    try {
                        picturefill();
                    }
                    catch(err) {

                    }
                }else{
                    img.srcset = element.getAttribute('data-src');
                }
            }else{
                img.src = element.getAttribute('data-src');
            }
        },

        checkLazyImages: function () {

            // Builds the list of the images to be loaded and loads them
            // Returns the number of loaded images.
            // When 0, we need to set the Slider height directly, because no event is fired.

            var imgId, image, nbImgToLoad,
                nbLoaded = 0,
                imagesToLoad = [],

                completeLoad = function () {
                    nbLoaded += 1;
                    if (nbLoaded === nbImgToLoad) {
                        g.ev.trigger(g.events.afterLazyImagesLoaded);
                    }
                };

            for (imgId = 0; imgId < nbLazyImages; imgId += 1) {
                image = lazyImages[imgId];
                if (g.isElementVisible(image)) {
                    imagesToLoad.push(image);
                    lazyImages.splice(imgId, 1);
                    nbLazyImages -= 1;
                    imgId -= 1;
                }
            }

            nbImgToLoad = imagesToLoad.length;
            if (nbImgToLoad > 0) {
                for (imgId = 0; imgId < imagesToLoad.length; imgId += 1) {
                    g.loadImage(imagesToLoad[imgId], completeLoad);
                }
            }

            // Destroy the Lazy loading feature if no more images to be loaded
            if (!nbLazyImages) {
                // make sure event exists before we remove it, otherwise it will remove all window events
                if (evScroll != undefined) {
                    $window.off(evScroll);
                }
            }

            return nbImgToLoad;
        },

        doEqualiseSize: function ($wrapper, query) {
            var nbItems, $equaliseTabItems;

            if ($wrapper.is(':visible')) {
                $equaliseTabItems = $wrapper.find(query);
                nbItems = $equaliseTabItems.length;

                if ($window.innerWidth() < bp.sm) {

                    // get the margins of the elements and add them up
                    var total_margin = 0;
                    $equaliseTabItems.each(function (i, tabItem) {
                        this === tabItem;
                        var $_this = $(this);
                        total_margin += $_this.outerWidth(true) - $_this.outerWidth();
                    });

                    // if elements have a margin, calculate element size in px,
                    // taken into account of their margin and available space
                    if (total_margin > 0){
                        // get available width by getting the wrapper width minus the total margin
                        var available_width = $wrapper.width() - total_margin;

                        // change element widths to pixel value
                        $equaliseTabItems.css({width: Math.floor(available_width / nbItems) + "px"});
                    }
                    // otherwise we will just size by percentage
                    else {
                        $equaliseTabItems.css({width: (100 / nbItems) + "%"});
                    }

                    g.equaliseHeight($wrapper, query);
                } else {
                    $equaliseTabItems.css({width: "", height: ""}); // cancel any previous styles
                }
            }
        },

        equaliseSize: function ($wrapper, query) {
            resizeActions.push(function () {
                g.doEqualiseSize($wrapper, query);
            });
            g.doEqualiseSize($wrapper, query);
        },


        loadLazyImages: function () {
            evScroll = 'scroll' + evNS;

            // Waits for the lazy images to be loaded before equalize heights.
            g.ev.on(g.events.afterLazyImagesLoaded, function () {
                g.doEqualiseHeight();
            });

            g.checkLazyImages();

            $window.on(evScroll, function () {
                if (scrollTimer) {
                    clearTimeout(scrollTimer);
                }
                scrollTimer = setTimeout(function () {
                    g.checkLazyImages();
                }, 100);
            });
        },

        preLoadNonLazyImages: function () {

            // PreLoads the images
            // Triggers an event when all the images are loaded.
            // Waits for the images to be loaded before equalizing heights

            g.ev.one(g.events.afterImagesPreloaded, function () {
                g.doEqualiseHeight();
            });

            $imagesToLoad.one('load' + evNS, function () {
                nbImagesLoaded += 1;
                if (nbImagesLoaded === nbImagesToLoad) {
                    g.ev.trigger(g.events.afterImagesPreloaded);
                }
            }).each(function () {
                // Handles cached images
                if (this.complete) {
                    $(this).trigger('load');
                }
            });
        },

        initHero: function ($el) {
            var $heroHeadlines, $heroButtons, $pageMargins,
                padding = 0, isWrapped = false, defaultPadding = bp.gutter / 2;

            function setHeroPadding() {
                var offsetLeft = isWrapped ? padding : $pageMargins.offset().left + padding;

                if ($window.innerWidth() < bp.md) {
                    $heroButtons.css({left: offsetLeft + defaultPadding});
                } else {
                    $heroButtons.css({left: ''}); // reset the style
                }

                $heroHeadlines.css({paddingLeft: offsetLeft});
            }

            $heroHeadlines = $el.find(g.dot(g.classes.heroDarkGreen) + ', ' + g.dot(g.classes.heroLightGreen));
            $heroButtons = $el.find(g.dot(g.classes.heroAction));

            if ($heroHeadlines.length || $heroButtons.length) {

                if ($el.closest('[class|="' + g.classes.container + '"]').length) {
                    isWrapped = true;
                    padding = defaultPadding;
                } else {
                    $pageMargins = $(g.dot(g.classes.container));

                    if (!$pageMargins.length) {
                        padding = defaultPadding;
                        $pageMargins = $(g.dot(g.classes.container));

                        // Create a container if none on the page
                        if (!$pageMargins.length) {
                            $(doc.body).append($('<div/>').addClass(g.classes.container).css({visibility: 'hidden'}));
                            $pageMargins = $(g.dot(g.classes.container));
                        }
                    }
                }

                setHeroPadding();
                resizeActions.push(setHeroPadding);
            }
        },

        initLinks: function () {

            // Stand Alone Links (updated Feb 2014).
            // Define variables in other to checking if the link has child nodes
            // or if last word is part of child node or plain text
            // g.queryLinksExceptions is added by daniel.cho@td.com

            var $link, linkHtml, linkHtmlSplit, lastChild, linkText, linkCompile, lastText, firstText, linkHtml_last,
                arry_queryLinksExceptions = g.queryLinksExceptions.split(",");

            $(g.queryLinks).html(function () {
                $link = $(this);
                linkHtml = $link.html().trim();
                linkHtmlSplit = linkHtml.split(" ");
                linkHtml_last = linkHtmlSplit[linkHtmlSplit.length-1];
                linkHtmlSplit.pop();
                lastChild = $link.children().last();
                linkText = $link.text().trim().split(" ");
                lastText = linkText.pop();
                // check if link has plain text or child node
                if (lastChild.length) {
                    lastChild.html(lastChild.html().trim());
                }

                for(var i =0; i <= arry_queryLinksExceptions.length - 1; i++){
                    if( linkHtml_last.indexOf(arry_queryLinksExceptions[i]) >= 0){
                        lastText = linkHtml_last;
                    }
                }

                if (lastChild.text() === lastText) {
                    lastChild.wrap('<span  data-msg="Link Icon Chevron" class="td-link-lastword" />');
                    linkCompile = $link.html().trim();
                }
                else {
                    linkCompile = linkHtmlSplit.join(" ") + (" <span class='td-link-lastword'>" + lastText + "<span class='td-icon td-icon-rightCaret' aria-hidden='true'></span></span>");
                }

                return linkCompile;
            });

            $(g.queryBack).html(function () {
                $link = $(this);

                $($link).prepend($("<span class='td-link-firstword'> <span class='td-icon td-icon-leftCaret' aria-hidden='true'>&nbsp;</span></span>"));

            });

        },
        checkSrcset: function(src_val){
            var srcset_status = /\.(jpe?g|png|gif|bmp|svg)\s\d[x]/i.test(src_val);

            return srcset_status;
        },
        setFocusAnchorImage: function(){
            $("a").focus(function (e) {
                if($(this).children('img').get(0)){
                    $(this).css({'display':'block','outline-width':'2px','padding':'1px'});
                }
            });
        },
        removeProdFromDrawer: function(cb_fn,type,id,cp_type){
            if((typeof cb_fn !== 'function' || typeof cb_fn === 'undefined') && (typeof type !== 'string' || typeof type === 'undefined') && typeof id === 'undefined'){
                return false;
            }else{
                if(typeof cp_type === 'undefined') cp_type = '';

                g.ev.trigger(g.events.afterProdInfo,[{prodinfo:{prod_type:type,prod_id:id, comp_type:cp_type,evt_type:'remove', cb_func:cb_fn}}]);
            }
        },
        addProdToDrawer: function(cb_fn,type,id,name,img,cp_type,so,url){
            if((typeof cb_fn !== 'function' || typeof cb_fn === 'undefined') && (typeof type !== 'string' || typeof type === 'undefined') && typeof id === 'undefined'){
                return false;
            }else{
                if(typeof name === 'undefined') name = '';
                if(typeof img === 'undefined') img = '';
                if(typeof cp_type === 'undefined') cp_type = '';
                if(typeof so === 'undefined') so = false;
                if(typeof url === 'undefined') url = '#';

                g.ev.trigger(g.events.afterProdInfo,[{prodinfo:{prod_type:type,prod_id:id, prod_name:name, prod_img:img, comp_type:cp_type, evt_type:'add', speical_offer: so, prod_url:url, cb_func:cb_fn}}]);
            }
        },
        cookies:  {
            getItem: function (sKey) {
                if (!sKey || !this.hasItem(sKey)) { return null; }
                return unescape(document.cookie.replace(new RegExp("(?:^|.*;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"), "$1"));
            },
            setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
                if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/.test(sKey)) { return; }
                var sExpires = "";
                if (vEnd) {
                    switch (typeof vEnd) {
                        case "number": sExpires = "; max-age=" + vEnd; break;
                        case "string": sExpires = "; expires=" + vEnd; break;
                        case "object": if (vEnd.hasOwnProperty("toGMTString")) { sExpires = "; expires=" + vEnd.toGMTString(); } break;
                    }
                }
                document.cookie = escape(sKey) + "=" + escape(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
            },
            removeItem: function (sKey, path) {
                if (!sKey || !this.hasItem(sKey)) { return; }
                var path = path || "/";
                var oExpDate = new Date();
                oExpDate.setDate(oExpDate.getDate() - 1);
                document.cookie = escape(sKey) + "=; expires=" + oExpDate.toGMTString() + "; path=" + path;

                // document.cookie = escape(sKey) + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            },
            hasItem: function (sKey) { return (new RegExp("(?:^|;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie); }
        },
        flyToElement: function (flyer, flyingTo) {
            var divider = 3;
            var $flyerClone = $(flyer).clone();
            $flyerClone.height($(flyer).height());
            $flyerClone.width($(flyer).width());
            $flyerClone.css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 10001});

            $('body').append($flyerClone);
            //var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
            var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/2)/2;
            var gotoY = $(flyingTo).offset().top-100;// + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;

            $flyerClone.animate({
                    opacity: 0.4,
                    left: gotoX,
                    top: gotoY,
                    width: $(flyer).width()/2,
                    height: $(flyer).height()/2
                }, 500,
                function () {

                    $(this).animate({
                        top: gotoY+100,
                        opacity: 0
                    }, function () {
                        $(this).fadeOut('fast', function () {
                            $(this).remove();
                        });
                    });

                });


        },
        getUrlVars:	function(){
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                function(m,key,value) {
                    vars[key] = value;
                });
            return vars;
        },
        chk_param: function(){
            var anchor='';
            if(typeof g.getUrlVars()["tdtarget"] !== 'undefined' && g.getUrlVars()["tdtarget"] != ""){
                anchor = g.getUrlVars()["tdtarget"];

                var $obj_id = $('#'+anchor);
                g.slide_down($obj_id);
            }
        },
        slide_down: function($obj){
            var dis_offset = 0,
                $header_nav = $(".td-header-nav"),
                $top_message = $('.td_rq_top-message');

            //if($top_message.get(0)) dis_offset = dis_offset + $top_message.height();
            if($header_nav.get(0)) dis_offset = dis_offset + $header_nav.height();

            if (typeof $obj === 'object') {
                //$obj.attr('tabindex','0');
                $('html, body').animate({
                    scrollTop: $obj.offset().top - dis_offset
                }, 1000, function(){
                    $obj.focus().css({'outline-width': '0px'});
                    /*
                     $obj.on('focus blur keydown', function(event){
                     event.stopPropagation();
                     if ((event.which === 9) || (event.shiftKey && event.which === 9)){ // Tab key & Shift key + Tab key
                     $obj.removeAttr('tabindex');
                     }
                     });
                     */
                });
            }

        },
        buttonStates: function(){
            $('button').on('click',function(){
                $(this).blur();
            });
        },
        /**
         * detect IE
         * returns version of IE or false, if browser is not Internet Explorer
         */
        detectIE: function () {
            var ua = window.navigator.userAgent;

            // Test values; Uncomment to check result …

            // IE 10
            // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

            // IE 11
            // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

            // Edge 12 (Spartan)
            // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

            // Edge 13
            // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                // IE 10 or older => return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }

            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                // IE 11 => return version number
                var rv = ua.indexOf('rv:');
                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }

            var edge = ua.indexOf('Edge/');
            if (edge > 0) {
                // Edge (IE 12+) => return version number
                return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            }

            // other browser
            return false;
        },
        init: function () {
            var RESIZE_EV = (window.onorientationchange !== undefined ? 'orientationchange' : 'resize') + evNS,
                RESIZE_DELAY = 100,
                pushUp;

            g.initLinks();

            // Load lazy images if any,
            // pre-loads non-lazy images if any,
            // register equal-heights events listeners
            // and attach click event to click-able blocks

            lazyImages = $(g.queryLazyImages).toArray();
            nbLazyImages = lazyImages.length;

            $imagesToLoad = $(g.queryEqualHeight).find('img[src]');
            nbImagesToLoad = $imagesToLoad.length;

            // Ready to equalise height when no lazy nor preloaded images
            if (!nbImagesToLoad) {
                g.doEqualiseHeight();
            }

            if (nbImagesToLoad) {
                g.preLoadNonLazyImages();
            }

            if (nbLazyImages) {
                g.loadLazyImages();
            }

            resizeActions.push(function () {
                if (nbLazyImages) {
                    g.checkLazyImages();
                } else {
                    g.doEqualiseHeight();
                }
            });

            $.fn.initHero = g.getPluginDef(g.initHero, "initHero");
            $(g.dot(g.classes.heroWrapper)).initHero();

            // window resize
            $window.on(RESIZE_EV, function () {
                if (resizeTimer) {
                    clearTimeout(resizeTimer);
                }
                resizeTimer = setTimeout(function () {
                    var id;
                    for (id = 0; id < resizeActions.length; id += 1) {
                        resizeActions[id].call();
                    }
                }, RESIZE_DELAY);
            });

            g.ev.on(g.events.afterTabActiveChanged + ' ' + g.events.afterModalShown + ' ' + g.events.afterExpandForOthers, function (event, $element) {
                event.preventDefault();
                //g.equaliseHeight($element.find(g.queryEqualHeight), g.queryCallout);
                g.doEqualiseHeight();
                if (nbLazyImages){  g.checkLazyImages(); }
            });

            g.makeClickable($(g.queryClickable), g.queryClickableAnchor);

            // Push-up
            pushUp = function () {
                $('.td-push-top').each(function () {
                    var $_this = $(this);
                    var screenWidth = $(window).innerWidth();
                    if (screenWidth > 768 - 18) {//not sure why 18 pixel have to be subtracted but this works!
                        $_this.css('margin-top', '-' + $_this.metadata().val + 'px');
                    } else {
                        $_this.css('margin-top', 0);
                    }
                });
            };
            pushUp();
            resizeActions.push(pushUp);
            //g.setFocusAnchorImage();   // don't set focus outline as we are setting it for each component using CSS
            //g.buttonStates();

            // Daniel - Remove dot line(outline) except 'select', 'input', and 'textarea' elements
            /*
             $('*').on('keydown',function(event) {
             if ((event.which === 9) || (event.shiftKey && event.which === 9)){ // Tab key & Shift key + Tab key
             if (!$(event.currentTarget).is("select, input, textarea")){
             $(event.currentTarget).not("select, input, textarea").css({'outline-width': ''});
             }
             }
             });
             $("*").not("select, input, textarea").on('click',function(event) {
             if(event.which == 1){ // mouse click
             $(event.currentTarget).css({'outline-width': '0px'});
             }
             });
             */

            // Remove dot line(outline) except 'div', 'p', 'li', 'select', 'input', and 'textarea' elements and specific selectors when mouse event occours
            /*
             $("*").on("mousedown", function (event) {
             if (!$(event.target).is("div, p, li, i, strong, select, input, textarea, a[onclick], button > span.td-button-close, .td-product-compare.td-form-stacked label.card span.td-label-content-wrapper span.td-label-check, .td-product-compare.td-form-stacked label.card span.td-label-content-wrapper span.td-label-content, a[onclick] sup")) {
             event.preventDefault();
             }
             });
             */
            $(document).on('mousedown',function(event) {
                $('body').addClass('td-no-focus-outline');
            });
            $(document).on('keydown',function(event) {
                $('body').removeClass('td-no-focus-outline');
            });



            //This code adds outline style to elements focused by tab key and removes it from previous target
            //     var $previousActiveElement = $(document.activeElement);

            //     $(document).on( 'keyup', function( e ) {

            //         if( e.which == 9 ) {

            //             if($previousActiveElement != null && !($previousActiveElement.is(":focus")) ){
            //                 $previousActiveElement.css({
            //                     'outline-width': '0px'
            //                 });

            //                 $previousActiveElement = $(document.activeElement);
            //             }

            //             $(document.activeElement).css({
            //                 'outline-style': 'dotted',
            //                 'outline-width': '1px'
            //                 // 'outline-offset': '2px'
            //             });

            //         }
            //     });

            //     $(document).on( 'click', function( e ) {
            //         setTimeout(function(){
            //             if($previousActiveElement != null && !($previousActiveElement.is(":focus")) ){
            //                 $previousActiveElement.css({
            //                     'outline-width': '0px'
            //                 });
            //             }
            //         }, 1000);

            //     });

            var $emergency_messaging = $(),
                operational_msg_slide_down_event,
                operational_msg_equal_height_fn;

            g.ev.on(g.events.afterOperationalMessageDelay, function (event,data) {
                $emergency_messaging = data.$obj;
                operational_msg_slide_down_event = data.evt;
                operational_msg_equal_height_fn = data.fn;
            });

            $window.load(function(){
                g.chk_param();

                // Load Operational Message when page completely loads
                if($emergency_messaging.get(0)){
                    operational_msg_slide_down_event(operational_msg_equal_height_fn);
                }

            });

        }

    };

    g.init();

}(global));}],
td_rq_header_nav: [function (global) {/* Source: src/js/td-header-nav.js */
"use strict";
/*-----------------------------------------------------------------------------------
 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * Header & Navigation (TDCT)
 * Created by dylan.chan@td.com
 * @version 1.0
 *
 * This is the responsive header and navigation used on the TDCT site, it includes the mobile offcanvas menu.
 * Accessibility Fixed by daniel.cho@td.com
	- hide dropdown with ESC
	- hide dropdown when tabbing to next elements
	- made the focus move to the first li item when the accordion is open on mobile device
	- Fixed: made accordion items under td-accordion on mobile clickable.
 * Added secondary mobile menu, improved dropdown accessibility, improved search bar accessibility - dylan.chan@td.com
 * Fixed: 'skip to main' anchor issue because of conflict with AngularJS routing
 * Added 'td-dropdown-no-hover' optional class to disable open on hover (use click instead) on dropdown menus
 * Updated by daniel.cho@td.com
	- Added 'update_main_content_position_msg' function and afterHeaderHeightChanged listener to update a correct spacing to 'td-contentarea' div
 * Added option to pre-expand mobile accordion menu if it has the active accordion class
 * Added 'aria-haspopup' and 'aria-expanded' to mobile menu open buttons
 */

(function(window) {

var $ = window.jQuery;
    var $window = $(window);
    var g = window.g;

    // class names
    var DROPDOWN_CLASSNAME = "td-dropdown";
    var DROPDOWN_ACTIVE_CLASSNAME = "td-dropdown-active";
	var DROPDOWN_CONTENT_CLASSNAME = "td-dropdown-content";
    var DROPDOWN_NO_HOVER_CLASSNAME = "td-dropdown-no-hover";   // disable hover to open the dropdown
    var ACCORDION_CLASSNAME = "td-accordion";
    var ACCORDION_ACTIVE_CLASSNAME = "td-accordion-active";
    var ACCORDION_CONTENT_CLASSNAME = "td-accordion-content";
	var SEARCH_INPUT_CLASSNAME = "td-search-input";
    var MOBILE_MENU_HIDDEN_ELEMENTS = [".td-header-mobile", ".td-contentarea"];  // elements to hide for accessibility when mobile menu is opened
    var MOBILE_MENU_RIGHT_CLASSNAME = "td-nav-mobile-menu-right"; // class to specify that the menu is on the right-side, note that if there's no class the default is left-side
    var MOBILE_MENU_CLASSNAME = "td-nav-mobile-menu";
    var MOBILE_MENU_LIST_ITEM_CLASSNAME = "td-nav-mobile-menu-item";
    var DESKTOP_SEARCH_HIDDEN_ELEMENTS = [".td-nav-primary .td-section-left nav", ".td-nav-primary .td-section-right"];  // elements to hide for accessibility when desktop search is opened

    // constants
    var DROPDOWN_DELAY_TIMER = "dropdown_delay_timer";  // string id to assign delay timer to dropdown menu
    var DROPDOWN_HIDE_DELAY = 250;  // delay in ms before dropdown menu disappears after mouse leave

    var $header_nav_container = $(".td-header-nav");
    var $desktop_header_container = $(".td-header-desktop");
    var $desktop_dropdown_menus = $desktop_header_container.find("."+DROPDOWN_CLASSNAME);
    var $desktop_search_bar = $desktop_header_container.find(".td-nav-desktop-search");
    var $desktop_search_show_btn = $desktop_header_container.find(".td-desktop-search-show-btn");
    var $desktop_search_hide_btn = $desktop_header_container.find(".td-desktop-search-hide-btn");
    var $desktop_skip_to_main_link = $desktop_header_container.find(".td-skip > a");
    var $mobile_menu_container = $(".td-nav-mobile");
    var $mobile_menu = $("."+MOBILE_MENU_CLASSNAME).first();    // select the first one only, if there is a secondary menu it won't be selected - we also do this to ensure backwards compatibility with navs that don't have a secondary menu
    var $mobile_menu_btn_open = $(".td-mobile-menu-button");
    var $mobile_menu_btn_close = $(".td-mobile-menu-close");
    var $mobile_menu_secondary = $(".td-nav-mobile-menu-secondary");
    var $mobile_menu_secondary_btn_open = $(".td-mobile-menu-secondary-button");
    var $mobile_menu_overlay = $(".td-nav-mobile-overlay");
    var $mobile_menu_search_bar =$(".td-nav-mobile-menu-search");
    var $mobile_menu_last_focus;    // stores the last element that has focus before the menu opens, so we can return to it when the menu closes


   
    /**
     * Init
     */
    function init() {
        init_desktop_menu();
        init_mobile_menu();

        // bind resize event
        $window.resize(on_window_resize);

        update_main_content_position();
    }

    function init_desktop_menu(){

        /*** bind events ***/

        // dropdown menu show on hover on desktop
        $desktop_dropdown_menus.on("mouseenter", function(event){
            var current_dropdown = event.currentTarget;

            // show on hover if there is no 'td-dropdown-no-hover' class
            if ($(current_dropdown).hasClass(DROPDOWN_NO_HOVER_CLASSNAME) != true){
                desktop_close_all_dropdowns();  // close all dropdowns to prevent multiple dropdowns being opened at the same time
                desktop_dropdown_open($(event.currentTarget));

                // check if dropdown has a close delay timer, if so, remove the timer
                if (current_dropdown[DROPDOWN_DELAY_TIMER] !== undefined){
                    clearTimeout(current_dropdown[DROPDOWN_DELAY_TIMER]);
                    current_dropdown[DROPDOWN_DELAY_TIMER] = undefined;
                }
            }
        });

        // dropdown menu hides on hover out on desktop
        $desktop_dropdown_menus.on("mouseleave", function(event) {
            var current_dropdown = event.currentTarget;

            // hide on hover if there is no 'td-dropdown-no-hover' class
            if ($(current_dropdown).hasClass(DROPDOWN_NO_HOVER_CLASSNAME) != true) {
                // set a close delay timer so the dropdown doesn't disappear right away - this fixes a usability issue where the menu disappears if the mouse pass outside of the dropdown menu zone briefly
                current_dropdown[DROPDOWN_DELAY_TIMER] = setTimeout(function () {
                    desktop_dropdown_close($(current_dropdown));
                    current_dropdown[DROPDOWN_DELAY_TIMER] = undefined;
                }, DROPDOWN_HIDE_DELAY);
            }
        });

        // dropdown menu toggle on tap on mobile devices, we also want the 'click' event on desktop for JAWS keyboard accessibility
        $desktop_dropdown_menus.on("click", function(event){
            desktop_dropdown_toggle($(event.currentTarget));
        });

        $desktop_dropdown_menus.on("keydown", function(event){  // dropdown menu toggle on space keydown for accessibility
            if ( event.which === 32){ // space key
                desktop_dropdown_toggle($(event.currentTarget));
            }
            if ( event.which === 27){ // esc key
                desktop_dropdown_close($(event.currentTarget));
            }
        });

        $desktop_search_show_btn.on("click touchstart", function(event){
            if (event.type === "touchstart"){ event.preventDefault(); }  // prevent click event from firing again on touch devices
            desktop_show_search();
        });
        $desktop_search_hide_btn.on("click touchstart", function(event){
            if (event.type === "touchstart"){ event.preventDefault(); }  // prevent click event from firing again on touch devices
            desktop_hide_search();
        });
        // setup document click event
        $(document).on('click touchstart', function(event){
            on_document_click(event);
        });

        // setup 'skip to main'
        $desktop_skip_to_main_link.on('click', function(event){
            var anchor_id = $(event.currentTarget).attr('href');

            // only modify link behaviour if anchor exists
            if (anchor_id != undefined){
                var main_element = $(document).find(anchor_id)[0];

                if (main_element != undefined){
                    main_element.scrollIntoView();

                    // use setTimeout 0 to let scrolling finish completely (also allowing time for 'back to top' component to finish its scroll event updates), before attempting to focus on main element
                    setTimeout(function() {
                        main_element.setAttribute("tabindex", "-1");    // set the tabindex to -1 so element can be focused, we need to set this each time because otherwise the 'scroll to top' component has a scroll event which removes it
                        main_element.focus();
                    }, 0);


                }
                event.preventDefault();
            }
        });

        apply_desktop_accessibility();
        apply_search_bar_accessibility();
    }

    /**
     * Fires when the document is clicked.
     * This is used to detect if the space outside of a dropdown menu item is clicked, if so, close the dropdown menu
     */
    function on_document_click(event){
       if (!$(event.target).closest('.'+DROPDOWN_CLASSNAME).length){
           desktop_close_all_dropdowns();
       }
    }

    /**
     * Fires when window is resized
     */
    function on_window_resize(){
		update_main_content_position();
		
        // check if window is at Tablet Landscape, Desktop breakpoint (>= 1024px)
        if ($window.width() >= g.bpSet('md')){
            // check and make sure mobile menu is closed if it was opened previously, this is to unlock the page scrolling
            if ($mobile_menu_container.attr("style") !== undefined){
                if ($mobile_menu_container.attr('style').indexOf("display: block") !== -1){
                    mobile_menu_close();
                }
            }
        }
    }

    /**
     * Update the top padding of the main content so that it is visible below the header/nav
     */
    function update_main_content_position(){
        var header_height = $header_nav_container.outerHeight();
        $(".td-contentarea").css("padding-top", header_height); // padding to push down main content, since header/nav is in position fixed
    }

    /**
     * Opens or closes the dropdown menu
     */
    function desktop_dropdown_toggle($dropdown){
        if ($dropdown.hasClass(DROPDOWN_ACTIVE_CLASSNAME)){
            desktop_dropdown_close($dropdown);
        }
        else {
            desktop_close_all_dropdowns();
            desktop_dropdown_open($dropdown);
        }
    }

    function desktop_dropdown_open($dropdown){
        $dropdown.addClass(DROPDOWN_ACTIVE_CLASSNAME);
        $dropdown.children('a').attr('aria-expanded', 'true');
    }

    function desktop_dropdown_close($dropdown){
        $dropdown.removeClass(DROPDOWN_ACTIVE_CLASSNAME);
        $dropdown.children('a').attr('aria-expanded', 'false');
    }	
	
    function desktop_close_all_dropdowns(){
        desktop_dropdown_close($desktop_dropdown_menus);
    }

    function apply_desktop_accessibility(){
        // add aria tag to dropdowns
        $desktop_dropdown_menus.each(function(index){
            var $dropdown = $(this);
            var $dropdown_link = $dropdown.children('a');
            var $dropdown_content = $dropdown.find("."+DROPDOWN_CONTENT_CLASSNAME);
            var dropdown_link_id = 'td-desktop-nav-dropdown-link-' + index; // unique id to reference the dropdown link

            $dropdown_link.attr('aria-haspopup', 'true');
            $dropdown_link.attr('aria-expanded', 'false');
            $dropdown_link.attr('id', dropdown_link_id);    // add the unique ID

            // add aria tag to dropdown content
            $dropdown_content.attr('aria-labelledby', dropdown_link_id);    // reference dropdown link id to get the label value

            //Hide dropdown menu when tabbing out of last item with 'tab' key
            var $dropdown_content_last_item = $dropdown_content.find('li').last();
            $dropdown_content_last_item.on('keydown',function (event) {
            	if (!event.shiftKey && event.which === 9){ // Tab key only, no Shift
            		desktop_close_all_dropdowns();
            	}
            });
            // Hide dropdown menu when tabbing out of dropdown link with 'shift + tab' key
            $dropdown_link.on('keydown',function (event) {
                if (event.shiftKey && event.which === 9){ // Shift key + Tab key
                    desktop_close_all_dropdowns();
                }
            });
        });

		// add aria tag to search input
		// desktop
		var $search_inputbox_desktop = $desktop_search_bar.find('.'+SEARCH_INPUT_CLASSNAME);
		var placeholder_text_desktop = $search_inputbox_desktop.attr('placeholder');
		$desktop_search_bar.find('.'+SEARCH_INPUT_CLASSNAME).attr('aria-label', placeholder_text_desktop);
		
		// mobile
		var $search_inputbox_mobile = $mobile_menu_search_bar.find('.'+SEARCH_INPUT_CLASSNAME);
		var placeholder_text_mobile = $search_inputbox_mobile.attr('placeholder');
		$mobile_menu_search_bar.find('.'+SEARCH_INPUT_CLASSNAME).attr('aria-label', placeholder_text_mobile);		

    }

    function apply_search_bar_accessibility(){
        // if user 'shift-tab' out of the search input field (first item in the bar), then close the search bar
        $desktop_search_bar.find('.'+SEARCH_INPUT_CLASSNAME).on("keydown", function(event){
            if (event.shiftKey && event.which === 9){ // Shift key + Tab key
                desktop_hide_search();
            }
        });
        // if user 'tab' out of the search bar close button (last item in the bar), then close the search bar
        $desktop_search_hide_btn.on("keydown", function(event){
            if (!event.shiftKey && event.which === 9){ // No Shift, Tab key only
                desktop_hide_search();
            }
        });
    }

    function desktop_show_search(){
        $desktop_search_bar.show();
        $desktop_search_bar.find(".td-search-input").focus();

        // a11y - hide elements hidden by search bar
        DESKTOP_SEARCH_HIDDEN_ELEMENTS.forEach(function(item){
            $(item).attr("aria-hidden", "true");

            // find any links and disable tab focus
            $(item).find("a").each(function(){
               $(this).attr("tabindex", "-1");
            });
        });
    }

    function desktop_hide_search(){
        $desktop_search_bar.hide();
        $desktop_search_show_btn.focus();   // return focus to search button

        // a11y - unhide elements previously hidden by search bar
        DESKTOP_SEARCH_HIDDEN_ELEMENTS.forEach(function(item){
            $(item).attr("aria-hidden", "false");

            // find any links and restore tab focus
            $(item).find("a").each(function(){
                $(this).removeAttr("tabindex");
            });
        });
    }

    function init_mobile_menu(){

        /*** bind events ***/
        $mobile_menu_btn_open.on("click touchstart", function(event){
            if (event.type === "touchstart"){ event.preventDefault(); }  // prevent click event from firing again on touch devices
            mobile_menu_open($mobile_menu, event);
        });
        $mobile_menu_btn_open.attr("aria-haspopup", "true");
        $mobile_menu_btn_open.attr("aria-expanded", "false");

        $mobile_menu_secondary_btn_open.on("click touchstart", function(event){
            if (event.type === "touchstart"){ event.preventDefault(); }  // prevent click event from firing again on touch devices
            mobile_menu_open($mobile_menu_secondary, event);
        });
        $mobile_menu_secondary_btn_open.attr("aria-haspopup", "true");
        $mobile_menu_secondary_btn_open.attr("aria-expanded", "false");

        $mobile_menu_btn_close.add($mobile_menu_overlay).on("click touchstart", function(event){
            if (event.type === "touchstart"){ event.preventDefault(); }  // prevent click event from firing again on touch devices
            mobile_menu_close();
        });

        // click on mobile accordion menu - don't use 'touchstart' or it will interfere with swiping to scroll the menu
        $mobile_menu_container.find("."+ACCORDION_CLASSNAME).on("click", function(event){
            mobile_accordion_toggle(event);
        });
        $mobile_menu_container.find("."+ACCORDION_CLASSNAME+" ."+ACCORDION_CONTENT_CLASSNAME).on("click", function(event){
            event.stopPropagation();
        });

        apply_mobile_accessibility($mobile_menu);
        apply_mobile_accessibility($mobile_menu_secondary);

        // pre-expand any accordion menu that has the class ACCORDION_ACTIVE_CLASSNAME
        mobile_expand_active_accordion($mobile_menu);
        mobile_expand_active_accordion($mobile_menu_secondary);

    }

    /**
     * Expands/collapses the accordion menu
     */
    function mobile_accordion_toggle(event){
        var ANIMATION_SPEED = 300;
        var $this = $(event.currentTarget);
        var $accordion_content = $this.find("."+ACCORDION_CONTENT_CLASSNAME);

        // already opened, close it
        if ($this.hasClass(ACCORDION_ACTIVE_CLASSNAME)) {
            $accordion_content.slideUp(ANIMATION_SPEED, function () {
                $this.removeClass(ACCORDION_ACTIVE_CLASSNAME);
            });
            $this.children('a').attr("aria-expanded", "false");
        }
        // already closed, open it
        else {
            $this.addClass(ACCORDION_ACTIVE_CLASSNAME);
            $accordion_content.slideDown(ANIMATION_SPEED);
			$accordion_content.find('li:first-child a').focus(); // move the focus to first li item
            $this.children('a').attr("aria-expanded", "true");

        }
    }

    /**
     * Expand accordion menu(s) if it has the active class name
     */
    function mobile_expand_active_accordion($this_menu){
        $this_menu.find("."+ACCORDION_CLASSNAME).each(function(){
            var $this = $(this);
            if ($this.hasClass(ACCORDION_ACTIVE_CLASSNAME)) {
                var $accordion_content = $this.find("."+ACCORDION_CONTENT_CLASSNAME);
                $accordion_content.slideDown(0);
                $this.children('a').attr("aria-expanded", "true");
            }
        });
    }

    function mobile_menu_open($this_menu, event){
        var ANIMATION_SPEED = 200;
        var animation_obj = {}; // store animation properties, in this case it's for the left/right position
        var menu_side = get_mobile_menu_side($this_menu);   // left or right side
        var $this_menu_open_btn = $(event.currentTarget);

        lock_main_content_scroll();

        $mobile_menu_container.show();

        $this_menu.show();

        // set initial menu position outside of the visible area
        $this_menu.css(menu_side, '-' + $this_menu.outerWidth() + 'px');

        // animate menu to open
        animation_obj[menu_side] = 0;
        $this_menu.animate(animation_obj, ANIMATION_SPEED, function(){
            $this_menu.find("a:visible,button:visible").first().focus();  // accessibility: put focus on first focusable item, we want to do this after the menu animation is complete otherwise in Safari it causes a spacing bug because it tries to focus on an off-screen item
        });

        $mobile_menu_overlay.css('opacity', 0);
        $mobile_menu_overlay.animate({
            opacity: 1.0
        }, ANIMATION_SPEED);

        $mobile_menu_last_focus = $this_menu_open_btn;  // store last focus, so we can return to it when the menu closes

        $this_menu_open_btn.attr("aria-expanded", "true"); // set aria state on open menu button

        // accessibility - hide page elements
        MOBILE_MENU_HIDDEN_ELEMENTS.forEach(function(item){
            $(item).attr("aria-hidden", "true");
        });
    }

    function mobile_menu_close(){
        var ANIMATION_SPEED = 200;
        var animation_obj = {}; // store animation properties, in this case it's for the left/right position

        // close primary menu
        animation_obj[get_mobile_menu_side($mobile_menu)] = '-' + $mobile_menu.outerWidth() + 'px';

        $mobile_menu.animate(animation_obj, ANIMATION_SPEED, function(){
            $mobile_menu_container.hide();
            $mobile_menu.hide();
        });
        $mobile_menu_btn_open.attr("aria-expanded", "false"); // set aria state on open menu button

        // close secondary menu
        animation_obj = {};
        animation_obj[get_mobile_menu_side($mobile_menu_secondary)] = '-' + $mobile_menu_secondary.outerWidth() + 'px';

        $mobile_menu_secondary.animate(animation_obj, ANIMATION_SPEED, function(){
            $mobile_menu_container.hide();
            $mobile_menu_secondary.hide();
        });
        $mobile_menu_secondary_btn_open.attr("aria-expanded", "false"); // set aria state on open menu button

        // hide overlay
        $mobile_menu_overlay.animate({
            opacity: 0.0
        }, ANIMATION_SPEED);

        $mobile_menu_last_focus.focus(); // return focus

        unlock_main_content_scroll();

        // accessibility - unhide page elements
        MOBILE_MENU_HIDDEN_ELEMENTS.forEach(function(item){
            $(item).removeAttr("aria-hidden");
        });
    }

    function apply_mobile_accessibility($menu){
        
        // add a last item used for detecting keyboard tabbing out of the menu
        var $menu_last_item = $("<span>");

        $menu_last_item.attr("tabindex", "0");  // allow it to get keyboard focus
        // when focus is put on last item, loop focus to the first item
        $menu_last_item.on("focus", function(event){
            var $this = $(event.currentTarget);
            var $this_menu = $this.parents("."+MOBILE_MENU_CLASSNAME); // select the mobile menu which is a parent of this element

            $this_menu.find("a:visible,button:visible").first().focus();  // accessibility: put focus on first focusable item
        });
        $menu.append($menu_last_item);

        // add a first item used for detecting keyboard shift-tabbing out of the menu
        var $menu_first_item = $("<span>");

        $menu_first_item.attr("tabindex", "0");  // allow it to get keyboard focus
        // when focus is put on first item, loop focus to the last item
        $menu_first_item.on("focus", function(event){
            var $this = $(event.currentTarget);
            var $this_menu = $this.parents("."+MOBILE_MENU_CLASSNAME); // select the mobile menu which is a parent of this element

            $this_menu.find("."+MOBILE_MENU_LIST_ITEM_CLASSNAME+":visible").last().find("a:visible").focus(); // put focus on last menu item
        });
        $menu.prepend($menu_first_item);

        // accordion menu - add aria tag
        $mobile_menu_container.find("."+ACCORDION_CLASSNAME).children('a').each(function(){
            $(this).attr("aria-expanded", "false");
            $(this).attr("role", "button");
        });

    }

    /**
     * Returns if the mobile menu is on the left or right side by checking the CSS class
     */
    function get_mobile_menu_side($menu) {
        if ($menu.hasClass(MOBILE_MENU_RIGHT_CLASSNAME)) {
            return 'right';
        }
        return 'left';
    }

    /**
     * Prevent main content from scrolling
     */
    function lock_main_content_scroll(){
        $("html, body").css("overflow-y", "hidden");
        $("body").css("height", "100%");
        $window.trigger('resize');  // trigger window resize, since hiding the scroll bar will cause the page size to change
    }

    function unlock_main_content_scroll(){
        $("html, body").css("overflow-y", "");
        $("body").css("height", "");
        $window.trigger('resize');  // trigger window resize, since hiding the scroll bar will cause the page size to change
    }

	
    init();

    /**
     * Global Events
     */
    // event when the height of the header has changed
	
    g.ev.on(g.events.afterHeaderHeightChanged, function(event) {
        event.preventDefault();
        update_main_content_position();
    });

}(global));}],
td_rq_top_message: [function (global) {/* Source: src/js/td-top-message.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
* @namespace td-top-message js
* @author Jason Katz (v1.0)
		  Daniel Cho danielcho80@gmail.com(v1.0.1)
* @version 1.0.1
* @description TD top message<br>
				- versoin 1.0: simple effect to open and close message
				- versoin 1.0.1: removed mobile version of close button from click event since component is restructured.<br>
								 added an interaction between the back-to-top icon and operational message.
								 added accessibility codes.
			    - version 2.0: Fixed duplicate events and made some changes on transition with custom events to support OAS Ads
				- version 3.0: Made the hug transition change
								- g.ev.on(g.events.afterABanner, fn) is added to listen to A Banner (td-a-banner.js)
								- g.ev.trigger(g.events.afterOperationalMessage, params) is added for the fallback transition for g.ev.on(g.events.afterABanner, fn) to communicate with A Banner (td-a-banner.js)
								- g.ev.trigger(g.events.afterOperationalMessageDelay, params) is added to trigger all transitions after DOM is loaded competely.
				- version 3.1: Disabled g.ev.on(g.events.afterABanner, fn) that A Banner is being pushed down eariler	
				- version 3.1.1: Made the body have margin-top when A Banner actually pushed down.
													
*/  
(function (window) {

var $ = window.jQuery,
        $window = $(window),
		g = window.g,
        document = window.document,
        $doc = $(document),
        $previous_focus,     // save the element on the page that had focus before the modal opened
		BREAKPOINT_SM_VALUE = g.bpSet('sm'),
		version = g.detectIE(),

        /**
         * Top Message
         */
        topMessage = function(element, options) {
			var $td_rq_linkToTop,
				msg_status = true,
				$content_divs = element.find('.equal-height'),
				$body = $('body'),
				slide_down_status=true,
				aBanner_reposition_fn,
				msg_loaded = false;
/*			
			g.ev.on(g.events.afterABanner, function (event, data) {
				aBanner_reposition_fn = data.cb_func;
			});			
*/			
			function init(){
				events();
			}
			
			function events(){
									
				g.ev.trigger(g.events.afterOperationalMessageDelay,{$obj:$(element),evt:slideDown_event,fn:resize_height});
				element.find('.td-top-message-close-anchor').off('click');
				
				element.find('.td-top-message-close-anchor').on('click',function (event) { // close for both desktop and mobile
					event.preventDefault(); 
					slide_down_status = false;
					element.not(":animated").slideUp({
												duration: 400,
												start: function(){
													var position_y = $(this).outerHeight();
													
													if (version === false){
														$body.animate({'margin-top': 0});
													}else if (version >= 11){
														$body.css({'margin-top': 0});
													}else{}
														
													if(typeof aBanner_reposition_fn === 'function'){
														aBanner_reposition_fn.call('',$(this),'close',position_y);
													}else{
														g.ev.trigger(g.events.afterOperationalMessage,{obj:$(this),ems:'close',position_y:position_y});
													}													
					
												},
												complete: function(){

												}
					});
					
					msg_status = false;
					if ($td_rq_linkToTop !== undefined){
						$td_rq_linkToTop.css('visibility','visible');
						$td_rq_linkToTop.attr('aria-hidden','false');
					}
				});	
				
			}
			function slideDown_event(callback){
				element.not(":animated").delay(700).slideDown({
														duration: 400, 
														start: function(){
															if (version === false){
																var position_y = $(this).outerHeight();
																//$body.animate({'margin-top': position_y}); 
																
																if(typeof aBanner_reposition_fn === 'function'){
																	aBanner_reposition_fn.call('',$(this),'open',position_y);		
																}else{
																	g.ev.trigger(g.events.afterOperationalMessage,{obj:$(this),ems:'open',position_y:position_y});																	
																}
															}
														},
														complete: function(){
															
															callback.call();
																
															$(this).find('.td-top-message-illustration .td-icon').animate({opacity: 1.0}, 400);
															$(this).find('.inner_content').fadeIn(400);
															$(this).css('display','block');
															//$body.css({'margin-top': $(this).outerHeight()});// to have complete height
															
															if (version >= 11){
																var position_y = $(this).outerHeight();
																//$body.animate({'margin-top': position_y}); 
																//$('.td-a-banner').animate({'background-position-y': '+=' + position_y});
																if(typeof aBanner_reposition_fn === 'function'){
																	aBanner_reposition_fn.call('',$(this),'open',position_y);
																}else{
																	g.ev.trigger(g.events.afterOperationalMessage,{obj:$(this),ems:'open',position_y:position_y});
																}	
															}
															
															$(this).trigger("resize");
														}
													}); 				
			}
			function resize_height(){
				if(window.innerWidth >= BREAKPOINT_SM_VALUE){
					var eqheight = g.getEqualHeight($content_divs);
					$content_divs.css("height", eqheight + "px");	
				}else{
					$content_divs.css("height", "auto");	
				}	
			
			}
			function resize_body_margin_top(){
				(slide_down_status) ? $body.css('margin-top', element.outerHeight()) : $body.css('margin-top',0);
			}

			// wait until back-to-top link gets triggered.
			/*
			g.ev.on(g.events.afterBackToTop, function (event, el_btt) {
				$td_rq_linkToTop = $(el_btt);
				if(msg_status != false){
					$td_rq_linkToTop.css('visibility','hidden');
					$td_rq_linkToTop.attr('aria-hidden','true');
				}else{
					$td_rq_linkToTop.css('visibility','visible');
					$td_rq_linkToTop.attr('aria-hidden','false');					
				}
			});
			*/

			init();
			$window.resize(function(){
				resize_height();
				if(msg_loaded) resize_body_margin_top();
			});
			$window.load(function(){
				msg_loaded = true;
			});
            // Custom Event for OAS
			document.addEventListener("initEmergencyMessaging", function(e) {
				init();
				//console.log("Emergency Message!");
			}); 	
			/* Below snippets need to be pushed with OAS
			<script>
				var event = document.createEvent('Event');
				event.initEvent('initEmergencyMessaging', true, true);
				document.dispatchEvent(event); 
			</script>
			*/			
			
        };

    // PLUGINS DEFINITION
    // ==================
    $.fn.topMessage = g.getPluginDef(topMessage, 'topMessage');

    $(".td_rq_top-message").topMessage();

}(global));}],
td_rq_modal_cookie: [function (global) {/* Source: src/js/td-modal-cookie.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
* @namespace td-modal-cookie js
* @author Jason Katz (v1.0)
		  Daniel Cho danielcho80@gmail.com(v1.0.1)
* @version 1.0.1
* @description TD Cookie message<br>
				- versoin 1.0: made cookie message appear
				- versoin 1.0.1: added a function for repositioning the cookie message for each breakpoint.<br>
								 added an interaction between the back-to-top icon and cookie message.
								 added accessibility codes
				
*/ 

(function (window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),
        $doc = $(document),
        BREAKPOINT_SM_VALUE = g.bpSet('sm'),		
		BREAKPOINT_MD_VALUE = g.bpSet('md'),
		
        $previous_focus,     // save the element on the page that had focus before the modal opened

        /**
         * Cookie Message
         */
        modalCookie = function(element, options) {
			var $cookie_msg_obj = element,
				$td_container = $(".td-container"),
				$td_rq_linkToTop,
				msg_status = true;				
				
			init();
            $cookie_msg_obj.find('.close-button').click(function () { // close for both desktop and mobile
                $cookie_msg_obj.hide(500);
				msg_status = false;
				if ($td_rq_linkToTop !== undefined){
					$td_rq_linkToTop.css('visibility','visible');
					$td_rq_linkToTop.attr('aria-hidden','false');	
				}					
				$cookie_msg_obj.attr('aria-hidden','true');				
            });
			
			$window.resize(function(){
				cookie_msg_pos();
			});			
			
			function init(){
				cookie_msg_pos();
				$cookie_msg_obj.attr('aria-hidden','false');
				$cookie_msg_obj.attr("tabindex", "0");
				
				
				// wait until back-to-top link gets triggered.
				g.ev.on(g.events.afterBackToTop, function (event, el_btt) {
					$td_rq_linkToTop = $(el_btt);
					if(msg_status != false){
						$td_rq_linkToTop.css('visibility','hidden');
						$td_rq_linkToTop.attr('aria-hidden','true');
					}else{
						$td_rq_linkToTop.css('visibility','visible');
						$td_rq_linkToTop.attr('aria-hidden','false');
					}
				});					
			}			
			// cookie message position
			function cookie_msg_pos(){
				var cur_browser_width = window.innerWidth;
				
				if(window.innerWidth >= BREAKPOINT_MD_VALUE) {
					var	td_container_width;
						
					if ($td_container.get(0)){
						td_container_width = $td_container.outerWidth();
						$cookie_msg_obj.css('right',cal_pos(cur_browser_width,td_container_width,103)+'px');
					}else{
						$cookie_msg_obj.css('right','0px');
					}	
				}else if(window.innerWidth < BREAKPOINT_MD_VALUE && window.innerWidth >= BREAKPOINT_SM_VALUE){
					var cookie_msg_obj_width = $cookie_msg_obj.outerWidth();
					$cookie_msg_obj.css('right',cal_pos(cur_browser_width,cookie_msg_obj_width)+'px');
				}else{
					$cookie_msg_obj.css('right','0px');
				}
			}

			function cal_pos(b_w,o_w,e_pixel){
				if(e_pixel === undefined) e_pixel = 0;
				return ((b_w - o_w) / 2) + e_pixel;
			}
        };

    // PLUGINS DEFINITION
    // ==================
    $.fn.modalCookie = g.getPluginDef(modalCookie, 'modalCookie');

    $(".td_rq_modal-cookie").modalCookie();

}(global));}],
td_rq_link_to_top: [function (global) {/* Source: src/js/td-link-to-top.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
* @namespace td-link-to-top js
* @author Jason Katz (v1.0)
		  Daniel Cho danielcho80@gmail.com(v1.0.1)
* @version 1.0.1
* @description TD Back to top<br>
	- versoin 1.0: added a function for scrolling back to top
	- versoin 1.0.1: added a function for repositioning the back-to-top icon for each breakpoint.
					 added accessibility codes
* @versoin 1.0.2
	- made the back to link icon show up when the scroll reaches footer tag.			
	
* @versoin 1.0.3
	- made the back-to-top icon appears when scrolling down to footer and the footer is not visually shown in page as the page loads.
* @version 1.0.4
	- made it have aria-hidden='false' all the time for mobile and tablet devices.(Otherwise, the focus won't go to the 'back-to-top' properly)
				
*/
(function (window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),
		BREAKPOINT_MD_VALUE = g.bpSet('md'),
        BREAKPOINT_LG_VALUE = g.bpSet('lg'),		

        backToTop = function(element, options) {
			var $btt_icon = element,
				$td_container = $(".td-container"),
				//settings = $.extend($.fn.backToTop.defaults, $btt_icon.metadata(), options),
				//min_page_height = settings.min_page_height,				
				//$td_header_nav = $('.td-header-nav'),
				$td_main = $("#main"),
				$td_footer = $('footer'),
				$body = $('body'),
				timer,
				refresh,
				footer_hasFocus = false,
				$top_nav = $('.td_rq_header-nav'),
				click_status = false,
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())),
				$second_span = $btt_icon.find("> span > span:nth-child(2)"),
				$third_span = $btt_icon.find("> span > span:nth-child(3)");
			
			init();
			
			// back-to-top click event
			$btt_icon.on('click',function () {
				var topVal = $td_main.offset().top - $body.css('margin-top').split('px')[0] ;
				$('body,html').animate({scrollTop: topVal}, '500', 'swing');
				//$td_header_nav.attr('tabindex',0).focus();
				$td_main.attr('tabindex',0).focus();
				$btt_icon.removeClass('activate');
				click_status = true;
				return false;
			});
			$btt_icon.focus(function(e){
				$(this).css('text-decoration','none');
			});	
			
	 		// back-to-top icon will appear when scroll stops
			refresh = function () { 
				$btt_icon.addClass('activate').removeClass('activate_disabled');
				$btt_icon.attr("aria-hidden", "false");	
			};
	
            $window.scroll(function(){
				footer_hasFocus = false;

                if (( $window.scrollTop() > ($td_footer.offset().top - $window.height()) ) && ( ($(document).height() - $td_footer.height()) > $(window).height() )){ // back-to-top icon appears when scrolling down to footer and the footer is not visually shown in page as the page loads.
					$td_main.attr('tabindex',0);
					$td_footer.removeAttr('tabindex');					
					
					clearTimeout(timer);
					timer = setTimeout( refresh , 150 );

					g.ev.trigger(g.events.afterBackToTop,$btt_icon);	
					$btt_icon.removeClass('activate').addClass('activate_disabled').attr('tabindex', 0);
					$btt_icon.attr("aria-hidden", "true");	
					
                } else {
					//$td_main.removeAttr('tabindex').blur();	
					if($window.scrollTop() == 0) click_status = false;
					if(!click_status) $td_main.attr('tabindex',-1);
					
					
					$td_footer.attr('tabindex',0);
					clearTimeout(timer);
					$btt_icon.removeClass('activate').removeClass('activate_disabled').attr('tabindex', -1);
					//$btt_icon.attr("aria-hidden", "true");		

					if(mobile){
						$btt_icon.attr("aria-hidden", "false"); // for tablet & mobile devices
					}else{
						$btt_icon.attr("aria-hidden", "true"); 
					}
					
                }
            });
            $window.scroll();
			
			// Accessibility - make back-to-top icon get focus by tab key
			$window.keyup(function (e) {
				if ( ( $(document).height() - $td_footer.height()) <= $(window).height() ){
					$td_footer.removeAttr('tabindex');
				}
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 9) {
					$td_footer.focus(function(e){
						if(!footer_hasFocus){
							$(this).attr("aria-hidden", "true");
							
							setTimeout(function(){
								if($btt_icon.hasClass('activate')){
									$btt_icon.focus();
								}
							}, 200);
							
							footer_hasFocus = true;
							$(this).removeAttr("aria-hidden");
						}
						//$(this).blur();
						
					});
				}
			});	
			
			// repositioning back-to-top icon
			$window.resize(function(){
				btt_icon_pos();
			});
			
			// back to top icon position
			function btt_icon_pos(){
				if(window.innerWidth >= BREAKPOINT_LG_VALUE) {
					var cur_browser_width = window.innerWidth,
						td_container_width,
						r_icon_pos;
						
					if ($td_container.get(0)){
						td_container_width = $td_container.outerWidth();
						r_icon_pos = ((cur_browser_width - td_container_width) / 2) + 7;
						$btt_icon.css('right',r_icon_pos+'px');
					}else{
						$btt_icon.css('right','0px');
					}	
					
				}else if(window.innerWidth < BREAKPOINT_LG_VALUE && window.innerWidth >= BREAKPOINT_MD_VALUE){
					$btt_icon.css('right','15px');
				}else{
					$btt_icon.css('right','0px');
				}
			}
			function init(){
				btt_icon_pos();
				$second_span.attr('tabindex',-1);
				$third_span.attr('tabindex',-1);
			}
        };

    // PLUGINS DEFINITION
    // ==================
    $.fn.backToTop = g.getPluginDef(backToTop, 'backToTop');
    $.fn.backToTop.defaults = {
		//min_page_height:1000 // minium page height
    };
    $(".td_rq_link-to-top").backToTop();

}(global));}],
td_rq_large_modal_overlay: [function (global) {/* Source: src/js/td-large-modal-overlay.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Modal
 * Created by Dylan Chan (dylan.chan@td.com)
 * @version 1.0
 * Modified by Daniel Cho (daniel.cho80@gmail.com)

 * @version 1.1 - Added a custom option (custom_arrangement) to rearrange item(s) in modal.
 - Fixed options and default setting
 - Cloned the close button that only appears on mobile device when there are 5 and more quoters.
 - Cloned the close button as float when the page height is more than window height

 * Modal component
 * @version 1.2 - Original component (td-modal) refactored for use in TDCT
 * Modified by Dennis Erny (dennis.erny@td.com)
 - Moved as much styling as possible from JS into the Sass file
 - Added 'scroll_height' option which triggers

 * @version 1.2.1 - Original component (td-modal) refactored for multiple modals in TDCT
 * Modified by Ojaswita Pradhan (ojaswita.pradhan@td.com)
 - Added 'modal_heading' and 'modal_content' option for Generic Modal

 * @version 1.2.2 - Fixed 'document.activeElement' not working on iOS devices (accessibility issue).
 * Modified by daniel.cho@td.com
 - Fixed it with '$(document).click' function
 * updated by daniel.cho@td.com
 - Added the 'rte' class name(for rich text editor) to $tdModalContent

 * @version 1.3  - Updated and fixed by daniel.cho@td.com
 - Removed 'slim scroll' plugin due to limitation
 - Added getBodyContHeight() to interact with for custom scollbar(td-scrollbar.js)
 - Modified by daniel.cho@td.com
 - Accessiblity Fixes - removed 'focus element'
 - changed the tabindex '0'to '-1'
 - hided top nav when modal appears adding '$(".td-header-nav")' to the 'elements_to_hide' array for TDCT
 - Added the focus trap JS
 * @version 1.3.2 - Updated by daniel.cho@td.com
 - Added anchor down(function scroll_down) functionality to scroll down to targeted element inside modal window

 * @version 1.3.3 - Updated by daniel.cho@td.com
 - Added a new option 'accessibility_modal_description'

 * @version 1.3.4 - Updated by daniel.cho@td.com
 - comment out $content_focus_element and add 'aria-label' to '.td-modal'.
 * @version 1.3.5 - Updated by daniel.cho@td.com
 - added $modal.focus() - when modal loads
 - changed from -1 to 0 in $modal.attr("role", "dialog").attr("tabindex","0");
 * @version 1.3.6 - Updated by daniel.cho@td.com
 - added new options to update fixed width and height to modal window.
 - made the footer div (.td-modal-footer) interacts with scrollbar within modal window
 - new option('overlayClose') added to turn on/off closing modal window by clicking overlay div.
 * @version 1.3.7 - Updated by daniel.cho@td.com
 - made the height of modal window component have 100% when there is no footer with fixed height
 * @version 1.3.8 - Updated by daniel.cho@td.com
 - Since iOS doesn't support 'document.activeElement', made workaround for iOS utilizing $(document).click and its event (toElement)
 * @version 1.3.9 - Updated by daniel.cho@td.com
 - Added a if statement to check the existance of pinned footer (footer_space) in modal window.
 * @version 1.3.10 - Updated by daniel.cho@td.com
 - Fixed the height of modal window changing IF statement to have correct cont_max_height.
 * @version 1.3.11 - Updated by daniel.cho@td.com
 - Fixed the max-height of modal window with adding/removing 'overwrite_maxHeight' CSS class
 * @version 1.3.12 - Removed '<span class="td-forscreenreader">Close</span>' and added a new option 'btn_close_aria_label' for aria-label in order to support mutiple language.
 * @version 1.3.13 - Delayed moving the focus to 'td-modal' with setTimeout();
 * @version 1.3.14 - Added custom close button trigger.
 * @versoin 1.3.15 - Made the 'ESC' key not function when the 'overlayClose' is 'false'
 */

(function(window) {

var $ = window.jQuery,
		$window = $(window),
		document = window.document,
		$doc = $(document),
		$previous_focus, // save the element on the page that had focus before the modal opened
		previous_focus_e,

		/**
		 * Modal
		 */
		modal = function(element, options) {
			var $container = $(element),
				settings = $.extend($.fn.tdModal.defaults, $container.metadata(), options),
				custom_arrangement = settings.custom_arrangement,
				modal_status = settings.show,
				clone_btn_close = settings.clone_btn_close,
				clone_btn_close_float = settings.clone_btn_close_float,
				anchor_status = settings.anchor.status,
				anchor_targetedID = settings.anchor.targetedID,
				anchor_scrolling_speed = settings.anchor.scrolling_speed,
				accessibility_modal_description = settings.accessibility_modal_description,
				td_modal_size_status = settings.size.status,
				td_modal_width = settings.size.width,
				td_modal_height = settings.size.height,
				td_overlay_close = settings.overlayClose,
				btn_close_aria_label = settings.btn_close_aria_label,
				td_modal = ".td-modal",
				$modal = $container.find(td_modal),
				td_modal_content = ".td-modal-content",
				$close_button = $modal.find(".close-button"),
				$close_button_custom = $modal.find(".close-button-custom"),
				elements_to_hide = [$(".td-header-nav"), $("footer"), $(".td-left-menu"), $(".td-right-menu"), $(".td-header"), $(".td-quick-actions-mobile"), $(".td-sticky-nav"), $(".td-contentarea")], // elements to hide from accessibility when modal is opened
				MODAL_FOCUS_CLASSNAME = 'content_focus_element', // class name of element to put focus on when modal opens
				ID_NAME = 'modal', // id for this component
				MODAL_SHOW_CLASSNAME = "td-modal-show",
				HIDDEN_ELEMENT_CLASSNAME = "td-modal-hidden-element", // apply this class to hide non-modal elements when modal is shown, this marks the element so we know to unhide it when modal is closed
				DATA_ATTR_INITIATED = 'initiated', // data attributed to signify that modal has been initiated
				ANIMATION_DURATION = 200, // fade in-out animation duration
				VIDEO_CLASSNAME = ".td-video-player .video-js",
				td_modal_quoter_item = 'td-modal-quoter-item',
				$item = $modal.find('.' + td_modal_quoter_item),
				breakpoint_sm = g.bpSet('sm') - 1,
				$cloned_btn,
				$tdModalHeading = $container.find(".td-heading"),
			//$tdModalContent = $container.find(".td-modal-body-content"),
				$tdModalContent = $container.find(".td-modal-body-content .rte"),
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())),
				mobile_iOS = (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase())),
				$scrollable_area = $container.find('.td_rq_scrollbar'),
				$td_modal_header = $container.find('.td-modal-header'),
				$td_modal_header_h2 = $td_modal_header.children('h2'),
				$td_modal_footer = $container.find('.td-modal-footer'),
				td_rq_expand = '.td_rq_expand';

			// init the modal if it hasn't been already, we know the modal has been initiated if a specific data attribute is set to true
			if ($container.data(DATA_ATTR_INITIATED) != 'true') {
				//setTimeout(function(){init(); }, 0);
				init();
			}


			// apply options
			switch (modal_status) {
				case true:
					show(show_callback);
					break;
				case false:
					hide();
					break;
			}

			function init() {
				$container.data(DATA_ATTR_INITIATED, 'true');

				// close button event
				$close_button.click(function(event) {
					hide();
				});
				// custom close button within modal window.
				$close_button_custom.click(function(event) {
					hide();
				});

				setup_accessibility();

				var numOfItems = $item.length;

				if (clone_btn_close) {
					// clone close button
					if (numOfItems > 0) clone_close_button(numOfItems);
				}

				// clone the float close button
				if (clone_btn_close_float) clone_close_button_init();
				// custom arrangement: Quoter
				if (custom_arrangement && numOfItems > 0) change_layout(numOfItems);
			}

			// get the height of content div for custom scrollbar
			if($scrollable_area.get(0)) getBodyContHeight();

			function setSize(fixed_size){
				(fixed_size == true) ? $modal.css({'width':td_modal_width+'px','height':td_modal_height+'px'}) : $modal.css({'width':'auto','height':'100%'});
			}

			function getBodyContHeight(){

				var obj = $scrollable_area;
				var obj_id = obj.attr("id");

				var $obj = $('#'+obj_id).find('> div'); //'.modal-content'
				$obj.css('margin-right','1px');

				//console.log("td_modal_size_status:"+td_modal_size_status);

				if(window.innerWidth < 768){

					// set modal window size
					if(td_modal_size_status == true) setSize(false);

					var m_cont_max_height = window.innerHeight;
					var td_modal_header_h2_val = 0;
					if($td_modal_header_h2.get(0)){
						td_modal_header_h2_val = parseInt($td_modal_header_h2.outerHeight(true));
					}

					var header_space = parseInt($td_modal_header.css('marginTop').split('px')[0]) + td_modal_header_h2_val;
					var footer_space = 0;
					if($td_modal_footer.get(0)){ footer_space = $td_modal_footer.outerHeight(true); }

					var tdExpand_height = 0;
					var total_modal_scrollable_area_height = parseInt($obj.css('height').split('px')[0]);
					m_cont_max_height = m_cont_max_height - header_space - footer_space;

					if(total_modal_scrollable_area_height > m_cont_max_height){
						g.ev.trigger(g.events.afterLargeModalOverlay,[{scrollbarInfo:{id:obj_id,height:m_cont_max_height+'px',status:'yes'}}]);
					}else{
						g.ev.trigger(g.events.afterLargeModalOverlay,[{scrollbarInfo:{id:obj_id,height:total_modal_scrollable_area_height,status:'no'}}]);
					}

				}else{
					// set modal window size
					if(td_modal_size_status == true) setSize(true);

					var cont_max_height;
					if(td_modal_size_status == true){

						if(parseInt(td_modal_height) > document.documentElement.clientHeight){
							$modal.css('height','100%');
							$modal.removeClass('overwrite_maxHeight');
							cont_max_height = document.documentElement.clientHeight * 0.85;

							//console.log('A');
						}else{
							$modal.css('height',td_modal_height+'px');
							$modal.addClass('overwrite_maxHeight');
							cont_max_height = parseInt(td_modal_height);

							//console.log('B');
						}

						/*
						 if( !$td_modal_footer.get(0) ){
						 $modal.css('height','100%');
						 cont_max_height = document.documentElement.clientHeight * 0.85;
						 }else{
						 $modal.css('height',td_modal_height+'px');
						 cont_max_height = parseInt(td_modal_height);
						 }
						 */
						/*
						 if( parseInt(td_modal_height)  >= document.documentElement.clientHeight){
						 $modal.css('height','100%');
						 cont_max_height = document.documentElement.clientHeight * 0.85;
						 }else{
						 $modal.css('height',td_modal_height+'px');
						 cont_max_height = parseInt(td_modal_height);
						 }
						 */
					}else{
						cont_max_height = document.documentElement.clientHeight * 0.85;
					}

					var td_modal_header_h2_val = 0;
					if($td_modal_header_h2.get(0)){
						td_modal_header_h2_val = parseInt($td_modal_header_h2.outerHeight(true));
					}

					var header_space = parseInt($td_modal_header.css('marginTop').split('px')[0]) + td_modal_header_h2_val;
					var footer_space = 0;
					if($td_modal_footer.get(0)){ footer_space = $td_modal_footer.outerHeight(true); }

					var tdExpand_height = 0;
					var total_modal_scrollable_area_height = parseInt($obj.css('height').split('px')[0]);


					cont_max_height = cont_max_height - (header_space + 12) - footer_space; //(header_space + 40);

					if(total_modal_scrollable_area_height > cont_max_height){
						g.ev.trigger(g.events.afterLargeModalOverlay,[{scrollbarInfo:{id:obj_id,height:cont_max_height+'px',status:'yes'}}]);
					}else{
						g.ev.trigger(g.events.afterLargeModalOverlay,[{scrollbarInfo:{id:obj_id,height:total_modal_scrollable_area_height,status:'no'}}]);
					}

				}
			}

			function clone_close_button_init() {
				var $cloned_obj = $close_button.clone(true);
				$cloned_obj.addClass('cloned_button');

				$container.append($cloned_obj);
				$cloned_btn = $('.cloned_button');
				$cloned_btn.hide();
			}

			function clone_close_button_float() {
				var modal_content_height = $modal.find(td_modal_content).outerHeight(true),
					viewport_height = window.innerHeight,
					$cloned_btn = $('.cloned_button');

				if (viewport_height <= modal_content_height) {
					$close_button.hide();
					$cloned_btn.show(show_callback);
				} else {
					$close_button.show(show_callback);
					$cloned_btn.hide();
				}
			}

			function clone_close_button(n) {
				if (typeof n === "number") {
					if (n > 4) {
						var $cloned_obj = $close_button.clone(true);
						$cloned_obj.css({
							'top': 'auto',
							'bottom': '12px'
						});
						$cloned_obj.addClass('visible-xs');
						$close_button.parent().append($cloned_obj);
					}
				}
			}

			function change_layout(n) {
				if (typeof n === 'number') {
					switch (n) {
						case 5:
						case 6:
						case 3:
							$item.removeClass('td-col-sm-3').addClass('td-col-sm-4');
							break;
						case 2:
							$item.eq(0).addClass('td-col-sm-offset-3');
							break;
					}
				}
			}

			function show(cb) {
				$modal.attr("role", "dialog").attr("tabindex","0");
				$container.addClass(MODAL_SHOW_CLASSNAME);
				$container.fadeTo(0, 0);
				$container.fadeTo(ANIMATION_DURATION, 1.0);
				//console.log("==========> show");
				$container.attr("aria-hidden", "false");

				var top_position = 0;

				//Set Modal Heading
				if (settings.modal_heading) {
					$tdModalHeading.html(settings.modal_heading);
					settings.modal_heading = '';
				}

				//Set Modal Content
				if (settings.modal_content) {
					$tdModalContent.html(settings.modal_content);
					settings.modal_content = '';
				}

				// Check for scroll height setting
				/*
				 if (settings.scroll_height) {
				 $('.td-slim-scroll').slimScroll({
				 height: settings.scroll_height,
				 alwaysVisible: true
				 });
				 }
				 */
				if(window.innerWidth < 768 && td_modal_size_status != true){
					$container.css('height','100%');
				}else{
					$container.height($window.height()); // set container height to viewport height (could normally use height: 100vh instead but there is a bug in Safari iOS)
				}

				// ScrollTop if modal height is greater than the viewport
				if ($modal.outerHeight() > $window.height()) {
					$modal.scrollTop(1);
				}

				$('html, body').css({
					"overflow": "hidden !important",
					"position": "fixed"
				}); // disable page scrollbars, prevent iOS overflow scrolling

				// bind window resize event
				$window.on("resize.tdModal", on_window_resize);

				// accessibility
				// save the previous focus on the page before the modal window is opened
				if (mobile_iOS) { // iOS doesn't support 'document.activeElement'
					$(document).off('click');
					$(document).click(function(e) {
						if(e.toElement.tagName != 'A' && e.toElement.tagName != 'BUTTON'){
							$previous_focus = $(e.toElement).parents('a,button');
						}else{
							$previous_focus = $(e.toElement);
						}
					});
				} else {

					$previous_focus = $(document.activeElement);

				}

				//$modal.find("." + MODAL_FOCUS_CLASSNAME).focus(); // put focus into modal

				//$close_button.focus(); // focus stays in close button when modal window loads
				//$modal.focus(); // focus stays in modal div when modal window loads

				// hide elements that are underneath the modal
				for (var i = 0; i < elements_to_hide.length; i++) {
					var element = elements_to_hide[i];

					// only apply to elements that does not already have aria-hidden=true
					if (element.attr("aria-hidden") != "true") {
						element.addClass(HIDDEN_ELEMENT_CLASSNAME); // add a class so we can identify this element later
						element.attr("aria-hidden", "true");
					}
				}

				// if content has video, autostart playback
				$modal.find(VIDEO_CLASSNAME).each(function() {
					var $player = $(this);
					var player_id = $player.attr('id');
					var videojs_player = videojs(player_id);

					if (videojs_player != undefined) {
						videojs_player.play();
						// set focus on play button, a slight delay is needed in order for the controls to become visible first, otherwise focus doesn't work
						setTimeout(function() {
							$player.find('.vjs-play-control').focus();
						}, 100);
					}
				});

				g.ev.trigger(g.events.afterModalShown, [$container]);

				// check if the close button needs to be float
				if (clone_btn_close_float) clone_close_button_float();

				// Anchor down
				if($scrollable_area.get(0) && anchor_status == true && typeof anchor_targetedID !== 'undefined'){
					scroll_down(anchor_targetedID);
				}else{
					if($scrollable_area.get(0)){
						scroll_init();
					}
				}

				cb();
			}
			function show_callback(){
				setTimeout(function(){ $modal.focus(); }, 300); // focus stays in modal div when modal window loads
			}
			function hide() {

				$container.fadeTo(ANIMATION_DURATION, 0.0, function() {
					$container.removeClass(MODAL_SHOW_CLASSNAME);
				});
				//console.log("==========> hide");
				$container.attr("aria-hidden", "true").css({
					"display": "none"
				});

				$('html, body').css({
					"overflow": "",
					"position": ""
				}); // re-enable page scrollbars and iOS overflow scrolling

				// unbind window resize event
				$window.off("resize.tdModal");

				// if content has video, stop playback
				$modal.find(VIDEO_CLASSNAME).each(function() {
					var player_id = $(this).attr('id');
					var videojs_player = videojs(player_id);

					if (videojs_player != undefined) {
						videojs_player.currentTime(0);
						videojs_player.pause();
					}
				});

				// accessibility
				// remove aria-hidden true that was previously applied to elements underneath the modal
				for (var i = 0; i < elements_to_hide.length; i++) {
					var element = elements_to_hide[i];

					if (element.hasClass(HIDDEN_ELEMENT_CLASSNAME)) {
						element.removeClass(HIDDEN_ELEMENT_CLASSNAME);
						element.attr("aria-hidden", "false");
					}
				}


				if ($previous_focus != undefined) {
					$previous_focus.focus(); // accessibility restore focus to before modal window opened
				}

				$modal.scrollTop(1); // scroll down slightly, this fixes a bug on iOS where scrolling is frozen if user scrolls up when scroll position is 0

				g.ev.trigger(g.events.afterModalHidden);

				$modal.removeAttr('role tabindex');
			}

			function on_window_resize() {
				// update container height
				//sconsole.log('resize');
				//$container.height($window.height());

				if(window.innerWidth < 768 && td_modal_size_status != true){
					$container.css('height','100%');
				}else{
					$container.height($window.height()); // set container height to viewport height (could normally use height: 100vh instead but there is a bug in Safari iOS)
				}

				// check if the close button needs to be float
				if (clone_btn_close_float) clone_close_button_float();
				if($scrollable_area.get(0)) getBodyContHeight();
			}

			/**
			 * Anchor down - scrolling down to targeted element
			 */
			function scroll_down(gID){
				scroll_init();

				var $targetedObj = $('#'+gID),
					distance = $targetedObj.offset().top,
					dis_offset = $scrollable_area.offset().top,
					distance = distance - dis_offset;

				$targetedObj.attr('tabindex','0');
				$scrollable_area.animate({
					scrollTop: distance
				}, anchor_scrolling_speed,function(){
					$targetedObj.focus().css({'outline-width': '0px'});

				});

				// reset - anchor donw settings for other modal window links
				settings.anchor.status = false;
				settings.anchor.targetedID = '';
			}
			function scroll_init(){ // initiated
				var dis_offset = $scrollable_area.offset().top;
				$scrollable_area.scrollTop(0);
			}

			/**
			 * Set up accessibility
			 */
			function setup_accessibility() {
				//$modal.attr("role", "dialog").attr("tabindex","-1");
				$modal.attr("role", "dialog").attr("tabindex","0");
				//console.log("==========> accessibility");
				$container.attr("aria-hidden", "true");
				$close_button.attr("aria-label", btn_close_aria_label);

				// create a screen reader text element that can get focus and be read, this is necessary for VoiceOver as it needs some content to focus on
				$modal.attr('aria-label',accessibility_modal_description);

				/*
				 var $content_focus_element = $('<div tabindex="0" class="' + MODAL_FOCUS_CLASSNAME + ' td-forscreenreader">'+accessibility_modal_description+'</div>');
				 $modal.prepend($content_focus_element);
				 */

				var focusableElementsString = "a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]";

				// trap the focus inside modal window
				$('body').on( "keydown", td_modal, function( event ) {
					var $this = $(this);

					if ( event.keyCode == 9) { // tab

						// get list of all children elements in given object
						var children = $this.find('*');

						// get list of focusable items
						var focusableItems = children.filter(focusableElementsString).filter(':visible');

						// get currently focused item
						var focusedItem = $( document.activeElement );

						// get the number of focusable items
						var numberOfFocusableItems = focusableItems.length;

						var focusedItemIndex = focusableItems.index(focusedItem);

						if ( !event.shiftKey && (focusedItemIndex == numberOfFocusableItems - 1) ){
							focusableItems.get(0).focus();
							event.preventDefault();
						}
						if ( event.shiftKey && focusedItemIndex == 0 ){
							focusableItems.get(numberOfFocusableItems - 1).focus();
							event.preventDefault();
						}
					}
				}).on( "keyup", ":not("+td_modal+")", function( event ) {
					var $this = $(this),
						$obj_modal = $(td_modal),
						focusedItem = $( document.activeElement ),
						in_modal = focusedItem.parents(td_modal).length ? true : false;
					//$close = $('#js-modal-close');

					if ( $obj_modal.length && event.keyCode == 9 && in_modal === false ) { // tab
						$close_button.focus();
					}

				});

				/*	Commented out due to accessibility issue

				 // create a first & last focusable element in the modal container, when this element gets focus we set the focus back to the modal
				 // this is to prevent tabbing from going outside the modal on to other page elements hidden underneath
				 var $last_focus_element = $('<div tabindex="0" class="focus_element"></div>');
				 $container.append($last_focus_element);
				 $last_focus_element.focus(function(e) {
				 e.stopPropagation();
				 //$modal.focus();
				 $close_button.focus();
				 });

				 var $first_focus_element = $('<div tabindex="0" class="focus_element"></div>');
				 $container.prepend($first_focus_element);
				 $first_focus_element.focus(function(e) {
				 e.stopPropagation();
				 //$modal.focus();
				 //$close_button.focus();

				 $modal.find("[tabindex='0']:visible:not('focus_element'), a:visible, button:visible").last().focus(); // focus on last element that can receive focus
				 });
				 */

				// add escape key to close modal
				$container.keydown(function(e) {
					console.log("td_overlay_close: "+settings.overlayClose);
					if(td_overlay_close != false) {
						if (e.which === 27) {
							hide();
						}
					}
				});

				// add click event to outside container to close modal
				$container.click(function(e) {
					if(td_overlay_close != false) {
						hide();
					}
				});

				// prevent container's child click events from propagating
				$container.children().click(function(e) {
					e.stopPropagation();
				});
			}

		};

	// create unique id to keep track of each instance of the tabs-carousel
	if (!$.tdModalModules) {
		$.tdModalModules = {
			uid: 0
		};
	}

	// PLUGINS DEFINITION
	// ==================
	$.fn.tdModal = g.getPluginDef(modal, 'tdModal');

	// DEFAULT SETTINGS
	// ================
	$.fn.tdModal.defaults = {
		custom_arrangement: true,
		modal_status: false,
		clone_btn_close: false,
		clone_btn_close_float: true,
		anchor:{
			status: false,
			targetedID: '',
			scrolling_speed: 600
		},
		accessibility_modal_description: 'Modal content',
		size:{
			status: false,
			width: 'auto',
			height: '100%'
		},
		overlayClose: true,
		btn_close_aria_label: 'close'
	};

	$(".td-modal-container, td_rq_large-modal-overlay").tdModal();

}(global));}],
td_rq_a_banner_product: [function (global) {/* Source: src/js/td-a-banner-product.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD A-Banner
 * Created by dylan.chan@td.com
 * Start date: 2016/06/23
 * @version 1.0
 *
 * A-Banner Product component - to be used for both 1.10 Accounts & Credit Cards components
 - Added api(fn_compare_drawer) for 'Compare Drawer' so that 'A Product Banner' component can be intracted with 'Compare Drawer' (added by: daniel.cho@td.com)
 - Integrated 'A Product Banner' to 'Compare Drawer' api for interaction (updated by: dylan.chan@td.com)
 - Added checkbox_toggle event for flying selected item (added by: daniel.cho@td.com)
 - Added special offer indicator option (added by: daniel.cho@td.com)
 - Added compare checkbox option (added by: daniel.cho@td.com)
 - re-organize options (updated by: daniel.cho@td.com)
 - Added padding-top:75px when special offer indicator appears (added by: daniel.cho@td.com) 
 - Modified logic controlling top padding + special offers (added by: dennis.erny@td.com) - (removed by daniel.cho@td.com)
 - Added accessibility - checkbox uses h2 as description (added by dmitri.kokoshkyn@td.com)
 - Added a new function getProdImgHeight()to grab image height and add it to .td-product-info-right(added by: daniel.cho@td.com)
 - Added an unique id for h1 tag for accessibility(added by: daniel.cho@td.com)
 - Added 'special_offer_indicator_alignment()' and fixed the alignment of the special offers (added and fixed by daniel.cho@td.com)
 - Changed the selector from '.td-a-banner-product' to '.td_rq_a-banner-product' (updated by daniel.cho@td.com)
 - Added touch events for '$product_info_right' (updated by daniel.cho@td.com)
 - Include margins in height calculations of the special offer indicator component (updated by dylan.chan@td.com)
 - added a language option to grab language specific cookies (daniel.cho@td.com)
 - Removed $(this).blur(); from $checkbox due to losing focus in IE11 and added new JS snippets to hide/show outline style.
 - Added the setHeight function to have .td-product-feature-subtext eqaul height (updated by daniel.cho@td.com)
 - Added "for" attribute to label (updated by daniel.cho@td.com)
 - Added e.preventDefault(); to 'label' elements due to 'for' which is associsted to input box. (updated by daniel.cho@td.com)
 *
 *  2018-08-02 by daniel.cho@td.com
 *  - Added a new type of sepcial offer indicator(Type2)
 *
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,
        BREAKPOINT_MD_VALUE = g.bpSet('md'),		
        BREAKPOINT_SM_VALUE = g.bpSet('sm'),
        aBannerProduct = function (element, options) {

            var $banner = $(element);
			var $h1 = $banner.find("h1");
            var $h2 = $banner.find("h2");
            var $checkbox_container = $banner.find(".td-product-compare");
			var $checkbox_description = $('.td-label-content-wrapper');			
            var $checkbox_input = $checkbox_container.find("input");
            var $checkbox_label = $checkbox_container.find("label");
            var $checkbox = $checkbox_container.find(".td-label-content-wrapper");
            var $product_image = $banner.find(".td-product-image");
            var $special_offer = $banner.find(".td-indicator-offer"); // special offer indicator - Type 1
			var $special_offer_type2 = $banner.find(".td-indicator-offer-type2"); // special offer indicator - Type 2
			var $product_info_left = $banner.find('.td-product-info-left');
			var $product_info_right = $banner.find('.td-product-info-right');
			var $back_text_link = $banner.prev('.td-back-text-link');
			var $td_product_feature_subtext = $banner.find('.td-product-feature-subtext');
			var mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
			
            var settings = $.extend($.fn.tdABannerProduct.defaults, $banner.metadata(), options),
				lang = settings.lang, // language			
                g_compare_checkbox = settings.compare_checkbox.status, // show/hide compare checkbox (true/false)
                g_prodtype = settings.compare_checkbox.prod_type, // Product Type
                g_prodid = settings.compare_checkbox.prod_id, // Product ID
                g_prodname = settings.compare_checkbox.prod_name, // Product Name
                g_prodimg = settings.compare_checkbox.prod_img, // Product Img
                g_classname = settings.compare_checkbox.classname, // componanet's unique css classname
                g_comp_type = settings.compare_checkbox.comp_type, // component type (eg. a-banner-product/catalogue-card),
                g_cookie_ids = settings.compare_checkbox.cookie_ids, // Credit Card: 'prod_ids_cc', Bank Account: 'prod_ids_ba'
                g_prod_special = settings.compare_checkbox.prod_special.status, // Special Offer status
                g_prod_special_pos = settings.compare_checkbox.prod_special.position, // Special Offer position (left/right)
				g_prod_special_type = settings.compare_checkbox.prod_special.type, // Special Offer Type (default: 1)
                g_prod_url = settings.compare_checkbox.prod_url;// relevant page



            var fn_cd_obj = {}, fn_compare_drawer = {},
                COOKIE_NAME = g_cookie_ids+'_'+lang, //Credit Card: 'prod_ids_cc_en', Bank Account: 'prod_ids_ba_en'
                COMPARE_CHECKBOX = '.td-product-compare', // compare checkbox's CSS name
                COMPARE_DRAWER = '.td_rq_compare-sticky-drawer'; // compare drawer's CSS name

            // compare drawer object
            fn_cd_obj.cookie_name = COOKIE_NAME;
            fn_cd_obj.compare_checkbox = $banner.find(COMPARE_CHECKBOX);
            fn_cd_obj.allCheckboxes = $(g_classname).find(COMPARE_CHECKBOX);
            fn_cd_obj.flyToDrawer_compare_drawer = $(COMPARE_DRAWER);

            fn_compare_drawer = {
                cb_return: true,
                max_card_num: 3,
                cur_card_num: 0,
                init: function () {

                    // assign id to checkbox based on settings
                    $checkbox_input.attr('id', g_prodtype + "_" + g_prodid);
					$checkbox_label.attr('for', g_prodtype + "_" + g_prodid);
					
                    // set max number of cards to load on physical phone devices
                    if (mobile && (window.innerWidth < g.bpSet('sm'))) {
                        fn_compare_drawer.max_card_num = 2;
                    }

                    // Load cookies - product ids
                    fn_compare_drawer.loadCookieIds();
                },
                checkbox_check_evt: function (ev) {
                    // get a number of ids from cookies
                    if (g.cookies.getItem(fn_cd_obj.cookie_name) != null && g.cookies.getItem(fn_cd_obj.cookie_name) != '') {
                        fn_compare_drawer.cookieids_arr = unescape(g.cookies.getItem(fn_cd_obj.cookie_name)).split(',');
                        fn_compare_drawer.cur_card_num = fn_compare_drawer.cookieids_arr.length;
                    } else {
                        fn_compare_drawer.cur_card_num = 0;
                    }

                    g.addProdToDrawer(fn_compare_drawer.check_callback, g_prodtype, g_prodid, g_prodname, g_prodimg, g_comp_type, g_prod_special, g_prod_url);
                },
                checkbox_uncheck_evt: function (ev) {
                    // get a number of ids from cookies
                    if (g.cookies.getItem(fn_cd_obj.cookie_name) != null && g.cookies.getItem(fn_cd_obj.cookie_name) != '') {
                        fn_compare_drawer.cookieids_arr = unescape(g.cookies.getItem(fn_cd_obj.cookie_name)).split(',');
                        fn_compare_drawer.cur_card_num = fn_compare_drawer.cookieids_arr.length;
                    } else {
                        fn_compare_drawer.cur_card_num = 0;
                    }

                    g.removeProdFromDrawer(fn_compare_drawer.uncheck_callback, g_prodtype, g_prodid, g_comp_type);
                },
                check_callback: function () {

                    fn_compare_drawer.cb_return = arguments[0];

                    // return true, product successfully added
                    if (fn_compare_drawer.cb_return) {

                        // flying to drawer
                        setTimeout(function () {
                            fn_compare_drawer.flyToDrawer($product_image);
                        }, 0);	// put a timeout 0, so drawer has time to appear if it's the first time
                        checkbox_check();
                    }
                    // return false, product not added
                    else {
                        checkbox_uncheck();
                    }
                },
                uncheck_callback: function () {

                    fn_compare_drawer.cb_return = arguments[0];

                    // return true, product successfully removed
                    if (fn_compare_drawer.cb_return) {
                        checkbox_uncheck();
                    }
                    // return false, product not removed
                    else {
                        checkbox_check();
                    }
                },
                flyToDrawer: function (flyToDrawer_image) {
                    g.flyToElement(flyToDrawer_image, fn_cd_obj.flyToDrawer_compare_drawer);
                },
                cookieids_arr: [],
                loadCookieIds: function () { // Cookies (credit card: prod_ids_cc, bank account: prod_ids_ba)
                    if (g.cookies.getItem(fn_cd_obj.cookie_name) != null && g.cookies.getItem(fn_cd_obj.cookie_name) != '') {
                        fn_compare_drawer.cookieids_arr = unescape(g.cookies.getItem(fn_cd_obj.cookie_name)).split(',');

                        fn_compare_drawer.cur_card_num = fn_compare_drawer.cookieids_arr.length;

                        // check for product if it exists in cookie
                        var index = fn_compare_drawer.cookieids_arr.indexOf(lang+'_'+$checkbox_input.attr('id'));

                        // product exists in cookie
                        if (index > -1) {
                            checkbox_check();
                        }
                        else {
                            checkbox_uncheck();
                        }
                    }
                }
            };
 	
            init();
			$window.resize(function () {
				// add the height of .td-product-info-left to .td-product-info-right
                getProdImgHeight();
				setHeight();
            });
			$window.load(function(){
				// add the height of .td-product-info-left to .td-product-info-right
				getProdImgHeight();
			});
			
            function init() {	
			    // Added back the outline style
				$('*').on('keydown',function(event) {
					if ((event.which === 9) || (event.shiftKey && event.which === 9)){ // Tab key & Shift key + Tab key
						$checkbox.css({'outline-width': '1px'});
					}
				});
				
				// Added this function due to "for" attuibute		
				$checkbox_label.click(function(e) { 
					e.preventDefault();
				});						
						
                if (g_compare_checkbox == true) {
                    $checkbox.click(checkbox_toggle);	// add checkbox event
                    $checkbox_input.change(on_checkbox_input_change);
                    apply_accessibility();
                    fn_compare_drawer.init();

                    // special offer indicator
                    special_offer_indicator(g_prod_special, g_prod_special_pos, g_prod_special_type);
					
                } else {
                    // hide 'compare checkbox'
                    $checkbox_container.hide();
                }
				
				setHeight();

            }	 
            /**
             * function to detect changes to the checkbox if it has been modified externally by the Compare Drawer component
             */
            function on_checkbox_input_change() {
                var $checkbox = $(this);

                console.log('checkbox input change');
                // make sure the visual checkbox corresponds to the state of the invisible <input> checkbox
                if ($checkbox.is(':checked') === false) {
                    checkbox_uncheck();
                }
                else {
                    checkbox_check();
                }
            }

            function checkbox_toggle(ev) {
				//remove dotted line onclick only
				$checkbox.on('click',function(event) {
					if(event.which == 1){ // mouse click
						$(this).css({'outline-width': '0px'});								
					}
				});
/*
			   $checkbox.click(function(){
				 $(this).blur();
			   });	
*/		   
		  // if already checked, uncheck it
                if ($checkbox_input.prop('checked')) {		
                    //checkbox_uncheck();
                    fn_compare_drawer.checkbox_uncheck_evt(ev);	 // send event to remove product from drawer
                }
                // otherwise, check it
                else {
                    //checkbox_check();
                    fn_compare_drawer.checkbox_check_evt(ev);	// send event to add product to drawer
                }
            }

            function checkbox_check() {
                $checkbox_input.prop('checked', true);
                $checkbox_input.addClass("checked");		// add css class to make the checked state show
                $checkbox_label.addClass("label-checked");	// add css class to make the checked state show
                $checkbox.attr("aria-checked", "true");		  
            }

            function checkbox_uncheck() {
                $checkbox_input.prop('checked', false);
                $checkbox_input.removeClass("checked");		// remove css class to make the checked state not show
                $checkbox_label.removeClass("label-checked");	// remove css class to make the checked state not show
                $checkbox.attr("aria-checked", "false");
		  $checkbox.attr('tabindex',0).focus();	
		  //$('.td_rq_compare-sticky-drawer').attr('tabindex','0').focus(); //make sure if user unchecks focus goes to compare drawer	
            }

            function apply_accessibility() {
				var uniqueID = 	'_title';	
				//add attribute aria-describedby to checkbox span instead of checkbox itself
				$checkbox_description.attr('aria-describedby', g_prodtype + "_" + g_prodid + uniqueID);

				//add id to h2 or h1
				($h1.get(0)) ? $h1.attr('id', g_prodtype + "_" + g_prodid + uniqueID) : $h2.attr('id', g_prodtype + "_" + g_prodid + uniqueID);		
				
                // make <input> hidden
                $checkbox_input.attr("aria-hidden", "true");
                $checkbox_input.attr("tabindex", -1);

                $checkbox.attr("tabindex", 0);	// make checkbox <div> get tab focus
                $checkbox.attr("role", "checkbox");	// accessibility: assign checkbox role

                // add keyboard accessibility for checkbox select
                $checkbox.on("keyup", function (event) {
                    if (event.which === 32 || event.which === 13) {
                        checkbox_toggle();
                    }
                });
				
				//remove dotted line onclick only
				$checkbox.on('click',function(event) {
					if(event.which == 1){ // mouse click
						$(this).css({'outline-width': '0px'});								
					}
				});				
/*
				$checkbox.click(function(){
					$(this).trigger('blur'); 
				});
*/				
                // prevent spacebar from scrolling page
                $checkbox.on("keydown", function (event) {
                    if (event.which === 32) {
                        event.preventDefault();
                    }
                });
            }

            function special_offer_indicator(s, p, t) {
                if (s == true) {
					
					if(t == 2) {
						$special_offer_type2.addClass('show'); 
					}else{
						if (p == 'left') $special_offer.addClass('left');
						$special_offer.show();
						
						// to avoid the indicator appears on the top of the title
						special_offer_indicator_alignment();

						$window.resize(function () {
							special_offer_indicator_alignment();
						});
					}
                }
            }
			function special_offer_indicator_alignment(){
				if($back_text_link.get(0)){
					if (window.innerWidth >= BREAKPOINT_MD_VALUE){
						var top_pos = $back_text_link.outerHeight(true) * -1;
						$special_offer.css('top',top_pos+'px');
					}else{
						$special_offer.css('top','0px');
					}
				}
				if (window.innerWidth < BREAKPOINT_SM_VALUE){
					var h_special_offer = $special_offer.outerHeight(true);
					($h1.get(0)) ? $h1.css({'padding-left':'0px','padding-right':'0px','margin-top':h_special_offer+'px'}) : $h2.css({'padding-left':'0px','padding-right':'0px','margin-top':h_special_offer+'px'});
				}else{
					($h1.get(0)) ? $h1.css({'padding-left':'10%','padding-right':'10%','margin-top':'20px'}) : $h2.css({'padding-left':'10%','padding-right':'10%','margin-top':'20px'});
				}				
			}

			var h_left, h_right;
			document.body.addEventListener('touchstart', function(e){
				if(window.innerWidth >= BREAKPOINT_SM_VALUE){
					if(h_left >= h_right){
						$product_info_right.find('> div').css('height',h_left+'px');
					}else{
						$product_info_right.find('> div').css('height','auto');
					}
				}
			}, false);			
			document.body.addEventListener('touchmove', function(e){
				if(window.innerWidth >= BREAKPOINT_SM_VALUE){
					if(h_left >= h_right){
						$product_info_right.find('> div').css('height',h_left+'px');
					}else{
						$product_info_right.find('> div').css('height','auto');
					}
				}
			}, false);	
			document.body.addEventListener('touchend', function(e){
				if(window.innerWidth >= BREAKPOINT_SM_VALUE){
					if(h_left >= h_right){
						$product_info_right.find('> div').css('height',h_left+'px');
					}else{
						$product_info_right.find('> div').css('height','auto');
					}
				}
			}, false);
		
			function getProdImgHeight(){
				//var h_left = $product_info_left.height();
				//var h_right = $product_info_right.find('> div > div').height();	
				h_left = $product_info_left.height();
				h_right = $product_info_right.find('> div > div').height();			
				
				if(window.innerWidth < BREAKPOINT_SM_VALUE){
					$product_info_right.find('> div').css('height','auto');
				}else{
					if(h_left >= h_right){
						$product_info_right.find('> div').css('height',h_left+'px');
						$product_info_right.find('> div > div').removeClass('no_vert_center');
					}else{ 
						$product_info_right.find('> div').css('height','auto');
						$product_info_right.find('> div > div').addClass('no_vert_center');								
					}
				}
			}
			
			function setHeight(){
				var subtext_height = g.getEqualHeight($td_product_feature_subtext);
                if ($window.width() > 750) {
                    $td_product_feature_subtext.css("height", subtext_height + "px");
                } else {
                    $td_product_feature_subtext.css("height", "auto");
                }				
			}
			
        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdABannerProduct = g.getPluginDef(aBannerProduct, 'tdABannerProduct');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdABannerProduct.defaults = {
		lang: 'en',		
        compare_checkbox: {
            status: true,
            prod_type: 'cc',
            prod_id: '',
            prod_name: '',
            prod_img: '',
            classname: '.td_rq_a-banner-product',
            comp_type: '',
            cookie_ids: 'prod_ids_cc',
            prod_special: {
                status: false,
                position: 'right',
				type: 1
            },
            prod_url: '#'
        }
    };

    $(".td_rq_a-banner-product").tdABannerProduct();
}(global));}],
td_rq_compare_sticky_drawer: [function (global) {/* Source: src/js/td-compare-sticky-drawer.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
* @namespace td-compare-sticky-drawer js
* @author Daniel Cho daniel.cho@td.com/danielcho80@gmail.com
* @version 1.0
* @description TD Compare Sticky Drawer<br>
				- versoin 1.0:	- Positioning Drawer<br>
								- Drawer Visibility<br>
								- Expend/Collapes Drawer<br>
								- Add/Remove a Card<br>
								- Interaction with 'Compare Checkbox'<br>
								- Store data in cookies
								- Restore data from cookies and display product(s) in drawer.
 								- Modified 'remove_prod' function to uncheck the checkbox by using a click event (modified by: dylan.chan@td.com)
								- Changed 'mouseup and keyup' events to 'click' event to support JAWS. (modified by; daniel.cho@td.com)
								- Made back-to-top icon show when the drawer is collapsed and hide when the drawer is expanded
								- Added Special Offer tag (triangle shape)
								- Added green arrow link to title
								- Added url catalogue option
								- Added focus to sticky drawer after checkbox is checked (dmitri.kokoshkyn@td.com)
								- Added offscreen Special Offer text (dmitri.kokoshkyn@td.com)
 								- Modified HTML of the 'td-indicator-offer-triangle' to have the content text in HTML (dylan.chan@td.com)
								- Added two new variables('lang' and 'special_label' within 'prod_text') and added French templates (daniel.cho@td.com)
								- Added language specific cookies with 'objFn.lang' (eg. fr_ba_1_ba) (daniel.cho@td.com)
				- version 1.0.1: accessibility fixes (daniel.cho@td.com)
								1.	Remove the alt text for the CC images in the drawer, should just be alt="".
								2.	Add the following to <div class="td-container">
									a.	role="region"
									b.	aria-label="Compare" ! his must be customizable to account for different languages ! OR you could reused the text that has been specified in the <h2>
									c.	tabindex="0"
								3.	Remove tabindex="0" from <div class="heading">
								4.	Change the aria-label to just ="Compare" for the <a href> that opens the Compare 	drawer.
								5.  Add aria-hidden="true" tabindex="-1" to empty slot image
								6.  Remove $(this).blur(); from arrow icon due to losing focus in IE11
				- version 1.0.2: usability fixes (daniel.cho@td.com)	
								- //REMOVED- Added a callback founction to visibility function in order to open and close the compare drawer when page loads.
								- Made green tip load all the time.
								- Added ev_chk_other_prod function and a new option ('prod_landing': true/false) in order to acheive two cases(case 1 - prod_landing:true : + image to add more card will close compare drawer in landing page. case 2 - prod_landing:false : the + image will go to related to catalogue page)
				- version 1.0.3: added an option(context) to add cookie path. (daniel.cho@td.com)	
				- version 1.0.4: added new data layers
								- prod_text:{empty_slot:'Add another card to compare'}
								- heading_text:{prefix:'Compare up to ',mobile_prefix:'Compare ',suffix:' accounts', mobile_suffix:' accounts'}
				- version 1.0.5: 
								- added 'data-analytics-click' to icon + image
							
*/
(function (window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),
        BREAKPOINT_LG_VALUE = g.bpSet('lg'),	
		BREAKPOINT_SM_VALUE	= g.bpSet('sm'),	

        compareStickyDrawer = function(element, options) {
			var objFn={},fn_drawer={},fn_events={},fn_templates={},fn_cookies={},fn_utils={},fn_ab={},
				TD_CONTAINER = ".td-container",
				TD_DRAWER_HEADER = ".heading",
				TD_DRAWER_CONTENT = ".content",
				TD_BACK_TO_CHECKBOX = "#backToCheckbox a",
				TD_ICON = ".td-icon",
				TD_ICON_UP = "td-icon-upCaret",
				TD_ICON_DOWN = "td-icon-downCaret",
				$el = $(element),
				settings = $.extend($.fn.compareStickyDrawer.defaults, $el.metadata(), options),
				prod_type = settings.prod_type,
				targetComponent = settings.targetComponent,
				chk_compare = settings.checkboxCSS,
				targetUrl = settings.targetUrl,
				placeholder_image = settings.placeholder_image,
				disable_remaining_checkboxes = settings.disable_remaining_checkboxes,
				prod_catalogue_url = settings.prod_catalogue_url,
				prod_special_label = settings.prod_text.special_label,
				prod_empty_slot = settings.prod_text.empty_slot,
				heading_prefix = settings.heading_text.prefix,
				heading_m_prefix = settings.heading_text.mobile_prefix,
				heading_suffix = settings.heading_text.suffix,
				heading_m_suffix = settings.heading_text.mobile_suffix,				
				lang = settings.prod_text.lang,
				ab_td_container_text = settings.accessibility.td_container,
				ab_icon_text = settings.accessibility.icon,
				prod_landing = settings.prod_landing,
				cookie_context = settings.context,
				analytics_title = settings.analytics_title,
				$el_content = $el.find(TD_DRAWER_CONTENT),
				$el_header = $el.find(TD_DRAWER_HEADER),
				$btn_trigger = $el_header.children('a'),
				$btn_trigger_rm_prod = $el_content.find('.btn_rm_prod'),
				$trigger_panel = $el_content.find('.trigger_panel'),
				$btn_compare = $trigger_panel.find('button'),
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

			objFn.product_type = prod_type;
			objFn.prod_catalogue_url = prod_catalogue_url;
			objFn.csd = $el;
			objFn.td_container = $el.find(TD_CONTAINER);
			objFn.td_back_to_checkbox = $el.find(TD_BACK_TO_CHECKBOX);
			objFn.heading = $el_header;
			objFn.content = $el_content;
			objFn.btn_trigger = $btn_trigger;
			objFn.trigger_panel = $trigger_panel;
			objFn.btn_compare = $btn_compare;
			objFn.td_rq_linkToTop;	
			objFn.placeholder_image = placeholder_image;
			objFn.prod_special_label = prod_special_label;
			objFn.ab_td_container_text = ab_td_container_text;
			objFn.ab_icon_text = ab_icon_text;
			objFn.lang = lang;
			objFn.prod_landing = prod_landing;

			// Accessibility
			fn_ab = {
				init_drawer: function(){
					objFn.td_container.attr({'role':'region','aria-label':objFn.ab_td_container_text,'tabindex':0});
				},
				show_drawer: function(){
					objFn.csd.attr('aria-hidden','false');	
					//objFn.heading.attr('tabindex','0');	
					fn_ab.expanded_content();
				},
				hide_drawer: function(){
					objFn.csd.attr('aria-hidden','true');
					//objFn.heading.attr('tabindex','-1');	
					fn_ab.collapsed_content();					
				},
				expanded_content: function(){
					//objFn.heading.children('a').attr('aria-label','Collapse, compare drawer');	
					objFn.heading.children('a').attr('aria-label',objFn.ab_icon_text);	
					objFn.heading.children('a').attr('aria-expanded','true');
					objFn.content.attr('aria-hidden','false');
					objFn.content.attr('tabindex','0');					
					
				},
				collapsed_content: function(){
					//objFn.heading.children('a').attr('aria-label','Expand, compare drawer');
					objFn.heading.children('a').attr('aria-label',objFn.ab_icon_text);					
					objFn.heading.children('a').attr('aria-expanded','false');		
					objFn.content.attr('aria-hidden','true');
					objFn.content.attr('tabindex','-1');					
				},
				backtoprevious: function(){ 
					//$('.td-contentarea').attr('tabindex','0').focus();
				}
			};
			
			// Functions			
			fn_drawer = {
				btt_defaut_bottom: 30,
				drawer_status: false,
				drawer_content_status: true,
				//btt_msg_status: false, // msg from back-to-top component
				init: function(){
					
					// set up accessibility for td container
					fn_ab.init_drawer();
					
					//accessibility link patched in
					objFn.td_back_to_checkbox.click(function(){
						$('.td-label-content-wrapper').focus();
					});
					
					// refresh layout for phone device
					if(mobile && (window.innerWidth < BREAKPOINT_SM_VALUE)){
						objFn.load_phone_layout();
					}

/*					
					$('.td_rq_compare-sticky-drawer .heading, .td_rq_compare-sticky-drawer .heading a').click(function(){
						$(this).blur();
					});
*/					
					//outline-style: none;
					
					objFn.loadCookieIds(objFn.product_type); // load product ids from cookies	
					objFn.ev_afterBackToTop(); // interaction with back-to-top icon
					objFn.pos(); // initialize drawer's position	
					objFn.ev_afterProdInfo(); // interaction with other component that sends product info
					
					// add Slide-up and Slide-down event for content section
					objFn.btn_trigger.on('click',{clickable_el:true},objFn.ev_drawer);	// down arrow/up arrow
					objFn.heading.on('click',{clickable_el:false},objFn.ev_drawer);	    // header bar
					//objFn.heading.on('keypress',{clickable_el:false},objFn.ev_drawer);	    // header bar

					// Update title and image of empty slot(s) with catalogue url
					objFn.updateEmptySlots();

					// check if cookies exist. if so, bringing up card(s) in drawer
					objFn.chkCookiesExistance();
					
					// compare button
					objFn.btn_compare.on('click',{getUrl:targetUrl},objFn.ev_compare);
					
					$window.resize(function(){
						// repositioning compare-sticky-drawer
						objFn.pos();
						
						// equal columns' height
						if(mobile){
							objFn.equalColumnHeight();
						}else{
							var previousCss  = objFn.content.attr("style");
							objFn.content.css({
									position:   'absolute', 
									visibility: 'hidden',
									display:    'block'
								});
								
							if(fn_drawer.special_offer_check() > 0){
								objFn.content.find('.slot').find('h3').css('margin-top',fn_drawer.special_offer_height());
								objFn.equalColumnHeight({res_ev:true,no_ani:true});
								objFn.content.attr("style", previousCss ? previousCss : "");
							}else{								
								objFn.equalColumnHeight({res_ev:true,no_ani:true});
								objFn.content.attr("style", previousCss ? previousCss : "");
							}								
						}
						
						// re-positioning back-to-top icon					
						if(objFn.drawer_status) {
							fn_drawer.pos_btt({ani:false});	
						}
				
					});	
					$window.scroll(function(){
						// collapse when scrolling starts
						if(objFn.drawer_content_status){
							objFn.collapse();
							objFn.drawer_content_status = false; 
						}

						// re-positioning back-to-top icon	
						if(objFn.drawer_status && objFn.drawer_content_status == false) {
							fn_drawer.pos_btt({ani:false});							
						}
						
						/*else{
							fn_drawer.pos_btt({s:true,ani:false});								
						}*/
					});					
					
					// update header text
					fn_utils.printHeading();
					fn_utils.printProdAmt();
					
				},
				chkCookiesExistance: function(){
					
					if(objFn.cookieids_arr.length > 0){ 
						for(var x=0;x <= objFn.cookieids_arr.length-1;x++){	
							var cookie_id = objFn.cookieids_arr[x],
								type = cookie_id.split('_')[1],
								prod_type = objFn.cookie_name(cookie_id,type),
								prod_id = objFn.cookie_name(cookie_id,'id'),
								prod_name = objFn.cookie_name(cookie_id,'name'),
								prod_img = objFn.cookie_name(cookie_id,'img'),
								comp_type = objFn.cookie_name(cookie_id,'cp_type'),
								special_offer = objFn.cookie_name(cookie_id,'s_offer'),	
								prod_url = objFn.cookie_name(cookie_id,'url');	
/*
							console.log(g.cookies.getItem(prod_type));
							console.log(g.cookies.getItem(prod_id));
							console.log(g.cookies.getItem(prod_name));
							console.log(g.cookies.getItem(prod_img));
							console.log("===================================");
*/							
							objFn.add_prod('fromcookie',g.cookies.getItem(prod_type),g.cookies.getItem(prod_id),g.cookies.getItem(prod_name),g.cookies.getItem(prod_img),g.cookies.getItem(comp_type),g.cookies.getItem(special_offer),g.cookies.getItem(decodeURI(prod_url)));
							
							fn_drawer.remove_btn_compare();	
						}
						
						//(fn_utils.getProdAmt() == 1) ? objFn.visibility(true) : objFn.visibility(true,{open:true});
						objFn.visibility(true);
						
						
					}						
				},
				pos: function(){ // compare-sticky-drawer position
					var cur_body_width = document.body.clientWidth,
						td_container_width,
						r_pos,
						$td_container_width_default = $(TD_CONTAINER).css('width');

					if(window.innerWidth >= BREAKPOINT_LG_VALUE){
						td_container_width = $td_container_width_default;
						
						r_pos = ((cur_body_width - objFn.nopx(td_container_width)) / 2);
						objFn.td_container.css('width',td_container_width);	
						objFn.csd.css('left',r_pos+'px');					
					}else{
						objFn.td_container.css('width',document.body.clientWidth+'px');	
						objFn.csd.css('left','0px');					
					}				
				},
				visibility: function(s,options){
					var open = false;
					if(typeof options !== 'undefined'){
						if(options.open == true){
							open = true;
						}
					}
					if(!s){
						objFn.csd.slideUp('normal',function(){
							
							/* [Before] back-to-top button gets activated when compare drawer is hidden
							objFn.btt_msg_status = false;
							if (objFn.td_rq_linkToTop !== undefined){
								objFn.td_rq_linkToTop.css('visibility','visible');
								objFn.td_rq_linkToTop.attr('aria-hidden','false');	
							}	
							*/
							
							(open == true) ? objFn.expand() : objFn.collapse();
							
							objFn.drawer_status = false;
							
							// re-positioning back-to-top icon
							//fn_drawer.pos_btt({s:true});		
							fn_drawer.pos_btt();							
						});
						// accessibility
						fn_ab.hide_drawer();	
						
					}else{
						objFn.csd.slideDown('normal',function(){
							
							/* [Before] back-to-top button gets hidden when compare drawer is activated
							objFn.btt_msg_status = true;
							if (objFn.td_rq_linkToTop !== undefined){
								objFn.td_rq_linkToTop.css('visibility','hidden');
								objFn.td_rq_linkToTop.attr('aria-hidden','true');	
							}
							*/
							
							//(open == true) ? objFn.expand() : objFn.collapse();		
							(open == true) ? objFn.expand(options.callback) : objFn.collapse();					
							
							objFn.drawer_status = true;

						}); 
						// accessibility
						fn_ab.show_drawer();
					}
				},
				expand: function(callback){		
					// hide back-to-top icon
					fn_drawer.btt_hidden();
				
					objFn.content.removeClass("height-transition-hidden");			
					objFn.content.slideDown(function(){
						//console.log('[EXPAND]');
						objFn.equalColumnHeight();

						if(fn_utils.getProdAmt() != 1) fn_drawer.add_btn_compare();			

						objFn.drawer_content_status = true;
						
						// re-positioning back-to-top icon
						// fn_drawer.pos_btt();
					});

					// change arrow icon (added margin-top to re-render pseudo element in safari browser)
					objFn.btn_trigger.find(TD_ICON).addClass('switched').css('margin-top','0px');;

					// accessibility
					fn_ab.expanded_content();

					// callback function
					if(typeof callback !== 'undefined'){
						setTimeout(function(){ callback(); }, 1500);
					}				
				},
				collapse: function(){
					// show back-to-top icon
					if($window.scrollTop() > 0) fn_drawer.btt_visible();	
					
					objFn.content.addClass("height-transition-hidden");
					objFn.content.slideUp(function(){

						//console.log('hide');
						fn_drawer.remove_btn_compare();		
						objFn.drawer_content_status = false;

						// re-positioning back-to-top icon
						//(fn_utils.getProdAmt() == 0) ? fn_drawer.pos_btt({s:true}) : fn_drawer.pos_btt();						
					});
					// change arrow icon (added margin-top to re-render pseudo element in safari browser)
					objFn.btn_trigger.find(TD_ICON).removeClass('switched').css('margin-top','1px'); 
					
					// accessibility
					fn_ab.collapsed_content();
					// accessibility - restore focus to before drawer
					fn_ab.backtoprevious();
					
					
					/*
					console.log(objFn.previous_focus);
					if(objFn.previous_focus !== undefined){
						console.log('A');
						objFn.previous_focus.focus(); 
					}else{
						console.log('B');
						$('.td-contentarea').attr('tabindex','0').focus();
					}
					*/					
				},
				add_prod: function(where,p_type,p_id,p_name,p_img,cp_type,s_offer,p_url,cb_fn){
					//console.log("add_prod==>cp_type: "+cp_type);
					var count = objFn.content.find('.slot.added').length;					
					objFn.content.find('.slot').each(function(idx, el){
						var $slot = $(this);
						if(!$slot.hasClass('added')){
							if(count == 0){
								$slot.hide().addClass('added');
								$slot.html(objFn.html_prod(p_type,p_id,p_name,p_img,s_offer,p_url)).promise().done(function(){
									g.initLinks();
									$slot.fadeIn('normal', function(){	
										var $_this = $(this);
										//$_this.find('.prod_image').removeClass('hidden');
										
										if(s_offer == true || s_offer == 'true'){
											fn_drawer.special_offer_onoff('on');
										}										

										if(where == 'fromcookie'){ // from cookies
											$_this.find('.btn_rm_prod').on('click',{w:where,t:p_type,cp_t:cp_type},objFn.ev_delete);

										}else{ // not from cookies
											$_this.find('.btn_rm_prod').on('click',{cb:cb_fn,t:p_type,cp_t:cp_type},objFn.ev_delete);
											objFn.setCookie(p_type,p_id,p_name,p_img,cp_type,s_offer,p_url);											
										} 
										
										// re-positioning back-to-top icon
										fn_drawer.pos_btt();											
									});	
								});	
								
							}else{
								$slot.addClass('added');
								$slot.fadeOut(1000,function(){
									var $_this = $(this);
									$_this.children().remove();
									$_this.append(objFn.html_prod(p_type,p_id,p_name,p_img,s_offer,p_url)).promise().done(function(){
										g.initLinks();
										$slot.fadeIn('normal', function(){	
											var $_this = $(this);
											//$_this.find('.prod_image').css('opacity','0');
											
											
											if(where == 'fromcookie'){ // from cookies
												$_this.find('.btn_rm_prod').on('click',{w:where,t:p_type,cp_t:cp_type},objFn.ev_delete);
												
												if(idx == (fn_utils.getProdAmt()-1)){
													//console.log('[ADD ITEM from Cookies]');
													//=>before adding special tag: objFn.equalColumnHeight();
													if(s_offer == true || s_offer == 'true' || fn_drawer.special_offer_check() > 0){
														fn_drawer.special_offer_onoff('on',true);
													}else{
														objFn.equalColumnHeight();
													}														
												}
											}else{ // not from cookies
												$_this.find('.btn_rm_prod').on('click',{cb:cb_fn,t:p_type,cp_t:cp_type},objFn.ev_delete);
												objFn.setCookie(p_type,p_id,p_name,p_img,cp_type,s_offer,p_url);	
											
												//console.log('[ADD ITEM]');
												//=>before adding special tag: objFn.equalColumnHeight();
												if(s_offer == true || fn_drawer.special_offer_check() > 0){
													fn_drawer.special_offer_onoff('on',true);
												}else{
													objFn.equalColumnHeight();
												}																												
											} 
											
											// re-positioning back-to-top icon
											//fn_drawer.pos_btt();											
										});	
									});									
									
								});	
								if(fn_utils.getProdAmt() > 0) fn_drawer.add_btn_compare();				
							}
							
							return false;
						}
					});								
				},
				remove_prod_by_id: function(type, id){
							var checkbox_id = type+'_'+id;
							var $slot = $('#dr_'+checkbox_id).parent();
							$slot.fadeOut('normal', function(){
								var $_this = $(this);
								$_this.children().remove();	
								$_this.removeClass('added');
								$_this.html(objFn.html_emptySlot(type));	
								objFn.ev_chk_other_prod();		
								
								$_this.fadeIn('normal',function(){
									//console.log('[REMOVE]');
									
									
									(fn_drawer.special_offer_check() == 0) ? fn_drawer.special_offer_onoff('off',true) : objFn.equalColumnHeight();
									
									if(fn_utils.getProdAmt() == 1) fn_drawer.remove_btn_compare();	
									
									// re-positioning back-to-top icon											
									//if(fn_utils.getProdAmt() == 0) fn_drawer.pos_btt();											
								});  

								if(objFn.content.find('.slot.added').length == 0){
									objFn.visibility(false);											
									
									// accessibility - restore focus to before drawer
									fn_ab.backtoprevious();
								}					
							});
							
							// delete the product from cookie
							objFn.delCookie('rm_'+checkbox_id); 					
							
							// enable remianing checkboxes if there are only 2 selections	
							if(disable_remaining_checkboxes) objFn.enableCheckboxes();
							
							// update header text
							fn_utils.printHeading()
							fn_utils.printProdAmt();					
				},
				remove_prod: function(_this,options){
					var $slot = $(_this).parent().parent().parent(),
						btn_close_id = _this.id,
						cb_fn = options.cb,
						p_type = options.t,
						where = options.w,
						cp_type = options.cp_t;
		
					$slot.fadeOut('normal', function(){
						var $_this = $(this);
						var checkbox_id = objFn.getIdFromClosebtn(btn_close_id);

						if(where == 'fromcookie'){	// cookie doesn't have any interaction
							if(cp_type == 'a-banner-product' || cp_type == 'catalogue-card'){ // component type - a banner product 

								var $checkbox = $('#'+checkbox_id);
								// enable checkbox once a card is removed from drawer			
								$checkbox.removeAttr("disabled");

								// if checkbox is checked, uncheck it by calling a click event (this can then be detected with a 'change' listener)
								if ($checkbox.is(":checked")){ $checkbox.click(); }

								// make sure it's really unchecked
								$checkbox.attr("checked", false);
							}else{

								// enable checkbox once a card is removed from drawer			
								$('#'+checkbox_id).removeAttr("disabled").attr("checked",false);
							}
						}else{
							// send it to the component that the drawer currently interacts with
							cb_fn.call('',false); 
						}
						$_this.children().remove();	
						$_this.removeClass('added');
						$_this.html(objFn.html_emptySlot(p_type));	
					    objFn.ev_chk_other_prod();			
						
						$_this.fadeIn('normal',function(){
							//console.log('[REMOVE]');
							
							// remove 'style' attribute from h3 when there is no special offer in the drawer
							(fn_drawer.special_offer_check() == 0) ? fn_drawer.special_offer_onoff('off',true) : objFn.equalColumnHeight();
									
							if(fn_utils.getProdAmt() == 1) fn_drawer.remove_btn_compare();

							// re-positioning back-to-top icon							
							//if(fn_utils.getProdAmt() == 0) fn_drawer.pos_btt();							
						});  

						if(objFn.content.find('.slot.added').length == 0){
							objFn.visibility(false);								
							
							// accessibility - restore focus to before drawer
							fn_ab.backtoprevious();
							
							/*
							if(checkbox_id){
								$('#'+checkbox_id).focus();
							}else{
							
							$('.td-contentarea').attr('tabindex','0').focus();
							}
							*/
						}														
					});
					
					// delete the product from cookie
					objFn.delCookie(btn_close_id); 					
					
					// enable remianing checkboxes if there are only 2 selections		
					if(disable_remaining_checkboxes) objFn.enableCheckboxes();
					
					// update header text
					fn_utils.printHeading()
					fn_utils.printProdAmt();
				},
				add_btn_compare: function(){
					objFn.trigger_panel.addClass('activate');
					// re-positioning back-to-top icon
					/*
					if(window.innerWidth < BREAKPOINT_SM_VALUE){
						setTimeout(function(){
							fn_drawer.pos_btt();	
						},500);		
					}
					*/
				},
				remove_btn_compare: function(){
					objFn.trigger_panel.removeClass('activate');
					// re-positioning back-to-top icon
					/*
					if(window.innerWidth < BREAKPOINT_SM_VALUE){
						setTimeout(function(){
							fn_drawer.pos_btt();	
						},500);		
					}
					*/					
				},
				load_phone_layout: function(){

					objFn.content.find('.td-col-xs-4').each(function(idx, el){
						$(this).addClass('td-col-xs-6').removeClass('td-col-xs-4');
						if(idx == 2){
							$(this).remove();
						}
					})	
				},
				pos_btt: function(options){

					if(typeof objFn.td_rq_linkToTop !== 'undefined'){
						var dist = objFn.heading.height()+15,
							//dist = objFn.td_container.height()+5,
							ani = true;
						if(typeof options !== 'undefined') {
							if(options.s) dist = fn_drawer.btt_defaut_bottom;
							if(!options.a) ani = false;
						}	
					
						if(objFn.td_container.height() == 0 || fn_utils.getProdAmt() == 0){
							dist = fn_drawer.btt_defaut_bottom;
						}
						
						(ani) ? objFn.td_rq_linkToTop.animate({bottom:dist},200) : objFn.td_rq_linkToTop.css('bottom',dist);	
					}					
				},
				btt_visible: function(){
					if(typeof objFn.td_rq_linkToTop !== 'undefined'){
						objFn.td_rq_linkToTop.addClass('activate').promise().done(function(){
							$(this).css('z-index','9001');
						});
					}
				},
				btt_hidden: function(){
					if(typeof objFn.td_rq_linkToTop !== 'undefined'){
						objFn.td_rq_linkToTop.removeClass('activate').promise().done(function(){
							$(this).css('z-index','8000');
						});
					}					
				},
				special_offer_height: function(s){
					var g_margin_top;
					g_margin_top = fn_utils.nopx($(".td-indicator-offer-triangle").css('border-bottom-width'))-10;
					
					return g_margin_top;
				},
				special_offer_check: function(){
					return objFn.content.find('.slot').find('.td-indicator-offer-triangle').length;					
				},
				special_offer_onoff: function(h,s){
					var height = 0;
					if(h == 'off'){
						height = 30; // default margin-top
					}else{
						height = fn_drawer.special_offer_height();
					}
					objFn.content.find('.slot').find('h3').animate({'margin-top':height},200,function(){
						if(s == true) objFn.equalColumnHeight();
					});					
				},
				updateEmptySlots: function(){
					var $obj = objFn.content.find('.slot');
					$obj.children().remove();
					$obj.html(objFn.html_emptySlot(objFn.product_type));
		
					objFn.ev_chk_other_prod();					
				}		
			};
			
			// Events
			fn_events = {
				max_prod_num: function(){ // max number of cards in drawer 
					var max_n = 3;
					
					if(mobile && (window.innerWidth < BREAKPOINT_SM_VALUE)) max_n = 2;
					
					return max_n;
				}, 
				ev_afterProdInfo: function(){
					g.ev.on(g.events.afterProdInfo, function (event, data) {
						event.preventDefault();
						var type = data.prodinfo.prod_type,
							id = data.prodinfo.prod_id,
							name = data.prodinfo.prod_name,
							img = data.prodinfo.prod_img,	
							comp_type = data.prodinfo.comp_type,
							evt_type =  data.prodinfo.evt_type,
							speical_offer = data.prodinfo.speical_offer,
							url = data.prodinfo.prod_url,
							cbfn = data.prodinfo.cb_func,
							prodnum = objFn.content.find('.slot.added').length;
						
						if(evt_type == 'add'){
							if(prodnum > (fn_events.max_prod_num()-1)){
								cbfn.call('',false);	
							}else{
								objFn.add_prod(g.events.afterProdInfo,type,id,name,img,comp_type,speical_offer,url,cbfn);
								cbfn.call('',true);
								
								// nothing remains in array when first card gets added
								(fn_utils.getProdAmt() == 0) ? objFn.visibility(true) : objFn.visibility(true,{open:true});
								
								// accessibility - save the previous focus on the page before drawer
								// objFn.previous_focus = $(document.activeElement);	
							}
						}else if(evt_type == 'remove'){
							if(prodnum != 0){
								objFn.remove_prod_by_id(type, id);
								cbfn.call('',true);
							}else{
								cbfn.call('',false);
							}						
						}else{}
					});							
				},
				ev_compare: function(ev){
					var url = '';
					if(ev.data.getUrl != ''){
						url = ev.data.getUrl+'?prod_ids='+escape(objFn.cookieids_arr.join(','));
					}else{
						url = window.location.pathname+'?prod_ids='+escape(objFn.cookieids_arr.join(','));
					}
					document.location.href = url;			
				},
				ev_drawer: function(ev){
					ev.stopPropagation();
					ev.preventDefault();
					var keycode = (ev.keyCode ? ev.keyCode : ev.which);	
					
					if(ev.type == 'click' || keycode == 1){ // || ev.type == 'keypress'
						var $_this = ev.currentTarget;
						if(ev.data.clickable_el == false){
							if(!$($_this).next().hasClass("height-transition-hidden")){
								objFn.collapse();
							}else{
								objFn.expand();
							}								

						}else{
							if(!$($_this).parent().next().hasClass("height-transition-hidden")){
								objFn.collapse();
							}else{
								objFn.expand();
							}						
						}
					}
				},
				ev_delete: function(ev){
					ev.preventDefault();
					var keycode = (ev.keyCode ? ev.keyCode : ev.which);
					if(ev.type == 'click' || keycode == 1){
						var $_this = ev.currentTarget;
						if(ev.data.w == 'fromcookie'){
							objFn.remove_prod($_this,{t:ev.data.t,w:ev.data.w,cp_t:ev.data.cp_t});
						}else{
							objFn.remove_prod($_this,{cb:ev.data.cb,t:ev.data.t,w:ev.data.w});
						}
					}
				},
				ev_afterBackToTop: function(){
					// wait until back-to-top link gets triggered.
					g.ev.on(g.events.afterBackToTop, function (event, el_btt) {
						objFn.td_rq_linkToTop = $(el_btt);
						//fn_drawer.btt_defaut_bottom = fn_utils.nopx(objFn.td_rq_linkToTop.css('bottom'));

						//console.log("objFn.btt_msg_status: "+objFn.btt_msg_status);
						
						/* [Before] back-to-top button gets hidden when compare drawer is activated
						if(objFn.btt_msg_status != false){
							//console.log('A');
							objFn.td_rq_linkToTop.css('visibility','hidden');
							objFn.td_rq_linkToTop.attr('aria-hidden','true');
						}else{
							//console.log('B');
							objFn.td_rq_linkToTop.css('visibility','visible');
							objFn.td_rq_linkToTop.attr('aria-hidden','false');
						}
						*/
					});					
				},
				ev_chk_other_prod: function(){
					if(objFn.prod_landing == true){
						objFn.content.find('.anchor_emptyslot, .emptyslot').attr({'cursor':'pointer','href':'javascript:void(0);'});
						objFn.content.find('.anchor_emptyslot, .emptyslot').click(function(){
							objFn.collapse();
						});
					}else{
						objFn.content.find('.emptyslot').attr('href',objFn.prod_catalogue_url);
						objFn.content.find('.anchor_emptyslot').attr('href',objFn.prod_catalogue_url);						
					}
				}
			};
			
			// Templates			
			fn_templates = {
				placeholder_image: objFn.placeholder_image,
				prod_special_label: objFn.prod_special_label,
				html_prod: function(p_type,p_id,p_name,p_img,s_offer,p_url){
					var a_label='',str='',id = p_type+'_'+p_id,desc='',offer='',h3_margin_top='';

					// add special offer indicator
					if(s_offer == true || s_offer == 'true'){
						offer='<span class="td-indicator-offer-triangle"><span class="td-indicator-offer-text">'+this.prod_special_label+'</span></span>'; //TODO replace 'special offer' text with variable
					}

					// set fixed margin-top to h3 tag when if it already exists
					if(fn_drawer.special_offer_check() > 0) h3_margin_top='style="margin-top:'+fn_drawer.special_offer_height()+'px"';
					
					if(objFn.lang == 'fr'){ // French
						
						if(p_type == 'cc'){  // credit card
							//desc = 'card';
							a_label = 'remove card'; // French Text goes here
							str += offer;
							str +=	'<h3 id="dr_'+id+'" class="prod_name" '+h3_margin_top+'><a href="'+p_url+'" class="td-link-standalone">'+p_name+' </a></h3>'+
									'<div class="td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3">'+								
									'	<div class="prod_image">'+
									'		<a aria-describedby="dr_'+id+'" id="rm_'+id+'" href="javascript:void(0);" aria-label="'+a_label+'" class="btn_rm_prod"><span class="td-icon" aria-hidden="true"></span></a>'+										
									'		<img class="img-responsive" src="'+p_img+'" alt="'+desc+'">'+
									'	</div>'+
									'</div>';						
						}else{ // account
							//desc = 'account';
							a_label = 'remove account';	// French Text goes here
							str += offer;
							str +=	'<h3 id="dr_'+id+'" class="prod_name" '+h3_margin_top+'><a href="'+p_url+'" class="td-link-standalone">'+p_name+'</a></h3>'+
									'<div class="td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3">'+								
									'	<div class="prod_image">'+
									'		<a aria-describedby="dr_'+id+'" id="rm_'+id+'" href="javascript:void(0);" aria-label="'+a_label+'" class="btn_rm_prod"><span class="td-icon" aria-hidden="true"></span></a>'+										
									'		<img class="img-responsive" src="'+p_img+'" alt="'+desc+'">'+
									'	</div>'+
									'</div>';	
						}						
						
					}else{ // English
						if(p_type == 'cc'){ // credit card
							//desc = 'card';
							a_label = 'remove card';
							str += offer;
							str +=	'<h3 id="dr_'+id+'" class="prod_name" '+h3_margin_top+'><a href="'+p_url+'" class="td-link-standalone">'+p_name+' </a></h3>'+
									'<div class="td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3">'+								
									'	<div class="prod_image">'+
									'		<a aria-describedby="dr_'+id+'" id="rm_'+id+'" href="javascript:void(0);" aria-label="'+a_label+'" class="btn_rm_prod"><span class="td-icon" aria-hidden="true"></span></a>'+										
									'		<img class="img-responsive" src="'+p_img+'" alt="'+desc+'">'+
									'	</div>'+
									'</div>';						
						}else{ // account
							//desc = 'account';
							a_label = 'remove account';	
							str += offer;
							str +=	'<h3 id="dr_'+id+'" class="prod_name" '+h3_margin_top+'><a href="'+p_url+'" class="td-link-standalone">'+p_name+'</a></h3>'+
									'<div class="td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3">'+								
									'	<div class="prod_image">'+
									'		<a aria-describedby="dr_'+id+'" id="rm_'+id+'" href="javascript:void(0);" aria-label="'+a_label+'" class="btn_rm_prod"><span class="td-icon" aria-hidden="true"></span></a>'+										
									'		<img class="img-responsive" src="'+p_img+'" alt="'+desc+'">'+
									'	</div>'+
									'</div>';	
						}
					}
					return str;					
				},
				html_emptySlot: function(p_type){
					var prod='',
						prod_image_holder=fn_templates.placeholder_image,
						str='',
						//desc='empty slot',
						desc='',
						h3_margin_top='',
						analyticsStr='';
						
					if(typeof analytics_title !== 'undefined' && analytics_title != ''){
						analyticsStr = 'data-analytics-click="'+analytics_title+'"';
						console.log(analyticsStr);
					}						
						
					// set fixed margin-top to h3 tag when if it already exists
					if(fn_drawer.special_offer_check() > 0) h3_margin_top='style="margin-top:'+fn_drawer.special_offer_height()+'px"';						
						
					if(objFn.lang == 'fr'){ // French - the 'Add another','to compare' needs to be updated in French
						if(p_type == 'cc'){ // credit card  
							//prod = 'card';
									// '<h3 '+h3_margin_top+'><a class="emptyslot">Add another '+prod+' to compare</a></h3>'+
							str =	'<h3 '+h3_margin_top+'><a class="emptyslot">'+prod_empty_slot+'</a></h3>'+
									'<div class="td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3">'+								
									'	<div class="prod_image">'+								
									'		<a class="anchor_emptyslot" aria-hidden="true" tabindex="-1" '+analyticsStr+'><img class="img-responsive" src="'+prod_image_holder+'" alt="'+desc+'"></a>'+
									'	</div>'+
									'</div>';						
						}else{ // account
							//prod = 'account';
							str =	'<h3 '+h3_margin_top+'><a class="emptyslot">'+prod_empty_slot+'</a></h3>'+
									'<div class="td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3">'+								
									'	<div class="prod_image">'+								
									'		<a class="anchor_emptyslot" aria-hidden="true" tabindex="-1" '+analyticsStr+'><img class="img-responsive" src="'+prod_image_holder+'" alt="'+desc+'"></a>'+
									'	</div>'+
									'</div>';	
						}
						
					}else{ // English
						if(p_type == 'cc'){ // credit card
							//prod = 'card';
							str =	'<h3 '+h3_margin_top+'><a class="emptyslot">'+prod_empty_slot+'</a></h3>'+
									'<div class="td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3">'+								
									'	<div class="prod_image">'+								
									'		<a class="anchor_emptyslot" aria-hidden="true" tabindex="-1" '+analyticsStr+'><img class="img-responsive" src="'+prod_image_holder+'" alt="'+desc+'"></a>'+
									'	</div>'+
									'</div>';						
						}else{ // account
							//prod = 'account';
							str =	'<h3 '+h3_margin_top+'><a class="emptyslot">'+prod_empty_slot+'</a></h3>'+
									'<div class="td-col-sm-8 td-col-sm-offset-2 td-col-md-6 td-col-md-offset-3">'+								
									'	<div class="prod_image">'+								
									'		<a class="anchor_emptyslot" aria-hidden="true" tabindex="-1" '+analyticsStr+'><img class="img-responsive" src="'+prod_image_holder+'" alt="'+desc+'"></a>'+
									'	</div>'+
									'</div>';	
						}
					}
					
					return str;
				},
				html_prodAmt: function(n,m){ 
					var str = '';

					if(typeof n === 'number' && typeof m === 'number'){
						str='('+n+'/'+m+')';

					}else{
						str = '(0/'+m+')';
					}
					
					return str;
				},				
				html_heading: function(t,m){ 
					/*
					var prefix = 'Compare up to ';
					if(objFn.lang == 'fr'){ // French - the 'Compare up to' needs to be updated in French
						prefix = 'Compare up to '; 
					}
					
					var suffix = ' cards';
					if(objFn.lang == 'fr'){ // French - the 'cards' needs to be updated in French
						suffix = ' cards'; 
					}
					
					if(typeof t !=='undefined'){
						if(objFn.lang == 'fr'){ // French - the 'cards' and 'accounts' need to be updated in French
							if(t == 'cc'){
								suffix = ' cards';
							}else{
								suffix = ' accounts';
							}
						}else{ // English
							if(t == 'cc'){
								suffix = ' cards';
							}else{
								suffix = ' accounts';
							}
						}
					}					
					*/
					var prefix = heading_prefix,
						m_prefix = heading_m_prefix,
						suffix = heading_suffix,
						m_suffix = heading_m_suffix;
						
					if(mobile && (window.innerWidth < BREAKPOINT_SM_VALUE)) {
						prefix = m_prefix;
						suffix = m_suffix;
					}

					return prefix + m + suffix;
				}
			};

			// Utilities			
			fn_utils = {			
				nopx: function(val){
					return val.split('px')[0];
				},
				getIdFromClosebtn: function(c){
					var type = c.split('_')[1],
						id = c.split('_')[2];
					return type+'_'+id;
				},
				enableCheckboxes: function(){
					//console.log(targetComponent);
					//console.log(objFn.cookieids_arr.length);	
					if(mobile && (window.innerWidth < BREAKPOINT_SM_VALUE)){
						if(objFn.cookieids_arr.length == 1){
							$(targetComponent).find(chk_compare).removeAttr("disabled");
							
							for(var x=0;x <= objFn.cookieids_arr.length-1;x++){
								//console.log(objFn.cookieids_arr[x]);
								$('#'+objFn.cookieids_arr[x]).attr("disabled",true).attr("checked",true);
							}
						}						
					}else{					
						if(objFn.cookieids_arr.length == 2){
							$(targetComponent).find(chk_compare).removeAttr("disabled");
							
							for(var x=0;x <= objFn.cookieids_arr.length-1;x++){
								//console.log(objFn.cookieids_arr[x]);
								$('#'+objFn.cookieids_arr[x]).attr("disabled",true).attr("checked",true);
							}
						}		
					}
				},
				equalColumnHeight: function(options){
					var $h3 = objFn.content.find('h3'),

						$prod_image = objFn.content.find('.prod_image'),
						h3_height_full = fn_utils.getEqualHeight($h3,{full:true}),
						prod_image_full = fn_utils.getEqualHeight($prod_image,{full:true}),
						total_height = h3_height_full + prod_image_full,
						
						max_height, 
						no_animation = false,
						res_evt = false;

						
					if(typeof options !== 'undefined'){							
						if(options.no_ani != '') no_animation = options.no_ani;
						if(options.res_ev != '') res_evt = options.res_ev;										
					}
					max_height = fn_utils.getEqualHeight($h3,{na:no_animation});

					objFn.content.children('.td-row').children('div').css('height', total_height+'px');						
					
					if(res_evt){ // resizing brower
						$h3.css('height',max_height+'px');
					}else{
						
						$h3.each(function () {
							var $_this = $(this);
							if($_this.outerHeight() != max_height){
								$_this.animate({height: max_height}, 100);
							}
						});	
					}			
					
					// 'Compare Now' button behaviour
					if (window.innerWidth < BREAKPOINT_SM_VALUE) { 
						objFn.content.children('.td-row').children('div:last-child').css('height', 'auto');
					}
				},
				getEqualHeight: function ($elements,options) {
					var $docElement,
						height = 0,
						no_ani=false,
						full=false;
						
					if(typeof options !== 'undefined'){
						if(options.no_ani != '') no_ani = options.na;
						if(options.full !== undefined) full = options.full;
					}
					
					$elements.each(function () {
						$docElement = $(this);
						if(no_ani) $docElement.css({height: 'auto'});
						height = Math.max(height, $docElement.outerHeight(full));
					});	
					
					return height;
				},
				getProdAmt: function(){
					var num = objFn.cookieids_arr.length;
					return num;
				},
				printProdAmt: function(){
					var new_count = fn_templates.html_prodAmt(fn_utils.getProdAmt(),fn_events.max_prod_num());
					objFn.csd.find(TD_DRAWER_HEADER).children('h2').children('span').eq(1).text(new_count);
				},
				printHeading: function(){
					var new_heading = fn_templates.html_heading(objFn.product_type,fn_events.max_prod_num());
					objFn.csd.find(TD_DRAWER_HEADER).children('h2').children('span').eq(0).text(new_heading);
				}
				
			};
			
			// Cookies			
			fn_cookies = {
				cookieids_arr:[],
				expires: 2592000,//30 days (eg. 60*60*24 = 1 day)
				loadCookieIds: function(t){
					var prod_ids = 'prod_ids_'+t+'_'+objFn.lang;
					if(g.cookies.getItem(prod_ids) != null && g.cookies.getItem(prod_ids) != ''){
						objFn.cookieids_arr = unescape(g.cookies.getItem(prod_ids)).split(',');
					}
					//g.cookies.removeItem('prod_ids');
				},
				setCookie: function(type,id,name,img,cp_type,s_offer,url){				
					 if((typeof type === 'string' && typeof type !== 'undefined') && (typeof id !== 'undefined')){
						var cookie_id = fn_cookies.cookie_name(objFn.lang,type),
							cookie_id = fn_cookies.cookie_name(cookie_id,id),
							prod_type = fn_cookies.cookie_name(cookie_id,type),
							prod_id = fn_cookies.cookie_name(cookie_id,'id'),
							prod_name = fn_cookies.cookie_name(cookie_id,'name'),
							prod_img = fn_cookies.cookie_name(cookie_id,'img'),
							comp_type = fn_cookies.cookie_name(cookie_id,'cp_type'),
							special_offer = fn_cookies.cookie_name(cookie_id,'s_offer'),
							prod_url = fn_cookies.cookie_name(cookie_id,'url');								
							
							fn_cookies.add_cookie_id(cookie_id,type);
						 
						 g.cookies.setItem(prod_type, type, fn_cookies.expires, cookie_context); 
						 g.cookies.setItem(prod_id, id, fn_cookies.expires, cookie_context); 					 
						 g.cookies.setItem(prod_name, name, fn_cookies.expires, cookie_context); 	
						 g.cookies.setItem(prod_img, img, fn_cookies.expires, cookie_context); 
						 g.cookies.setItem(comp_type, cp_type, fn_cookies.expires, cookie_context);		
						 g.cookies.setItem(special_offer, s_offer, fn_cookies.expires, cookie_context);	
						 g.cookies.setItem(prod_url, encodeURI(url), fn_cookies.expires, cookie_context);						 
					 }
				},
				delCookie: function(btnId){
					// eg. rm_cc_1/rm_ba_1
					var type = btnId.split('_')[1],
						id = btnId.split('_')[2],
						cookie_id = fn_cookies.cookie_name(objFn.lang,type),
						cookie_id = fn_cookies.cookie_name(cookie_id,id),					
						prod_type = fn_cookies.cookie_name(cookie_id,type),
						prod_id = fn_cookies.cookie_name(cookie_id,'id'),
						prod_name = fn_cookies.cookie_name(cookie_id,'name'),
						prod_img = fn_cookies.cookie_name(cookie_id,'img'),
						comp_type = fn_cookies.cookie_name(cookie_id,'cp_type'),
						special_offer = fn_cookies.cookie_name(cookie_id,'s_offer'),
						prod_url = fn_cookies.cookie_name(cookie_id,'url');						

						fn_cookies.del_cookie_id(cookie_id,type);
					
					g.cookies.removeItem(prod_type, cookie_context);
					g.cookies.removeItem(prod_id, cookie_context);
					g.cookies.removeItem(prod_name, cookie_context);
					g.cookies.removeItem(prod_img, cookie_context);
					g.cookies.removeItem(comp_type, cookie_context);
					g.cookies.removeItem(special_offer, cookie_context);					
					g.cookies.removeItem(prod_url, cookie_context);					
				},
				add_cookie_id: function(id,t){
					
					objFn.cookieids_arr.push(id);
					var prod_ids = 'prod_ids_'+t+'_'+objFn.lang;
					g.cookies.setItem(prod_ids, escape(objFn.cookieids_arr.join(',')), fn_cookies.expires, cookie_context);	
					
					// update a number of cards
					fn_utils.printProdAmt();					
				},
				del_cookie_id: function(id,t){
					var idx = objFn.cookieids_arr.indexOf(id);
					if (idx > -1) objFn.cookieids_arr.splice(idx, 1);

					var prod_ids = 'prod_ids_'+t+'_'+objFn.lang;
					g.cookies.setItem(prod_ids, escape(objFn.cookieids_arr.join(',')), fn_cookies.expires, cookie_context);
					
				},				
				cookie_name: function(prefix,suffix){
					return prefix+'_'+suffix;
				}
			};			
			
			
			$.extend(objFn,fn_drawer,fn_events,fn_templates,fn_cookies,fn_utils,fn_ab);
						
			objFn.init();
					
			return objFn;
		
        };

    // PLUGINS DEFINITION
    // ==================
    $.fn.compareStickyDrawer = g.getPluginDef(compareStickyDrawer, 'compareStickyDrawer');
    $.fn.compareStickyDrawer.defaults = {
		prod_type:'cc', // creditcard: cc/bankaccount: ba
		targetUrl:'', // targetUrl to send ids (selected products)
		placeholder_image:'', // placeholder image path
		targetComponent:'', // targeted drawer' CSS class name (for disabling all checkboxes in targeted component when max 3 cards in drawer)
		checkboxCSS:'', // 'compare checkbox' CSS class name(for disabling all checkboxes in targeted component when max 3 cards in drawer)
		disable_remaining_checkboxes: false,
		prod_catalogue_url:'#', // main page that where you can see all products (credit cards/acconts)
		prod_text: {
			lang: 'en', // language
			special_label: 'Special Offer', // text for the special offer indicator 
			empty_slot: 'Add another card to compare' // empty slot
		},
		heading_text: {
			prefix: 'Compare up to ',
			mobile_prefix: 'Compare ',
			suffix: 'cards',
			mobile_suffix: 'cards'
		},
		accessibility: {
			td_container: 'Compare',
			icon: 'Compare'
		},
		prod_landing: false, // true - plus image to add more product(s) will have the close event , false - plus image turns into normal link
		context: '/',   // cookie path
		analytics_title:''
    };
	
    $(".td_rq_compare-sticky-drawer").compareStickyDrawer();


}(global));}],
td_rq_dataInteractionTest: [function (global) {/* Source: src/js/dataInteractionTest.js */
"use strict";
(function (window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),				

        dataInteraction = function(element, options) {
			
			var $container = $(element),

			    settings = $.extend($.fn.dataInteractionTest.defaults, $container.metadata(), options),
				g_prodtype = settings.prod_type, // Product Type
				g_prodid = settings.prod_id, // Product ID
				g_prodname = settings.prod_name, // Product Name
				g_prodimg = settings.prod_img, // Product Img
				g_classname = settings.classname, // componanet's unique css classname	
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));		
					
					
			var fn_cd_obj={},fn_compare_drawer = {},
				COOKIE_NAME = 'prod_ids_cc', //Credit Card: 'prod_ids_cc', Bank Account: 'prod_ids_ba'
				COMPARE_CHECKBOX = '.chk_compare', // compare checkbox's CSS name
				COMPARE_DRAWER = '.td_rq_compare-sticky-drawer'; // compare drawer's CSS name
				
			fn_cd_obj.cookie_name = COOKIE_NAME; 
			fn_cd_obj.compare_checkbox = $container.find(COMPARE_CHECKBOX);
			fn_cd_obj.compare_checkbox_id = fn_cd_obj.compare_checkbox.attr('id');
			fn_cd_obj.allCheckboxes = $(g_classname).find(COMPARE_CHECKBOX);
			fn_cd_obj.flyToDrawer_compare_drawer = $(COMPARE_DRAWER);
			
			fn_compare_drawer ={
				cb_return: true,
				max_card_num: 3,
				cur_card_num: 0,				
				init: function(){
					
					// set max number of cards to load on physical phone devices
					if(mobile && (window.innerWidth < g.bpSet('sm'))){	
						fn_compare_drawer.max_card_num = 2;
					}					
					// Interaction with 'Compare drawer' component
					fn_cd_obj.compare_checkbox.on('click',fn_compare_drawer.checkbox_evt);

					// Load cookies - product ids
					fn_compare_drawer.loadCookieIds();
				},
				checkbox_evt: function(ev){
					var $_this = $(ev.currentTarget);
					// get a number of ids from cookies
					if(g.cookies.getItem(fn_cd_obj.cookie_name) != null && g.cookies.getItem(fn_cd_obj.cookie_name) != ''){
						fn_compare_drawer.cookieids_arr = unescape(g.cookies.getItem(fn_cd_obj.cookie_name)).split(',');
						fn_compare_drawer.cur_card_num = fn_compare_drawer.cookieids_arr.length;	
					}else{
						fn_compare_drawer.cur_card_num = 0;
					}		
					
					g.addProdToDrawer(fn_compare_drawer.callback, g_prodtype, g_prodid, g_prodname, g_prodimg);
					
					
					// the image object to fly to drawer
					// Important: DOM Query can be changed depending on component's DOM structure 
					var get_product_image =  $_this.parent().parent().children('img'); 
					// flying to drawer
					fn_compare_drawer.flyToDrawer(get_product_image);					
				},
				callback: function(){
					//console.log('cb_return: '+arguments[0]);
				
					fn_compare_drawer.cb_return = arguments[0];
					
					if(fn_compare_drawer.cb_return) {
						fn_compare_drawer.cur_card_num += 1;
						if(fn_compare_drawer.cur_card_num == fn_compare_drawer.max_card_num){
							fn_cd_obj.allCheckboxes.attr("disabled",true);
						}
						fn_cd_obj.compare_checkbox.attr("disabled",true);
					}else{
						fn_compare_drawer.cur_card_num--;
						fn_cd_obj.compare_checkbox.removeAttr("disabled").attr("checked",false);	
					}
				},
				flyToDrawer: function(flyToDrawer_image){
					g.flyToElement(flyToDrawer_image, fn_cd_obj.flyToDrawer_compare_drawer);					
				},
				cookieids_arr:[],
				loadCookieIds: function(){ // Cookies (credit card: prod_ids_cc, bank account: prod_ids_ba)
					if(g.cookies.getItem(fn_cd_obj.cookie_name) != null && g.cookies.getItem(fn_cd_obj.cookie_name) != ''){
						fn_compare_drawer.cookieids_arr = unescape(g.cookies.getItem(fn_cd_obj.cookie_name)).split(',');	
						
						fn_compare_drawer.cur_card_num = fn_compare_drawer.cookieids_arr.length;
						
						if(fn_compare_drawer.cookieids_arr.length >= fn_compare_drawer.max_card_num){
							fn_cd_obj.allCheckboxes.attr("disabled",true);
						}else{
							//$(".td_rq_dataInteractionTest").find('.chk_compare').attr("disabled",false);
						}
						var index = fn_compare_drawer.cookieids_arr.indexOf(fn_cd_obj.compare_checkbox_id);
						if(index > -1){
							$('#'+fn_cd_obj.compare_checkbox_id).attr("disabled",true).attr("checked",true);
						}					
					}
				}				
				
			};
			
			$.extend(fn_cd_obj,fn_compare_drawer);
			
			fn_cd_obj.init();
			
			
			return fn_cd_obj;
        };
 
    // PLUGINS DEFINITION
    // ==================
    $.fn.dataInteractionTest = g.getPluginDef(dataInteraction, 'dataInteractionTest');
    $.fn.dataInteractionTest.defaults = {
		prod_type: 'cc',
        prod_id: '',
		prod_name: '',
		prod_img: '',
		classname: '.td_rq_dataInteractionTest'
    };
	
    $(".td_rq_dataInteractionTest").dataInteractionTest();
 
}(global));}],
td_rq_questions: [function (global) {/* Source: src/js/td-questions.js */
"use strict";
(function (window) {
$('.td-side .td-link-wrapper a').hover(function(){
		highlightIcon();
	});

	$('.td-side .td-link-wrapper a').mouseout(function(){
		normalIcon();
	});

	function highlightIcon(){
		$('.icon-medium').addClass('td-dark-bg');
		$('.td-copy-gray').addClass('td-text-white');	
	}

	function normalIcon(){
		$('.icon-medium').removeClass('td-dark-bg');
		$('.td-copy-gray').removeClass('td-text-white');		
	}

}(global));}],
td_rq_expand: [function (global) {/* Source: src/js/td-expand.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Expand/Collapse
 * @version 1.0
 *
 * @version 1.1 Updated by daniel.cho@td.com
 * - Removed 'aria-haspopup', 'aria-label', and 'title' due to incorrect accessibility codes
 * - Added a trigger event(g.events.afterExpand) to interact with videoplayer in modal window.
 *
 * @version 1.2 Updated by daniel.cho@td.com
 * - Made the accordion state close as default to remove inline style 'display:none' from html
 * - Made a page scroll down to specific accordion item with open state by hash param
 *
 * @version 1.2.1 Updated by daniel.cho@td.com
 * - Made title text not display under icone when it has mutiple lines
 *
 * @version 1.2.2 Updated by daniel.cho@td.com
 * - removed $(this).blur(); when clicking accordion header - JAWs lost its focus
 *
 * @version 1.2.3 Updated by daniel.cho@td.com
 * - check existance of the lazy load image and make img shown right away when open state
 *
 * @version 1.3 Updated by daniel.cho@td.com
 * - added g.ev.on(g.events.afterAnchorDown) to listen to Anchor down component in order to make 'open' state after scrolling down to specific expand & collapse item.
 *
 * @version 1.3.2 Updated by daniel.cho@td.com
 * - added g.ev.trigger(g.events.afterExpandForOthers) to communicate with othre components which reside within Expand and Collapse content area
 * - Excluded adding click event to '.td-link-toggle' for Chart component
 *
 * @version 1.4 Updated by daniel.cho@td.com
 * - added 'data-analytics-expand' and 'data-analytics-collapse' to icon (+/-)
 *
 * @version 1.4.1 Updated by daniel.cho@td.com
 * - totally excluded all charts - Chart component has same '.td-link-toggle' CSS Classname so It needs to be excluded.
 *
 * @version 1.4.2 Updated by daniel.cho@td.com
 * - added .ios class name to fix alignment issue on '-' sign
 */

(function (window) {

var $ = window.jQuery,
        g = window.g,


    // EXPAND/COLLAPSE
    // ===============

        collapse = function (link, options, group) {
            var $icon, that = {},
                $link = $(link),
                $link_parent = $link.parent(),
                $link_icon = $link.children('span:first-child'),
                $link_title = $link.children('span:last-child'),
                settings = $.extend($.fn.tdCollapse.defaults, $link.metadata(), options),
                addedToModal = settings.addedToModal,
                targeted_el_status = settings.targetelement_status,
                analytics_title = settings.analytics_title,
                td_rq_expand = '.td_rq_expand',
                td_rq_player = '.td_rq_player',
                $header_nav = $(".td-header-nav"), // TDCT Top Nav
                device_ios = (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase())),
                device_android = (/android/i.test(navigator.userAgent.toLowerCase()));

            that.target = $(settings.targetelement || $link.attr("data-target"));
            settings.fx = $link.metadata().fx;
            $icon = $link.find(".td-triggericon").html(""); // this avoids to change the existing markup

            function addEventHandlers($el, fn, data) {
                $el.css('cursor','default');
                $el.on('click', function (event) {
                    //$(this).blur();
                    event.preventDefault();
                    fn(data);
                }).on('keypress', function (event) {
                    if (event.which === 13) {
                        event.preventDefault();
                        fn(data);
                    }
                });
            }

            if(!$link.parents('.td-complex-chart').get(0) && !$link.parents('.td-chart').get(0)){ // Chart component has same '.td-link-toggle' CSS Classname so It needs to be excluded.
                addEventHandlers($link, function () {
                    that[that.target.is(":visible") ? 'hide' : 'show']();
                });
            }
            $.extend(that, {

                init: function () {
                    if(!$link.parents('.td-complex-chart').get(0) && !$link.parents('.td-chart').get(0)){ // Chart component has same '.td-link-toggle' CSS Classname so It needs to be excluded.
                        // initialize width for title
                        // that.resize_title_width();
                        // $(window).resize(function(){
                        // 	that.resize_title_width();
                        // });

                        // commented out due to due to incorrect accessibility codes
                        //$link.attr({"aria-haspopup": "true", tabindex: '0'});
                        $icon.attr("aria-hidden", "true");

                        // that.changeState(that.target.is(":visible"));
                        if(targeted_el_status == 'open' || that.chk_param() == true){
                            that.changeState(true);
                            that.show();
                        }else{
                            that.changeState(that.target.is(":visible"));
                            //that.changeState(false);
                            //that.hide();
                        }

                        if(device_ios){
                            $icon.addClass('ios');
                        }
                    }

                },

                changeState: function (expanded) {
                    var altText = $link.text().trim(),
                        prefix = expanded ? "Collapse, " : "Expand, ";

                    if (expanded) {
                        $link.addClass('expanded');
                    } else {
                        $link.addClass('collapsed').removeClass('expanded');
                    }

                    $link
                        .attr("aria-expanded", expanded.toString());
                    /* commented out due to incorrect accessibility codes
                     .attr("aria-label", prefix + altText)
                     .attr("title", prefix + altText);
                     */
                    $icon.toggleClass("td-triggericon-expanded", expanded);

                    if(typeof analytics_title !== 'undefined' && analytics_title != ''){
                        if(expanded){
                            $icon.removeAttr('data-analytics-expand').attr('data-analytics-collapse',analytics_title);
                        }else{
                            $icon.removeAttr('data-analytics-collapse').attr('data-analytics-expand',analytics_title);
                        }
                    }

                    that.target.attr('aria-hidden', !expanded);
                },

                hide: function () {
                    if ((that.target.is("table,tr,th,td")) || settings.fx === 'quickfade') {
                        that.target.animate({
                            opacity: 0
                        }, 250, function () {
                            that.target.hide();
                        });
                    } else if (settings.fx === 'none') {
                        that.target.animate({
                            opacity: 0
                        }, 0, function () {
                            that.target.hide();
                        });
                    } else {
                        that.target.animate({
                            height: "toggle",
                            opacity: "toggle"
                        }, 10, function(){

                            // interaction with modal window and custom scrollbar
                            if(addedToModal){
                                var t_height = parseInt($link.parents(td_rq_expand).outerHeight(true));
                                g.ev.trigger(g.events.afterExpand,[{tdExpandInfo:{videoplayer:td_rq_player,t_h:t_height,status:'hide'}}]);
                            }
                        });
                    }

                    that.changeState(false);
                },

                show: function () {
                    if (settings.accordion) {
                        $(group.nodes).each(function () {
                            if (this.target.is(":visible")) {
                                this.hide();
                            }
                        });
                    }

                    if ((that.target.is("table,tr,th,td")) || settings.fx === 'quickfade') {
                        that.target.show()
                            .css({'opacity': '0'})
                            .animate({opacity: 1}, 500);
                    } else if (settings.fx === 'none') {
                        that.target.show()
                            .css({'opacity': '0'})
                            .animate({opacity: 1}, 0);
                    } else {
                        that.target.animate({
                            height: "toggle",
                            opacity: "toggle"}, 10, function(){

                            // check existance of the lazy load image and make img shown right away when open state
                            if(that.target.find(g.queryLazyImages).get(0)) g.checkLazyImages();

                            // interaction with modal window and custom scrollbar
                            if(addedToModal){
                                var t_height = parseInt($link.parents(td_rq_expand).outerHeight(true));
                                g.ev.trigger(g.events.afterExpand,[{tdExpandInfo:{videoplayer:td_rq_player,t_h:t_height,status:'show'}}]);
                            }

                            // interaction with components which reside within Expand and Collapse content area
                            g.ev.trigger(g.events.afterExpandForOthers,[{obj:$link.parents(td_rq_expand)}]);

                        });
                    }

                    that.changeState(true);
                },
                resize_title_width: function(){
                    var cur_width = $link_parent.outerWidth(true),
                        icon_width = $link_icon.outerWidth(true),
                        title_width = cur_width - icon_width - 18;

                    $link_title.css('width',title_width+'px');
                },
                chk_param: function(){
                    var anchor = window.location.hash,
                        selected_obj_id = $link.attr('id'),
                        $selected_obj_id = $('#'+selected_obj_id);

                    anchor = decodeURIComponent(anchor);
                    anchor = anchor.substring(1); // remove #

                    // decode any html entities by creating a temporary text area and getting the value
                    var temp_textarea = document.createElement("textarea");
                    temp_textarea.innerHTML = anchor;
                    anchor = temp_textarea.value;


                    if(selected_obj_id == anchor){
                        that.slide_down($selected_obj_id);

                        return true;
                    }else{
                        return false;
                    }
                },
                slide_down: function($obj){

                    var dis_offset = 0;
                    if($header_nav.get(0)) dis_offset = $header_nav.height() + 20;
                    //console.log("dis_offsetdd: "+dis_offset);

                    if (typeof $obj === 'object') {
                        $obj.attr('tabindex','0');
                        $('html, body').animate({
                            scrollTop: $obj.offset().top - dis_offset
                        }, 1000, function(){
                            $obj.focus().css({'outline-width': '0px'});

                            $obj.on('focus blur keydown', function(event){
                                event.stopPropagation();
                                if ((event.which === 9) || (event.shiftKey && event.which === 9)){ // Tab key & Shift key + Tab key
                                    $obj.focus().css({'outline-width': '1px'});
                                }
                            });
                        });
                    }

                }
            });

            // Listening to Andchor down component
            g.ev.on(g.events.afterAnchorDown, function (event,data) {
                var gType = data.type,
                    gID = data.id;

                if( (gType == 'expandCollapse') && ($link.attr('id') == gID) && !that.target.is(":visible") ){
                    that['show']();
                }
            });

            that.init();
            return that;
        },

        collapseGroup = function (element, options) {
            var that = {},
                $el = $(element),
                settings = {},
                attributes;

            that.nodes = [];

            // Builds the options from **data-collapse** attribute.
            // The parameters should be separated by a space.

            attributes = $el.attr("data-collapse") || "";
            $.each(attributes.split(" "), function () {
                if (this) {
                    settings[this] = true;
                }
            });

            $.extend(settings, options);

            $el.find('> :even').each(function () {
                var $this = $(this).css({cursor: "pointer"});

                if (!settings['no-icons']) {
                    $this.prepend('<span class="td-triggericon"></span>');
                }

                settings.targetelement = $this.next();
                that.nodes.push(collapse(this, settings, that));
            });

            return that;
        };

    // PLUGIN DEFINITION
    // =================
    $.fn.tdCollapse = g.getPluginDef(collapse, "tdCollapse");
    $.fn.tdCollapseGroup = g.getPluginDef(collapseGroup, "tdCollapseGroup");

    $.fn.tdCollapse.defaults = {
        addedToModal:false, // to interact with modal window
        targetelement_status: 'close',
        analytics_title:''
    };

    $('.td-link-toggle').tdCollapse();
    $('[data-collapse]').tdCollapseGroup();
}(global));}],
td_rq_tools_swipe: [function (global) {/* Source: src/js/td-tools-swipe.js */
"use strict";
/**
 * TD Swipable components script
 * Created by david.lu@td.com
 * @version 1.0
 * Modified by danielcho80@gmail.com
 * @versoin 1.1
 *		- Added a function(sc_equalHeight) for eaqual height for all .slick-content-copy divs
 *		  in order to stay the button at the bottom all the time.
 * Modified by Paul.Z.Bradley@td.com
 * @versoin 1.2
 *      - Fixed bug when slick-content-second-row was unslicked, adding inline width style
 *      - Fixed bug when sc_equalHeight() was called not all slick-content-copy weren't found
 *
 * Modified by daniel.cho@td.com
 * @versoin 1.3
 * 		- Accessibility fixes
 *      	- Made the dots have aria-hidden='true'.
 *      	- Made the content divs have aria-hidden='false'.
 *			- Added tabindex='-1' to dot buttons
 *
 * Fixed by daniel.cho@td.com
 * @versoin 1.3.1
 * 		- Fixed adding items in '.slick-content-second-row' div (the selector is not specific so it had some issues with multiple use)
 *
 * @version 1.3.1.2
 * 		- Made callback for sc_equalHeight() with scc_div_rte(scc_div)
 *
 * @version 1.3.1.3
 *		- Created chk_slick_dots() to remove margin-top from .slick-list div when there are no dots
 *
 * @version 1.3.1.4
 *		- Made equalHeight work by row (visually) all across breakpoint for all component #11.*.
 *
 * @version 1.4
 *		- Added g.ev.on(g.events.afterExpandForOthers to reiniticate when the open state of Expand & collpase component gets triggered.
 * @version 1.4.1
 *		- Removed 'aria-live' and added aria-hidden='true' to swipe tool when reinitiation communicating with Expand & Callapse and Tab components and set its value to false later due to Voiceover
 * @version 1.4.2
 *		- Fixed alignment issue in portrait view(equalHeight was not triggered) specific to component #16
 * @versoin 1.4.3
 *		- Made H3(.td-cta-heading) have equal height.
 * Modified by ayeon.chung@td.com
 *		- Added $is_product_illustaration_cta to check whether it belongs to #11.4 component
 *	 	- Will not add 30px margin-bottom in portrait view if it's component #11.4
 * Modified by ayeon.chung@td.com
 * 		- Made H3(.td-cta-heading) have equal height for Tablet Portrait
 */

(function (window) {
	var $ = window.jQuery,
		$window = $(window),
		document = window.document,
		BREAKPOINT_SM_VALUE = g.bpSet('sm') - 1,
		BREAKPOINT_MD_VALUE = g.bpSet('md') - 1;

	var tdSwipeable = function (element, options) {
		var $element = $(element),
			$element_css = $element.attr('class'),
            H3 = '.td-cta-heading',
            $H3 = $element.find(H3),
			hiddenIndex = [],
			hiddenContent = [],
			settings = $.extend({}, $.fn.tdSwipeable.defaults, $element.metadata(), options),
			$slicker = $element.children().find(".slick-content"),
			slick_content_copy = ".slick-content-copy",
			$slick_content_copy = $element.find(slick_content_copy),
			slick_content_copy_rte = ".rte",
			$slick_content_copy_rte = $element.find(slick_content_copy_rte),
		//win_inner_width = window.innerWidth,
			SLICK_CONTENT_SECOND_ROW = ".slick-content-second-row",
			SLICK_BLOCK_SECOND_ROW = ".slick-block-second-row",
			SLICK_DOTS = ".slick-dots",
			$is_product_illustaration_cta = $element.parent().parent().hasClass('td-product-illustration-cta');

		//find unslick name and put in array
		$slicker.slick({
			arrows: settings.arrows,
			dots: settings.dots,
			infinite: settings.infinite,
			accessibility: settings.accessibility
		});

		$slicker.on('swipe', function(event, slick, direction){
			accessibility();
		});
		$slicker.on('beforeChange afterChange', function(){
			accessibility();
		});

		g.ev.on(g.events.afterExpandForOthers, function (event, data) {
			if( $element.parents(data.obj).get(0) ) {
				if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
					if(window.innerWidth < BREAKPOINT_MD_VALUE) {
						if($element.children('div').length < 2){
/* commented out the block below for https://track.td.com/browse/NAPST-570
                            $H3.css({height: 'auto'});
							$slick_content_copy.css({height: 'auto'});
							$slick_content_copy_rte.css({height: 'auto'});

							var str = $slicker.find('.slick-block').attr('class'),
								regex_sm_12 = new RegExp("\\btd-col-sm-12\\b"),
								regex_sm_6 = new RegExp("\\btd-col-sm-6\\b");

							if(!regex_sm_6.test(str)){
								$slick_content_copy.css({height: 'auto'});
								$slick_content_copy_rte.css({height: 'auto'});
							}else{
								scc_div_rte(scc_div);
							}
*/
							scc_div_rte(scc_div); //for https://track.td.com/browse/NAPST-570

							//console.log($slicker.find('.slick-block').attr('class'));

							if(!$is_product_illustaration_cta && regex_sm_12.test(str)){
								$slicker.find('.slick-block').css('margin-bottom','30px');
							}
						}else{
							scc_div_rte(scc_div);
						}
					}else{
						// sc_equalHeight();
						if($element.children('div').length < 2){
							$slicker.find('.slick-block').css('margin-bottom','inherit');
						}
						scc_div_rte(scc_div);
					}
				}else{
					$slicker.attr('aria-hidden','true');
					$slicker.slick("unslick");
					$slicker.slick({
						arrows: settings.arrows,
						dots: settings.dots,
						infinite: settings.infinite,
						accessibility: settings.accessibility,
					});

                    $H3.css({height: 'auto'});
					$slick_content_copy.css({height: 'auto'});
					$slick_content_copy_rte.css({height: 'auto'});

					if($element.children('div').length < 2){
						$slicker.find('.slick-block').css('margin-bottom','inherit');
					}
					chk_slick_dots();
					setTimeout(function(){ $slicker.attr('aria-hidden','false'); }, 1000);
				}


				// Accessibility
				accessibility();

			}
		});

		g.ev.on(g.events.afterTabActiveChanged, function (event, $el) {
			event.preventDefault();

			if ($el.has($element).length > 0){

				if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
					if(window.innerWidth < BREAKPOINT_MD_VALUE) {
						if($element.children('div').length < 2){
/* commented out the block below for https://track.td.com/browse/NAPST-570
                            $H3.css({height: 'auto'});
							$slick_content_copy.css({height: 'auto'});
							$slick_content_copy_rte.css({height: 'auto'});

							var str = $slicker.find('.slick-block').attr('class'),
								regex_sm_12 = new RegExp("\\btd-col-sm-12\\b"),
								regex_sm_6 = new RegExp("\\btd-col-sm-6\\b");

							if(!regex_sm_6.test(str)){
								$slick_content_copy.css({height: 'auto'});
								$slick_content_copy_rte.css({height: 'auto'});
							}else{
								scc_div_rte(scc_div);
							}
*/
							scc_div_rte(scc_div); //for https://track.td.com/browse/NAPST-570

							//console.log($slicker.find('.slick-block').attr('class'));

							if(!$is_product_illustaration_cta && regex_sm_12.test(str)){
								$slicker.find('.slick-block').css('margin-bottom','30px');
							}
						}else{
							scc_div_rte(scc_div);
						}
					}else{
						// sc_equalHeight();
						if($element.children('div').length < 2){
							$slicker.find('.slick-block').css('margin-bottom','inherit');
						}
						scc_div_rte(scc_div);
					}
				}else{
					$slicker.attr('aria-hidden','true');
					$slicker.slick("unslick");
					$slicker.slick({
						arrows: settings.arrows,
						dots: settings.dots,
						infinite: settings.infinite,
						accessibility: settings.accessibility,
					});

                    $H3.css({height: 'auto'});
					$slick_content_copy.css({height: 'auto'});
					$slick_content_copy_rte.css({height: 'auto'});

					if($element.children('div').length < 2){
						$slicker.find('.slick-block').css('margin-bottom','inherit');
					}
					chk_slick_dots();
					setTimeout(function(){ $slicker.attr('aria-hidden','false'); }, 1000);
				}


				// Accessibility
				accessibility();

			}
		});

		function scc_div_rte(callback){
			//sc_equalHeight('.td-row.td-swipeable-blocks '+slick_content_copy_rte);
			switch($element.children('div').length){
				case 1:
                    sc_equalHeight(' > div:nth-child(1) '+H3);
					sc_equalHeight(' > div:nth-child(1) '+slick_content_copy_rte);
					break;
				case 2:
					if(window.innerWidth >= BREAKPOINT_MD_VALUE && $element.find(' > div:nth-child(1)').css('float') == 'left'){
                        sc_equalHeight(' > div '+H3);
						sc_equalHeight(' > div '+slick_content_copy_rte);
					}else{
                        sc_equalHeight(' > div:nth-child(1) '+H3);
                        sc_equalHeight(' > div:nth-child(2) '+H3);
						sc_equalHeight(' > div:nth-child(1) '+slick_content_copy_rte);
						sc_equalHeight(' > div:nth-child(2) '+slick_content_copy_rte);
					}
					break;
				default:
                    sc_equalHeight(' > div:nth-child(1) '+H3);
                    sc_equalHeight(' > div:nth-child(2) '+H3);
					sc_equalHeight(' > div:nth-child(1) '+slick_content_copy_rte);
					sc_equalHeight(' > div:nth-child(2) '+slick_content_copy_rte);
			}
			callback();
		}
		function scc_div(){
			//sc_equalHeight(slick_content_copy);

			switch($element.children('div').length){
				case 1:
					sc_equalHeight(' > div:nth-child(1) '+slick_content_copy);
					break;
				case 2:
					if(window.innerWidth >= BREAKPOINT_MD_VALUE && $element.find(' > div:nth-child(1)').css('float') == 'left'){
						sc_equalHeight(' > div '+slick_content_copy);
					}else{
						sc_equalHeight(' > div:nth-child(1) '+slick_content_copy);
						sc_equalHeight(' > div:nth-child(2) '+slick_content_copy);
					}
					break;
				default:
					sc_equalHeight(' > div:nth-child(1) '+slick_content_copy);
					sc_equalHeight(' > div:nth-child(2) '+slick_content_copy);
			}
		}
		/*
		 function cssNamesToSingleName(obj){
		 var arry = obj.split(" "),
		 result='';

		 for (var i=0; i <= arry.length - 1; i++){
		 result = result + '.'+arry[i];
		 }

		 return result;
		 }
		 */
		/*
		 if (win_inner_width < BREAKPOINT_SM_VALUE) {
		 //add second row of slider elements
		 $slicker.slick('slickAdd', $element.children().find(SLICK_CONTENT_SECOND_ROW).children(SLICK_BLOCK_SECOND_ROW));
		 sc_equalHeight(slick_content_copy);
		 sc_equalHeight(slick_content_copy_rte);
		 }
		 */
		if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
			$slicker.slick('unslick');
			if (window.innerWidth < BREAKPOINT_MD_VALUE) {
				if($element.children('div').length < 2){
/* commented out the block below for https://track.td.com/browse/NAPST-570
                    $H3.css({height: 'auto'});
					$slick_content_copy.css({height: 'auto'});
					$slick_content_copy_rte.css({height: 'auto'});

					//console.log($slicker.find('.slick-block').attr('class'));
					var str = $slicker.find('.slick-block').attr('class'),
						regex_sm_12 = new RegExp("\\btd-col-sm-12\\b"),
						regex_sm_6 = new RegExp("\\btd-col-sm-6\\b");

					if(!regex_sm_6.test(str)){
						$slick_content_copy.css({height: 'auto'});
						$slick_content_copy_rte.css({height: 'auto'});
					}else{
						scc_div_rte(scc_div);
					}
*/
					scc_div_rte(scc_div); //https://track.td.com/browse/NAPST-570

					//console.log($slicker.find('.slick-block').attr('class'));

					if(!$is_product_illustaration_cta && regex_sm_12.test(str)){
						$slicker.find('.slick-block').css('margin-bottom','30px');
					}
				}else{
					scc_div_rte(scc_div);
				}
			}else{
				if($element.children('div').length < 2){
					$slicker.find('.slick-block').css('margin-bottom','inherit');
				}
				scc_div_rte(scc_div);
			}
		}else{
			$slicker.slick('slickAdd', $element.children().find(SLICK_CONTENT_SECOND_ROW).children(SLICK_BLOCK_SECOND_ROW));
            $H3.css({height: 'auto'});
			$slick_content_copy.css({height: 'auto'});
			$slick_content_copy_rte.css({height: 'auto'});

			if($element.children('div').length < 2){
				$slicker.find('.slick-block').css('margin-bottom','inherit');
			}
			chk_slick_dots();
		}
		// Accessibility
		accessibility();

		var scrollTop;
		$window.on('resize', function () {
			if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
				$slicker.slick("unslick");
				$slicker.slick('slickRemove', false);
				//$element.children().find(".slick-content").children(SLICK_BLOCK_SECOND_ROW).appendTo(SLICK_CONTENT_SECOND_ROW);
				$element.children().find(".slick-content").children(SLICK_BLOCK_SECOND_ROW).appendTo($element.find(SLICK_CONTENT_SECOND_ROW));

				if (window.innerWidth < BREAKPOINT_MD_VALUE) {
					if($element.children('div').length < 2){
/* commented out the block below for https://track.td.com/browse/NAPST-570
                        $H3.css({height: 'auto'});
						$slick_content_copy.css({height: 'auto'});
						$slick_content_copy_rte.css({height: 'auto'});

						var str = $slicker.find('.slick-block').attr('class'),
							regex_sm_12 = new RegExp("\\btd-col-sm-12\\b"),
							regex_sm_6 = new RegExp("\\btd-col-sm-6\\b");

						if(!regex_sm_6.test(str)){
							$slick_content_copy.css({height: 'auto'});
							$slick_content_copy_rte.css({height: 'auto'});
						}else{
							scc_div_rte(scc_div);
						}
*/
						scc_div_rte(scc_div); //https://track.td.com/browse/NAPST-570
						//console.log($slicker.find('.slick-block').attr('class'));

						if(!$is_product_illustaration_cta && regex_sm_12.test(str)){
							$slicker.find('.slick-block').css('margin-bottom','30px');
						}
					}else{
						scc_div_rte(scc_div);
					}
				}else{
					// sc_equalHeight();
					if($element.children('div').length < 2){
						$slicker.find('.slick-block').css('margin-bottom','inherit');
					}
					scc_div_rte(scc_div);
				}
			} else {
				if(!$slicker.hasClass('slick-initialized')) {
					$slicker.slick("unslick");
					$slicker.slick({
						arrows: settings.arrows,
						dots: settings.dots,
						infinite: settings.infinite,
						accessibility: settings.accessibility,
					});
					$slicker.slick('slickAdd', $element.children().find(SLICK_CONTENT_SECOND_ROW).children(SLICK_BLOCK_SECOND_ROW));
				}
                $H3.css({height: 'auto'});
				$slick_content_copy.css({height: 'auto'});
				$slick_content_copy_rte.css({height: 'auto'});

				if($element.children('div').length < 2){
					$slicker.find('.slick-block').css('margin-bottom','inherit');
				}

				chk_slick_dots();
			}

			accessibility();
			/*
			 sc_equalHeight(slick_content_copy);
			 sc_equalHeight(slick_content_copy_rte);
			 */
		});

		// Remove margin-top from .slick-list div when there are no dots
		function chk_slick_dots(){
			if(typeof $element.find(SLICK_DOTS).get(0) === 'undefined') {
				$element.find('.slick-list').css('margin-top','0px');
			}
		}

		function sc_equalHeight(qry){
			var $obj = $element.find(qry),
				height = g.getEqualHeight($obj);
			$obj.css("height", height+"px");
		}

		function accessibility(){
			// accessibility
			if($element.find(".slick-block").get(0)) $element.find(".slick-block").attr('aria-hidden','false');
			if($element.find(SLICK_BLOCK_SECOND_ROW).get(0)) $element.find(SLICK_BLOCK_SECOND_ROW).attr('aria-hidden','false');
			if($element.find(SLICK_DOTS).get(0)) {
				$element.find(SLICK_DOTS+" li").attr('aria-hidden','true');
				$element.find(SLICK_DOTS+" li button").attr('tabindex','-1');
			}

			if(window.innerWidth < BREAKPOINT_SM_VALUE){
				$slicker.find('.slick-list').removeAttr('aria-live');
			}
		}
	};
	var setTimeoutConst, setTimeoutConstOut;
	// PLUGINS DEFINITION
	// ==================
	$.fn.tdSwipeable = g.getPluginDef(tdSwipeable, 'tdSwipeable');
	// DEFAULT SETTINGS
	// ================
	$.fn.tdSwipeable.defaults = {
		infinite: false,
		dots: true,
		arrows: false,
		accessibility: false
	};
	$(".td-swipeable-blocks").tdSwipeable();
}(global));}],
td_rq_multiple_cta: [function (global) {/* Source: src/js/td-multiple-cta.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Mutliple Ctas
 * Created by dennis.erny@td.com
 * Start date: 2016/06/24
 * @version 1.0
 *
 * Multiple CTA - This is required for all variants (2,3,4)
 * 
 * Updated by daniel.cho@td.com
 * 		Replace p tag with .rte for $cta_description variable
 * 		@version 1.0.1
 *
 * 		Added 'aria-expanded' to the more option button and link for accessibility
 * 		@version 1.0.2
 *
 * 		Added tab interaction
 * 		@version 1.0.3
 * 
 * 		Removed apply_accessibility(); when page loads
 * 		@version 1.0.3.1
 *
 * 		Added setInitEqualHeight funciton as callback for setHeight in order to make 'More Options' vertically center in LG,MD, and SM views
 * 		@version 1.0.3.2
 *
 *      Fixed to have more option button and more option link get added to proper place in mutiple use.
 *		@version 1.0.3.3
 *
 *		Added g.ev.on(g.events.afterExpandForOthers to reiniticate when the open state of Expand & collpase component gets triggered.
 *  	@version 1.0.4
 *
 *		Made the focus go to the 'td-cta-content' div when a user clicks the 'More Options' button or link.
 *		@version 1.0.5
 *
 * 		Made the component re-trigger when it only exsits wihtin expand & collpase
 * 		@version 1.0.6
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,

        multipleCTA = function (element, options) {
            var $ctaContainer = $(element);
			var settings = $.extend($.fn.tdMultipleCTA.defaults, $ctaContainer.metadata(), options),
				opt_expand = settings.expand,
				opt_btn_more_label = settings.btn_more_label;			
            var $cta = $ctaContainer.find(".td-cta");
			var $cta_first_item = $ctaContainer.find(".td-cta:eq( 0 )");
			var $cta_second_item = $ctaContainer.find(".td-cta:eq( 1 )");
            var $cta_h3 = $ctaContainer.find(".td-cta-heading");
            //var $cta_description = $ctaContainer.find("p");
			var $cta_description = $ctaContainer.find(".rte");
            var $cta_image = $ctaContainer.find(".td-illustration");
            var $cta_action = $ctaContainer.find(".td-cta-action");
            var $moreOptions;
            var moreOptionsLink = "<div class='td-more-options-div'><button class='td-button td-button-block td-button-large td-button-clear-green td-more-options' aria-expanded='false'>"+opt_btn_more_label+"</button></div>";
            var moreOptionsMobileLink = "<div class='td-more-options-mobile'><a href='javascript:void(0);' aria-expanded='false' role='button'>"+opt_btn_more_label+"<span class='td-icon' aria-hidden='true'></span></a></div>";

            init();

            function init() {
					
				if($ctaContainer.find('.td-more-options-div').get(0)) $ctaContainer.find('.td-more-options-div').remove();
				if($ctaContainer.find('.td-more-options-mobile').get(0)) $ctaContainer.find('.td-more-options-mobile').remove();		
					
				// 3 & 4 columns
				//setInitEqualHeight();	
					
				// Expand divs 	
				if(opt_expand == true) {
					setExpand();
				}else{
					setHeight(setInitEqualHeight);
				}
				
                // Append link for mobile (Less hassle for CMS)
                //$(moreOptionsMobileLink).appendTo(".td-multiple-cta .td-cta:first");
				$ctaContainer.find(".td-cta:first").append(moreOptionsMobileLink);

                // Alter structure of 2nd CTA for 3+ arrangements. Again, this will provide less confusion for CMS
                if ($ctaContainer.hasClass("td-cta-3") || $ctaContainer.hasClass("td-cta-4")) {
					
					var g_tabindex = " tabindex='0'";
					if(opt_expand == true) g_tabindex = "";
					
                    $ctaContainer.find(".td-cta:first + .td-cta").children().wrapAll("<div class='td-cta-content' "+g_tabindex+" />");
                    //$(moreOptionsLink).appendTo(".td-multiple-cta .td-cta:first + .td-cta");
					$ctaContainer.find(".td-cta:first + .td-cta").append(moreOptionsLink);

                }

                $moreOptions = $ctaContainer.find(".td-more-options, .td-more-options-mobile");
				$moreOptions.off('click');				
                $moreOptions.on("click", function () {
                    setExpand();
                });


                // Check setHeight again on a resize
                //$window.resize(setHeight);
				$window.resize(function(){
					
					//setHeight();
					
					// For collpase for 3 & 4 columns
					setHeight(setInitEqualHeight);
					
				});
				
				// tab interaction
				/*
				g.ev.on(g.events.afterTabActiveChanged, function (event, $wrapper) {
					event.preventDefault();
					if ($wrapper.has($ctaContainer).length > 0) {
						if(opt_expand == true) setExpand();
					}
				});
				*/
            }
			
			g.ev.on(g.events.afterExpandForOthers, function (event, data) {
				if( data.obj.has($ctaContainer).length > 0 ) {
					 init();		
				}
			});				
						
			g.ev.on(g.events.afterTabActiveChanged, function (event, $wrapper) {
				event.preventDefault();
				if ($wrapper.has($ctaContainer).length > 0) {
					init();
				}
			});
			function setInitEqualHeight(){
				// 3 & 4 columns
				if(!$ctaContainer.hasClass("td-cta-2") && opt_expand != true && $ctaContainer.hasClass("collapsed")){ 
					// console.log("3 & 4 and not open");

					if ($window.width() > 750) {
						var cta_item_height = g.getEqualHeight($cta_first_item);
						$cta_second_item.css("height", cta_item_height + "px");
						
					}else{
						$cta.css("height", "auto");
					}
					
				}				
			}
			
			function setExpand(){
				$ctaContainer.removeClass("collapsed");
				$cta.hide();
				$cta.fadeIn(function () {	
					setHeight(setInitEqualHeight);
					apply_accessibility();
					$cta.css("height", "auto"); // reset $cta height
					if(opt_expand != true) $ctaContainer.find('.td-cta-content').focus().css('outline',0).attr('tabindex','-1');
				});				
			}
			
            function setHeight(callback) {
                //var height = g.getEqualHeight($cta) + 40;
                var cta_h3_height = g.getEqualHeight($cta_h3);
                var cta_desc_height = g.getEqualHeight($cta_description);
                var cta_action_height = g.getEqualHeight($cta_action);
                if ($window.width() > 750) {
                    $cta_h3.css("height", cta_h3_height + "px");
                    $cta_description.css("height", cta_desc_height + "px");
                    $cta_action.css("height", cta_action_height + "px");
                    // $cta.css("height", height + "px");
                } else {
                    $cta_h3.css("height", "auto");
                    $cta_description.css("height", "auto");
                    $cta_action.css("height", "auto");
                    // $cta.css("height", "auto");
                }
				
				if(!$ctaContainer.hasClass("td-cta-2") && opt_expand != true && $ctaContainer.hasClass("collapsed")){ 
					callback();
				}
            }

            function apply_accessibility() {
				$ctaContainer.find('.td-cta-content').next('button').attr('aria-expanded',true);
				$ctaContainer.find('.td-more-options-mobile a').attr('aria-expanded',true);
            }

        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdMultipleCTA = g.getPluginDef(multipleCTA, 'tdMultipleCTA');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdMultipleCTA.defaults = {
		expand: false,
		btn_more_label: 'More options'
	};

    $(".td-multiple-cta").tdMultipleCTA();
}(global));}],
td_rq_product_illustration_cta: [function (global) {/* Source: src/js/td-product-illustration-cta.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Product Illustration CTA
 * Created by dennis.erny@td.com
 * Start date: 2016/06/27
 * @version 1.0
 *
 *
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,

        prodCTA = function (element, options) {
            var $cta = $(element);

            init();

            function init() {
                apply_accessibility();
            }


            function apply_accessibility() {


            }

        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdProdCTA = g.getPluginDef(prodCTA, 'tdProdCTA');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdProdCTA.defaults = {};

    $(".td-product-illustration-cta").tdProdCTA();
}(global));}],
td_rq_product_service_illustration_grid: [function (global) {/* Source: src/js/td-product-service-illustration-grid.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Product/Service Illustration Grid
 * Created by dennis.erny@td.com
 * Start date: 2016/07/25
 * @version 1.0
 *
 * Fixed/Updated by danie.cho@td.com
 * date: 2016/08/10
 * - fixed auto height functionality
 * date: 2016/08/12
 * - fixed removing placeholder div on portrait and mobile
 * date: 2016/12/06
 * - added tab interaction (afterTabActiveChanged)
 * date: 2016/12/08
 * - Maded the equal height works row by row - remember '[ADDED DUE TO PLACEDHOLDER]' added due to placeholder that needs to be removed from JS and HTML in future
 * date: 2017/01/06
 * - Fixed interaction with tab (not working properly for mutiple use)
 * date: 2017/06/22
 * - new option(cat_type) added and equal height for .td-product-description-2 div added.
 * - Added callback function for setHeight and setBorder
 * - Made eqault height work when image fully loaded.
 * date: 2017/06/29
 * - Changed product_h3 to product_h2
 * date: 2017/07/10
 * - Added 'chk_offer_recently_viewed' function to have the 'Special Offer' and 'Recently Viewed' when the 'cat_type' is 'borrowing'
 * date: 2017/10/05
 * - Added g.ev.on(g.events.afterExpandForOthers to reiniticate when the open state of Expand & collpase component gets triggered.
 * date: 2017/10/13
 * - Made the alignment work when adding '.hide' CSS class to Catalogue Card Item
 * date: 2017/10/24
 * - Added re_render() to re-render Equal Height and border
 * date: 2017/10/27
 * - Made sure to run 'removeClass('border-bottom-removed-byJS')' whenever filtering with new options and stwiching sequence of setHeight() and setBorder() for TS
 *
 * date: 2018/02/22 by ayeon.chung@td.com
 * - Removed unused function getEqualHeight_daniel()
 * - Set h2 padding per ROW when there is special indicator or recently viewed indicator
 * - Added set_h2_padding()
 * - Made num_col, arry_rows as component level variables
 * date: 2018/03/01 by ayeon.chung@td.com
 * - Added add_id_to_h2() to add an unique ID to <h2> tag for accessibility fix for "View Details" link in RTE field
 * - Reduced h2 padding-top for mobile devices : from recently_viewed_height + 29px to recently_viewed_height + 2px
 *
 * date: 2018/03/15 by ayeon.chung@td.com
 * - Defect fix in chk_offer_recently_viewed() to calculate height of indicators correctly when there are mutilple instances of #27, #27.1 and #27.2 in one page under tabs
 */


(function (window) {

var $ = window.jQuery,
		$window = $(window),
		g = window.g,
		document = window.document,
		BREAKPOINT_MD_VALUE = g.bpSet('md'),
		BREAKPOINT_SM_VALUE = g.bpSet('sm'),

		productServiceIllustrationGrid = function (element, options) {
			var $productServiceContainer = $(element),
			//var $productService = $productServiceContainer.find(".td-product-service > div");
				settings = $.extend($.fn.tdProductServiceIllustrationGrid.defaults, $productServiceContainer.metadata(), options),
				cat_type = settings.cat_type,
				trigger_re_render = settings.re_render,
				TD_PRODUCT_SERVICE = ".td-product-service",
				$TD_PRODUCT_SERVICE = $productServiceContainer.find(TD_PRODUCT_SERVICE),
				$product_h2 = $productServiceContainer.find("h2"),
				$product_h3 = $productServiceContainer.find("h3"),
				$product_description = $productServiceContainer.find(".td-product-description"),
				$product_description2 = $productServiceContainer.find(".td-product-description-2"),
				$product_image = $productServiceContainer.find(".td-product-image"),
				$product_action = $productServiceContainer.find(".td-product-action"),
				$last_item,// = $productServiceContainer.find(TD_PRODUCT_SERVICE+':nth-last-child(1) > div'),
				$second_last_item,// = $productServiceContainer.find(TD_PRODUCT_SERVICE+':nth-last-child(2) > div'),
				$third_last_item,// = $productServiceContainer.find(TD_PRODUCT_SERVICE+':nth-last-child(3) > div'),
				indicatorOffer = '.td-indicator-offer',
				recentlyViewed = '.td-indicator-recently-viewed';
			var num_col = 0,
				arry_rows;

			init();

			g.ev.on(g.events.afterExpandForOthers, function (event, data) {
				if( $productServiceContainer.parents(data.obj).get(0) ) {
					init(true);
				}
			});

			function re_render(){
				$TD_PRODUCT_SERVICE = $productServiceContainer.find(TD_PRODUCT_SERVICE+':not(.hide)');
				$TD_PRODUCT_SERVICE.each(function(index, element){
					if( index == ($TD_PRODUCT_SERVICE.length - 1) ){
						$last_item = $(element).find(' > div');
					}else if( index == ($TD_PRODUCT_SERVICE.length - 2) ){
						$second_last_item = $(element).find(' > div');
					}else if( index == ($TD_PRODUCT_SERVICE.length -3) ){
						$third_last_item = $(element).find(' > div');
					}else{}
				});

				chk_offer_recently_viewed(setAdjustGrid);
			}

			function add_id_to_h2() {
				var uniqueID = 	'_title';
				//add id to h2
				$TD_PRODUCT_SERVICE.each(function(index, element){
					var prodid = $(element).attr('id');
					var $h2 = $(element).find('h2');
					if(prodid && $h2.get(0)){$h2.attr('id', prodid + uniqueID);}
				});
			}

			function init(redo) {
				if(!trigger_re_render){
					if(!redo && typeof redo === 'undefined'){
						/** [ADDED DUE TO PLACEDHOLDER] Begin **/
						var $last_obj = $productServiceContainer.find(TD_PRODUCT_SERVICE+':last');
						$last_obj.remove();

						/*
						 $last_item = $productServiceContainer.find(TD_PRODUCT_SERVICE+':nth-last-child(1) > div');
						 $second_last_item = $productServiceContainer.find(TD_PRODUCT_SERVICE+':nth-last-child(2) > div');
						 $third_last_item = $productServiceContainer.find(TD_PRODUCT_SERVICE+':nth-last-child(3) > div');
						 */

						/** [ADDED DUE TO PLACEDHOLDER] END **/
					}

					$TD_PRODUCT_SERVICE = $productServiceContainer.find(TD_PRODUCT_SERVICE+':not(.hide)');
					$TD_PRODUCT_SERVICE.each(function(index, element){
						if( index == ($TD_PRODUCT_SERVICE.length - 1) ){
							$last_item = $(element).find(' > div');
						}else if( index == ($TD_PRODUCT_SERVICE.length - 2) ){
							$second_last_item = $(element).find(' > div');
						}else if( index == ($TD_PRODUCT_SERVICE.length -3) ){
							$third_last_item = $(element).find(' > div');
						}else{}
					});

					// Apply Accessibility
					add_id_to_h2();

					// Set initial container height
					chk_offer_recently_viewed(setAdjustGrid);

					// wait until images load
					$window.load(function(){
						chk_offer_recently_viewed(setAdjustGrid);
					});

					// Check setHeight and setBorder again on a resize
					$window.resize(function(){
						arry_rows = get_stored_rows();
						chk_offer_recently_viewed(setAdjustGrid);
					});


					//setHeight();
					//setBorder();

					// Check setHeight again on a resize
					//$window.resize(setHeight);
					//$window.resize(setBorder);

					// tab interaction
					g.ev.on(g.events.afterTabActiveChanged, function (event, $wrapper) {
						event.preventDefault();
						if ($wrapper.has($productServiceContainer).length > 0) {
							chk_offer_recently_viewed(setAdjustGrid);
						}
					});
				}else{
					re_render();
				}
			}

			function set_h2_padding(arry, qry_str, padding) {

				$.each(arry,function(i, value){
					$(this).find(qry_str).css({"height":"auto", "padding-top":padding+"px"});
				});
			}

			function chk_offer_recently_viewed(callback){
				if(cat_type == 'borrowing'){

					var specialOffer_height = $productServiceContainer.find(indicatorOffer).innerHeight(), //67;
						recentlyViewed_height = $productServiceContainer.find(recentlyViewed).innerHeight(),
						title_padding = 0;

					$product_h2.css('padding-top', title_padding+'px');

					/** [ADDED DUE TO PLACEDHOLDER] Begin **/
					$TD_PRODUCT_SERVICE = $productServiceContainer.find(TD_PRODUCT_SERVICE+':not(.hide)');
					/** [ADDED DUE TO PLACEDHOLDER] End **/

					if (window.innerWidth >= BREAKPOINT_SM_VALUE){

						if (!arry_rows)
							arry_rows = get_stored_rows();

						$TD_PRODUCT_SERVICE.each(function(index, element){
							var hasIndicator = false;

							if( $(element).find(indicatorOffer).hasClass('show') ){
								title_padding = specialOffer_height + 1;
								hasIndicator = true;

							} else if( $(element).find(recentlyViewed).hasClass('show') ){
								title_padding = recentlyViewed_height + 1;
								hasIndicator = true;
							}
							if (hasIndicator) {
								var row = parseInt(index / num_col);
								set_h2_padding(arry_rows[row], "h2", title_padding);
							}

						});
					} else {

						$TD_PRODUCT_SERVICE.each(function(index, element){
							if( $(element).find(indicatorOffer).hasClass('show') ){
								title_padding = specialOffer_height + 29;
								$(element).find('h2').css('padding-top', title_padding+'px');

							} else if( $(element).find(recentlyViewed).hasClass('show') ){
								title_padding = recentlyViewed_height + 2;
								$(element).find('h2').css('padding-top', title_padding+'px');

							}
						});
					}
				}

				callback(setBorder);
			}

			function setAdjustGrid(callback){
				// Set initial height
				callback();
				setHeight();
				// callback();
			}

			function setHeight() {
				// var height = g.getEqualHeight($productService) + 50;



				if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
					/*
					 var product_h2_height =  g.getEqualHeight($product_h2);
					 //var product_image_height =  g.getEqualHeight($product_image);
					 var prod_desc_height =  g.getEqualHeight($product_description);
					 var prod_action_height =  g.getEqualHeight($product_action);

					 $product_h2.css("height", product_h2_height + "px");
					 //$product_image.css("height", product_image_height + "px");
					 $product_description.css("height", prod_desc_height + "px");
					 $product_action.css("height", prod_action_height + "px");
					 //$productService.css("height", height + "px");
					 */


					/* eqaul height for specific row Begin*/
					if (!arry_rows)
						arry_rows = get_stored_rows();

					for(var i=0;i <= arry_rows.length-1;i++){
						if(cat_type == 'borrowing'){
							// instead of $product_h2
							equalHeight_range(arry_rows[i],"h2");
						}else{
							equalHeight_range(arry_rows[i],"h3");
						}
						// instead of $product_image
						equalHeight_range(arry_rows[i],".td-product-image");

						// instead of $product_description
						equalHeight_range(arry_rows[i],".td-product-description");

						if(cat_type == 'borrowing'){
							// instead of $product_description2
							equalHeight_range(arry_rows[i],".td-product-description-2");
						}
						// instead of $product_action
						equalHeight_range(arry_rows[i],".td-product-action");
					}
					/* eqaul height for specific row End*/

				} else {
					if(cat_type == 'borrowing'){
						$product_h2.css("height", "auto");
					}else{
						$product_h3.css("height", "auto");
					}
					$product_image.css("height", "auto");
					$product_description.css("height", "auto");
					if(cat_type == 'borrowing'){
						$product_description2.css("height", "auto");
					}
					$product_action.css("height", "auto");
					//$productService.css("height", "auto");

				}

				$productServiceContainer.animate({opacity: 1});
			}


			// apply equal height row by row
			function equalHeight_range(arry,qry_str){
				var height = setEqualHeight_range(arry,qry_str);

				$.each(arry,function(){
					$(this).find(qry_str).css("height", height + "px");
				});
			}

			// return equal height for each row
			function setEqualHeight_range(arry,qry_str,options){
				var $docElement,
					height = 0,
					full=false;

				if(typeof options !== 'undefined'){
					if(options.full !== undefined) full = options.full;
				}

				$.each(arry,function(){
					$docElement = $(this).find(qry_str);
					$docElement.css({height: 'auto'});
					height = Math.max(height, $docElement.outerHeight(full));
				});

				return height;
			}

			// return an array that contains all rows
			function get_stored_rows(){
				var obj_arry = new Array(),
					row = 0,
					col = 0;
				num_col = 0;

				if(window.innerWidth >= BREAKPOINT_MD_VALUE){ // MD & LG
					num_col = 3;
				}else if (window.innerWidth < BREAKPOINT_MD_VALUE && window.innerWidth >= BREAKPOINT_SM_VALUE) { //SM
					num_col = 2;
				}

				// store jquery objects from each row - BEGIN
				obj_arry[row] = new Array();

				/** [ADDED DUE TO PLACEDHOLDER] Begin **/
				$TD_PRODUCT_SERVICE = $productServiceContainer.find(TD_PRODUCT_SERVICE+':not(.hide)');
				/** [ADDED DUE TO PLACEDHOLDER] End **/

				$TD_PRODUCT_SERVICE.each(function(index, element){
					index++;

					obj_arry[row][col] = $(element);
					col++;

					// prepare for next row
					if((index % num_col) == 0){
						col = 0;
						row++;
						obj_arry[row] = new Array();
					}
				});
				// store jquery objects from each row - END

				return obj_arry;
			}

			/*
			 // normal equal height
			 function getEqualHeight_daniel($elements, options) {
			 var $docElement,
			 height = 0,
			 full=false;

			 if(typeof options !== 'undefined'){
			 if(options.full !== undefined) full = options.full;
			 }

			 $elements.each(function () {
			 $docElement = $(this);
			 $docElement.css({height: 'auto'});
			 height = Math.max(height, $docElement.outerHeight(full));
			 });

			 return height;
			 }
			 */

			// set border
			function setBorder(){
				/** [ADDED DUE TO PLACEDHOLDER] Begin **/
				$TD_PRODUCT_SERVICE = $productServiceContainer.find(TD_PRODUCT_SERVICE+':not(.hide)');
				/** [ADDED DUE TO PLACEDHOLDER] End **/

				var count = $TD_PRODUCT_SERVICE.length,
					remain = 0;

				initBorder();

				if(window.innerWidth < BREAKPOINT_SM_VALUE){ // XS
					$last_item.addClass('border-bottom-removed-byJS');
				}else if (window.innerWidth < BREAKPOINT_MD_VALUE && window.innerWidth >= BREAKPOINT_SM_VALUE) { //SM
					remain = count % 2;
					resetBorderSM(remain);
				}else { // MD & LG
					remain = count % 3;
					resetBorderMDLG(remain);
				}

				$TD_PRODUCT_SERVICE.each(function(index, element){
					var $obj = $(element).find(' > div');
					if(window.innerWidth < BREAKPOINT_SM_VALUE){ // XS
						$obj.addClass('border-right-removed-byJS');
					}else if (window.innerWidth < BREAKPOINT_MD_VALUE && window.innerWidth >= BREAKPOINT_SM_VALUE) { //SM
						if( (index+1) % 2 == 0){
							$obj.addClass('border-right-removed-byJS');
						}
					}else { // MD & LG
						if( (index+1) % 3 == 0){
							$obj.addClass('border-right-removed-byJS');
						}
					}

				});

			}

			// initialize border
			function initBorder(){
				if(typeof $productServiceContainer.find(TD_PRODUCT_SERVICE+' > div ') !== 'undefined') $productServiceContainer.find(TD_PRODUCT_SERVICE+' > div ').removeClass('border-right-removed-byJS').removeClass('border-bottom-removed-byJS');

				// Below codes are only need for our demo
				if(typeof $last_item !== 'undefined') $last_item.removeClass('border-bottom-removed-byJS');
				if(typeof $second_last_item !== 'undefined') $second_last_item.removeClass('border-bottom-removed-byJS');
				if(typeof $third_last_item !== 'undefined') $third_last_item.removeClass('border-bottom-removed-byJS');
			}

			// reset border for SM
			function resetBorderSM(g_remain){

				switch(g_remain) {
					case 1:
						$last_item.addClass('border-bottom-removed-byJS');

						break;

					default:
						$last_item.addClass('border-bottom-removed-byJS');//.addClass('border-right-removed-byJS');	
						$second_last_item.addClass('border-bottom-removed-byJS');
				}
			}

			// reset border for MD & LG			
			function resetBorderMDLG(g_remain){
				switch(g_remain) {
					case 1:
						$last_item.addClass('border-bottom-removed-byJS');

						break;
					case 2:
						$last_item.addClass('border-bottom-removed-byJS');
						$second_last_item.addClass('border-bottom-removed-byJS');

						break;

					default:
						$last_item.addClass('border-bottom-removed-byJS');//.addClass('border-right-removed-byJS');	
						$second_last_item.addClass('border-bottom-removed-byJS');
						$third_last_item.addClass('border-bottom-removed-byJS');
				}
			}

			function isEmpty( el ){
				return !$.trim(el.html())
			}
		};


	// PLUGINS DEFINITION
	// ==================
	$.fn.tdProductServiceIllustrationGrid = g.getPluginDef(productServiceIllustrationGrid, 'tdProductServiceIllustrationGrid');

	// DEFAULT SETTINGS
	// ================
	$.fn.tdProductServiceIllustrationGrid.defaults = {
		cat_type: 'common',
		re_render: false
	};

	$(".td-product-service-illustration-grid").tdProductServiceIllustrationGrid();
}(global));}],
td_rq_image_blocks_123_with_bullet_list: [function (global) {/* Source: src/js/td-image-blocks-123-with-bullet-list.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD image blocks 123 with bullet list
 * Created by dmitri.kokoshkyn@td.com
 * used td-multiple-cta.js as a template
 * Start date: 2016/06/24
 * @version 1.0
 *
 * image blocks 123 with bullet list
 *
 * 
 * Updated by daniel.cho@td.com
 * @version 1.0.1
 * - Added printout image - made the default image to print out.
 * @version 1.0.2
 * - Added the 'more details' link to sepecific selector
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,
	 BREAKPOINT_SM_VALUE	= g.bpSet('sm'),		

        Img123withBulletList = function (element, options) {
            var $imgListContainer = $(element);
            var $listContainer = $imgListContainer.find(".td-show-more-content");
            var $moreDetails;
            var moreDetailsLink = "<div class='td-more-details-mobile'><a href='javascript:void(0);' tabindex='0' aria-controls='coll-0' aria-expanded='false' aria-label='More details' title='More details'>More details<span class='td-icon' aria-hidden='true'></span></a></div>";
			var $picture = $imgListContainer.find(".td-container-fluid picture");
			var $img = $picture.find('img').attr('srcset');
			var img_printout = "<img src='"+$img+"' class='visible-print' aria-hidden='true'>";
			
            init();

            function init() {
				// printout image
				//console.log(img_printout);
				$('.td-container-fluid .td-lazy').append(img_printout);
                //add button on xs
				$imgListContainer.find('.td-more-details').append(moreDetailsLink);				
				//$(moreDetailsLink).appendTo(".td-image-blocks-123-with-bulleted-list .td-more-details");	
				
				//button that hides container on mobile
				$moreDetails = $imgListContainer.find(".td-more-details-mobile");
				if (window.innerWidth <= BREAKPOINT_SM_VALUE) { 
					$listContainer.attr("aria-hidden","true");
				} else {
					$listContainer.attr("aria-hidden","false");
				}			
				$moreDetails.on("click", function () {
					//hide button
					$(this).parent().fadeOut();	 				
					$(this).children("a").attr("aria-expanded","true");
					//fade in hidden container		
						$listContainer.fadeIn();
					$listContainer.attr("aria-hidden","false").focus();			  
				});

				$window.resize(onWindowResize);		  
            }		
	     //change hidden container aria attribute on window resize
	     function onWindowResize() {
		if (window.innerWidth > BREAKPOINT_SM_VALUE) {
		  $listContainer.attr("aria-hidden","false");
		} else {
		  $listContainer.attr("aria-hidden","true");	
		}			
	     }
        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdImg123withBulletList = g.getPluginDef(Img123withBulletList, 'tdImg123withBulletList');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdImg123withBulletList.defaults = {};

    $(".td-image-blocks-123-with-bulleted-list").tdImg123withBulletList();
}(global));}],
td_rq_a_banner: [function (global) {/* Source: src/js/td-a-banner.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD A-Banner
 * Created by dylan.chan@td.com
 * @version 1.0
 *
 * A-Banner component
 * - retrieves data attribute from HTML to set responsive background image
 */

/**
 * @namespace td-a-banner js
 * @author dylan.chan@td.com (v1.0)
 *    	   daniel.cho@td.com/danielcho80@gmail.com (v2.0)
 *    	   dylan.chan@td.com (v2.1)
 *    	   daniel.cho@td.com/danielcho80@gmail.com (v2.1.1)
 * @version 2.1
 * @description TD A-Banner<br>
 - versoin 1.0: retrieves data attribute from HTML to set responsive background <br>
 - version 2.0: added the parallax effects
 - version 2.1: fixed bug on Firefox with blank space above the parallax bg image. This is caused by the 'transform: translate3d(0px, 0px, 0px);' css that's added to the background. Bug fixed by removing this CSS.
 added auto change buttons' width.
 - version 2.2: fixed bug where parallax image is shown distorted momentarily until the image is completely loaded. Fixed by setting background-position to 0 (hiding the image) initially, until the onload event sets the final size.
 - version 2.3.0: added the 'setPrintImg' function for printout
 - version 2.3.1: added new options (callout_min_height & callout_min_height_sizes) for content within the callout box can be aligned vertically center only for single line/double lines (eg. 	OAS Banner Design 2)
 - version 2.4: - added new option (loginbox)
				- added an extra div (id="ABanner") for Target Premium
 - version 2.5: Fixed image crop issues (images zoomed in so images were not identical on destkop and physical mobile devices(TL, TP, and Phone)	
 - version 2.6: Updated setVideoplayImg function for positioning video play icon
 - version 2.7: Added afterEmergencyMessaging and updated resize and scroll functions to change the position of background images when the emergency message loads.
 - version 3.0: Transition Update
				- Added fade-in transition when the bannre is loaded.
				- g.ev.trigger(g.events.afterABanner,params) is added to communicate with the Operational Message (td-top-messages.js)
				- g.ev.on(g.events.afterOperationalMessage, fn) is added for the fallback transition for g.ev.trigger(g.events.afterABanner,params)
				- Updated '$window.scroll' and 'getBgImageSize' and added 'aBanner_repositioning' and 'aBanner_repositioning_logic' in order to reposition the background image for parallax effect
 - version 3.1: Disabled g.ev.trigger(g.events.afterABanner,params) that A Banner is being pushed down eariler	
 - version 3.1.1: Made the body have margin-top when A Banner actually pushed down. 
				
 */

(function (window) {

var $ = window.jQuery,
		$window = $(window),
		document = window.document,
		$doc = $(document),
		BREAKPOINT_XS = 'xs',
		BREAKPOINT_SM = 'sm',
		BREAKPOINT_MD = 'md',
		BREAKPOINT_LG = 'lg',
		BREAKPOINT_XS_VALUE = 0, // xs value is the lowest, since we're using min-width we want to use 0, this includes all widths up to sm
		BREAKPOINT_SM_VALUE = g.bpSet('sm'),
		BREAKPOINT_MD_VALUE = g.bpSet('md'),
		BREAKPOINT_LG_VALUE = g.bpSet('lg'),
		version = g.detectIE(),

	/**
	 * A-Banner
	 */
		/**
		 * @function aBanner
		 * @memberof td-a-banner js
		 * @description A-Banner display a banner image for each breakpoint
		 * @param {object} element - $(selector) - JQuery object.
		 * @param {array} options - contains optional parameters.
		 */
		aBanner = function(element, options){

			var $banner = $(element),
				BANNER_CLASSNAME = ".td-a-banner",
				TD_CONTAINER = '.td-container',
				$TD_CONTAINER = $banner.find(TD_CONTAINER),
				uid, // unique index for each a-banner instance,
				ID_NAME = 'aBanner';   // id for this component
			var settings = $.extend($.fn.tdABanner.defaults, $banner.metadata(), options),
				logo_status = settings.logo,
				btn_width_status = settings.btnEqualWidth,
				parallax_status = settings.parallax,
				parallax_speed = settings.parallax_speed,
				parallax_top_pos = settings.parallax_top,
				parallax_top_m_pos = settings.parallax_top_m,
				c_min_h_status = settings.callout_min_height,
				c_min_h_lg = settings.callout_min_height_sizes.lg,
				c_min_h_md = settings.callout_min_height_sizes.md,
				c_min_h_sm = settings.callout_min_height_sizes.sm,
				c_min_h_xs = settings.callout_min_height_sizes.xs,
				videoplay = settings.videoplay,
				videoplay_icon =  settings.videoplay_icon,
				$videoplay_icon = $banner.find(videoplay_icon),
				loginbox_status = settings.loginbox.status,
				loginbox_targetedElement = settings.loginbox.targetedElement,
				img_info = new Array(4),
				bpArray = [BREAKPOINT_XS,BREAKPOINT_SM,BREAKPOINT_MD,BREAKPOINT_LG],
				banner_width,
				banner_height,
				bg_srcset = $banner.data('bg-srcset'),
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())),
				dIconImg = ".dIconImg",
				mIconImg = ".mIconImg",
				videoplayImg = ".videoplayImg",
				chkImgLoad = [[]],
				bpnum=0,
				top_pos,
				$header_nav = $(".td-header-nav"), // TDCT Top Nav
				$header_nav_tdi_xs_sm = $('.td-header'), // TDI Top Nav - TL & Mobile
				$header_nav_tdi_md_lg = $('.td-navigation'), // TDI Top Nav - TP & Desktop
				$emergency_messaging = $(),// = $('.batman-message'), // Emergency Message
				emergency_messaging_status,
				$body = $('body');

			g.ev.on(g.events.afterOperationalMessage, function (event, data) {
				
				var	position_y = data.position_y;
				$emergency_messaging = data.obj;
				emergency_messaging_status = data.ems;

				aBanner_repositioning_logic(position_y,emergency_messaging_status);
			});	
			
			init();
				
			// Listening - Target Premium.
			document.addEventListener("aBanner", function(e){
				init(); 
				//console.log('Listening!'); 
			});				
				
			if(parallax_status && !mobile ){
				//http://code.tutsplus.com/tutorials/simple-parallax-scrolling-technique--net-27641
				$window.scroll(function() {
					//var top_pos;
					
					if($header_nav.get(0)){ // TDCT Top Nav
						top_pos = $header_nav.height();
						
					}else if($header_nav_tdi_xs_sm.get(0) && $header_nav_tdi_md_lg.get(0)){  // TDI Top Nav	
						(getCurBreakPoint(window.innerWidth) == 'xs' || getCurBreakPoint(window.innerWidth) == 'sm') ? top_pos = $header_nav_tdi_xs_sm.height() : top_pos = $header_nav_tdi_md_lg.height();

					}else{ 	
						(getCurBreakPoint(window.innerWidth) == 'xs') ? top_pos = parallax_top_m_pos : top_pos = parallax_top_pos;

					}
					
					var yPos = -($window.scrollTop() / parallax_speed) + top_pos,
						em_h = 0,
						top_pos_temp = yPos;
						
					if( $emergency_messaging.get(0) &&  emergency_messaging_status == 'open' ) { // Check emergency_messaging existence and if its state is 'OPEN'
						em_h = $emergency_messaging.height();
						top_pos_temp = yPos + em_h;
						
					}else if( $emergency_messaging.get(0) &&  emergency_messaging_status == 'close' ){ // Check emergency_messaging existence and if its state is 'CLOSE'
						top_pos_temp = yPos;
						
					}else{}

					// Put together our final background position
					var bpx = $banner.css('backgroundPosition').split(' ')[0];
					var coords = bpx+' '+ ( top_pos_temp ) + 'px';

					// Move the background
					$banner.css({ backgroundPosition: coords });
				});


				$window.resize(function(e){
					//if(!e.isTrigger){
						//console.log('human');
						banner_width = $banner.outerWidth();
						banner_height = $banner.outerHeight();
						var cur_breakPoint = getCurBreakPoint(window.innerWidth);
						//var img_ratio = img_info[cur_breakPoint][0];
						var chkExist = false;

						for(var i=0;i <= chkImgLoad.length-1;i++){
							if(chkImgLoad[i][0] == cur_breakPoint){
								chkExist = true;
							}
						}

						if(!chkExist){
							var actualImage = new Image();
							actualImage.src = img_info[cur_breakPoint][0];

							// intially set the bg size to 0 so that the image doesn't show until it's fully loaded (this prevents seeing a distorted image before it's fully loaded)
							$banner.css({ 'backgroundSize':0 });

							actualImage.onload = function() {
								var img_ratio = getImageRatio(this.width,this.height);
								chkImgLoad[bpnum] = new Array(2);
								chkImgLoad[bpnum][0] = cur_breakPoint;
								chkImgLoad[bpnum][1] = img_ratio;
								bpnum++;

								getBgImageSize(cur_breakPoint,img_ratio,banner_width,banner_height,true);
							}
						}else{
							for(var y=0;y <= chkImgLoad.length-1;y++){
								if(chkImgLoad[y][0] == cur_breakPoint){
									getBgImageSize(cur_breakPoint,chkImgLoad[y][1],banner_width,banner_height, true);
								}
							}
						}
					//}
				});

			}
			
			// All across breakpoints
			$window.resize(function(){
				if(c_min_h_status && (uid !== 'undefined' || uid != '')) setTimeout(set_callout_min_height, 300);
				
				// Video Play Icon
				if(videoplay) setVideoplayImg()
			});

			$doc.ready(function(){
				
				if(videoplay) setVideoplayImg();
			});
			
			function init() {
				// assign unique id
				uid = ID_NAME + $.tdABannerModules.uid;
				$.tdABannerModules.uid += 1;
				$banner.attr('id', uid);

				if(btn_width_status) equalBtnWidth();
				set_bg_image(parallax_status);
				if(parallax_status && !mobile ) checkImages();
				setPrintImg(); // for printout - grabbed desktop img
				if(logo_status) resize_logo2x();
				//if(videoplay) setVideoplayImg();

				if(c_min_h_status && (uid !== 'undefined' || uid != '')) setTimeout(set_callout_min_height, 300);
				
				// check if it's login banner (#1.11)
				if(loginbox_status == true) clone_login_content();
				
				//if(!$emergency_messaging.get(0)) { // Execute when there is NO Emergency Message
					$banner.animate({opacity: 1});
					if($banner.parent().parent().find('.a-login-banner-loginbox').get(0)) $banner.parent().parent().find('.a-login-banner-loginbox').animate({opacity: 1.0});
				//}
				
				//g.ev.trigger(g.events.afterABanner,[{cb_func:aBanner_repositioning}]);
				
			}
			
			/** 
			 * Repositioning background image and passing the operational message OBJECT and its status.
			 */				
			function aBanner_repositioning(){
				
				var	position_y = arguments[2];
				$emergency_messaging = arguments[0];
				emergency_messaging_status = arguments[1];

				aBanner_repositioning_logic(position_y,emergency_messaging_status);
				
			}			
			function aBanner_repositioning_logic(pos_y,msg){
				
				if(parallax_status && !mobile ){
					if(version >= 11){
						var cur_breakPoint = getCurBreakPoint(window.innerWidth),
							pos_in_IE;
						
						if($header_nav.get(0)){ // TDCT Top Nav
							pos_in_IE = $header_nav.height();
						}else if($header_nav_tdi_xs_sm.get(0) && $header_nav_tdi_md_lg.get(0)){  // TDI Top Nav	
							(cur_breakPoint == 'xs' || cur_breakPoint == 'sm') ? pos_in_IE = $header_nav_tdi_xs_sm.height() : pos_in_IE = $header_nav_tdi_md_lg.height();
						}else{ 			
							(cur_breakPoint == 'xs') ? pos_in_IE = parallax_top_m_pos : pos_in_IE = parallax_top_pos;
						}					
						if(msg == 'open'){	
							$body.animate({'margin-top': pos_y});
							pos_y = pos_y + pos_in_IE;
							$banner.css('background-position-y',pos_y);
						}else{
							$banner.css({'background-position-y':(-($window.scrollTop() / parallax_speed) + pos_in_IE)+'px'});
						}
					}else{
						if(msg == 'open'){
							$banner.animate({'background-position-y': '+=' + pos_y});
							$body.animate({'margin-top': pos_y});
						}else{ 
							$banner.animate({'background-position-y': '-=' + pos_y});
						} 
					}
				}else{
					if(msg == 'open') $body.animate({'margin-top': pos_y}); 
				}
			}
			
			/** 
			 * Clone Body content(text & button) and add to Login Box overlay
			 */			
			function clone_login_content(){
				if($(loginbox_targetedElement).get(0)){
					var cloned = $(loginbox_targetedElement).children().clone();
					$(loginbox_targetedElement).children().html('');
					$banner.parent().parent().find('#login-body-content-cloned').html(cloned); // #login-body-content-cloned is fixed 
				}else{
					console.log('missing login content'); 
				}
			}

			/** 
			 * Set min height for the callout box 	
			 */
			function set_callout_min_height(){
				//var cur_callout_height = document.getElementById(uid).getElementsByClassName('td-a-banner-callout')[0].offsetHeight;

				if (window.innerWidth >= BREAKPOINT_LG_VALUE) { // LG
					get_callout_min_height(c_min_h_lg);
				}else if (window.innerWidth >= BREAKPOINT_MD_VALUE) { // MD
					get_callout_min_height(c_min_h_md);
				}else if (window.innerWidth >= BREAKPOINT_SM_VALUE) { // SM
					get_callout_min_height(c_min_h_sm);					
				}else{ // XS
					get_callout_min_height(c_min_h_xs);
				}
				
			}
			
			function get_callout_min_height(min_h){
				var callout_class = 'td-a-banner-callout',
					obj = document.getElementById(uid).getElementsByClassName(callout_class)[0],
					obj_child = document.getElementById(uid).querySelector("."+callout_class+' > div');
					
				// reset
				obj.style.minHeight = '0';
				obj.style.height = 'auto';
				obj_child.classList.remove("callout_center");
				
				if(min_h != 0){
					// set min-height
					obj.style.minHeight = min_h+'px';
					
					// set height to make content within callout center
					var cur_callout_height = obj.offsetHeight;
					if (cur_callout_height <= min_h){ 
						obj.style.height = cur_callout_height+'px'; 
						obj_child.classList.add("callout_center");
					}
				}
			}

			
			/** 
			 * Duplicate the lg image for print 	
			 */
			function setPrintImg(){
				var img_lg = bg_srcset[BREAKPOINT_LG];
				$banner.prepend('<div class="visible-print" aria-hidden="true"><img src="'+img_lg+'" style="display: block; width: 100%;height: auto;" ></div>');
			}
			
			/**
			 * Grab th bigger width between two butotns.
			 * Set both buttons with the width.
			 */
			function equalBtnWidth(){
				var $btns = $banner.find('a');

				if($btns.length > 1){
					if (window.innerWidth <= BREAKPOINT_SM_VALUE) {
						$btns.addClass("td-display-default");
					}

					var btn_w = $btns.eq(0).outerWidth();
					var btn2_w = $btns.eq(1).outerWidth();

					if (window.innerWidth <= BREAKPOINT_SM_VALUE) {
						$btns.removeClass("td-display-default");
					}
					$btns.css("width", (btn_w > btn2_w) ? (btn_w+1) : (btn2_w+1) + "px");
				}
			}
			function resize_logo2x(){
				var $img_obj1x = $banner.find(dIconImg);
				var $img_obj2x = $banner.find(mIconImg);
				if($img_obj1x.get(0)){
					var g_src = $img_obj1x.attr("src");
					var img1x = new Image();
					img1x.src = g_src;
					img1x.onload = function() {
						if($img_obj2x.get(0)){
							$img_obj2x.css({"width":this.width+"px","height":this.height+"px"});
						}
					}

				}
			}
			 
			function setVideoplayImg(){
				
				var pos_left = ($videoplay_icon.width() / 2),
					pos_top = ($videoplay_icon.height() / 2);
					
					
				if (window.innerWidth < BREAKPOINT_MD_VALUE && window.innerWidth >= BREAKPOINT_SM_VALUE) { // SM	

					pos_left = (( $TD_CONTAINER.width() - $TD_CONTAINER.find('> div.td-col').width() ) / 2) - 30;// - pos_left;
					pos_top = ($TD_CONTAINER.height() / 2) - pos_top;
					
				}else if(window.innerWidth < BREAKPOINT_SM_VALUE){ // XS
				
					pos_left = ($TD_CONTAINER.width() / 2) - pos_left;
					pos_top = (( $TD_CONTAINER.height() - $TD_CONTAINER.find('.td-a-banner-callout').innerHeight()) / 2) - pos_top;
					
				}else{ // MD & LG
				
					pos_left = ($TD_CONTAINER.width() / 2) - pos_left;
					pos_top = ($TD_CONTAINER.height() / 2) - pos_top;
					
				}
				
				$videoplay_icon.css({'top':pos_top+'px','left':pos_left+'px','display':'block'});
				//$videoplay_icon.fadeIn();
				
				
				//$videoplay_icon
			}
			
/*			
			function setVideoplayImg(){
				var $btn_img = $banner.find(videoplayImg);
				if($btn_img.get(0)){
					var g_src = $btn_img.attr("src");
					var img = new Image();
					img.src = g_src;
					img.onload = function() {
						$btn_img.parent().css({"width":this.width+"px","height":this.height+"px","display":"block"});
					}					
				}
			}
*/			
			
			
			function checkImages(){
				banner_width = $banner.outerWidth();
				banner_height = $banner.outerHeight();

				var actualImage = new Image();
				actualImage.src = getBgImage(window.innerWidth);

				actualImage.onload = function() {
					var cur_breakPoint = getCurBreakPoint(window.innerWidth);
					var aspectRatio = getImageRatio(this.width,this.height);

					chkImgLoad[bpnum] = new Array(2);
					chkImgLoad[bpnum][0] = cur_breakPoint;
					chkImgLoad[bpnum][1] = aspectRatio;
					bpnum++;

					getBgImageSize(cur_breakPoint, aspectRatio,banner_width,banner_height);
				}
			}

			function getCurBreakPoint(w){
				if(typeof w === 'number'){
					if (w >= BREAKPOINT_LG_VALUE) {
						return pullBgImage(BREAKPOINT_LG);
					}else if (w >= BREAKPOINT_MD_VALUE) {
						return pullBgImage(BREAKPOINT_MD);
					}else if (w >= BREAKPOINT_SM_VALUE) {
						return pullBgImage(BREAKPOINT_SM);
					}else{
						return pullBgImage(BREAKPOINT_XS);
					}
				}else{
					return false;
				}
			}

			function getBgImage(w){
				var getImgUrl;
				if(typeof w === 'number'){
					if (w >= BREAKPOINT_LG_VALUE) {
						getImgUrl = pullBgImage(BREAKPOINT_LG,true);//bg_srcset[BREAKPOINT_LG];
					}else if (w >= BREAKPOINT_MD_VALUE) {
						getImgUrl = pullBgImage(BREAKPOINT_MD,true);
					}else if (w >= BREAKPOINT_SM_VALUE) {
						getImgUrl = pullBgImage(BREAKPOINT_SM,true);
					}else {
						getImgUrl = pullBgImage(BREAKPOINT_XS,true);
					}
				}
				return getImgUrl;
			}

			function pullBgImage(b,s){
				var bArry = [];
				var result = '';
				if(typeof s === 'undefined') s = false;
				if(typeof b === 'string' && typeof s === "boolean"){

					bArry = getBreakPointSeq(b);

					for(var x=0;x <= bArry.length -1; x++){
						if(typeof bg_srcset[bArry[x]] === 'string'){
							(s == true) ? result = bg_srcset[bArry[x]] : result = bArry[x];
							break;
						}
					}
				}
				//console.log("============>:"+result);
				return result;
			}
			function getBreakPointSeq(b){
				var gArry = [];
				if(typeof b === "string"){
					switch(b){
						case 'lg':
							gArry = [BREAKPOINT_LG,BREAKPOINT_MD,BREAKPOINT_SM,BREAKPOINT_XS];
							break;
						case 'md':
							gArry = [BREAKPOINT_MD,BREAKPOINT_LG,BREAKPOINT_SM,BREAKPOINT_XS];
							break;
						case 'sm':
							gArry = [BREAKPOINT_SM,BREAKPOINT_MD,BREAKPOINT_LG,BREAKPOINT_XS];
							break;
						case 'xs':
							gArry = [BREAKPOINT_XS,BREAKPOINT_SM,BREAKPOINT_MD,BREAKPOINT_LG];
							break;
						default:
							gArry = [BREAKPOINT_LG,BREAKPOINT_MD,BREAKPOINT_SM,BREAKPOINT_XS];
					}
				}
				return gArry;
			}
			function getBgImageSize(b,r,w,h,r_t){
				var coords,
					top_pos,
					g_size = w+'px ' + (w / r)+'px',
					img_minHeight = h,// + $banner.offset().top;
					pos_y=0;

				if(img_minHeight > (w / r)) {
					g_size = (img_minHeight*r)+'px ' + img_minHeight+'px';
				}			

				if($header_nav.get(0)){ // TDCT Top Nav
					top_pos = $header_nav.height();
				}else if($header_nav_tdi_xs_sm.get(0) && $header_nav_tdi_md_lg.get(0)){  // TDI Top Nav	
					(b == 'xs' || b == 'sm') ? top_pos = $header_nav_tdi_xs_sm.height() : top_pos = $header_nav_tdi_md_lg.height();
				}else{ 			
					(b == 'xs') ? top_pos = parallax_top_m_pos : top_pos = parallax_top_pos;
				}

				if($emergency_messaging.get(0) && r_t == true && emergency_messaging_status == 'open') { // Add additional height when resizing and the emergency message is activated.
					pos_y = -($window.scrollTop() / parallax_speed) + $emergency_messaging.height();
				}else{
					pos_y = -($window.scrollTop() / parallax_speed);				
				}
				coords = '50% '+ (top_pos + pos_y) +'px';			
				$banner.css({ 'backgroundSize': g_size,'backgroundPosition':coords });
				
			}
			function getImageRatio(w,h){
				return w / h;
			}
			/* all images were loaded - this made page load slow.
			 function getActualImageInfo(breakpoint, img_url){
			 var actualImage = new Image();
			 actualImage.src = img_url;

			 actualImage.onload = function() {
			 img_info[breakpoint] = new Array(2);
			 img_info[breakpoint][0] = getImageRatio(this.width,this.height);
			 img_info[breakpoint][1]	= getBreakPointSeq(breakpoint);
			 }
			 }
			 */
			function getImageInfo(breakpoint, img_url){
				img_info[breakpoint] = new Array(2);
				img_info[breakpoint][0] = img_url
				img_info[breakpoint][1]	= getBreakPointSeq(breakpoint);
			}
			function getBreakPoint(b){
				var breakpoint;
				switch(b){
					case BREAKPOINT_XS:  breakpoint=BREAKPOINT_XS_VALUE; break;
					case BREAKPOINT_SM:  breakpoint=BREAKPOINT_SM_VALUE; break;
					case BREAKPOINT_MD:  breakpoint=BREAKPOINT_MD_VALUE; break;
					case BREAKPOINT_LG:  breakpoint=BREAKPOINT_LG_VALUE; break;
				}

				return breakpoint;
			}

			/**
			 * Set the bg image by appending CSS style in the html
			 * Image url and breakpoint are retrieved from the HTML data-bg-srcset
			 */
			function set_bg_image(ps){
				var bg_srcset = $banner.data('bg-srcset');
				var style_string = "";

				// add css style to head
				style_string += "<style type='text/css'>";

				// get breakpoint and url data, push into another array so it can be sorted
				var bg_srcset_array = [];
				for (var breakpoint in bg_srcset){
					var img_url = bg_srcset[breakpoint];

					if(ps && !mobile ) {
						//getActualImageInfo(breakpoint, img_url);
						getImageInfo(breakpoint, img_url);

						if(bpArray.indexOf(breakpoint) != -1){
							bpArray.splice(bpArray.indexOf(breakpoint),1);
						}
					}

					// convert known breakpoint labels into numerical value, otherwise keep numerical value
					breakpoint = getBreakPoint(breakpoint);
					bg_srcset_array.push([breakpoint, img_url]);
				}

				if(ps && !mobile ) {
					for(var y=0; y <= bpArray.length - 1;y++){
						var g_bp = bpArray[y];
						var img_url = pullBgImage(g_bp,true);

						bg_srcset_array.push([getBreakPoint(g_bp), img_url]);
						//console.log(bpArray[y]);
					}
				}

				bg_srcset_array.sort(function(a,b) { return a[0] - b[0] } );    // sort array so lowest breakpoint comes first

				// add a style for each breakpoint and include the corresponding background image url
				for (var i=0; i<bg_srcset_array.length; i++){
					var breakpoint = bg_srcset_array[i][0];
					var img_url = bg_srcset_array[i][1];

					if(ps && !mobile ){
						// in the css below, we intially set the bg size to 0 so that the image doesn't show until it's fully loaded (this prevents seeing a distorted image before it's fully loaded)
						style_string += "@media(min-width:" + breakpoint +"px) { " +
							"#"+ uid +
							" {background-image: url('" + img_url + "');background-position:50% 0px; background-repeat: no-repeat;background-attachment: fixed;width: 100%;position: relative; background-size:0; } " +
							"} ";
					}else{
						style_string += "@media(min-width:" + breakpoint +"px) { " +
							"#"+ uid +
							" {background-image: url('" + img_url + "');} " +
							"} ";
					}
				}
				style_string += "</style>";

				$("head").append(style_string);

			}				
			
		};

	// create unique id to keep track of each instance of the tabs-carousel
	if (!$.tdABannerModules) {
		$.tdABannerModules = {uid: 0};
	}

	// PLUGINS DEFINITION
	// ==================
	/**
	 * @memberOf td-a-banner js
	 * @property {object} constructor - aBanner
	 * @property {string} name - "tdABanner"
	 * @example
	 * $.fn.tdABanner = g.getPluginDef(aBanner, "tdABanner");
	 */
	$.fn.tdABanner = g.getPluginDef(aBanner, 'tdABanner');

	// DEFAULT SETTINGS
	// ================
	$.fn.tdABanner.defaults = {
		logo: false,
		btnEqualWidth: false,
		parallax: true,
		parallax_speed: 5,
		parallax_top: 62,
		parallax_top_m: 47,
		videoplay: false,
		videoplay_icon: '',
		callout_min_height: false,
		callout_min_height_sizes:{
			lg: 156,
			md: 156,
			sm: 131,
			xs: 0
		},
		loginbox:{
			status: false,
			targetedElement: ''
		}
	};
	$(".td-a-banner").tdABanner();
	
}(global));}],
td_rq_catalogue_card: [function (global) {/* Source: src/js/td-catalogue-card.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD A-Banner
 * Created by daniel.cho@td.com
 * @version 1.0
 *
 * Catalogue card component - to be used for Accounts, Credit Cards, and Borrowing components
 - created based on 'td-a-banner-product.js'
 - added a new option for 'Recently View' (updated by daniel.cho@td.com)
 - added a language option to grab language specific cookies (daniel.cho@td.com)
 - Fixed the special offer indicator appears on the top of the card title.
 - Added special_offer_indicator function to window resize
 - Added "for" attribute to label
 - Added "aria-describedby" attribute for checkbox description
 - Added e.preventDefault(); to 'label' elements due to 'for' which is associsted to input box.

 * 2018-02-01 by ayeon.chung@td.com
 * - Fixed issue on special offer indicator/recently view indicator not showing when compare status is false
 * - Applied h2_padding() when either of special offer indicator or recently view indicator is set
 * - Changed h2_padding() to match with the spacing on 27.2 borrowing as per DCX request
 *
 * 2018-02-015 by ayeon.chung@td.com
 * - Removed extra h2 padding-top when there is no recently view indicator at the same row
 * - Added re_render() to re-render h2_padding() with updated indicator status
 * - Added getCardOrder() to deduce which row this card belons in a breakpoint
 * - Added an extra parameter index to afterCatalogueIndicator to notify the order of the card in a grid
 *
 * 2018-02-28 by ayeon.chung@td.com
 * - Added add_id_to_h2() to add id to h2 tag, will be called when a component is initialized
 * - Removed code which add id to h2 tag from apply_accessibility()
 *
 * 2018-03-14 by ayeon.chung@td.com
 * - added tab interaction (afterTabActiveChanged)
 * - added expand interaction to re-render when the open state of Expand/collapse component gets triggered (afterExpandForOthers)
 * - added a parameter card_type when trigger afterCatalogueIndicator event, to cover a case with catalogue_cards_account & catalogue_cards_credit components under tabs
 *
 * 2018-03-22 by ayeon.chung@td.com
 * - removed a parameter card_type when trigger afterCatalogueIndicator event, to cover a case with multiple catalogue_cards_account components under tabs
 * - instead of card_type, will check ID of the each card (CDS code will be responsible to make ID unique)
 *
 *  2018-06-15 by ayeon.chung@td.com (R18.5)
 *  - updated  $window.resize event to prevent page content jump when scrolling with mobile devices/tablet
 *    Page jump occurs because mobile devices(tablet) fires resize event when scroll
 *
 *  2018-08-01 by daniel.cho@td.com
 *  - Added a new type of sepcial offer indicator(Type2) that will appear under credit card image + star ratings
 *
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,
        BREAKPOINT_SM_VALUE = g.bpSet('sm'),
        catalogueCard = function (element, options) {

            var $card = $(element);
            var $h2 = $card.find("h2");
            var $checkbox_container = $card.find(".td-product-compare");
            var $checkbox_description = $card.find('.td-label-content-wrapper');
            var $checkbox_input = $checkbox_container.find("input");
            var $checkbox_label = $checkbox_container.find("label");
            var $checkbox = $checkbox_container.find(".td-label-content-wrapper");
            var $product_image = $card.find(".td-product-image");
            var $special_offer = $card.find(".td-indicator-offer"); // special offer indicator - Type 1
			var $special_offer_type2 = $card.find(".td-indicator-offer-type2"); // special offer indicator - Type 2
            var $recently_view = $card.find(".td-indicator-recently-viewed"); // recently view indicator
            var $container = $card.parent();

            var settings = $.extend($.fn.tdCatalogueCard.defaults, $card.metadata(), options),
                lang = settings.lang, // language
                trigger_re_render = settings.re_render,
                g_compare_checkbox = settings.compare_checkbox.status, // show/hide compare checkbox (true/false)
                g_prodtype = settings.compare_checkbox.prod_type, // Product Type
                g_prodid = settings.compare_checkbox.prod_id, // Product ID
                g_prodname = settings.compare_checkbox.prod_name, // Product Name
                g_prodimg = settings.compare_checkbox.prod_img, // Product Img
                g_classname = settings.compare_checkbox.classname, // componanet's unique css classname
                g_comp_type = settings.compare_checkbox.comp_type, // component type (eg. a-banner-product/catalogue-card),
                g_cookie_ids = settings.compare_checkbox.cookie_ids, // Credit Card: 'prod_ids_cc', Bank Account: 'prod_ids_ba'
                g_prod_special = settings.compare_checkbox.prod_special.status, // Special Offer status
                g_prod_special_pos = settings.compare_checkbox.prod_special.position, // Special Offer position (left/right)
				g_prod_special_type = settings.compare_checkbox.prod_special.type, // Special Offer Type (default: 1)
                g_prod_recentlyview = settings.compare_checkbox.prod_recentlyview.status, // recently view status
                g_prod_recentlyview_pos = settings.compare_checkbox.prod_recentlyview.position, // recently view position (left/right)
                g_prod_url = settings.compare_checkbox.prod_url,// relevant page
                mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));


            var fn_cd_obj = {}, fn_compare_drawer = {},
                COOKIE_NAME = g_cookie_ids+'_'+lang, //Credit Card: 'prod_ids_cc_en', Bank Account: 'prod_ids_ba_en'
                COMPARE_CHECKBOX = '.td-product-compare', // compare checkbox's CSS name
                COMPARE_DRAWER = '.td_rq_compare-sticky-drawer'; // compare drawer's CSS name

            // compare drawer object
            fn_cd_obj.cookie_name = COOKIE_NAME;
            fn_cd_obj.compare_checkbox = $card.find(COMPARE_CHECKBOX);
            fn_cd_obj.allCheckboxes = $(g_classname).find(COMPARE_CHECKBOX);
            fn_cd_obj.flyToDrawer_compare_drawer = $(COMPARE_DRAWER);

            fn_compare_drawer = {
                cb_return: true,
                max_card_num: 3,
                cur_card_num: 0,
                init: function () {

                    // assign id to checkbox based on settings
                    $checkbox_input.attr('id', g_prodtype + "_" + g_prodid);
                    $checkbox_label.attr('for', g_prodtype + "_" + g_prodid);

                    // set max number of cards to load on physical phone devices
                    if (mobile && (window.innerWidth < g.bpSet('sm'))) {
                        fn_compare_drawer.max_card_num = 2;
                    }

                    // Load cookies - product ids
                    fn_compare_drawer.loadCookieIds();
                },
                checkbox_check_evt: function (ev) {
                    // get a number of ids from cookies
                    if (g.cookies.getItem(fn_cd_obj.cookie_name) != null && g.cookies.getItem(fn_cd_obj.cookie_name) != '') {
                        fn_compare_drawer.cookieids_arr = unescape(g.cookies.getItem(fn_cd_obj.cookie_name)).split(',');
                        fn_compare_drawer.cur_card_num = fn_compare_drawer.cookieids_arr.length;
                    } else {
                        fn_compare_drawer.cur_card_num = 0;
                    }

                    g.addProdToDrawer(fn_compare_drawer.check_callback, g_prodtype, g_prodid, g_prodname, g_prodimg, g_comp_type, g_prod_special, g_prod_url);
                },
                checkbox_uncheck_evt: function (ev) {
                    // get a number of ids from cookies
                    if (g.cookies.getItem(fn_cd_obj.cookie_name) != null && g.cookies.getItem(fn_cd_obj.cookie_name) != '') {
                        fn_compare_drawer.cookieids_arr = unescape(g.cookies.getItem(fn_cd_obj.cookie_name)).split(',');
                        fn_compare_drawer.cur_card_num = fn_compare_drawer.cookieids_arr.length;
                    } else {
                        fn_compare_drawer.cur_card_num = 0;
                    }

                    g.removeProdFromDrawer(fn_compare_drawer.uncheck_callback, g_prodtype, g_prodid, g_comp_type);
                },
                check_callback: function () {

                    fn_compare_drawer.cb_return = arguments[0];

                    // return true, product successfully added
                    if (fn_compare_drawer.cb_return) {

                        // flying to drawer
                        setTimeout(function () {
                            fn_compare_drawer.flyToDrawer($product_image);
                        }, 0);	// put a timeout 0, so drawer has time to appear if it's the first time
                        checkbox_check();
                    }
                    // return false, product not added
                    else {
                        checkbox_uncheck();
                    }
                },
                uncheck_callback: function () {

                    fn_compare_drawer.cb_return = arguments[0];

                    // return true, product successfully removed
                    if (fn_compare_drawer.cb_return) {
                        checkbox_uncheck();
                    }
                    // return false, product not removed
                    else {
                        checkbox_check();
                    }
                },
                flyToDrawer: function (flyToDrawer_image) {
                    g.flyToElement(flyToDrawer_image, fn_cd_obj.flyToDrawer_compare_drawer);
                },
                cookieids_arr: [],
                loadCookieIds: function () { // Cookies (credit card: prod_ids_cc_en, bank account: prod_ids_ba_en)
                    if (g.cookies.getItem(fn_cd_obj.cookie_name) != null && g.cookies.getItem(fn_cd_obj.cookie_name) != '') {
                        fn_compare_drawer.cookieids_arr = unescape(g.cookies.getItem(fn_cd_obj.cookie_name)).split(',');

                        fn_compare_drawer.cur_card_num = fn_compare_drawer.cookieids_arr.length;

                        // check for product if it exists in cookie
                        var index = fn_compare_drawer.cookieids_arr.indexOf(lang+'_'+$checkbox_input.attr('id'));

                        // product exists in cookie
                        if (index > -1) {
                            checkbox_check();
                        }
                        else {
                            checkbox_uncheck();
                        }
                    }
                }
            };

            var cachedWidth =$(window).width();

            init();

            // re-render h2_padding() with updated indicator status
            function re_render(){
                // update the status of indicators
                g_prod_special = $special_offer.hasClass('show');
                g_prod_recentlyview = $recently_view.hasClass('show');

                $('.td-catalogue-card h2').css('padding-top', 0); //reset padding
                h2_padding (g_prod_special, g_prod_recentlyview); // apply h2 padding
            }

            function init() {
                if(!trigger_re_render) {
                    // Added back the outline style
                    $('*').on('keydown', function (event) {
                        if ((event.which === 9) || (event.shiftKey && event.which === 9)) { // Tab key & Shift key + Tab key
                            $checkbox.css({'outline-width': '1px'});
                        }
                    });

                    // Added this function due to "for" attuibute
                    $checkbox_label.click(function (e) {
                        e.preventDefault();
                    });

                    add_id_to_h2();

                    if (g_compare_checkbox == true) {
                        $checkbox.click(checkbox_toggle);	// add checkbox event
                        $checkbox_input.change(on_checkbox_input_change);
                        apply_accessibility();
                        fn_compare_drawer.init();

                    } else {
                        // hide 'compare checkbox'
                        $checkbox_container.hide();
                    }

                    // special offer indicator
                    special_offer_indicator(g_prod_special, g_prod_special_pos, g_prod_special_type);

                    // recently view indicator
                    recently_view_indicator(g_prod_recentlyview, g_prod_recentlyview_pos);

                    // add extra padding on h2 to avoid the indicator appears on the top of the title
                    h2_padding(g_prod_special, g_prod_recentlyview);

                    // tab interaction, interaction when expand
                    g.ev.on(g.events.afterTabActiveChanged + ' ' + g.events.afterExpandForOthers, function (event, $element) {
                        event.preventDefault();
                        h2_padding(g_prod_special, g_prod_recentlyview);
                    });

                } else {
                    re_render();
                }
            }

            $window.resize(function(){
                if ($(window).width() != cachedWidth ){ // added this condition to prevent page jump with mobile/tablet devices

                    cachedWidth =$(window).width();

                    $('.td-catalogue-card h2').css('padding-top', 0); //reset padding
                    h2_padding (g_prod_special, g_prod_recentlyview);
                }
            });

            /**
             * function to detect changes to the checkbox if it has been modified externally by the Compare Drawer component
             */
            function on_checkbox_input_change() {
                var $checkbox = $(this);

                console.log('checkbox input change');
                // make sure the visual checkbox corresponds to the state of the invisible <input> checkbox
                if ($checkbox.is(':checked') === false) {
                    checkbox_uncheck();
                }
                else {
                    checkbox_check();
                }
            }

            function checkbox_toggle(ev) {

                //remove dotted line onclick only
                $checkbox.on('click',function(event) {
                    if(event.which == 1){ // mouse click
                        $(this).css({'outline-width': '0px'});
                    }
                });

                // if already checked, uncheck it
                if ($checkbox_input.prop('checked')) {
                    //checkbox_uncheck();
                    fn_compare_drawer.checkbox_uncheck_evt(ev);	 // send event to remove product from drawer
                }
                // otherwise, check it
                else {
                    //checkbox_check();
                    fn_compare_drawer.checkbox_check_evt(ev);	// send event to add product to drawer
                }
            }

            function checkbox_check() {
                $checkbox_input.prop('checked', true);
                $checkbox_input.addClass("checked");		// add css class to make the checked state show
                $checkbox_label.addClass("label-checked");	// add css class to make the checked state show
                $checkbox.attr("aria-checked", "true");
            }

            function checkbox_uncheck() {
                $checkbox_input.prop('checked', false);
                $checkbox_input.removeClass("checked");		// remove css class to make the checked state not show
                $checkbox_label.removeClass("label-checked");	// remove css class to make the checked state not show
                $checkbox.attr("aria-checked", "false");
            }

            function add_id_to_h2() {
                var uniqueID = 	'_title';
                //add id to h2
                if($h2.get(0)){$h2.attr('id', g_prodtype + "_" + g_prodid + uniqueID);}
            }

            function apply_accessibility() {

                var uniqueID = 	'_title';
                //add attribute aria-describedby to checkbox span instead of checkbox itself
                $checkbox_description.attr('aria-describedby', g_prodtype + "_" + g_prodid + uniqueID);

                /*
                 //add id to h2
                 if($h2.get(0)){$h2.attr('id', g_prodtype + "_" + g_prodid + uniqueID);}
                 */

                // make <input> hidden
                $checkbox_input.attr("aria-hidden", "true");
                $checkbox_input.attr("tabindex", -1);

                $checkbox.attr("tabindex", 0);	// make checkbox <div> get tab focus
                $checkbox.attr("role", "checkbox");	// accessibility: assign checkbox role

                // add keyboard accessibility for checkbox select
                $checkbox.on("keyup", function (event) {
                    if (event.which === 32 || event.which === 13) {
                        checkbox_toggle();
                    }
                });

                //remove dotted line onclick only
                $checkbox.on('click',function(event) {
                    if(event.which == 1){ // mouse click
                        $(this).css({'outline-width': '0px'});
                    }
                });

                // prevent spacebar from scrolling page
                $checkbox.on("keydown", function (event) {
                    if (event.which === 32) {
                        event.preventDefault();
                    }
                });
            }

            function special_offer_indicator(s, p, t) {
                if (s == true) {
					if(t == 2) {
						$special_offer_type2.addClass('show'); 
					}else{
						if (p == 'left') $special_offer.addClass('left');
						$special_offer.addClass('show'); 
					}
                }
            }

            // add extra padding on h2 to avoid the indicator appears on the top of the title
            function h2_padding(showSpecial, showRecentlyView){
                if (showSpecial || showRecentlyView) {
                    var specialOffer_height = $special_offer.innerHeight(),
                        recentlyViewed_height = $recently_view.innerHeight(),
                        title_padding = 0;

                    if (window.innerWidth >= BREAKPOINT_SM_VALUE){
                        if( $special_offer.hasClass('show') ){
                            title_padding = specialOffer_height + 1
                        } else if($recently_view.hasClass('show') ){
                            title_padding = recentlyViewed_height + 1;
                        }
                    } else {
                        if( $special_offer.hasClass('show') ){
                            title_padding = specialOffer_height + 4;

                        } else if($recently_view.hasClass('show') ){
                            title_padding = recentlyViewed_height + 2;
                        }
                        ///                    $card.find('h2').css('padding-top', title_padding + 'px');
                    }

                    var inx = getCardOrder();
                    // wait until the current padding set takes effect
                    setTimeout(function(){
                        g.ev.trigger(g.events.afterCatalogueIndicator,[{indicator_status:true,obj:$card,h2_padding_top:title_padding, index:inx}]);
                    }, 10);
                }
            }

            // get the order of the current card in a grid to calculate which row it belongs with the current breakpoint
            function getCardOrder () {
                var arr = $container.find('.td_rq_catalogue-card:not(.hide)');
                var id = $card.attr('id');
                for (var i=0; i < arr.length; i++) {
                    if ($(arr[i]).attr('id') == id) {
                        return i;
                    }
                }
            }
            /*
             function h2_padding(showSpecial, showRecentlyView){
             var h2_extra_padding_top = 55;
             if (!showSpecial && showRecentlyView) {
             h2_extra_padding_top = 22;
             }
             if (window.innerWidth >= BREAKPOINT_SM_VALUE){
             $('.td-catalogue-card h2').css('padding-top', h2_extra_padding_top+'px');
             }else{
             $card.find('h2').css('padding-top', h2_extra_padding_top+'px');
             }
             g.ev.trigger(g.events.afterCatalogueIndicator,[{indicator_status:true,obj:$card.find('h2'),obj_extra_padding_top:h2_extra_padding_top}]);
             }
             */
            function recently_view_indicator(s, p){
                if (s == true) {
                    if (p == 'left') $recently_view.addClass('left');
                    $recently_view.addClass('show');
                }
            }
        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdCatalogueCard = g.getPluginDef(catalogueCard, 'tdCatalogueCard');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdCatalogueCard.defaults = {
        lang: 'en',
        re_render: false,
        compare_checkbox: {
            status: true,
            prod_type: 'cc',
            prod_id: '',
            prod_name: '',
            prod_img: '',
            classname: '.td-a-banner-product',
            comp_type: '',
            cookie_ids: 'prod_ids_cc',
            prod_special: {
                status: false,
                position: 'right',
				type: 1
            },
            prod_recentlyview: {
                status: false,
                position: 'right'
            },
            prod_url: '#'
        }
    };

    $(".td-catalogue-card").tdCatalogueCard();
}(global));}],
td_rq_tabs_carousel: [function (global) {/* Source: src/js/td-tabs-carousel.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Tabs-Carousel Prototype
 * Created by dylan.chan@td.com
 * @version 2.0
 *
 * This component is a tab menu that combines features of a carousel. It uses the Slick carousel library as well as DragDealer.
 * Slick (http://kenwheeler.github.io/slick/) is used for the main carousel functionality.
 * - the carousel displays different # of tabs per view, depending on the breakpoint.
 * - nav arrows are shown depending on the carousel position
 * - in mobile view, nav arrows are hidden and the tabs take 100% of the width while individual tabs automatically re-size to fit
 * DragDealer (http://skidding.github.io/dragdealer/) is used in place of Slick's drag/swipe because we want to avoid the snapping to slide effect of Slick. DragDealer
 * allows a 'natural' scrolling that doesn't snap and swipes have a 'velocity' effect.
 *
 * @version 2.1
 * - added accessibility fix for VoiceOver not able to select tabs that are off-screen
 **
 * Updated by daniel.cho@td.com
 * @version 2.1.1
 * - updated CSS class name for arrow button due to icon font updates for TDCT
 *
 * Updated by dylan.chan@td.com
 * @version 2.1.2
 * - improved url anchor functionality to accept complex anchor links with uri decode, also decodes HTML entities
 *
 * Updated by daniel.cho@td.com
 * @version 2.1.3
 * - hid .accessibility-instructions in mobile view
 * - added 'aria-selected' to tabs
 *
 * Updated by daniel.cho@td.com
 * @version 2.1.4
 * - added $(".td_rq_tabs-carousel").css('opacity','1').slideDown(300); to delay tab labels' loading.
 *
 * Updated by daniel.cho@td.com
 * @version 2.1.5
 * - added getUrlVars() to have param from URL
 * - made to select sepecific tab with 'tdTab'
 * - changed parameter name from 'tdTab' to tdtab
 * - made tab get selected by number value (eg tdtab=1)
 *
 * Updated by daniel.cho@td.com
 * - added custom event ('afterTabActiveChangedForExternal') for logic tools (AngularJS)
 */

(function (window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),

        /**
         * Tabs-Carousel
         */
        tabsCarousel = function(element, options){

            var $container = $(element),
                $carousel = $container.find('.td-tabs-carousel'),
                $content = $container.find('.td-tabs-carousel-content'),
                mouseDownPositionX = NaN,
                drag_controller, // instance of DragDealer class for controlling dragging
                uid, // unique index for each tabs-carousel instance,
                first_tab_selected = false, // flag to determine if a tab has been selected for the first time
                settings_keyboard_navigation = true,  // flag to determine if custom keyboard navigation using arrow keys is enabled
                settings_carousel_only = false, // flag to determine if this is a simple carousel or a carousel with tabs & tab content
                keyboard_screenreader_focus = false,  // flag to determine if focus is placed on a tab with keyboard or screen reader
                DRAG_THRESHOLD = 15, // the threshold in which a position change is considered as dragged
                TABS_CLASSNAME = '.td-tabs-carousel-tab',
                SLICK_LIST_CLASSNAME = '.slick-list',
                ARROW_NEXT_CLASSNAME = '.slick-next',
                ARROW_PREV_CLASSNAME = '.slick-prev',
                CONTENT_CONTAINER_CLASSNAME = '.td-container',
                FIXED_TAB_HEIGHT_CLASS = 'fixed-tab-height',
                DISABLED_CAROUSEL_CLASSNAME = 'td-tabs-carousel-disabled',  // class to disable carousel from initiating
                SCROLL_TO_OFFSET = -100, // sets the position to which the scroll to function will go to (when a tab is clicked)
                ID_NAME = 'tabsCarousel',   // id for this component
                SLICK_INIT_SETTINGS = {
                    prevArrow: '<button type="button" data-role="none" class="' + ARROW_PREV_CLASSNAME.substr(1) + '" aria-label="Previous" tabindex="-1" role="button"><span class="td-icon td-icon-arrowLeft"></span></button>',
                    nextArrow: '<button type="button" data-role="none" class="' + ARROW_NEXT_CLASSNAME.substr(1) + '" aria-label="Next" tabindex="-1" role="button"><span class="td-icon td-icon-arrowRight"></span></button>',
                    centerMode:false,
                    infinite:false,
                    arrows:true,
                    slidesToShow:5,
                    slidesToScroll: 5,
                    initialSlide: 0,
                    swipeToSlide:true,
                    variableWidth:false,
                    draggable: false,
                    touchMove: false,
                    accessibility: false,
                    waitForAnimate: false,
                    speed: 500,
                    useCSS: false,
                    // touchThreshold: 9999,
                    // edgeFriction: 1.0,
                    responsive: [
                        {
                            breakpoint: g.bpSet('md'),
                            settings: {
                                centerMode:false,
                                infinite:false,
                                swipeToSlide:true,
                                variableWidth:false,
                                draggable: false,
                                touchMove: false,
                                slidesToShow: 5,
                                slidesToScroll: 5,
                                useCSS: false
                            }
                        },
                        {
                            breakpoint: g.bpSet('sm'),
                            settings: {
                                centerMode:false,
                                infinite:false,
                                swipeToSlide:true,
                                variableWidth:false,
                                draggable: false,
                                touchMove: false,
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                useCSS: false
                            }
                        },
                        {
                            breakpoint: g.bpSet('xs'),
                            settings: {
                                centerMode:false,
                                infinite:false,
                                swipeToSlide:true,
                                variableWidth:false,
                                draggable: false,
                                touchMove: false,
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                useCSS: false
                            }
                        }
                    ]
                };

            // initialize carousel only if it does not have the disabled class
            if ($carousel.hasClass(DISABLED_CAROUSEL_CLASSNAME) == false){
                init();
            }

            function init() {

                // assign unique id
                uid = ID_NAME + $.tdTabsCarouselModules.uid;
                $.tdTabsCarouselModules.uid += 1;

                // initialize Slick library
                $carousel.slick(SLICK_INIT_SETTINGS);

                // retrieve settings
                if ($carousel.data('settings') != undefined){
                    if ($carousel.data('settings').keyboardNavigation != undefined) settings_keyboard_navigation = $carousel.data('settings').keyboardNavigation;
                    if ($carousel.data('settings').carouselOnly != undefined) settings_carousel_only = $carousel.data('settings').carouselOnly;
                }

                // add events
                $carousel.on('setPosition', function(slick){ on_position_change(); });
                $carousel.on('breakpoint', function(slick){ on_breakpoint_change(); });
                $carousel.find(SLICK_LIST_CLASSNAME).scroll(on_track_scroll);
                $carousel.find(TABS_CLASSNAME).mousedown(on_slide_mousedown);
                $carousel.find(TABS_CLASSNAME).mouseup(on_slide_mouseup);
                $carousel.find(TABS_CLASSNAME).click(on_slide_click);
                $carousel.find(TABS_CLASSNAME).blur(on_slide_blur);
                $carousel.find(TABS_CLASSNAME).focus(on_slide_focus);
                $carousel.find(TABS_CLASSNAME).keydown(on_slide_keydown);

                // override default arrow nav click events with custom ones
                $carousel.find(ARROW_NEXT_CLASSNAME).off('click');      // disable default slick click event
                $carousel.find(ARROW_NEXT_CLASSNAME).click(on_arrow_next_click);
                $carousel.find(ARROW_PREV_CLASSNAME).off('click');      // disable default slick click event
                $carousel.find(ARROW_PREV_CLASSNAME).click(on_arrow_previous_click);

                setup_accessibility();
                enable_dragging();
                add_separators();
                select_initial_slide();
                if (settings_carousel_only == true){ unselect_all_slides(); }
                set_position_to_slide(get_selected_slide());
                center_slides();
                equalize_tabs_height();
                disable_arrow_tabbing();
                disable_hover_on_touch();
            }

            function setup_accessibility() {
                // accessibility for tabs carousel
                if (settings_carousel_only == false){
                    // add ARIA parameters
                    $carousel.attr('role', 'tablist');
                    $carousel.find(TABS_CLASSNAME).attr('role', 'tab');

                    // remove aria-live, which causes reading of extra un-needed information when the tabs are switched
                    $carousel.find('.slick-list').removeAttr('aria-live');

                    // associate tab with content
                    $carousel.find(TABS_CLASSNAME).each(function(index){
                        var id = uid+'_tab'+index;
                        //$(this).attr('aria-controls', id);
						$(this).attr({'aria-controls':id, 'aria-selected':false});
                        $($content.get(index)).attr('id', id);
                    });

                    $content.attr('role', 'tabpanel');
                    $content.each(function(){
                        var $first_element = get_content_first_element($(this));

                        //$first_element.attr('tabindex', 0);   // allow first element of content to be focusable
                        $first_element.blur(on_content_blur);     // add event to track when focus is lost
                        $first_element.mousedown(function(e){
                            $(this).css('outline', 'none');     // don't show outline when element is clicked
                        });
                    });
                }
                // accessibility for simple carousel
                else if (settings_carousel_only == true){
					$carousel.attr('role', 'presentation');
                    $carousel.find(TABS_CLASSNAME).attr('role', 'link');
                }

                //$carousel.find('.accessibility-instructions').attr('');

            }


            /**
             * Enable dragging, using the Dragdealer library
             */
            function enable_dragging() {
                //console.log("------enable_draggging-------");
                var drag_id = 'drag-id-' + uid;    // create a unique id for Dragdealer library to reference
                $carousel.children(SLICK_LIST_CLASSNAME).attr('id', drag_id);

                var drag_steps = get_num_of_slides() - get_slides_per_page() + 1;   // number of steps for the drag controller: this equals how many extra slides beyond the amount in one page, plus 1 as the first step is also counted.

                // init Dragdealer library
                drag_controller = new Dragdealer(drag_id, {
                    handleClass: 'slick-track',
                    loose: true,
                    dragStopCallback: on_drag_release,
                    vertical: false,
                    steps: drag_steps,
                    snap: false
                });

                // if the number of slides is less than the amount that can be shown at once
                // disable dragging so it's not possible to slide at all
                if (get_num_of_slides() <= get_slides_per_page()){
                    drag_controller.disable();
                }
            }

            function on_drag_release(x, y) {

                var currentSlide,
                    slidesPerPage = get_slides_per_page(),
                    numOfSlides = get_num_of_slides(),
                    widthOfSlide = get_slide_width(),
                //maxDistance = (numOfSlides - slidesPerPage) * widthOfSlide // total distance that the carousel can be dragged

                //console.log('drag release: ' + x + ' maxDistance: ' + maxDistance);

                // calculate what the current slide should be based on the dragged position
                    currentSlide = Math.round((numOfSlides - slidesPerPage) * x);
                if (currentSlide < 0) currentSlide = 0;
                //console.log('drag release, currentSlide=' + currentSlide + " numOfSlides=" + numOfSlides + " widthOfSlide=" + widthOfSlide);

                // update the current slide value in Slick
                $carousel.slick('setCurrentSlide', currentSlide);
                $carousel.slick('updateArrows');
            }

            function on_position_change() {
                //console.log("-------POSITION CHANGE-------");

                // check if carousel is taking 100% width, it does when the container is the same width as the carousel
                if ($container.outerWidth() == $carousel.outerWidth()){
                    // are there more slides than the maximum per view?
                    if (get_num_of_slides() > get_slides_per_page()) {
                        resize_slides_with_partial();// resize slides to show partially the next/prev tab when in mobile view
                    }
					$container.find('.accessibility-instructions').attr('aria-hidden',true);
                }else{
					$container.find('.accessibility-instructions').removeAttr('aria-hidden');
				}
                $carousel.find(TABS_CLASSNAME).attr('aria-hidden', 'false');   // make tabs always visible in accessibility, even if they are off-screen (this overrides Slick's behaviour)
                //$carousel.find(TABS_CLASSNAME).attr('aria-disabled', 'false');
                equalize_tabs_height();
            }

            function on_breakpoint_change() {
                // console.log("-------BREAKPOINT-------");

                enable_dragging();
                reset_slide_index();
                set_position_to_slide(get_selected_slide());
                center_slides();
                equalize_tabs_height();
                disable_arrow_tabbing();

                $carousel.find(SLICK_LIST_CLASSNAME).scroll(on_track_scroll);

                // override default arrow nav click events with custom ones
                $carousel.find(ARROW_NEXT_CLASSNAME).off('click');      // disable default slick click event
                $carousel.find(ARROW_NEXT_CLASSNAME).click(on_arrow_next_click);
                $carousel.find(ARROW_PREV_CLASSNAME).off('click');      // disable default slick click event
                $carousel.find(ARROW_PREV_CLASSNAME).click(on_arrow_previous_click);
            }

            /**
             * Resize the slides so that they fit in the carousel with partially showing the next slide
             */
            function resize_slides_with_partial() {
                var slide_width;
                var partial_amount = 0.5;    // % of the next slide to partially show (e.g. 0.25 = show 1/4)
                var total_track_width = 0;  // total width of the slide track (sum of all the slide outerwidths)

                slide_width = Math.round($carousel.outerWidth() / (get_slides_per_page() + partial_amount));
                $carousel.find(TABS_CLASSNAME).each(function() {
                    $(this).css('width', slide_width);
                    total_track_width += $(this).outerWidth(true);  // add up width of slide including border and margin
                });

                $carousel.find(".slick-track").width(total_track_width);    // update carousel track width to match new tabs width
                drag_controller.reflow();   // recalculate drag controller's width since carousel track width changed
            }

            /**
             * Equalize height of all tabs to the highest tab
             */
            function equalize_tabs_height() {
                var height = 0;

                // change height only if it is not defined as fixed
                if ($carousel.hasClass(FIXED_TAB_HEIGHT_CLASS) == false) {
                    // find highest height by measuring the content
                    $carousel.find(TABS_CLASSNAME).each(function(){
                        var $content = $(this).children();
                        if ($content.outerHeight() > height) height = $content.outerHeight();
                    });

                    $carousel.find(TABS_CLASSNAME).each(function(){
                        $(this).height(height);
                    });
                }

            }

            /**
             * Event when carousel track scrolls, caused by browser offsetting to make item visible (e.g. keyboard tabbing, VoiceOver selection)
             */
            function on_track_scroll(e) {
                // disable dragging controller, otherwise it will conflict with the scroll position, causing bugs
                drag_controller.disable();
            }

            function on_slide_mousedown(e){
                mouseDownPositionX = e.pageX;   // record initial mouse down value for later use
            }

            function on_slide_mouseup(e){
                var slide = e.currentTarget;

                // prevent inline onclick events from triggering, save them instead to trigger later
                // this is done to control when the inline onclick is triggered, for example when the carousel is dragged we don't want to trigger it
                if (slide.onclick){
                    $(slide).data('onclick', slide.onclick);    // save the onclick function into data
                    $(slide).removeAttr('onclick');             // remove the inline function
                }
            }

            function on_slide_keydown(e) {
                //console.log("on_slide_keydown:" + e.which);
                mouseDownPositionX = NaN;   // clear the mouseDownPositionX value so we know the click action is triggered by the keyboard, not the mouse

                // make sure dragging position is 0, otherwise keyboard focusing won't work
                //console.log(drag_controller.getValue()[0]);
                if (drag_controller.getValue()[0] != 0){
                    drag_controller.setValue(0, 0, true);
                }

                // check key
                switch(e.which){
                    // space or enter
                    case 32:
                    case 13:
                        e.preventDefault();
                        focus_on_content($(e.currentTarget).index(), false);

                        // check for inline onclick event and trigger it
                        if (e.currentTarget.onclick){
                            e.currentTarget.onclick.call(e.currentTarget, e || window.e);
                        }
                        // check for saved onclick event and trigger it
                        if ($(e.currentTarget).data('onclick')){
                            $(e.currentTarget).data('onclick').call(e.currentTarget, e || window.e);
                        }

                        break;
                    // right arrow
                    case 39:
                        if (settings_keyboard_navigation == true) { select_next_slide($(e.currentTarget), true); }
                        break;
                    // left arrow
                    case 37:
                        if (settings_keyboard_navigation == true) { select_prev_slide($(e.currentTarget), true); }
                        break;
                }
            }

            // when the slide loses focus
            function on_slide_blur(e) {
                $(e.currentTarget).css('outline','');   // reset the css outline to default
            }

            /**
             * When a slide gets focus and it is off-screen, position it so that it is in view
             */
            function on_slide_focus(e) {
                //var $slide = $(e.currentTarget);

                // if slide gets focused from keyboard or screenreader, flag this
                keyboard_screenreader_focus = true;

                //console.log('on_slide_focus');
            }

            function on_slide_click(e) {
                var scroll_to = false;  // scroll to carousel when tab is clicked?
                var slide = e.currentTarget;

                //console.log("on slide click, drag distance: " + Math.abs(mouseDownPositionX - e.pageX));

                // check if mouseDown was previously triggered by checking if there is a mouse down position value,
                // if instead a keyDown was triggered previously, then mouse down position should be NaN
                if (isNaN(mouseDownPositionX)){
                    select_slide($(slide));
                }
                // mouseDown was previously triggered
                else {
                    // determine if the carousel was dragged, if so, do not trigger click event, otherwise select tab
                    if (Math.abs(mouseDownPositionX - e.pageX) < DRAG_THRESHOLD) {

                        // has a tab been selected for the first time? If not, enable scrolling to the carousel
                        if (!first_tab_selected) {
                            scroll_to = true;
                            first_tab_selected = true;
                        }
                        // if detected the use of keyboard or screen reader to focus slide,
                        // don't do the set_position_to_slide functionality, this avoids a bug with VoiceOver selection shifting the position of the carousel
                        if (!keyboard_screenreader_focus){
                            set_position_to_slide($(slide), false);  // put slide into view if it's not already
                        }
                        if (settings_carousel_only == false) {
                            select_slide($(slide), false, scroll_to);   // *focusing on a slide that's partially outside the carousel view seems to cause a bug in IE (cannot drag afterwards), should avoid using focus for now
                        }
                        $(slide).css('outline','none');   // hide css outline since it's clicked with mouse
                        focus_on_content($(slide).index(), true);   // focus on content for screen readers

                        // check for saved onclick events and trigger them
                        if ($(slide).data('onclick')){
                            $(slide).data('onclick').call(slide, e || window.e);
                        }
                    }
                }
            }

            function on_arrow_next_click(e){
                goto_next_page();
            }

            function on_arrow_previous_click(e){
                goto_previous_page();
            }

            /**
             * Select the initial slide when the carousel first loads.
             * If there is a url anchor, then attempt to match the anchor value to a tab id and select it,
             * otherwise the default is the first tab.
             */
            function select_initial_slide(){
				var anchor = window.location.hash;
				var target_tab;
				
				if(typeof getUrlVars()["tdtab"] !== 'undefined' && getUrlVars()["tdtab"] != ""){
					anchor = getUrlVars()["tdtab"];
					
					var str = anchor,
						regex = new RegExp("Tab_");
						
					if(typeof parseInt(anchor) === 'number' && !regex.test(str)){
						anchor = 'Tab_'+anchor;
					}

				}else{
					anchor = decodeURIComponent(anchor);
					anchor = anchor.substring(1); // remove #

					// decode any html entities by creating a temporary text area and getting the value
					var temp_textarea = document.createElement("textarea");
					temp_textarea.innerHTML = anchor;
					anchor = temp_textarea.value;

				}
                select_slide($carousel.find(TABS_CLASSNAME).eq(0)); // initially select first tab

                // check for anchor in url
                if (anchor.length > 0){
                    // check if anchor matches an id in the tabs
                    $carousel.find(TABS_CLASSNAME).each(function(){
                        if($(this).attr('id') == anchor){
                            target_tab = $(this);
                        }
                    });

                    if (target_tab != undefined){
                        if (target_tab.length > 0){
                            select_slide(target_tab, false, true);
                            first_tab_selected = true;
                        }
                    }
                }
            }
            /**
             * getUrlVars - to get param from URL
			 */
			function getUrlVars() {
				var vars = {};
				var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
				function(m,key,value) {
					vars[key] = value;
				});
				return vars;
			}			
			
            /**
             * Select the tab/slide
             * @param $slide - the slide object
             * @param focus_slide - if set to true, place focus on slide otherwise slide is selected without focus
             * @param scroll_to - if set to true, scrolls the page to the carousel
             */
            function select_slide($slide, focus_slide, scroll_to) {
                unselect_all_slides();
                $slide.addClass('td-tabs-carousel-tab--on');
				$slide.attr('aria-selected',true);
                $slide.attr('tabindex', '0');

                if (focus_slide)  $slide.focus();   // focusing on a slide that's partially outside the carousel view seems to cause a bug in IE (cannot drag afterwards), should avoid using focus with mouse interaction. Focus is used for keyboard interaction only.

                if (scroll_to) {
                    $('html, body').animate({
                        scrollTop: $carousel.offset().top + SCROLL_TO_OFFSET
                    }, 1000);
                }
                show_content($slide.index());
            }

            function select_next_slide($slide, focus){
                var index = $slide.index();
                //console.log('next slide ' + index);
                if (index >= (get_num_of_slides()-1)){
                    index = 0;
                }
                else {
                    index++;
                }
                select_slide($($carousel.find(TABS_CLASSNAME).get(index)), focus);
            }

            function select_prev_slide($slide, focus){

                var index = $slide.index();
                if (index <= 0){
                    index = get_num_of_slides()-1;
                }
                else {
                    index--;
                }
                select_slide($($carousel.find(TABS_CLASSNAME).get(index)), focus);
            }

            /**
             * Position the carousel so that the given slide is visible, if it is off-screen
             */
            function set_position_to_slide($slide, snap_to_position) {
                var slide_index = $slide.index();
                var slides_per_page = get_slides_per_page();

                if (snap_to_position == undefined) snap_to_position = true;

                //console.log('set_position_to_slide index:' + slide_index + ' slides_per_page:' + slides_per_page);

                var drag_position = drag_controller.getStep()[0] - 1;   // get the drag position, -1 because steps starts from 1 not 0
                var new_position;

                // is slide outside the current page view, on the right?
                if (slide_index > ((slides_per_page - 1)  + drag_position)){
                    new_position = slide_index - (slides_per_page - 1);

                    // set new position in Slick carousel
                    $carousel.slick('slickGoTo', new_position, true);

                    // set new position in drag handler
                    drag_controller.setStep(new_position+1, 1, snap_to_position);    // +1 because drag controller steps starts from 1 not 0
                }
                // is slide outside the current page view, on the left?
                else if (slide_index < drag_position){
                    new_position = slide_index;

                    // set new position in Slick carousel
                    $carousel.slick('slickGoTo', new_position, true);

                    // set new position in drag handler
                    drag_controller.setStep(new_position+1, 1, snap_to_position);    // +1 because drag controller steps starts from 1 not 0
                }
            }

            function unselect_all_slides(){
                $carousel.find(TABS_CLASSNAME).each(function() {
                    var $_this = $(this);
                    $_this.removeClass('td-tabs-carousel-tab--on');
					$_this.attr('aria-selected',false);
                    $_this.attr('aria-hidden', 'false');   // make tabs always visible in accessibility, even if they are off-screen
                    if (settings_keyboard_navigation == true){
                        $_this.attr('tabindex', '-1');  // prevent keyboard tabbing, since tab navigation will be done using arrow keys
                    }
                    else {
                        $_this.attr('tabindex', '0');  // allow keyboard tabbing, if keyboard navigation with arrows is false
                    }
                });
            }

            // show content by index number
            function show_content(index){
                hide_all_content();
                var $current_content = $($content.get(index));

                $current_content.show();
                $current_content.attr('aria-hidden', 'false');
                g.ev.trigger(g.events.afterTabActiveChanged, [$current_content]);
				
				// Custom Event for Logic Tool (AngularJS)
				var event = document.createEvent('Event');
				event.initEvent(g.events.afterTabActiveChangedForExternal, true, true);
				document.dispatchEvent(event);
				
            }

            function hide_all_content() {
                $content.each(function() {
                    var $_this = $(this);
                    $_this.hide();
                    $_this.attr('aria-hidden', 'true');
                });
            }

            function on_content_blur(e) {
                //console.log('on_content_blur ');
                $(e.currentTarget).css('outline','');   // reset the css outline to default
            }

            /**
             * Set focus on first element of content for accessibility
             * After a tab is selected, this places the cursor in the corresponding tab content
             * Since this is for accessibility purpose, we don't want the page to actually scroll, so we reset the position
             * @index index of content
             */
            function focus_on_content(index, hide_outline) {
                var scroll_x = $(window).scrollTop();     // capture initial window position
                var $current_content = $($content.get(index));
                var $first_element = get_content_first_element($current_content);

                $first_element.focus();
                $(window).scrollTop(scroll_x);  // scroll back to initial window position, in case the focus changed it
                if (hide_outline) { $first_element.css('outline', 'none'); }
            }

            /**
             * Center the slides if it is necessary to do so
             */
            function center_slides() {
                if ($carousel.slick('slickGetOption', 'centerMode') == false) {
                    var maxSlides = get_slides_per_page(),
                        numOfSlides = get_num_of_slides(),
                        widthOfSlide = get_slide_width();

                    //console.log('center_slides(), current slide: ' + $carousel.slick('slickCurrentSlide'));

                    // if number of slides is less than or equal to the maximum allowed, center the slides in the container
                    if (numOfSlides <= maxSlides) {
                        var offset_x = ($carousel.innerWidth() - (widthOfSlide * numOfSlides)) / 2;
                        $carousel.find('.slick-track').css('padding-left', offset_x); //center
                        //console.log("center_slides: " + maxSlides + ", " + numOfSlides + ", " + widthOfSlide + " offset: " +  offset_x);
                    }
                }
            }

            /** reset the slide index to 0 **/
            function reset_slide_index() {
                // console.log("reset_slide_index currentSlide: " + $carousel.slick('slickCurrentSlide'));
                //$carousel.slick('slickSetOption', {'speed':0});   // set speed to 0 to remove animation
                $carousel.slick('setCurrentSlide', 0);
                //$carousel.slick('slickSetOption', 'speed', 500);   // set speed back to original value
                $carousel.slick('updateArrows');
                $carousel.find(SLICK_LIST_CLASSNAME).scrollLeft(0); // reset scroll position in case it got changed (i.e. keyboard control)
            }

            /**
             * Go to the next page of slides. E.g. if there are 10 slides total and 5 slides per page, this function will advanced to the 6th slide.
             */
            function goto_next_page(){
                var current_index = $carousel.slick('slickCurrentSlide');    // index of carousel's position (NOT the index of the currently selected slide)
                var max_index = get_num_of_slides() - get_slides_per_page(); // index of the first slide on the last page, we don't want the carousel to advance beyond this and leave blank spaces
                var goto_index = current_index + get_slides_per_page();      // new index is the current index plus the number of slides in a page, this makes it go to the next page

                // cap the maximum index value
                if (goto_index > max_index){ goto_index = max_index; }

                $carousel.slick('slickGoTo', goto_index);
            }

            /**
             * Go to the previous page of slides. E.g. if there are 10 slides total and 5 slides per page, this function will advanced to the 6th slide.
             */
            function goto_previous_page(){
                var current_index = $carousel.slick('slickCurrentSlide');    // index of carousel's position (NOT the index of the currently selected slide)
                var goto_index = current_index - get_slides_per_page();      // new index is the current index minus the number of slides in a page, this makes it go to the previous page

                // preventing going below the min index value
                if (goto_index < 0){ goto_index = 0; }

                $carousel.slick('slickGoTo', goto_index);
            }

            /**
             * remove nav arrows from keyboard tabbing, since it's possible to advance to next slide using keyboard arrows
             */
            function disable_arrow_tabbing() {
                //console.log('disable arrow tabbing');
                $carousel.find(ARROW_PREV_CLASSNAME).attr('tabindex', -1).attr('aria-disabled', 'true').attr('aria-hidden', 'true');
                $carousel.find(ARROW_NEXT_CLASSNAME).attr('tabindex', -1).attr('aria-disabled', 'true').attr('aria-hidden', 'true');
            }

            /**
             * disable hover effect on touch devices
             * UNFINISHED - IN PROGRESS
             */
            function disable_hover_on_touch() {

                $carousel.find(TABS_CLASSNAME).each(function() {
                    $(this).on("touchstart", function(e) {
                        //e.preventDefault();
                        // on_slide_mousedown(e);
                        // console.log('event type: ' + e.type);
                        // if (e.type == 'touchstart'){
                        //     e.preventDefault();
                        // }

                    })
                });
            }

            /** update the width of the carousel track to be relative to the number of items **/
            function update_carousel_track_width() {
                // calculate total width
                var total_width = 0;
                $carousel.find(TABS_CLASSNAME).each(function() {
                    total_width += $(this).outerWidth(true);
                });
                //console.log("set_carousel_width: " + total_width + " track_width: " + $carousel.find('.slick-track').width());
            }

            function add_separators() {
                $carousel.find(TABS_CLASSNAME).each(function() {
                    $(this).append("<div class='separator'></div>");
                });
            }		

            /** Getters **/
            function get_num_of_slides() {
                return $carousel.find(TABS_CLASSNAME).length;
            }

            function get_slides_per_page() {
                return $carousel.slick('slickGetOption', "slidesToShow");
            }

            function get_slide_width() {
                return $carousel.find(TABS_CLASSNAME).first().outerWidth(true);
            }

            function get_selected_slide() {
                return $carousel.find(TABS_CLASSNAME+"[aria-selected='true']");
            }

            function get_content_first_element($content) {
                return $content.find(CONTENT_CONTAINER_CLASSNAME).children().eq(0);
            }
        }

    // create unique id to keep track of each instance of the tabs-carousel
    if (!$.tdTabsCarouselModules) {
        $.tdTabsCarouselModules = {uid: 0};
    }

    // PLUGINS DEFINITION
    // ==================
    $.fn.tdTabsCarousel = g.getPluginDef(tabsCarousel, 'tdTabsCarousel');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdTabsCarousel.defaults = {

    };

    $(".td-tabs-carousel-container").tdTabsCarousel();
    //show content	
	$(".td_rq_tabs-carousel").css('opacity','1').slideDown(300);
    $(".td-product-service-icon-links").css('opacity','1').slideDown(300);
}(global));}],
td_rq_compare_table_sticky: [function (global) {/* Source: src/js/td-compare-table-sticky.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
* @namespace td-compare-table-sticky js
* @author Daniel Cho danielcho80@gmail.com
* @version 1.0
* @description TD Compare Table Sticky<br>
				- versoin 1.0:	- <br>
								- positioning sticky compare table<br>
								- mobile layout (only 2columns required on physical phone device)<br>
								- sticky buttons on mobile breakpoint<br>
								- interaction for Back-to-top component<br>
								- interaction for Compare Table component(thead)<br>
								- accessibility<br>
				- version 1.0.1: - Made certain cells hidden by 'td-hide-cell' in HTML	
								 - Added init_reset function to remove '.td-col-last-spacing' 
				- version 1.0.2: - Made sitcky buttons for mobile have equal height
							
*/
(function (window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),
		BREAKPOINT_SM_VALUE	= g.bpSet('sm'),	

        compareTableSticky = function(element, options) {
			var objFn={},fn_sticky={},fn_events={},fn_utils={},fn_ab={},
				$el = $(element),
				settings = $.extend($.fn.compareTableSticky.defaults, $el.metadata(), options),
				prod_type = settings.prod_type,				
				$top_nav = $('.td_rq_header-nav'),
				$compare_table = $('.td_rq_compare-table').find('thead'),
				$footer = $('footer'),
				$m_buttons = $('.td_rq_compare-table-sticky-mobile'),
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

			objFn.product_type = prod_type;
			objFn.el = $el;
			objFn.m_btns = $m_buttons;
			objFn.top_nav = $top_nav;
			objFn.footer = $footer;
			objFn.compareTable;
			objFn.activeElement;

			// Accessibility
			fn_ab = {
				show_sticky: function(){
					objFn.el.attr('aria-hidden','false');	
					objFn.el.attr('tabindex','0');//.focus();	
				},
				hide_sticky: function(){
					objFn.el.attr('aria-hidden','true');
					objFn.el.attr('tabindex','-1');					
				},
				show_sticky_btns: function(){
					objFn.m_btns.attr('aria-hidden','false');	
					objFn.m_btns.attr('tabindex','0');	
				},
				hide_sticky_btns: function(){
					objFn.m_btns.attr('aria-hidden','true');
					objFn.m_btns.attr('tabindex','-1');					
				}				
			};
			
			// Functions			
			fn_sticky = {
				get_footer_pos: function(){
					var pos = objFn.footer.offset().top;
					return pos;
				},
				get_compareTable_pos: function(){
					var $obj,
						pos;
					if(typeof objFn.compareTable === 'undefined'){
						$obj = $compare_table;
					}else{
						$obj = objFn.compareTable;
					}
					pos = $obj.offset().top+$obj.height()-$top_nav.height();

					return pos;
				},
				btt_defaut_bottom: 30,
				status: false,
				init_scroll: false,
				init: function(){

					fn_sticky.init_reset(fn_sticky.init_load_default_layout);
					
					$window.scroll(function(){
						//alert(fn_sticky.get_compareTable_pos());
						if ($window.scrollTop() > fn_sticky.get_compareTable_pos()){
							fn_sticky.status = true;
							
							objFn.el.removeClass('deactivate').addClass('activate').promise().done(function(){
								fn_ab.show_sticky();
								if(fn_sticky.init_scroll == false){
									fn_utils.equalHeight('.slot > a',true);
									fn_sticky.init_scroll = true;
								}
							});
				
							if(window.innerWidth < BREAKPOINT_SM_VALUE){ // sticky botton interaction
								//document.documentElement.scrollTop || document.body.scrollTop
								if(($(document).scrollTop()+objFn.footer.height()) > fn_sticky.get_footer_pos()){ 
									objFn.m_btns.addClass('deactivate').removeClass('activate');
									fn_ab.hide_sticky_btns();
								}else{
									objFn.m_btns.removeClass('deactivate').addClass('activate');
									fn_ab.show_sticky_btns();
									var equal_button_height = g.getEqualHeight($m_buttons.find('button'));
									$m_buttons.find('button').css('height',equal_button_height+'px');	
								}
							}
							
						} else {
							objFn.el.addClass('deactivate').removeClass('activate').promise().done(function(){
								fn_ab.hide_sticky();
								fn_sticky.status = false;
							});	
							
							// sticky 'apply now' button on mobile
							if(window.innerWidth < BREAKPOINT_SM_VALUE){
								objFn.m_btns.addClass('deactivate').removeClass('activate');
								fn_ab.hide_sticky_btns();
							}							
						}
					});
					$window.scroll();				
				},
				init_reset: function( callback ){
					objFn.el.find('td').removeClass('td-col-last-spacing');	
					
					callback(fn_sticky.init_default);
				},				
				init_load_default_layout: function(callback){
					objFn.el.find('td:not(.td-hide-cell)').each(function(idx){
						if(idx == 3){
							$(this).addClass('td-col-last-spacing');	
						}						
					});				
					
					callback.call();
				},
				init_default: function(){
					if(mobile && (window.innerWidth < BREAKPOINT_SM_VALUE)){
						fn_sticky.load_phone_layout(objFn.pos); // refresh layout for physical phone device
					}else{
						objFn.pos();
					}

					fn_events.ev_afterCompareTable(); // interaction with compare-table
					fn_events.ev_afterBackToTop(); // interaction with back-to-top icon

					$window.resize(function(){
						objFn.pos();
						if(fn_sticky.status == true) fn_utils.equalHeight('.slot > a');
						if(fn_sticky.status == false) fn_sticky.init_scroll = false;
						//fn_utils.m_btns_status();
						fn_sticky.pos_btt();

					});						
				},				
				pos:function(){
					objFn.el.css('top',$top_nav.height()+'px');
					fn_utils.m_btns_status();
				},
				pos_btt: function(){
					if(typeof objFn.td_rq_linkToTop !== 'undefined'){
						if(window.innerWidth < BREAKPOINT_SM_VALUE){
							objFn.td_rq_linkToTop.css('bottom',objFn.m_btns.height()+10+'px');
						}else{
							objFn.td_rq_linkToTop.css('bottom', fn_sticky.btt_defaut_bottom+'px');
						}
					}
				},
				load_phone_layout: function(cb){	
					objFn.el.find('td:not(.td-hide-cell)').each(function(idx){
						if(idx == 1 || idx == 2){
							$(this).addClass('mobile');
						}
						/* first approach
							if(idx == objFn.el.find('td').length-1) $(this).remove();
						*/
						/* second approach
						if(idx != 0 && idx != 1 && idx != 2){
							$(this).remove();
						}	
						*/
						if(idx != 0 && idx != 1 && idx != 2){
							$(this).addClass('td-hide-cell');
						}	
					});
					objFn.m_btns.find('td:not(.td-hide-cell)').each(function(idx){
						if(idx == 0 || idx == 1){
							$(this).addClass('mobile');
						}
						/* first approach 
						if(idx == objFn.m_btns.find('td').length-1) $(this).remove();
						*/
						/*
						if(idx != 0 && idx != 1){
							$(this).remove();
						}	
						*/
						if(idx != 0 && idx != 1){
							$(this).addClass('td-hide-cell');
						}							
					});
					cb.call();
				}					
			};
			
			// Events
			fn_events = {
				ev_afterBackToTop: function(){
					// wait until back-to-top link gets triggered.
					g.ev.on(g.events.afterBackToTop, function (event, el_btt) {
						objFn.td_rq_linkToTop = $(el_btt);

						fn_sticky.pos_btt();
					
					});					
				},
				ev_afterCompareTable: function(){
					g.ev.on(g.events.afterCompareTable, function (event, el) {
						objFn.compareTable = $(el);

					});	
				}
			};
			
			// Utilities			
			fn_utils = {			
				equalHeight: function(qstr,ani){
					if(typeof ani === 'undefined' || ani != true ) ani = false;
					var $elements = objFn.el.find(qstr),
						height = g.getEqualHeight($elements);
					(ani == true) ? $elements.animate({'height': height}, 300) : $elements.css('height',height+'px');
				},
				m_btns_status: function(){
					if(window.innerWidth < BREAKPOINT_SM_VALUE){
						if ($window.scrollTop() > fn_sticky.get_compareTable_pos()){
							if(($(document).scrollTop()+objFn.footer.height()) > fn_sticky.get_footer_pos()){
								objFn.addClass('deactivate').removeClass('activate');
								fn_ab.hide_sticky_btns();
							}else{
								objFn.m_btns.removeClass('deactivate').addClass('activate');
								fn_ab.show_sticky_btns();
								
								var equal_button_height = g.getEqualHeight($m_buttons.find('button'));
								$m_buttons.find('button').css('height',equal_button_height+'px');								
							} 
						}
					}else{
						objFn.m_btns.addClass('deactivate').removeClass('activate');
						fn_ab.hide_sticky_btns();
					}
				}
			};
			
			$.extend(objFn,fn_sticky,fn_events,fn_utils,fn_ab);
						
			objFn.init();
					
			return objFn;
		
        };

    // PLUGINS DEFINITION
    // ==================
    $.fn.compareTableSticky = g.getPluginDef(compareTableSticky, 'compareTableSticky');
    $.fn.compareTableSticky.defaults = {
		prod_type:'cc' // creditcard: cc/bankaccount: ba
    };
	
    $(".td_rq_compare-table-sticky").compareTableSticky();


}(global));}],
td_rq_compare_table: [function (global) {/* Source: src/js/td-compare-table.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
* @namespace td-compare-table js
* @author Daniel Cho danielcho80@gmail.com
* @version 1.0
* @description TD Compare Table<br>
				- versoin 1.0:	- cell alighnment for text across all breakpoints<br>
								- mobile layout (only 2columns required on physical phone device)<br>
								- trigger event (sending the thead object when table loads)<br>
								- accessibility<br>
								- removed 'apply now' on mobile view
								- custom 'rating star'
				- version 1.0.1: - Made certain cells hidden by 'td-hide-cell' in HTML
								 - Added init_reset function to remove '.td-col-last-spacing' 
								 - Added alignment_th to have equalHeight for th tag.
			    - version 1.0.2: - Made the table inside cell doesn't get affected
								 - Added the defaut_body_cell_height to have default fixed height of cell.
						
				- version 1.1.0: - Added 'alignment_th_in_nested_table' function to make cell have nested table. (Built for US Public Site)
*/
(function (window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),	
		BREAKPOINT_SM_VALUE	= g.bpSet('sm'),	

        compareTable = function(element, options) {
			var objFn={},fn_table={},fn_events={},fn_utils={},fn_ab={},
				$el = $(element),
				settings = $.extend($.fn.compareTable.defaults, $el.metadata(), options),
				prod_type = settings.prod_type,	
				$thead = $el.find('> div > table > thead > tr'),
				$tbody = $el.find('> div > table > tbody > tr'),
				nested_table_th = '> td.no-padding table.nested-table > thead > tr > th',
				$nested_table_th = $tbody.find(nested_table_th),
				//timer,
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())),
				defaut_body_cell_height ="74px";

			objFn.product_type = prod_type;
			objFn.el = $el;
			objFn.thead = $thead;
			objFn.tbody = $tbody;

			// Accessibility
			fn_ab = {
				add_col: function(){
					objFn.thead.find('> th').attr('scope','col');
				},				
				add_row: function(){
					objFn.tbody.find('> th').attr('scope','row');
				},
				add_describedby: function($obj,id){
					var $target = $obj.find('div').children('button');
					if($target.get(0)) $target.attr('aria-describedby',id);
				}
			};
			
			// Functions			
			fn_table = {
				init: function(){				
					
					//fn_table.init_load_default_layout(fn_table.init_default);
					fn_table.init_reset( fn_table.init_load_default_layout );
							
					/*
					objFn.thead.on('focus',function(){
						$('span.td-indicator-offer').attr('aria-hidden','false');
					});
					objFn.tbody.on('focus',function(){
						$('span.td-indicator-offer').attr('aria-hidden','true');
					});
					*/
					
				},
				init_reset: function( callback ){
					
					objFn.thead.find('> th').removeClass('td-col-last-spacing');	
					
					objFn.tbody.each(function(){
						var $_this = $(this);
						$_this.find('> td').removeClass('td-col-last-spacing');	
					});						
					
					callback(fn_table.init_default);
				},
				init_load_default_layout: function(callback){

					objFn.thead.find('> th:not(.td-hide-cell)').each(function(idx){

						if(idx == 3){
							$(this).addClass('td-col-last-spacing');	
						}
					});
					
					objFn.tbody.each(function(){
						var $_this = $(this);
						$_this.find('> td:not(.td-hide-cell)').each(function(idx){
							if(idx == 2){
								$(this).addClass('td-col-last-spacing');								
							}
						});
					});		

					callback();
				},
				init_default: function(){

					if(mobile && (window.innerWidth < BREAKPOINT_SM_VALUE)){ // refresh layout for physical phone device
						fn_table.load_phone_layout(fn_table.load_required_fn); 
						// for 320 width on portrait
						// if(window.innerWidth <= 320) $('div.td-star-ratings .td-comments').css('right','-15px');
					}else{
						fn_table.load_required_fn();
					}
					
					(!mobile && (window.innerWidth < BREAKPOINT_SM_VALUE)) ? fn_table.rating_star_mobile_custom() : fn_table.rating_star_mobile_custom_reset();
					
					$window.resize(function(){
						fn_table.load_required_fn();
					
						(!mobile && (window.innerWidth < BREAKPOINT_SM_VALUE)) ? fn_table.rating_star_mobile_custom() : fn_table.rating_star_mobile_custom_reset();
						//clearTimeout(timer);
						//timer = setTimeout( fn_table.load_required_fn , 1000 );
						
					});						
				},
				alignment: function(){
					
					objFn.thead.each(function(){
						var $_this = $(this);

						$_this.find('> th').each(function(){	
							var $_this_nested = $(this);
							fn_ab.add_describedby($_this_nested,$_this_nested.find('div > span:first-child').children('a').attr('id'));
						});
						// fn_utils.equalHeight($_this,'th > div > span:first-child');
						
						//fn_utils.equalHeight($_this,'th > div.td-indicator-wrapper');
						
						// offer indicator
						if($_this.find('th').find('.td-indicator-offer').is(':visible')){
							$_this.find('th').children('.td-indicator-wrapper').css('height','70px');
						}
						fn_utils.equalHeight($_this,'th > div:not(:has( > span.td-indicator-offer:first-child))  > span:first-child');
						
						
						/*
						if(window.innerWidth < BREAKPOINT_SM_VALUE){
							fn_utils.equalHeight($_this,'th');
						}	
						*/
					});

					objFn.tbody.each(function(){
						var $_this = $(this);
						if(window.innerWidth < BREAKPOINT_SM_VALUE){
							$_this.find('> td > span:not(:has(ul))').addClass('align');
							$_this.find('> td > span:has(ul)').removeClass('align');

							fn_utils.equalHeight($_this,'> td');
						}else{
							$_this.find('> td').css('height',defaut_body_cell_height);
							$_this.find('> td > span').removeClass('align');
							$_this.find('> td:has(span:has(ul))').css({'vertical-align':'top','height':'auto'});
						}
					});
					
				},
				alignment_th_in_nested_table: function(callback){
					if($nested_table_th.get(0)){
						objFn.tbody.each(function(){
							var $_this = $(this);
								
								$_this.find(nested_table_th).each(function(index){
									
									var $__this = $(this),
										temp_text = '',
										lastIndexStr = ' /';
									if(index == 0 || (index % 2 == 0)){
										if(window.innerWidth < BREAKPOINT_SM_VALUE){
											if($__this.text().indexOf(lastIndexStr) != -1){
												temp_text = $__this.text().substring(0, $__this.text().lastIndexOf(lastIndexStr));
												temp_text += lastIndexStr;
											}else{
												temp_text = $__this.text() + lastIndexStr;
											}
											$__this.text(temp_text);
										}else{
											if($__this.text().indexOf(lastIndexStr) != -1){
												temp_text = $__this.text().substring(0, $__this.text().lastIndexOf(lastIndexStr));
												$__this.text(temp_text);
											}
										}
										
										
										
									}
									
								});
								
							fn_utils.equalHeight($_this,nested_table_th+':last-child'); 
						});
					}
					callback();
				},
				alignment_th: function(){
					if(window.innerWidth < BREAKPOINT_SM_VALUE){
						objFn.thead.each(function(){
							var $_this = $(this);
							fn_utils.equalHeight($_this,'> th');
						});
					}else{
						objFn.thead.each(function(){
							var $_this = $(this);
							$_this.find('> th').css('height','auto');
						});						
					}
					
					$window.load(function(){
						if(window.innerWidth < BREAKPOINT_SM_VALUE){
							objFn.thead.each(function(){
								var $_this = $(this);
								fn_utils.equalHeight($_this,'> th');
							});
						}else{
							objFn.thead.each(function(){
								var $_this = $(this);
								$_this.find('> th').css('height','auto');
							});						
						}						
					});
				},
				load_phone_layout: function(cb){
					objFn.thead.find('> th:not(.td-hide-cell)').each(function(idx){
						if(idx == 1 || idx == 2){
							$(this).addClass('mobile');
						}
						/* Initial approach
						if(idx == objFn.thead.find('th').length-1) $(this).remove();
						*/
						
						/* second approach
						if(idx != 0 && idx != 1 && idx != 2){
							$(this).remove();
						}
						*/
						
						if(idx != 0 && idx != 1 && idx != 2){
							$(this).addClass('td-hide-cell');
						}						
					});					
				
					objFn.tbody.each(function(){
						var $_this = $(this);
						$_this.find('> td:not(.td-hide-cell)').each(function(idx){
							if(idx == 0 || idx == 1){
								$(this).addClass('mobile');
							}
							/* Initial approach
								if(idx == $_this.find('td').length-1) $(this).remove();
							*/
							/* second approach
							if(idx != 0 && idx != 1){
								$(this).remove();
							}	
							*/
							if(idx != 0 && idx != 1){
								$(this).addClass('td-hide-cell');
							}
							
						});
					});		
					cb.call();
					
				},
				remove_btn_applynow: function(callback){
					if(window.innerWidth < BREAKPOINT_SM_VALUE){
						objFn.thead.find('button').hide();
					}else{
						objFn.thead.find('button').show();
					}
					callback();
				},
				rating_star_mobile_custom: function(){
					$('div.td-star-ratings, div.td-stars, div.td-stars-clip, .td-comments').addClass('mobile');
				},
				rating_star_mobile_custom_reset: function(){
					$('div.td-star-ratings, div.td-stars, div.td-stars-clip, .td-comments').removeClass('mobile');
				},				
				load_required_fn: function(){
					fn_events.trigger();
					
					fn_table.alignment_th_in_nested_table( fn_table.alignment );
					//fn_table.alignment();
					
					fn_ab.add_col();
					fn_ab.add_row();
					
					fn_table.remove_btn_applynow(fn_table.alignment_th);	
				}
			};
			
			// Events
			fn_events = {
				trigger: function(){
					g.ev.trigger(g.events.afterCompareTable,objFn.el.find('thead'));	
				}
			};
			
			// Utilities			
			fn_utils = {			
				equalHeight: function(obj,qstr,ani){
					if(typeof ani === 'undefined' || ani != true ) ani = false;
					var $elements = obj.find(qstr),
						height = g.getEqualHeight($elements);
					(ani == true) ? $elements.animate({'height': height}, 300) : $elements.css('height',height+'px');
				}
			};
			
			$.extend(objFn,fn_table,fn_events,fn_utils,fn_ab);
						
			objFn.init();
					
			return objFn;
		
        };

    // PLUGINS DEFINITION
    // ==================
    $.fn.compareTable = g.getPluginDef(compareTable, 'compareTable');
    $.fn.compareTable.defaults = {
		prod_type:'cc', // creditcard: cc/bankaccount: ba
		placeholder_image:'' // placeholder image path
    };
	
    $(".td_rq_compare-table").compareTable();


}(global));}],
td_rq_blocks: [function (global) {/* Source: src/js/td-blocks.js */
"use strict";
/**
 * TD Grid Blocks
 * Created by david.lu@td.com
 * @version 1.0
 *
 * Grid blocks component
 * Updated by daniel.cho@td.com
 * @version 1.1
 * Accesssibility fixes 
	- fourColumnGridBlocks: changing the text(close) of the close button on overlay to 'More details' on mobile view.
 * @version 1.1.2
 * Added sc_equalHeight to grid-hover for both four and three columns
 * Fixed to made the focus go to the column where it previouly comes from when hit enter close button in green overlay.
 * Accesssibility fixes
	- fourColumnGridBlocks: 
		- changed accordions on mobile view adding role='link', aria-expaned='true/false', and tabindex='0'
		- added aria-hidden='true' to +/- icon and removed tabindex='0'
		- added outline style when tabbing begins
		- added role='button' to close button in overlay in TP,TL, and Desktop
		- Made all changes listed below
			1.	The images should have null alt text, alt="".
			2.	The arrow that expands to the full width of content needs the following attributes, tabindex="0", role="link" and aria-expanded="false/true". ="false" is the default closed state, ="true" is the open expanded sate (which will probably never be read since it becomes hidden when the grid block expands). It also needs the text changes, right now it says "close". Should be something like, "learn more" or "read more".
			3.	The "X" that closed the full width grid block requires, aria-expanded="true", toggle to ="false" when its closed, (though this probably won't be read either since when its closed its not visible).
			4.	The content itself that appears with mouse hover has aria-hidden="true" change this to ="false". That way the content can be read when navigating with the arrow keys. And then the button that expands it to full screen can be activated.
			5. I'm also seeing/hearing that the other grid block items can still be read when any of them are full width expanded, these other grid blocks cannot be activated 	though. They should be completely hidden when one of the other grid blocks is full width.
			6. I think an additional "close" button is required at the end of the content, it can be hidden offscreen but it should close the full width grid block

	- threeColumnGridBlocks:
		- added unique id to h3 tag for the circle icon (aria-describedby)
		- added 'aria-describedby' attribute and 'more information' text to 'td-forscreenreader' of the circle icon in hover state
		- Made all changes listed below  
			1. add, role="complementary" to the each of the <div>'s that make up the individual grid block items.
			2. remove role="link", tabindex="0" and aria-expanded from the text "Compare estimates for different vehicles"
			3. set aria-hidden="true" for the content that is shown during mouse hover. But ALWAYS keep it as ="true".
		- Made hover state have aria-hidden="false" all the time
		- Removed role="complementary"
		
	- GridBlocksFlexbox:
		1.	The <div> that is displayed on mouseover needs to have the tabindex either removed or set to ="-1".  
		2.	Remove the attribute role="button" from the <div> that wraps around the whole grid block item. 
		3.	Arrow Key - Change the aria-hidden attribute to ="false" when the mousehover affect is NOT shown. And set it to ="true" when IT IS shown. 
			Tabbing - Change the aria-hidden attribute to ="false". No matter hover state is on/off. When the large green overlay is activated, change the aria-hidden attribute to ="true"
		4.	Add, role="button" and aria-expanded="true  / false" to the "Learn more" button. Toggle the true/false depending on the open or closed state of the full width grid block item.
		5.	The group of Grid block items that are hidden when one is in full width mode requires aria-hidden="true". 
		6.	Close button. Requires to be WITHIN the <span> with a tabindex. Also add the following attributes, aria-expanded="true" and role="button".  

		Expected Interaction - Tabbing
		1.	Tabbing to grid block item activates mouse hover effect.
			a.	Screenreader will read entire contents of grid block item.
		2.	Pressing tab again when mouse hover effect is active moves focus to the "Learn more" arrow.
			a.	Screenreader will read "Learn more button collapsed"
		3.	Pressing Enter or Spacebar activates "Learn more" and expands grid block item to full width.
		4.	Pressing tab when full width grid block is open should move through the links/buttons within the full width grid block item.
			a.	Close button
				i.	Screenreader will read button as "Close, button, expanded" 
			b.	Learn more link
			c.	Close button (hidden offscreen)
				i.	Screenreader will read button as "Close, button, expanded" 

		Expected Interaction - Arrow Keys
		1.	Navigating with arrow keys will read the grid block items including the mouse hover effect content.
			a.	Mouse hover effect will NOT happen when using arrow keys.
			b.	Screenreader will read the content as-if the hover effect has been activated.
			c.	Screenreader will read "Learn more button collapsed"
		2.	Pressing Enter or Spacebar activates "Learn more" and expands grid block item to full width.
		3.	Navigating arrow keys navigates through the content within the full width grid block item
			a.	Close button
				i.	Screenreader will read button as "Close, button, expanded" 
			b.	Learn more link
			c.	Close button (hidden offscreen)
				i.	Screenreader will read button as "Close, button, expanded" 
		4.	Navigating to content above or below full width grid block item will NOT read any content other than what is visually on screen.
		
		Fixed:
			1. Removed tabindex='0' from 'grid-block grid-block-half'
			2. Added tabindex='0', role='button', and aria-expanded='false' to 'grid-over-title'


 * TP(target premium) fix			
	- Added unload_events function not to have duplicate events 
 *
 * Updated by daniel.cho@td.com
 * @version 1.2
 * - Updated the sequence of loading JS function and added its listener to threeColumnGridBlocks in order to support Target Premium.
 * @version 1.2.1
 * - Fixed moving focus to correct elements for GridBlocksFlexbox
 */
 
(function (window) {
    var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        body = window.body,
        $doc = $(document),
        breakpoint = g.bpSet('sm') - 1,
        nexus10 = (/Nexus 10 Build/i.test(navigator.userAgent.toLowerCase())),
		//objSlick, 

        threeColumnGridBlocks = function (element, options) {
            var $container = $(element).parent(),// use the container to reference this instance of the grid block, this is to prevent accidentally applying code (e.g. events) to all the grid blocks on a page if there are more than one
                $slick_content = $container.find('.slick-content'),
				THREE_COLUMN_GRID = '3-column-grid',
                $three_column_grid = $container.find('.'+THREE_COLUMN_GRID),
                GRID_HOVER = ".grid-hover",
                $grid_hover = $container.find(GRID_HOVER),
				objSlick;//,
				//hover_status = true;

			unload_events(main_events);
			
			function main_events(){

				if($three_column_grid.attr("role") == "button"){
					$three_column_grid.css("cursor","pointer");
				}

				objSlick = $slick_content.slick({
					arrows:false,
					dots: true,
					infinite: false,
					accessibility: false,
				});
	
				//({}).toString.call(currentSlide).match(/\s([a-zA-Z]+)/)[1].toLowerCase();

				$three_column_grid.each(function(){
					var $_this =  $(this);
					if(!$_this.attr("role")){
						//$_this.attr("role","complementary");
						//$_this.find(GRID_HOVER).attr('aria-hidden',true);
						$_this.find(GRID_HOVER).attr('aria-hidden',false);
					}
					
					// content on hover state
					var $obj_hover = $_this.find(GRID_HOVER);
					if($obj_hover.get(0)){
						var generated_id = "grid_block_hover_title_id_"+Math.floor((Math.random() * 1000) + 1);
						$obj_hover.find('h3').attr('id',generated_id);
						$obj_hover.find('a span.td-forscreenreader').attr({'aria-describedby':generated_id,'aria-hidden':true}).text('More information');					
					}
				});
				
				if (window.innerWidth >= breakpoint) {
					$slick_content.slick('unslick');
				}
				$(window).on('resize', function(){
					var $slicker = objSlick;//$slick_content;
					var is_video_fullscreen = false;    // indicates whether the resize event is caused by a video going fullscreen

					// check if there is a video that's fullscreen in the grid blocks carousel
					$slicker.find('.video-js').each(function(){
						var video_js_player = this.player;
						is_video_fullscreen = video_js_player.isFullscreen();
					});

					// only perform change to slick carousel if resize was not caused by a video going fullscreen
					if (!is_video_fullscreen){
						if (window.innerWidth >= breakpoint) {
							$slicker.slick("unslick");
						} else {
							if(!$slicker.hasClass('slick-initialized')) {
								$slicker.slick("unslick");
								$slicker.slick({
									arrows: false,
									dots: true,
									infinite: false
								});
							}
						}
					}

					sc_equalHeight($container.find('.grid-over'));
				});
				if(android === false && iOS === false) { //check touch device
					/* delay hover */

					$three_column_grid.hover(function (e) {
						//if(hover_status == true){
							e.preventDefault();
							$(".grid_over").fadeIn();
							hoverIn($(this), THREE_COLUMN_GRID);
							$(this).find('a span.td-forscreenreader').attr('aria-hidden',false);
						//}
					}, function (e) {
						//if(hover_status == true){
							e.preventDefault();
							hoverOut($(this), THREE_COLUMN_GRID);
							$(this).find('a span.td-forscreenreader').attr('aria-hidden',true);
						//}
					})

					$three_column_grid.focus(function (e) {
						
						var $_this = $(this);
						if (!$_this.children(GRID_HOVER).is(':visible')) {
	/* removed due to accessibility
							$_this.children(GRID_HOVER).children("div").children("h3").attr("tabindex","-1");
							$_this.children(GRID_HOVER).children("div").children("h3").blur();
							$_this.children(GRID_HOVER).children("div").children("p").attr("tabindex","-1");
							$_this.children(GRID_HOVER).children("div").children("p").blur();

							$_this.children(GRID_HOVER).children("div").children("h3").attr("tabindex","0");
							$_this.children(GRID_HOVER).children("div").children("h3").focus();
							$_this.children(GRID_HOVER).children("div").children("p").attr("tabindex","0");
							$_this.children(GRID_HOVER).children("div").children("p").focus();
	*/

							$(".grid-hover").fadeOut();
							$(".grid-over").fadeIn();
							hoverIn($_this, THREE_COLUMN_GRID);
						} else {
							return false;
						}
					});
	/*				
					$three_column_grid.find('.grid-over').on('keydown', function(e){
						if (e.which == 13){ // Enter key
							hover_status = false;
							var $_this = $(this);	

							$three_column_grid.find('.grid-hover').css('display','none');
							$_this.next('.grid-hover').css('display','block');
						}else{
							hover_status = true;
						}
					});
	*/				
					
					
				}
				if(('ontouchstart' in window)) {
					$three_column_grid.on("click", function (e) {
						var $link = $(this).attr("data-target");
						if ($link) {
							window.location.href = $link;
						}
						else {
							return true;
						}
					})
				}
				$three_column_grid.on("focus",focus_video);
				sc_equalHeight($container.find('.grid-over'));
			}
			function unload_events(callback){	

				if(typeof objSlick === 'object'){
					$slick_content.slick('unslick');
				}
				$three_column_grid.off("hover");
				$three_column_grid.off("focus");
				$three_column_grid.off('click');	
				
				callback();
									
			}			
        },



        fourColumnGridBlocks = function (element, options) {
            var $container = $(element).parent(),// use the container to reference this instance of the grid block, this is to prevent accidentally applying code (e.g. events) to all the grid blocks on a page if there are more than one
                GRID_EXPAND_TOGGLE = "grid-expand-toggle",
                $td_four_column_grid_title = $container.find(".td-4-column-grid-title"),
                TD_ICON_CLOSE = ".td-icon-close",
                $td_icon_close = $container.find(TD_ICON_CLOSE),
                TD_TRIGGERICON = "td-triggericon",
                TD_TRIGGERICON_OPEN = "td-triggericon-open",
                CUSTOM_FOURCOLGRID = "custom-fourcolgrid",
                TD_ICON_WRAPPER_GREEN = "td-icon-wrapper-green",
                OVERLAY_CLOSE = ".overlay-close",
                $overlay_close = $container.find(OVERLAY_CLOSE),
                TD_FOUR_COLUMN_GRID_CONTENT = ".td-4-column-grid-content",
                $td_four_column_grid_content = $container.find(TD_FOUR_COLUMN_GRID_CONTENT),
				TD_FORSCREENREADER = '.td-forscreenreader',
                $td_forscreenreader = $container.find(TD_FORSCREENREADER),
                FOUR_COLUMN_GRID = "4-column-grid",
                $four_column_grid = $container.find('.'+FOUR_COLUMN_GRID),
                GRID_HOVER = ".grid-hover",
                $grid_hover = $container.find(GRID_HOVER),
                GRID_OVER = ".grid-over",
                $grid_over = $container.find(GRID_OVER),
                BLOCK_SELECTED_NEXT = "block-selected-next",
                $block_selected_next = $container.find("."+BLOCK_SELECTED_NEXT),
				GRID_BLOCK_OVERLAY = ".grid-block-overlay",
				$grid_block_overlay =  $container.find(GRID_BLOCK_OVERLAY),
				tmp_overlay_close_status;

            if($four_column_grid.attr("role") == "button"){
                $four_column_grid.css("cursor","pointer");
            }

            if (window.innerWidth < breakpoint) {
                $td_four_column_grid_title.addClass(GRID_EXPAND_TOGGLE);
                $td_icon_close.addClass(TD_TRIGGERICON+" "+CUSTOM_FOURCOLGRID);
                $overlay_close.addClass(TD_ICON_WRAPPER_GREEN);
				$overlay_close.removeAttr('aria-expanded');
				$td_forscreenreader.html("More details");
				$grid_block_overlay.find(OVERLAY_CLOSE).removeAttr('role tabindex').attr({'aria-hidden':true});
				$grid_block_overlay.find('> div').attr({'role':'link','aria-expanded':false,'tabindex':0}).css({'cursor':'default','outline-width': '0px'});
				$grid_block_overlay.find('.'+GRID_EXPAND_TOGGLE+' > span').attr('aria-hidden',true);
				
            }else{
				//$td_forscreenreader.html("close");
				$four_column_grid.find(TD_FORSCREENREADER).html("Learn more");
				$grid_block_overlay.find(TD_FORSCREENREADER).html("Close");
				$grid_block_overlay.find(OVERLAY_CLOSE).attr({'tabindex':0,'role':'button'}).removeAttr('aria-hidden');
				$grid_block_overlay.find('> div').removeAttr('role aria-expanded tabindex');
				$grid_block_overlay.find('.'+GRID_EXPAND_TOGGLE+' > span').attr('aria-hidden',true);
				sc_equalHeight($container.find('.grid-over'));
				$overlay_close.attr('aria-expanded',false);
				tmp_overlay_close_status = false;
				
			}
            $container.on("click keydown",GRID_BLOCK_OVERLAY, function(event) {
				if(event.type == 'click' || (event.type == 'keydown' && event.which === 13) ){
					var $_this = $(this).find("."+GRID_EXPAND_TOGGLE);
				   
				   $_this.next(TD_FOUR_COLUMN_GRID_CONTENT).slideToggle();
					var $icon = $_this.children().eq(2).children().children(TD_ICON_CLOSE);
					var $iconWrapper = $_this.children().eq(2).children(OVERLAY_CLOSE);
					
					if($icon.hasClass(TD_TRIGGERICON_OPEN)){
						$icon.removeClass(TD_TRIGGERICON_OPEN);
						$(this).find('> div').attr('aria-expanded',false);
						
						//$grid_block_overlay.find('> div').attr('aria-expanded',false);
						//$iconWrapper.removeClass("td-icon-wrapper-white");
						//$td_forscreenreader.html("");
					} else {
						$icon.addClass(TD_TRIGGERICON_OPEN);
						$iconWrapper.addClass(TD_ICON_WRAPPER_GREEN);
						$(this).find('> div').attr('aria-expanded',true);
						
						//$grid_block_overlay.find('> div').attr('aria-expanded',true);
						//$td_forscreenreader.html("close");
					}
				}
            });
			$('body').on('focus blur keydown', function(e){
				if (event.which === 9){ // Tab key		
					$grid_block_overlay.find('> div').css({
						'outline-width': '1px'
					});
				}
				if (event.shiftKey && event.which === 9){ // Shift key + Tab key
					$grid_block_overlay.find('> div').css({
						'outline-width': '1px'
					});								
				}
			});			
			/*
            $container.on("click","."+GRID_EXPAND_TOGGLE, function() {
                var $_this = $(this);
                $_this.next(TD_FOUR_COLUMN_GRID_CONTENT).slideToggle();
                var $icon = $_this.children().eq(2).children().children(TD_ICON_CLOSE);
                var $iconWrapper = $_this.children().eq(2).children(OVERLAY_CLOSE);
                if($icon.hasClass(TD_TRIGGERICON_OPEN)){
                    $icon.removeClass(TD_TRIGGERICON_OPEN);
					$grid_block_overlay.find('> div').attr('aria-expanded',false);
                    //$iconWrapper.removeClass("td-icon-wrapper-white");
                    //$td_forscreenreader.html("");
                } else {
                    $icon.addClass(TD_TRIGGERICON_OPEN);
                    $iconWrapper.addClass(TD_ICON_WRAPPER_GREEN);
					$grid_block_overlay.find('> div').attr('aria-expanded',true);
                    //$td_forscreenreader.html("close");
                }
            });
			*/
            $(window).on('resize', function(){
                if (window.innerWidth < breakpoint) {
                    if (!$td_four_column_grid_title.hasClass(GRID_EXPAND_TOGGLE)) {
                        $td_four_column_grid_title.addClass(GRID_EXPAND_TOGGLE);
                        $td_icon_close.addClass(TD_TRIGGERICON+" "+CUSTOM_FOURCOLGRID);
                        $overlay_close.addClass(TD_ICON_WRAPPER_GREEN);
						$overlay_close.removeAttr('aria-expanded');
                        $td_four_column_grid_content.hide();
						$td_forscreenreader.html("More details");
						$grid_block_overlay.find(OVERLAY_CLOSE).removeAttr('role tabindex').attr('aria-hidden',true);
						$grid_block_overlay.find('> div').attr({'role':'link','aria-expanded':false,'tabindex':0});
						$grid_block_overlay.find('.'+GRID_EXPAND_TOGGLE+' > span').attr('aria-hidden',true);
						
						$four_column_grid.find('.overlay-close').removeAttr('role aria-expanded tabindex');
                    }
                } else {
                    $td_four_column_grid_title.removeClass(GRID_EXPAND_TOGGLE);
                    $td_icon_close.removeClass(TD_TRIGGERICON+" "+CUSTOM_FOURCOLGRID+" "+TD_TRIGGERICON_OPEN);
                    $overlay_close.removeClass(TD_ICON_WRAPPER_GREEN);
					$overlay_close.attr('aria-expanded',tmp_overlay_close_status);
                    $td_four_column_grid_content.show();
					//$td_forscreenreader.html("close");
					$four_column_grid.find(TD_FORSCREENREADER).html("Learn more");
					$grid_block_overlay.find(TD_FORSCREENREADER).html("Close");					
					$grid_block_overlay.find(OVERLAY_CLOSE).attr({'tabindex':0,'role':'button'}).removeAttr('aria-hidden');
					$grid_block_overlay.find('> div').removeAttr('role aria-expanded tabindex ');
					$grid_block_overlay.find('.'+GRID_EXPAND_TOGGLE+' > span').attr('aria-hidden',true);
					sc_equalHeight($container.find('.grid-over'));
					
					$four_column_grid.find('.overlay-close').attr({'role':'link','tabindex':'0','aria-expanded':false});
                }
            });
			
			var grid_block_click_count = 0
            $(GRID_HOVER+", ."+FOUR_COLUMN_GRID).on("click keypress", function(e) { //this line remove 4-column-grid
				grid_block_click_count += 1;
                var $_this_data_target = $(this).attr("data-target");
                var key = e.which;
                if(key == 1 || key == 13) {
                    if ($_this_data_target) {
                        $($_this_data_target).fadeIn("fast").focus();
                        $($_this_data_target).attr('aria-hidden', false).focus();
						$(this).find('.overlay-close').attr({'aria-expanded':true});
			
						if(grid_block_click_count == 2){ // FOUR_COLUMN_GRID
							$('.'+FOUR_COLUMN_GRID).attr('aria-hidden', true);
							$(this).attr('aria-hidden', false);							
						}
						
						$($_this_data_target).find(OVERLAY_CLOSE).attr('aria-expanded',true);
						tmp_overlay_close_status = true;
                    } else { 
                        return;
                    }
                }
				if(grid_block_click_count == 2){
					grid_block_click_count = 0;
				}
            });
            $(".grid-hover").focus(function() {
                $(this).attr("aria-hidden",false);
            });

            /* delay hover */
            if(android === false && iOS === false) { //check touch device
                $four_column_grid.hover(
                    function (e) {
                        e.preventDefault();
                        $grid_hover.fadeOut();
                        hoverIn($(this),FOUR_COLUMN_GRID);
                    },
                    function (e) {
                        e.preventDefault();
                        hoverOut($(this),FOUR_COLUMN_GRID);
                    })
                $four_column_grid.focus(function () {
                    var $_this = $(this);
                    if (!$_this.children(GRID_HOVER).is(':visible')) {
                        $four_column_grid.removeClass(BLOCK_SELECTED_NEXT);
                        $_this.addClass(BLOCK_SELECTED_NEXT);
                        $(".grid-hover").fadeOut();
                        $(".grid-over").fadeIn();
                        $four_column_grid.attr("tabindex", 0);
                        hoverIn($_this, FOUR_COLUMN_GRID);
                    }
                })
            }
            $overlay_close.on("click keypress", function(e) {
                var $_this_closest = $(this).closest('.grid-block-overlay');
                var key = e.which;
                if(key == 1 || key == 13) {
                    if (window.innerWidth < breakpoint) {
                        $("body").removeClass("td-modal-open");
                    } else {
                        $_this_closest.removeClass("in");
                        $_this_closest.fadeOut().attr('aria-hidden', true);
                        $grid_hover.slideUp();
                        $(".grid-over").fadeIn();
                        if(key == 13) $('.'+BLOCK_SELECTED_NEXT).focus();//$block_selected_next.focus();
						
						$(this).attr('aria-expanded',false);
						tmp_overlay_close_status = false;
						$('.'+FOUR_COLUMN_GRID).attr('aria-hidden', false);
						
                    }
                }
            })
            $four_column_grid.on("focus",focus_video);
        },



        GridBlocks = function (element, options){
            var $blockHeight, scrollTop = 0;
            var $td_grid_blocks_single = $(element).find(".grid-block"),
                $grid_blocks_full = $("#grid-blocks-full"),
                GRID_BLOCK_FULL = "#grid-block-full",
                $grid_block_full = $(GRID_BLOCK_FULL),
                GRID_BLOCK_HALF = ".grid-block-half",
                $grid_block_half = $(GRID_BLOCK_HALF),
                GRID_BLOCK_OVERLAY_FULL = ".grid-block-overlay-full",
                $grid_block_overlay_full = $(GRID_BLOCK_OVERLAY_FULL),
                GRID_BLOCK = ".grid-block",
                $grid_block = $(GRID_BLOCK),
                GRID_HOVER_HALF = ".grid-hover-half",
                $grid_hover_half = $(GRID_HOVER_HALF),
                BLOCK_SELECTED_NEXT = "block-selected-next",
                $block_selected_next = $("."+BLOCK_SELECTED_NEXT),
                GRID_HOVER = ".grid-hover";

            if($td_grid_blocks_single.attr("role") == "button"){
                $td_grid_blocks_single.css("cursor","pointer");
            }

            $(window).load(function () {
                if(!nexus10){
                    $blockHeight = $grid_block_full.find("img").height();
                }else{
                    var $blockWidthDiv = $grid_block_full.width();
                    var imgRatio;
                    var img1x = new Image();
                    img1x.src = $("#grid-block-full").find("img").attr("src");
                    img1x.onload = function() {

                        imgRatio = this.width / this.height;


                        $blockHeight = $blockWidthDiv / imgRatio;
                        //alert("$blockHeight:"+$blockHeight);
                        $grid_block_full.find("img").height($blockHeight);
                        var values = $grid_block_half.map(function(index, el) { return parseInt($(el).height()); }).get();

                        if($blockHeight == null ) {$grid_block_half.height(Math.max.apply(null, values))} else {

                            $grid_block_half.height(($blockHeight-3)/2);

                        }

                    };

                }
                var values = $grid_block_half.map(function(index, el) { return parseInt($(el).height()); }).get();
                if($blockHeight == null ) {$grid_block_half.height(Math.max.apply(null, values))} else {
                    $grid_block_half.width($grid_block_half - 40);
                    $grid_block_half.height(($blockHeight-3)/2);
                }
                //$("#grid-block-full").find("img").css("height",$blockHeight + 3);			
            });

            $(window).on('resize', function(){
                if(nexus10){
                    var $blockWidthDiv = $grid_block_full.width();
                    var imgRatio;
                    var img1x = new Image();
                    img1x.src = $("#grid-block-full").find("img").attr("src");
                    img1x.onload = function() {

                        imgRatio = this.width / this.height;


                        $blockHeight = $blockWidthDiv / imgRatio;
                        //alert("$blockHeight:"+$blockHeight);
                        $grid_block_full.find("img").height($blockHeight);
                        var values = $grid_block_half.map(function(index, el) { return parseInt($(el).height()); }).get();

                        if($blockHeight == null ) {$grid_block_half.height(Math.max.apply(null, values))} else {

                            $grid_block_half.height(($blockHeight-3)/2);

                        }

                    };
                }

                var values = $grid_block_half.map(function (index, el) {
                    return parseInt($(el).height());
                }).get();
                if ($(".grid-block-full").find("img").height() == null) {
                    $grid_block_half.height(Math.max.apply(null, values))
                } else {
                    $grid_block_half.height(($grid_block_full.height()-3) / 2);
                }
                $grid_block_overlay_full.width($grid_blocks_full.width());
                //$grid_block_full.find("img").css("height",$blockHeight + 3);
            });

            $(GRID_BLOCK+", "+GRID_HOVER_HALF).on("click keypress", function(e) {
                var $_this_data_target = $(this).attr("data-target");
                var key = e.which;
                scrollTop = $(window).scrollTop();
                if(key == 1 || key == 13 ) {
                    var $dataTarget=$_this_data_target
                    if ($dataTarget) {
                        if($dataTarget.substring(0,1) == "#") {
                            $grid_block_overlay_full.width($grid_blocks_full.width());
                            $($_this_data_target).fadeIn().focus();
                            $($_this_data_target + GRID_BLOCK_OVERLAY_FULL).addClass('in').attr('aria-hidden', false).focus();
                        } else {
                            window.location.href = $dataTarget;
                        }
                    } else {
                        return true;
                    }
                }
                if (window.innerWidth < breakpoint) {
                    $("body").addClass("td-modal-open-block");
                }
            });

            $td_grid_blocks_single.on("focus",focus_video);

            var setTimeoutConstFull, setTimeoutConstOutFull;
            function hoverInFull(thisObj) {
                //clearTimeout(setTimeoutConstOutFull);
                //setTimeoutConstFull = setTimeout(function(){
                if(thisObj.children(GRID_HOVER_HALF).length) {
                    thisObj.children(GRID_HOVER_HALF).fadeIn(150).attr('aria-hidden', false);
                    thisObj.children(".grid-over").fadeOut(150);
                }
                // }, 300);
            }
            function hoverOutFull(thisObj) {
                //clearTimeout(setTimeoutConstFull);
                //setTimeoutConstFull = setTimeout(function(){
                if(thisObj.children(GRID_HOVER_HALF).length) {
                    thisObj.children(GRID_HOVER_HALF).fadeOut(150).attr('aria-hidden', true);
                    thisObj.children(".grid-over").fadeIn(150);
                }
                //}, 300);
            }

            /* delay hover */
            if(android === false && iOS === false) { //check touch device

                $grid_block.hover(
                    function (e) {
                        e.preventDefault();
                        $grid_hover_half.fadeOut();
                        hoverInFull($(this));
                    },
                    function (e) {
                        e.preventDefault();
                        hoverOutFull($(this));
                    });

                $grid_block.focus(
                    function () {
                        var $_this = $(this);
                        if (!$_this.children(GRID_HOVER_HALF).is(':visible')) { // not slip up when focusing again
                            $grid_block.removeClass(BLOCK_SELECTED_NEXT);
                            $_this.addClass(BLOCK_SELECTED_NEXT);
                            $(".grid-hover-half").fadeOut();
                            $(".grid-over").fadeIn();
                            $grid_block_half.attr("tabindex", 0);
                            if (window.innerWidth >= breakpoint) {
                                hoverInFull($_this);
                            }
                        }
                    });
            }


            $(".overlay-close").on("click keypress", function(e) {
                var $_this_closest = $(this).closest('.grid-block-overlay-full');
                var key = e.which;
                if(key == 1 || key == 13) {
                    $_this_closest.removeClass("in");
                    $_this_closest.fadeOut().attr('aria-hidden', true);
                    //$grid_hover_half.slideUp();
                    if(key == 13) {$block_selected_next.focus();}
                }
                if (window.innerWidth < breakpoint) {
                    $("body").removeClass("td-modal-open-block");
                    $(window).scrollTop(scrollTop);
                    scrollTop = 0;
                }
            })

        },



        GridBlocksFlexbox = function (element, options){
            var $blockHeight, scrollTop = 0;
            var $td_grid_blocks_single = $(element).find(".grid-block"),
                $grid_blocks_full = $("#grid-blocks-full"),
                GRID_BLOCK_FULL = "#grid-block-full",
                $grid_block_full = $(GRID_BLOCK_FULL),
                GRID_BLOCK_HALF = ".grid-block-half",
                $grid_block_half = $(GRID_BLOCK_HALF),
                GRID_BLOCK_OVERLAY_FULL = ".grid-block-overlay-full",
                $grid_block_overlay_full = $(GRID_BLOCK_OVERLAY_FULL),
                GRID_BLOCK = ".grid-block",
                $grid_block = $(GRID_BLOCK),
                GRID_HOVER_HALF = ".grid-hover-half",
                $grid_hover_half = $(GRID_HOVER_HALF),
                BLOCK_SELECTED_NEXT = "block-selected-next",
                $block_selected_next = $("."+BLOCK_SELECTED_NEXT),
                GRID_HOVER = ".grid-hover",
				GRID_OVER = ".grid-over",
				GRID_OVER_TITLE = '.grid-over-title',
                $previous_focus = null,
				tabbing_hoverin = false,
				tabbing_large_overlay = false;

				
			unload_events();
			
            $('.overlay-close').prop('tabindex', 0);
			$grid_block_overlay_full.find('.overlay-close').attr({'role':'button','aria-expanded':true});
			$grid_hover_half.find('.overlay-close').attr({'aria-expanded':false,'role':'button'});
			$grid_hover_half.attr('aria-hidden',true); 
			
			$(GRID_OVER_TITLE).attr({'tabindex':0,'role':'button','aria-expanded':false});

            if($td_grid_blocks_single.attr("role") == "button"){
                $td_grid_blocks_single.css("cursor","pointer");
            }
			
            $(window).on('resize', function(){
                sc_equalHeight($grid_block.find(GRID_OVER));
            });

			var $temp_grid_hover_half; 
            $(GRID_BLOCK+", "+GRID_HOVER_HALF).on("click keypress", function(e) {
				//console.log("A");
				
				//console.log('large large');	
				var $_this = $(this);
				var $_this_data_target = $(this).attr("data-target");
                var key = e.which;

                $previous_focus = $(this)[0];

                scrollTop = $(window).scrollTop();
                if(key == 1 || key == 13 ) {
					$grid_block.removeClass(BLOCK_SELECTED_NEXT);
					$(GRID_HOVER_HALF+' .overlay-close').removeClass(BLOCK_SELECTED_NEXT);

					if(key == 13 && tabbing_hoverin == false && tabbing_large_overlay == true){
						$(this).find(GRID_HOVER_HALF).attr('aria-hidden', false);
						$temp_grid_hover_half = $(this).find(GRID_HOVER_HALF);
					}					
					
					$(this).find('.overlay-close').attr('aria-expanded',true);
					$td_grid_blocks_single.attr('aria-hidden',true);
					
                    var $dataTarget=$_this_data_target
                    if ($dataTarget) {
                        if($dataTarget.substring(0,1) == "#") {
				
							 if(android === false && iOS === false) {
								 
							 }else{
								$(this).find(GRID_OVER).css('display','none');	
								//$grid_block.find(GRID_OVER).css('display','none');	
							 }

							
                            //$grid_block_overlay_full.width($grid_blocks_full.width());
                            //$($_this_data_target).fadeIn().focus();
							$($_this_data_target).fadeIn(function(){
								$_this.find(GRID_HOVER_HALF+' .overlay-close').addClass(BLOCK_SELECTED_NEXT);							
							}).focus();
                            $($_this_data_target + GRID_BLOCK_OVERLAY_FULL).addClass('in').attr('aria-hidden', false).focus();
                        } else {
                            window.location.href = $dataTarget;
                        }
                    } else {
                        return true;
                    }
                }
                if (window.innerWidth < breakpoint) {
                    $("body").addClass("td-modal-open-block");
                }
            });

            $td_grid_blocks_single.on("focus",focus_video);

            var setTimeoutConstFull, setTimeoutConstOutFull;
			
            function hoverInFull(thisObj) {
                //clearTimeout(setTimeoutConstOutFull);
                //setTimeoutConstFull = setTimeout(function(){
                if(thisObj.children(GRID_HOVER_HALF).length) {
                    //thisObj.children(GRID_HOVER_HALF).fadeIn(150).attr('aria-hidden', false);
					//console.log(tabbing_hoverin);
					(tabbing_hoverin) ? thisObj.children(GRID_HOVER_HALF).fadeIn(150).attr('aria-hidden', false) : thisObj.children(GRID_HOVER_HALF).fadeIn(150).attr('aria-hidden', false);
                     thisObj.children(GRID_OVER).fadeOut(150);
					 thisObj.find(GRID_OVER_TITLE).attr('aria-expanded', true);
					
					tabbing_hoverin = false;
                }
                // }, 300);
            }
            function hoverOutFull(thisObj) {
                //clearTimeout(setTimeoutConstFull);
                //setTimeoutConstFull = setTimeout(function(){
                if(thisObj.children(GRID_HOVER_HALF).length) {
                    //thisObj.children(GRID_HOVER_HALF).fadeOut(150).attr('aria-hidden', true);
					thisObj.children(GRID_HOVER_HALF).fadeOut(150).attr('aria-hidden', true);
                    thisObj.children(GRID_OVER).fadeIn(150);
					thisObj.find(GRID_OVER_TITLE).attr('aria-expanded', false);
					
					tabbing_hoverin = false;
					tabbing_large_overlay = false;
                }
                //}, 300);
            }

			
            /* delay hover */
            if(android === false && iOS === false) { //check touch device
              
				$td_grid_blocks_single.each(function(){
					if( !$(this).find('.grid-over').get(0) ){
						$(this).attr('tabindex',0);
					}
				});			  
				
				$grid_block.find(GRID_OVER).on("click keypress", function(e) {
					//console.log("B");
					e.preventDefault();
					e.stopPropagation();
					var key = e.which;
	
					if(key == 1 || key == 13 ) {
						
						$grid_hover_half.fadeOut();
						$grid_hover_half.attr('aria-hidden',true);
						$grid_hover_half.removeAttr('tabindex');
						$grid_block.find(GRID_OVER).css('display','block');
						$(this).css('display','none'); 
						$(this).find(GRID_OVER_TITLE).attr('aria-expanded',true);
						$(this).parent().children(GRID_HOVER_HALF).fadeIn(function(){
							$(this).attr('tabindex',0).focus(); // added due to IE11
						});
						$(this).parent().children(GRID_HOVER_HALF).attr('aria-hidden',false);
					}
					
				});				
				
                $grid_block.hover(
                    function (e) {
                        e.preventDefault();
                        $grid_hover_half.fadeOut();
                        hoverInFull($(this));
                    },
                    function (e) {
                        e.preventDefault();
                        hoverOutFull($(this));
                    })

                $grid_block.focus(
                    function () {
                        var $_this = $(this);
						
                        if (!$_this.children(GRID_HOVER_HALF).is(':visible') && !$_this.find('.grid-over').get(0)) { // not slip up when focusing again
						
							tabbing_hoverin = true;
							tabbing_large_overlay = true;

                            $grid_block.removeClass(BLOCK_SELECTED_NEXT);
							$(GRID_HOVER_HALF+' .overlay-close').removeClass(BLOCK_SELECTED_NEXT);
                            $_this.addClass(BLOCK_SELECTED_NEXT);
                            $(".grid-hover-half").fadeOut();
                            $(GRID_OVER).fadeIn();
                            //$grid_block_half.attr("tabindex", 0);
							$(this).find('.GRID_BLOCK_HALF').attr("tabindex", 0);
							
                            if (window.innerWidth >= breakpoint) {
                                hoverInFull($_this);
                            }
                        }
                    })
            }

            $('.td-flexbox-overlay-container div:only-child a, .td-flexbox-overlay-content:last-child a').focusout(function(e) {

                $(this).closest('.grid-block-overlay-full').removeClass("in");
                $(this).closest('.grid-block-overlay-full').fadeOut().attr('aria-hidden', true);
                
            });

            $(".overlay-close").on("click keypress", function(e) {
				//console.log("C");
                var $_this_closest = $(this).closest('.grid-block-overlay-full');
                var key = e.which;

                if(key == 1 || key == 13) {

					if(key == 13 && tabbing_hoverin == false && tabbing_large_overlay == true){
						$temp_grid_hover_half.attr('aria-hidden', false);
						tabbing_large_overlay = false;
					}	
					$grid_hover_half.find('.overlay-close').attr('aria-expanded',false);
					$td_grid_blocks_single.removeAttr('aria-hidden');
					
                    $_this_closest.removeClass("in");
					
					if($_this_closest.is(':visible')){
						$_this_closest.fadeOut().attr('aria-hidden', true);
					}
                    //console.log('==================>');
                    //$grid_hover_half.slideUp();
					
					if(android === false && iOS === false) {
					 
					}else{
						$grid_block.find(GRID_OVER).css('display','block');			
					}					
                }
                if (window.innerWidth < breakpoint) {
                    $("body").removeClass("td-modal-open-block");
                    $(window).scrollTop(scrollTop);
                    scrollTop = 0;
                }
				if ($previous_focus != undefined) {
					$previous_focus.focus(); // accessibility restore focus to before modal window opened
					$previous_focus = $();
				}
				if(key == 13) {
					if($_this_closest.is(':visible')){
						$('.td-icon-wrapper-white.td-interactive-icon.overlay-close').focus();						
					}else{
						$previous_focus = $();
					}
				}	
				
            });
			
			function unload_events(){
				$(GRID_BLOCK+", "+GRID_HOVER_HALF).off("click keypress");
				$td_grid_blocks_single.off("focus");
				
				
				if(android === false && iOS === false) { //check touch device
					
					$grid_block.find(GRID_OVER).off("click keypress");
				}
				
				$(".overlay-close").off("click keypress");
				
				 $grid_block.off( "mouseenter mouseleave" );
			}
			
        };



    var setTimeoutConst, setTimeoutConstOut;
    var GRID_HOVER = ".grid-hover";
    
    function hoverIn(thisObj, type) {
        //clearTimeout(setTimeoutConstOut);
        //setTimeoutConst = setTimeout(function(){
        if(thisObj.children(GRID_HOVER).length) {
			if(type == '3-column-grid'){
				thisObj.children(GRID_HOVER).fadeIn(150);
				thisObj.children(".grid-over").fadeOut(150);	
			}else if(type == '4-column-grid'){
				thisObj.children(GRID_HOVER).fadeIn(150).attr('aria-hidden', true);
				thisObj.children(".grid-over").fadeOut(150).attr({'role':'link','tabindex':'0','aria-expanded':true});
				thisObj.children(".grid-hover").find('.overlay-close').attr({'role':'link','tabindex':'0','aria-expanded':false});
				
				
			}else{
				thisObj.children(GRID_HOVER).fadeIn(150).attr('aria-hidden', false);
				thisObj.children(".grid-over").fadeOut(150).attr({'role':'link','tabindex':'0','aria-expanded':true});
            }
        }
        //  }, 0);
    }

    function hoverOut(thisObj, type) {
        //clearTimeout(setTimeoutConst);
        //setTimeoutConst = setTimeout(function(){
        if(thisObj.children(GRID_HOVER).length) {
			if(type == '3-column-grid'){
				thisObj.children(GRID_HOVER).fadeOut(150);
				thisObj.children(".grid-over").fadeIn(150);		
			}else if(type == '4-column-grid'){
				thisObj.children(GRID_HOVER).fadeOut(150).attr('aria-hidden', false);
				thisObj.children(".grid-over").fadeIn(150).attr({'role':'link','tabindex':'0','aria-expanded':false});
				thisObj.children(".grid-hover").find('.overlay-close').removeAttr('role aria-expanded tabindex');				
			}else{
				thisObj.children(GRID_HOVER).fadeOut(150).attr('aria-hidden', true);
				thisObj.children(".grid-over").fadeIn(150).attr({'role':'link','tabindex':'0','aria-expanded':false});
			}
        }
        // }, 0);
    }

    function focus_video() {
        if ($(this).attr("onClick") != undefined) {
            $(this).keypress(function(e){
                if(e.which == 13){//Enter key pressed
                    $(this).click();//Trigger search button click event
                    e.preventDefault();
                }
                $(this).unbind("keypress");
            });
            $(this).css("cursor","pointer");
        }
    }

    function sc_equalHeight($element){
        var height = g.getEqualHeight($element);
        $element.css("height", height+"px");
    }

    // PLUGINS DEFINITION
    // ==================
    $.fn.td3GridBlocks = g.getPluginDef(threeColumnGridBlocks, 'td3GridBlocks');
    $.fn.td4GridBlocks = g.getPluginDef(fourColumnGridBlocks, 'td4GridBlocks');
    $.fn.tdGridBlocks = g.getPluginDef(GridBlocks, 'tdGridBlocks');
    $.fn.tdGridBlocksFlexbox = g.getPluginDef(GridBlocksFlexbox, 'tdGridBlocksFlexbox');
    // DEFAULT SETTINGS
    // ================
    $.fn.td3GridBlocks.defaults = {};
    $.fn.td4GridBlocks.defaults = {};
    $.fn.tdGridBlocks.defaults = {};
    $.fn.tdGridBlocksFlexbox.defaults = {};
    $(".td-3-column-grid-blocks").td3GridBlocks();
    $(".td-4-column-grid-blocks").td4GridBlocks();
    $(".td-grid-blocks").tdGridBlocks();
    $(".td-grid-blocks-flexbox").tdGridBlocksFlexbox();

	// Listening for Target Premium.
	document.addEventListener("threeColumnGridBlocks", function(e){
		$(".td-3-column-grid-blocks").td3GridBlocks();
		//console.log('Listening!'); 
	});
				
}(global));}],
td_rq_tooltip: [function (global) {/* Source: src/js/td-tooltip.js */
"use strict";
/* ========================================================================
 * Bootstrap: tooltip.js v3.3.6
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
// TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.6'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      that.$element
        .removeAttr('aria-describedby')
        .trigger('hidden.bs.' + that.type)
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var elOffset  = isBody ? { top: 0, left: 0 } : $element.offset()
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);}],
td_rq_popover: [function (global) {/* Source: src/js/td-popover.js */
"use strict";
/* ========================================================================
 * Bootstrap: popover.js v3.3.6
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
// POPOVER PUBLIC CLASS DEFINITION
    // ===============================

    var Popover = function (element, options) {
        this.init('popover', element, options);
    };

    if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

    Popover.VERSION = '3.3.6';

    Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
        placement: 'right',
        trigger: 'click',
        content: '',
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    });


    // NOTE: POPOVER EXTENDS tooltip.js
    // ================================

    Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype);

    Popover.prototype.constructor = Popover;

    Popover.prototype.getDefaults = function () {
        return Popover.DEFAULTS;
    };

    Popover.prototype.setContent = function () {
        var $tip = this.tip();
        var title = this.getTitle();
        var content = this.getContent();

        $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
        $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
            this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
            ](content)

        $tip.removeClass('fade top bottom left right in')

        // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
        // this manually by checking the contents.
        if (!$tip.find('.popover-title').html()) {
            $tip.find('.popover-title').hide();
        }
    };

    Popover.prototype.hasContent = function () {
        return this.getTitle() || this.getContent();
    };

    Popover.prototype.getContent = function () {
        var $e = this.$element;
        var o = this.options;

        return $e.attr('data-content')
            || (typeof o.content == 'function' ?
                o.content.call($e[0]) :
                o.content);
    };

    Popover.prototype.arrow = function () {
        return (this.$arrow = this.$arrow || this.tip().find('.arrow'));
    };


    // POPOVER PLUGIN DEFINITION
    // =========================

    function Plugin(option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('bs.popover');
            var options = typeof option == 'object' && option;

            if (!data && /destroy|hide/.test(option)) {
                return;
            }
            if (!data) {
                $this.data('bs.popover', (data = new Popover(this, options)));
            }
            if (typeof option == 'string') {data[option](); }
        });
    }

    var old = $.fn.popover;

    $.fn.popover = Plugin;
    $.fn.popover.Constructor = Popover;


    // POPOVER NO CONFLICT
    // ===================

    $.fn.popover.noConflict = function () {
        $.fn.popover = old
        return this
    };

}(jQuery);}],
td_rq_descriptor: [function (global) {/* Source: src/js/td-descriptor.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Descriptor
 * Created by Paul.Z.Bradley@td.com
 * Start date: 2016/07/22
 * @version 1.0
 * Fixed following issues by daniel.cho@td.com
 * - not able to close the popup div
 * - not able to tab in the popup div when there is links
 * - not able to open the popup with keyboard in IE11 + JAWs
 * - Added aria-label to close button with data-aria-label-close
 * - sometimes popover doesn't get closed clicking any area within body
 * - focus style doesn't show up properly when tab key
 * - switching from e.target.nextSibling.id to $(e.target).attr('aria-describedby') for $trigger_link_obj.on('shown.bs.popover')
 * - Replacing missing 'event' parameter (replacing 'e' with 'event')
 * 
 * @version 1.0.1
 * Fixed following issues by daniel.cho@td.com
 * - The dotted box is not showing around the descriptors anymore, unless the tab is pressed on desktop
 *
 * @version 1.0.2
 * Fixed following issues by ayeon.chung@td.com
 * - The descriptor not showing when the link resides in #11.3 component with mobile devices (https://track.td.com/browse/NAPST-707)
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,

        descriptor = function (element, options) {
            var $element = $(element),
                currentDescriptorId,
                $currentDescriptor = null,
                $trigger_link_obj = $element.find('[data-toggle="popover"]'),
                tempScrollPosition = null,
                options = {
                    template: '<div class="popover" role="tooltip" tabindex="-1"><div class="inner"><div class="close hairline" tabindex="0" role="button"></div><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div></div>',
                    html: true,
                    content: function(){
                        var descriptor = $(this).data('content-id');
		
                        return $element.find('#' + descriptor).html();
                    },
                    title: function() {
                        return $("#popover-title").html();
                    }
                };

            init();

            function init() {
					var $clicked_link_focus;
					var popover_status = false;
                    if ($.fn.popover){
						$('body').on('click', function(event){
							$('#'+currentDescriptorId).popover('hide');
							 // Hide if the overlay is visible
							if($('#td-descriptor-lightbox').is(':visible')){
								$('#td-descriptor-lightbox').hide();
							}							
							$('.td-bst-desc').css({
								'outline-width': '0px'
							});
						});

						$('body').on('focus blur keydown', function(event){
							if (event.which === 9){ // Tab key		
								$('.td-bst-desc').css({
									'outline-width': '0px'
								});
							}
							if (event.shiftKey && event.which === 9){ // Shift key + Tab key
								$('.td-bst-desc').css({
									'outline-width': '0px'
								});								
							}
						});
						
                        $trigger_link_obj.popover(options);

                        $trigger_link_obj.on('click', function(event) {
							//console.log('2');
							event.stopPropagation();
							event.preventDefault();
							
							$clicked_link_focus = $(document.activeElement);
							
							
							// remove open descriptor
							$('#'+currentDescriptorId).popover('hide');

							// Hide if the overlay is visible
							if($('#td-descriptor-lightbox').is(':visible')){
								$('#td-descriptor-lightbox').hide();
							}

                            var $trigger_link = $(this);
							
                            if (window.innerWidth > 767){
                                $trigger_link.popover('show');

                            } else {
								
//								 
								
                                $trigger_link.popover('show');
								$('#'+currentDescriptorId).find('.close.hairline').focus();
                                $('#td-descriptor-lightbox').show();
								
								
								
                            }	
							$('.td-bst-desc').css({
								'outline-width': '0px'
							});	
/*							
							$trigger_link.css({
								'outline-style': 'dotted',
								'outline-width': '1px',
								'outline-offset': '2px'
							});	
*/
							
                        });				
						
						$trigger_link_obj.focusout(function(e){
							e.stopPropagation();
							var $trigger_link = $(this);
							if(popover_status != false && !$('body').hasClass('td-no-focus-outline')){
								$clicked_link_focus.focus();
								popover_status = false;							
							}
							
/* removed and its functionality replaced by $('body').on('click', funciton(){});
							if(e.relatedTarget === null){ // e.relatedTarget.parentElement.className.indexOf("popover") === -1
							
								//console.log('4-1');
								
								$('#'+currentDescriptorId).popover('hide');
								 // Hide if the overlay is visible
								if($('#td-descriptor-lightbox').is(':visible')){
									$('#td-descriptor-lightbox').hide();
								}
								
							}else{
								
*/								
								//console.log('4-2');
/*								
								if(popover_status == false){ 
									//console.log('5::false');
									$trigger_link.css({
										'outline-width': '0px'
									});
								}else{

									//console.log('5::true');
									$clicked_link_focus.focus();
									$clicked_link_focus.css({
										'outline-style': 'dotted',
										'outline-width': '1px',
										'outline-offset': '2px'
									});		
									popover_status = false;
								}
*/								
															
//							}
						});

						$trigger_link_obj.focusin(function(e){
							//console.log('1');
							e.stopPropagation();
							$('.td-bst-desc').css({
								'outline-width': '0px'
							});								
							$(this).css({
								'outline-width': '1px'
							}); 
						});	
				
	                    $trigger_link_obj.on('shown.bs.popover', function(e) {	
							//console.log('3');
							e.stopPropagation();
							var $trigger_link = $(this);
							//currentDescriptorId = e.target.nextSibling.id;
							currentDescriptorId = $(e.target).attr('aria-describedby');	
							$('#'+currentDescriptorId).focus();	
						
							// Add aria-label to close button
							add_aria_label_close($trigger_link, $('#'+currentDescriptorId));

							$('.popover').find('.close.hairline').off('click keydown');
							$('.popover').off('focus blur keydown');
							
							$('#'+currentDescriptorId).on('click',function(e){
								e.stopPropagation();
							});
							
                            $('#'+currentDescriptorId).find('.close.hairline').on('click keydown', function(e){
								$('#'+currentDescriptorId).popover('hide');
								$('#'+currentDescriptorId).prev().focus();// For IE11
								 
								 // Hide if the overlay is visible
								 if($('#td-descriptor-lightbox').is(':visible')){
									 $('#td-descriptor-lightbox').hide();
								 }
                            });								
	
							$('#'+currentDescriptorId).on('focus blur keydown','.inner', function(event){//change e to event
								e.stopPropagation();
								if (event.which === 9){ // Tab key
									var $current = $(document.activeElement);
									if(!$current.next().length && !event.shiftKey){
										 $('#'+currentDescriptorId).popover('hide'); 
										 
										// Hide if the overlay is visible
										if($('#td-descriptor-lightbox').is(':visible')){
											 $('#td-descriptor-lightbox').hide();
										}	
										$trigger_link.focus();
										$trigger_link.css({
											'outline-width': '1px'
										});									
/*									
										if(popover_status == false){
											//console.log('popover::false');
											$trigger_link.css({
												'outline-width': '0px'
											});
										}else{

											//console.log('popover::true');
											$clicked_link_focus.focus();
											$clicked_link_focus.css({
												'outline-style': 'dotted',
												'outline-width': '1px',
												'outline-offset': '2px'
											});		
										}										
*/										
									}									
								}	
								if (event.shiftKey && event.which === 9){ // Shift key + Tab key
									var $current = $(document.activeElement);

									if(typeof $current.attr('class') !== 'undefined'){
										$('#'+currentDescriptorId).popover('hide');
										
										// Hide if the overlay is visible
										if($('#td-descriptor-lightbox').is(':visible')){
											 $('#td-descriptor-lightbox').hide();
										}	
										$trigger_link.focus();
										$trigger_link.css({
											'outline-width': '1px'
										});
/*									
										if(popover_status == false){
											//console.log('popover::false');
											$trigger_link.css({
												'outline-width': '0px'
											});
										}else{

											$clicked_link_focus.focus();
											$clicked_link_focus.css({
												'outline-style': 'dotted',
												'outline-width': '1px',
												'outline-offset': '2px'
											});		
										}
*/										
									}									
								}									
							});		
							//console.log('=========================3 END');
							popover_status = true;
						});	
						/*
	                    $trigger_link_obj.on('hidden.bs.popover', function(e) {				
						});	
                        $trigger_link_obj.on('inserted.bs.popover', function(e) {
                        });
						*/
                    }
			
                $(document).ready(function(){
					var $clicked_link_focus;
					var popover_status = false;
                    if ($.fn.popover){
						
						$('body').on('click', function(event){
							$('#'+currentDescriptorId).popover('hide');
							 // Hide if the overlay is visible
							if($('#td-descriptor-lightbox').is(':visible')){
								$('#td-descriptor-lightbox').hide();
							}							
							$('.td-bst-desc').css({
								'outline-width': '0px'
							});
						});

						$('body').on('focus blur keydown', function(event){
							if (event.which === 9){ // Tab key		
								$('.td-bst-desc').css({
									'outline-width': '0px'
								});
							}
							if (event.shiftKey && event.which === 9){ // Shift key + Tab key
								$('.td-bst-desc').css({
									'outline-width': '0px'
								});								
							}
						});
						
                        $trigger_link_obj.popover(options);

                        $trigger_link_obj.on('click', function(event) {
							//console.log('2');
							event.stopPropagation();
							event.preventDefault();
							
							$clicked_link_focus = $(document.activeElement);
							
							
							// remove open descriptor
							$('#'+currentDescriptorId).popover('hide');

							// Hide if the overlay is visible
							if($('#td-descriptor-lightbox').is(':visible')){
								$('#td-descriptor-lightbox').hide();
							}

                            var $trigger_link = $(this);
							
                            if (window.innerWidth > 767){
                                $trigger_link.popover('show');

                            } else {
								
                                $trigger_link.popover('show');

								/**
                                 * Defect fix : The descriptor not showing when the link resides in #11.3 component with mobile devices (https://track.td.com/browse/NAPST-707)
								 */
								if ($trigger_link.closest(".slick-slider").length > 0) {
									var $parent = $($trigger_link.closest(".slick-slider")[0]);
									var $slickTrack = $($parent.find(".slick-track")[0]);
									var cachedWidth = $slickTrack.css("width");
									$parent.css("z-index", "9000");
									$slickTrack.css("width", "auto");  //reset
									$slickTrack.css("width", cachedWidth); // and then set

									var $popover = $($parent.find(".popover")[0]);
									$popover.css("max-width", "none").css("width", window.innerWidth + "px");
								}

								$('#'+currentDescriptorId).find('.close.hairline').focus();
								$('#td-descriptor-lightbox').show();

                            }	
							$('.td-bst-desc').css({
								'outline-width': '0px'
							});	
/*							
							$trigger_link.css({
								'outline-style': 'dotted',
								'outline-width': '1px',
								'outline-offset': '2px'
							});	
*/
							
                        });				
						
						$trigger_link_obj.focusout(function(e){
							e.stopPropagation();
							var $trigger_link = $(this);
							
							if(popover_status != false && !$('body').hasClass('td-no-focus-outline')){
								$clicked_link_focus.focus();
								popover_status = false;							
							}
							
/* removed and its functionality replaced by $('body').on('click', funciton(){});
							if(e.relatedTarget === null){ // e.relatedTarget.parentElement.className.indexOf("popover") === -1
							
								//console.log('4-1');
								
								$('#'+currentDescriptorId).popover('hide');
								 // Hide if the overlay is visible
								if($('#td-descriptor-lightbox').is(':visible')){
									$('#td-descriptor-lightbox').hide();
								}
								
							}else{
								
*/								
								//console.log('4-2');
/*								
								if(popover_status == false){ 
									//console.log('5::false');
									$trigger_link.css({
										'outline-width': '0px'
									});
								}else{

									//console.log('5::true');
									$clicked_link_focus.focus();
									$clicked_link_focus.css({
										'outline-style': 'dotted',
										'outline-width': '1px',
										'outline-offset': '2px'
									});		
									popover_status = false;
								}
*/								
															
//							}
						});

						$trigger_link_obj.focusin(function(e){
							//console.log('1');
							e.stopPropagation();
							$('.td-bst-desc').css({
								'outline-width': '0px'
							});								
							$(this).css({
								'outline-width': '1px'
							}); 
						});	
				
	                    $trigger_link_obj.on('shown.bs.popover', function(e) {	
							//console.log('3');
							e.stopPropagation();
							var $trigger_link = $(this);
							//currentDescriptorId = e.target.nextSibling.id;
							currentDescriptorId = $(e.target).attr('aria-describedby');	
							$('#'+currentDescriptorId).focus();	
						
							// Add aria-label to close button
							add_aria_label_close($trigger_link, $('#'+currentDescriptorId));

							$('.popover').find('.close.hairline').off('click keydown');
							$('.popover').off('focus blur keydown');
							
							$('#'+currentDescriptorId).on('click',function(e){
								e.stopPropagation();
							});
							
                            $('#'+currentDescriptorId).find('.close.hairline').on('click keydown', function(e){
								$('#'+currentDescriptorId).popover('hide');
								$('#'+currentDescriptorId).prev().focus();// For IE11
								 
								 // Hide if the overlay is visible
								 if($('#td-descriptor-lightbox').is(':visible')){
									 $('#td-descriptor-lightbox').hide();
								 }
                            });								
	
							$('#'+currentDescriptorId).on('focus blur keydown','.inner', function(event){//change e to event
								e.stopPropagation();
								if (event.which === 9){ // Tab key
									var $current = $(document.activeElement);
									if(!$current.next().length && !event.shiftKey){
										 $('#'+currentDescriptorId).popover('hide'); 
										 
										// Hide if the overlay is visible
										if($('#td-descriptor-lightbox').is(':visible')){
											 $('#td-descriptor-lightbox').hide();
										}	
										$trigger_link.focus();
										$trigger_link.css({
											'outline-width': '1px'
										});									
/*									
										if(popover_status == false){
											//console.log('popover::false');
											$trigger_link.css({
												'outline-width': '0px'
											});
										}else{

											//console.log('popover::true');
											$clicked_link_focus.focus();
											$clicked_link_focus.css({
												'outline-style': 'dotted',
												'outline-width': '1px',
												'outline-offset': '2px'
											});		
										}										
*/										
									}									
								}	
								if (event.shiftKey && event.which === 9){ // Shift key + Tab key
									var $current = $(document.activeElement);

									if(typeof $current.attr('class') !== 'undefined'){
										$('#'+currentDescriptorId).popover('hide');
										
										// Hide if the overlay is visible
										if($('#td-descriptor-lightbox').is(':visible')){
											 $('#td-descriptor-lightbox').hide();
										}	
										$trigger_link.focus();
										$trigger_link.css({
											'outline-width': '1px'
										});
/*									
										if(popover_status == false){
											//console.log('popover::false');
											$trigger_link.css({
												'outline-width': '0px'
											});
										}else{

											$clicked_link_focus.focus();
											$clicked_link_focus.css({
												'outline-style': 'dotted',
												'outline-width': '1px',
												'outline-offset': '2px'
											});		
										}
*/										
									}									
								}									
							});		
							//console.log('=========================3 END');
							popover_status = true;
						});	
						/*
	                    $trigger_link_obj.on('hidden.bs.popover', function(e) {				
						});	
                        $trigger_link_obj.on('inserted.bs.popover', function(e) {
                        });
						*/
                    }
                });

            }
			function add_aria_label_close(g_anchor,g_popover){
				var g_label = g_anchor.data('aria-label-close');
				if(typeof g_label !== 'undefined') g_popover.find('.close.hairline').attr('aria-label',g_label);			
			}
        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdDescriptor = g.getPluginDef(descriptor, 'tdDescriptor');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdDescriptor.defaults = {};

    $(".td-descriptor").tdDescriptor();
}(global));}],
td_rq_color_test: [function (global) {/* Source: src/js/td-color-test.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Mutliple Ctas
 * Created by dennis.erny@td.com
 * Start date: 2016/06/24
 * @version 1.0
 *
 * Multiple CTA - This is required for all variants (2,3,4)
 *
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,

        colorTest = function (element, options) {
            var $color = $(element),
                $white = $color.find(".test-bg-white"),
                $gray = $color.find(".test-bg-gray"),
                $mint = $color.find(".test-bg-mint"),
                $component = $('.td-contentarea > section:first-of-type');

            init();

            function init() {
                $white.on("click", function () {
                    $component.removeClass("td-bg-light-gray td-bg-mint-green");
                });

                $gray.on("click", function () {
                    $component.removeClass("td-bg-mint-green").addClass("td-bg-light-gray");
                });

                $mint.on("click", function () {
                    $component.removeClass("td-bg-light-gray").addClass("td-bg-mint-green");
                });

            }

        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdColorTest = g.getPluginDef(colorTest, 'tdColorTest');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdColorTest.defaults = {};

    $(".td-color-selector").tdColorTest();
}(global));}],
td_rq_table_image_right: [function (global) {/* Source: src/js/td-table-image-right.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD 30.1 image right with copy
 * Created by dmitri.kokoshkyn@td.com
 * used td-multiple-cta.js as a template
 * Start date: 2016/08/04
 * @version 1.0
 *
 * 30.1 image right with copy
 * 
 * Updated by daniel.cho@td.com
 * - Made 'Load more' link once.
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,
	 BREAKPOINT_SM_VALUE	= g.bpSet('sm'),		

        TableImageRight = function (element, options) {
            var $imgListContainer = $(element);
            var $listContainer = $imgListContainer.find(".td-show-more-content-1");
            var $moreDetails;
            var moreDetailsLink = "<div class='td-more-details-mobile'><a href='javascript:void(0);' tabindex='0' aria-controls='coll-1' aria-expanded='false' aria-label='Load more' title='Load more'>Load more<span class='td-icon' aria-hidden='true'></span></a></div>";

            init();

            function init() {
                //add button on xs
			  //$(moreDetailsLink).appendTo(".td-image-block-right-with-copy .td-more-details-1");	
			  $imgListContainer.find('.td-more-details-1').append(moreDetailsLink);
			  //button that hides container on mobile
					$moreDetails = $imgListContainer.find(".td-more-details-mobile");
			  if (window.innerWidth <= BREAKPOINT_SM_VALUE) { 
				$listContainer.attr("aria-hidden","true");
			  } else {
				$listContainer.attr("aria-hidden","false");
			  }			
                $moreDetails.on("click", function () {
		      //hide button
		      $(this).parent().fadeOut();	 				
		      $(this).children("a").attr("aria-expanded","true");
		      //fade in hidden container		
                    $listContainer.fadeIn();
		      $listContainer.attr("aria-hidden","false").focus();			  
                });
		  
                $window.resize(onWindowResize);		  
            }		
	     //change hidden container aria attribute on window resize
	     function onWindowResize() {
		if (window.innerWidth > BREAKPOINT_SM_VALUE) {
		  $listContainer.attr("aria-hidden","false");
		} else {
		  $listContainer.attr("aria-hidden","true");	
		}			
	     }
        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdTableImageRight = g.getPluginDef(TableImageRight, 'tdTableImageRight');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdTableImageRight.defaults = {};

    $(".td-image-block-right-with-copy").tdTableImageRight();
}(global));}],
td_rq_chart: [function (global) {/* Source: src/js/td-chart.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Chart
 * Created by Ayeon on 2016-01-22, 12:01:40
 * @version 1.0
 * Updated by danielcho80@gmail.com
 * 	@version 1.1
 *		Fixed: it supports multiple charts
 * 	@version 1.2
 * 		Fixed: accesibiilty fixes on accordion header div on mobile view  adding expand & collapse events to header div
 * 	@version 1.3
 * 		Fixed: accesibiilty fixes - added two different senarios for complex chart and #10.*  
 *  @version 1.3.1
 *		Added 'open' CSS class to content div when it is open state.
 *  @version 1.3.2016-01-22
 * 		Added 'event.stopPropagation' not to expand and collapse when clikcing on a link within accordion header
 */

(function (window) {

var $ = window.jQuery,
		g = window.g,

		td_chart = function(element, option){

			var $element = $(element).parent().parent().parent().prev().find(".td-chart-m"),
				complex_chart = $(element).parents().hasClass('td-complex-chart'), // complex chart
				td_chart_item_head = ".td-chart-item-head",
				td_chart_item_expanded = "td-chart-item-expanded",
				td_chart_item_collapsed = "td-chart-item-collapsed",
				td_triggericon_expanded = "td-triggericon-expanded",
				td_triggericon = ".td-triggericon",
				td_forscreenreader = ".td-forscreenreader",
				td_link_toggle = ".td-link-toggle";
				
			$(window).resize(function() {
				equaliseHeightResize($element, td_chart_item_head);
			});
			g.equaliseHeight($element, td_chart_item_head);
				
			g.ev.on(g.events.afterTabActiveChanged, function (event, $wrapper) {
				event.preventDefault();
				if ($wrapper.has($element).length > 0) {
					g.equaliseHeight($element, td_chart_item_head);
				}
			});	
			
			/* Not to expand & collapse when clikcing on a link within accordion header */
			$(td_chart_item_head + " .rte a").on('click', function(event){
				event.stopPropagation();
			})
			
			if(complex_chart == false){ // only apply to #10.2 & #10.3
				// outline style on with tabkey
				$('*').on('keydown',function(event) {
					var key = event.which;
					$(this).find(td_chart_item_head).css({'outline-width': '1px'});
				});
			}
			
			function toggleClose(targetelement,triggerelement,triggericon,readerelement) {
				triggerelement.removeClass(td_chart_item_expanded);
				triggerelement.addClass(td_chart_item_collapsed);
				triggericon.removeClass(td_triggericon_expanded);
				if (triggericon.length) {
					/* removed for accessibility
					var alt = readerelement.html();
					alt = alt.replace("Collapse", "Expand");
					readerelement.text(alt);
					*/
				}
				targetelement.animate({
					height: "toggle",
					opacity: "toggle"
				}, function() {
					targetelement.removeClass("open");
				});
			}
			function toggleOpen(targetelement,triggerelement,triggericon,readerelement) {
				triggerelement.removeClass(td_chart_item_collapsed);
				triggerelement.addClass(td_chart_item_expanded);
				triggericon.addClass(td_triggericon_expanded);
				if (triggericon.length) {
					/* removed for accessibility
					var alt = readerelement.html();
					alt = alt.replace("Expand", "Collapse");
					readerelement.text(alt);
					*/
				}
				targetelement.animate({
					height: "toggle",
					opacity: "toggle"
				}, function() {
					targetelement.removeClass("hide").addClass("open");
				});
			}				
			return $element.each(function(){		

				$(this).find(td_chart_item_head).each(function(){
					var $triggerelement = $(this),
						$targetelement = $triggerelement.next(),
						$triggericon = $triggerelement.find(td_triggericon),
						$readerelement = $triggerelement.find(td_forscreenreader);
					
					if(complex_chart == false){ // only apply to #10.2 & #10.3
						$triggerelement.attr({'aria-expanded':false,'tabindex':'0','role':'button'});
						$triggericon.parent().parent().attr({'aria-hidden':true,'tabindex':'-1'});
					}else{ // complex chart
						$triggerelement.find(td_link_toggle).attr({'aria-expanded':false,'tabindex':'0'});
					}
					 
					if(complex_chart == false){ // only apply to #10.2 & #10.3 
					
						$triggerelement.on('click keydown',function(event) {				
							
							// outline style off when click
						
							var key = event.which;

							if(key == 1){
								$(this).css({'outline-width': '0px'});								
							}
						
							if(key == 1 || key == 13) {
							//Make links in header on mobile clickable
								if (event.target.className == "td-link-standalone" || event.target.className == "td-link-lastword"){
									return true;
								}	
								
								if (!$targetelement.is(":visible")) {
									toggleOpen($targetelement,$triggerelement,$triggericon,$readerelement);
									$triggerelement.attr({'aria-expanded':true});
									$triggerelement.focus();
									return false;
								}
								if ($targetelement.is(":visible")) {
									toggleClose($targetelement,$triggerelement,$triggericon,$readerelement);
									$triggerelement.attr({'aria-expanded':false});
									$triggerelement.focus();
									return false;
								}
							}
						});	
					}else{	// complext charts
					
						$triggerelement.on('click',function(event) {		
							if (event.target.className == "td-link-standalone" || event.target.className == "td-link-lastword"){
								return true;
							}	
							
							if (!$targetelement.is(":visible")) {
								toggleOpen($targetelement,$triggerelement,$triggericon,$readerelement);
								$triggerelement.find(td_link_toggle).attr({'aria-expanded':true});
								$triggerelement.focus();
								return false;
							}
							if ($targetelement.is(":visible")) {
								toggleClose($targetelement,$triggerelement,$triggericon,$readerelement);
								$triggerelement.find(td_link_toggle).attr({'aria-expanded':false});
								$triggerelement.focus();
								return false;
							}	
						});							
					}
				});
			});
			
			function equaliseHeightResize($wrapper, query) {
				var $elements = $wrapper.find(query),
					height = g.getEqualHeight($elements);

				$elements.css('height',height+'px');
			}

		};

	// PLUGINS DEFINITION
	// ==================
	$.fn.td_chart = g.getPluginDef(td_chart, 'td_chart');

	// DEFAULT SETTINGS
	// ================
	$.fn.td_chart.defaults = {

	};

	$("table.td-chart").td_chart();

}(global));}],
td_rq_daily_rates: [function (global) {/* Source: src/js/td-daily-rates.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Daily Rates
 * Created by victor.chan@td.com
 * Based on functions from TD A-Banner
 * Date: 2016/08/15
 * @version 1.0
 *
 *
 */


(function (window) {

var $ = window.jQuery,
    $window = $(window),
    g = window.g,
    document = window.document,

    BREAKPOINT_XS = 'xs',
		BREAKPOINT_SM = 'sm',
		BREAKPOINT_MD = 'md',
		BREAKPOINT_LG = 'lg',
		BREAKPOINT_XS_VALUE = 0, // xs value is the lowest, since we're using min-width we want to use 0, this includes all widths up to sm
		BREAKPOINT_SM_VALUE = g.bpSet('sm'),
		BREAKPOINT_MD_VALUE = g.bpSet('md'),
		BREAKPOINT_LG_VALUE = g.bpSet('lg'),

    /**
  	 * Daily-Rates
  	 */
		/**
		 * @function dailyRates
		 * @memberof td-daily-rates js
		 * @description Daily-Rates - function to equalize heights of various elements
     *    designated in design mockups at all breakpoints allowing for one
     *    exception: {selectors: ['.myClass', '.myOtherClass'], exceptions: ['xs', 'xs']}
		 * @param {object} element - $(selector) - JQuery object.
		 * @param {array} options - contains optional parameters.
		 */

    dailyRates = function (element, options) {
      var settings  = $.extend($.fn.tdDailyRates.defaults, $(element).metadata(), options),
        selectors   = settings.selectors,
        exceptions  = settings.exceptions;

      init();

      $(window).on('resize', function(){
        var cur_breakPoint = getCurBreakPoint(window.innerWidth);
        // console.log('breakpoint:', cur_breakPoint);

        $.each(selectors, function(index, val) {
          if (getCurBreakPoint(window.innerWidth) === exceptions[index]) {
            setInitialHeight(element.find(val));
          } else {
            setEqualHeight(element.find(val));
          }
        });

      });

      function init() {
        $.each(selectors, function(index, val) {
          if (getCurBreakPoint(window.innerWidth) !== exceptions[index]) {
            setEqualHeight(element.find(val));
          }
        });
      }

      function setEqualHeight($element){
        var height = g.getEqualHeight($element);
        $element.css("height", height+"px");
      }

      function setInitialHeight($element) {
        var height = g.getEqualHeight($element);
        $element.css("height", "auto");
      }

      function getCurBreakPoint(w){
				if(typeof w === 'number'){
					if (w >= BREAKPOINT_LG_VALUE) {
						return BREAKPOINT_LG;
					} else if (w >= BREAKPOINT_MD_VALUE) {
						return BREAKPOINT_MD;
					} else if (w >= BREAKPOINT_SM_VALUE) {
						return BREAKPOINT_SM;
					} else{
						return BREAKPOINT_XS;
					}
				}else{
					return false;
				}
			}
    };

  // PLUGINS DEFINITION
  // ==================
  $.fn.tdDailyRates = g.getPluginDef(dailyRates, 'tdDailyRates');

  // DEFAULT SETTINGS
  // ================
  $.fn.tdDailyRates.defaults = {
    // logo: false
  };

  $(".td-daily-rates").tdDailyRates();
}(global));}],
td_rq_simple_slidedown: [function (global) {/* Source: src/js/td-simple-slidedown.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Simple SlideDown
 * Created by danielcho80@gmail.com on 2016-01-23
 * @version 1.0
 * 
 * @version 1.0.1
 * Fixed - made aria-expanded change to 'true' when 'Load More Articles' is in open state.
 * @version 1.0.2
 * Fixed - made the 'Load More Articles' not appear in the same tab area when the case of multiple use.
 * @version 1.1
 * Fixed - Acceesibilty Fix on "Load More Articles" - Made the focus not to go to the top of the page.
 */

(function (window) {

var $ = window.jQuery,
        g = window.g,

		td_simple_slideDown = function(element, option){

			var $el = $(element),
				settings = $.extend($.fn.tdSimpleSlideDown.defaults, $el.metadata(), option),
				get_targetelement = settings.targetelement,
				$get_targetelement = $el.parents('.td-article-list').find(get_targetelement),
				$get_targetelement_parent = $el.parents('.td-article-list'),
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

			$el.attr('aria-expanded', 'false');
			$get_targetelement_parent.on('keydown', function (event) {
				if (event.which === 9) {
					$(get_targetelement).attr('tabindex',-1);
				}
			});
			if($get_targetelement.get(0)){
				var get_targetelementToStyle = settings.targetelementToStyle;
				var get_removeClass = settings.removeClass;
				var get_addClass = settings.addClass;
				
				$el.on('click', function (event) {
					event.preventDefault();

					if(get_targetelementToStyle != ''){
						if(get_removeClass != '') $el.find(get_targetelementToStyle).removeClass(get_removeClass)
						if(get_addClass != '' ) $el.find(get_targetelementToStyle).addClass(get_addClass);
					}

					$get_targetelement.slideDown({
													complete: function(){
														if(!mobile){
															$(this).trigger('resize');
														}else{
															var y = $(window).scrollTop();
															$(window).scrollTop(y+1);	
														}
														$(this).animate({opacity:1});
													}
					});

					//$(get_targetelement).find("a:first").focus();  
					$(get_targetelement).attr('tabindex',0).css('outline',0).focus();
					$(this).parent().hide();
					$(this).attr({'aria-hidden':'true','aria-expanded':'true'});

				}).on('keydown', function (event) {
					if (event.which === 13) {
						event.preventDefault();
						$get_targetelement.slideDown({
														complete: function(){
															if(!mobile){
																$(this).trigger('resize');
															}else{
																var y = $(window).scrollTop();
																$(window).scrollTop(y+1);	
															}	
															$(this).animate({opacity:1});
														}
						});
						//$get_targetelement.find("a:first").focus();
						$(get_targetelement).attr('tabindex',0).css('outline',0).focus();

						$(this).parent().hide();
						$(this).attr({'aria-hidden':'true','aria-expanded':'true'});
					}
				});

				// move the 'load more' button so that it's in between the visible items and the hidden items
				// this fixes a bug with JAWS where focus() doesn't work and instead the hidden items need to be after the button in order for the tab order to work
				var $el_container = $el.parent(".td-row");
				var $hidden_item =  $get_targetelement[0];

				$el_container.clone(true).insertBefore($hidden_item);	// copy container and associated events, insert it before the hidden item
				$el_container.remove(); // destroy original button

			}else{
				//console.log("targetelement is not defined");
			}
		};

 
    // PLUGINS DEFINITION
    // ==================
    $.fn.tdSimpleSlideDown = g.getPluginDef(td_simple_slideDown, 'tdSimpleSlideDown');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdSimpleSlideDown.defaults = {
		targetelement: 'slidingContent',
		targetelementToStyle:'',
		removeClass:'',
		addClass:''
    };

    $(".btn_load_more").tdSimpleSlideDown();
}(global));}],
td_rq_special_offer_indicator: [function (global) {/* Source: src/js/td-special_offer_indicator.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
* @namespace td-special_offer_indicator js
* @author Daniel Cho danielcho80@gmail.com or daniel.cho@td.com
* @version 1.1
* @description 	- versoin 1.1:	-  Adding spacing to h tag(h1/h2) and the banner so that they don't overlap each other when there are indicaor component and back text link component
*
* @version 1.2 - dylan.chan@td.com : Include margins in height calculations of the indicator component
*
* @version 1.3 - daniel.cho@td.com: Added g.ev.on(g.events.afterExpandForOthers) to reiniticate when the open state of Expand & collpase component gets triggered.
*			
*/
(function (window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),	
        BREAKPOINT_MD_VALUE = g.bpSet('md'),		
        BREAKPOINT_SM_VALUE = g.bpSet('sm'),

        specialOfferindicator = function(element, options) {
			var objFn={},fn_ab={},
				$el = $(element),
			    $h1 = $el.find("h1"),
                $h2 = $el.find("h2"),				
				$special_offer = $el.find(".td-indicator-offer"), // special offer indicator
				$back_text_link = $el.prev('.td-back-text-link'); // back text link component
				
			var settings = $.extend($.fn.tdSpecialOfferindicator.defaults, $el.metadata(), options),
                g_prod_special = settings.prod_special.status, // Special Offer status
                g_prod_special_pos = settings.prod_special.position; // Special Offer position (left/right)

			objFn ={
				init: function(){
					// special offer indicator
                    objFn.special_offer_indicator(g_prod_special, g_prod_special_pos); 
				},
				special_offer_indicator: function(s,p){
					if (s == true) {
						if (p == 'left') $special_offer.addClass('left');
						$special_offer.show();
						
						// to avoid the indicator appears on the top of the title
						objFn.special_offer_indicator_alignment();

						$window.resize(function () {
							objFn.special_offer_indicator_alignment();
						});
					}
				},
				special_offer_indicator_alignment: function(){
					if($back_text_link.get(0)){
						if (window.innerWidth >= BREAKPOINT_MD_VALUE){
							var top_pos = $back_text_link.outerHeight(true) * -1;
							$special_offer.css('top',top_pos+'px');
						}else{
							$special_offer.css('top','0px');
						}
					}
					if (window.innerWidth < BREAKPOINT_SM_VALUE){
						var h_special_offer = $special_offer.outerHeight(true);
						($h1.get(0)) ? $h1.css({'padding-left':'0px','padding-right':'0px','margin-top':h_special_offer+'px'}) : $h2.css({'padding-left':'0px','padding-right':'0px','margin-top':h_special_offer+'px'});
					}else{

						($h1.get(0)) ? $h1.css({'padding-left':'10%','padding-right':'10%','margin-top':'20px'}) : $h2.css({'padding-left':'10%','padding-right':'10%','margin-top':'20px'});
					}				
				}			
			};
			
			
			$.extend(objFn);
						
			objFn.init();
			
			g.ev.on(g.events.afterExpandForOthers, function (event, data) {
				if( $el.parents(data.obj).get(0) ) {
					objFn.init();		
				}
			});
			

			return objFn;
		
        };

    // PLUGINS DEFINITION
    // ==================
    $.fn.tdSpecialOfferindicator = g.getPluginDef(specialOfferindicator, 'tdSpecialOfferindicator');
    $.fn.tdSpecialOfferindicator.defaults = {
		prod_special: {
			status: false,
			position: 'right'
		},
    };
	
    $(".td_rq_special_offer_indicator").tdSpecialOfferindicator();


}(global));}],
td_rq_scrollbar: [function (global) {/* Source: src/js/td-scrollbar.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD td-scrollbar
 * Created by daniel.cho@td.com
 * @version 1.0
 * 	- Custom Scrollbar
 *  - Made it work with td-large-modal-overlay.js
 *  - Added interaction with td-large-modal-overlay.js and td-expand.js
 */

(function (window) { 

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),	
        BREAKPOINT_MD_VALUE = g.bpSet('md'),		
        BREAKPOINT_SM_VALUE = g.bpSet('sm'),

        tdScrollbar = function(element, options) {
			var objFn={},fn_ssb={},
				$el = $(element);
				
			var settings = $.extend($.fn.tdScrollbar.defaults, $el.metadata(), options);

			var fn_ssb = {
				aConts  : [],
				mouseY : 0,
				N  : 0, 
				sc : 0,
				sp : 0,
				to : 0,
				parentCSS: settings.parentCSS,
				parentHeight: "",
				scrollStatus: "yes",
				scrollbarWidth: 17,
				scrollbarTopPos: 10,
				asd:{sg: false},/*active scrollbar element*/

				getId: function(){
					var generated_id = "scrollbarid_"+Math.floor((Math.random() * 1000) + 1);
					$el.attr("id",generated_id);
					
					return generated_id;
				},
				
				// constructor
				scrollbar : function (cont_id) {
					var cont = document.getElementById(cont_id);

					// perform initialization
					if (! fn_ssb.initialization()) {
						return false;
					}
					
					var new_div = document.createElement("div");
					var parent_div = cont.parentNode;
					new_div.style.overflow = "hidden";
					new_div.style.position = "relative";
					if(settings.modalstatus){
						new_div.style.height = fn_ssb.parentHeight;
					}
					new_div.className = fn_ssb.parentCSS;
					parent_div.insertBefore(new_div, cont);
					new_div.appendChild(cont);
					
					cont.style.position = 'absolute';
					cont.style.left = cont.style.top = '0px';
					cont.style.width = cont.style.height = '100%';

					// adding new container into array
					fn_ssb.aConts[fn_ssb.N++] = cont;

					cont.sg = false;

					//creating scrollbar child elements
					cont.st = this.create_div('ssb_st', cont, new_div);
					if(fn_ssb.scrollStatus === "no"){
						cont.sb = this.create_div('ssb_sb ssb_sb_disable', cont, new_div);
					}else{
						cont.sb = this.create_div('ssb_sb', cont, new_div);
					}
					
					//=cont.su = this.create_div('ssb_up', cont, cont_clone);
					//=cont.sd = this.create_div('ssb_down', cont, cont_clone);

					// on mouse down processing
					cont.sb.onmousedown = function (e) {					
						if (! this.cont.sg) {
							if (! e){
								e = window.event;
							} 

							fn_ssb.asd = this.cont;
							this.cont.yZ = e.screenY;
							this.cont.sZ = cont.scrollTop;
							this.cont.sg = true;

							// new class name
							//this.className = 'ssb_sb ssb_sb_down';
						}
						return false;
					};
					// on mouse down on free track area - move our scroll element too
					cont.st.onmousedown = function (e) {
						if (! e) {
							e = window.event;
						}
						fn_ssb.asd = this.cont;

						fn_ssb.mouseY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
						for (var o = this.cont, y = 0; o !== null; o = o.offsetParent) {
							y += o.offsetTop;
						}
						this.cont.scrollTop = (fn_ssb.mouseY - y - (this.cont.ratio * this.cont.offsetHeight / 2) - this.cont.sw) / this.cont.ratio;
						this.cont.sb.onmousedown(e);
					};

					// onscroll - change positions of scroll element
					cont.ssb_onscroll = function () {
						this.ratio = (this.offsetHeight - 2) / this.scrollHeight;
						this.sb.style.top = Math.floor(fn_ssb.scrollbarTopPos + this.scrollTop * this.ratio) + 'px';					
						//=this.ratio = (this.offsetHeight - 2 * this.sw) / this.scrollHeight;
						//=this.sb.style.top = Math.floor(this.sw + this.scrollTop * this.ratio) + 'px';
					};

					// scrollbar width
					cont.sw = fn_ssb.scrollbarWidth;

					// start scrolling
					cont.ssb_onscroll();
					fn_ssb.refresh();
					
					// binding own onscroll event
					cont.onscroll = cont.ssb_onscroll;
					return cont;
				},

				// initialization
				initialization : function () {
					if (window.oper || (! window.addEventListener && ! window.attachEvent)) { return false; }

					// temp inner function for event registration
					function addEvent (o, e, f) {
						if (window.addEventListener) { 
							o.addEventListener(e, f, false); 
							fn_ssb.w3c = true; return true; 
						}
						if (window.attachEvent) {
							return o.attachEvent('on' + e, f);
						}
						return false;
					}

					// binding events
					addEvent(window.document, 'mousemove', fn_ssb.onmousemove);
					addEvent(window.document, 'mouseup', fn_ssb.onmouseup);
					addEvent(window, 'resize', fn_ssb.refresh);
					return true;
				},

				// create and append div finc
				create_div : function(c, cont, new_div) {
					var o = document.createElement('div');
					o.cont = cont;
					o.className = c;
					new_div.appendChild(o);
					return o;
				},
				// do clear of controls
				clear : function () {
					clearTimeout(fn_ssb.to);
					fn_ssb.sc = 0;
					return false;
				},
				// refresh scrollbar
				refresh : function () {
					for (var i = 0, N = fn_ssb.N; i < N; i++) {
						var o = fn_ssb.aConts[i];
						o.ssb_onscroll(); 
						o.st.style.width = o.sw + 'px';   					
						o.sb.style.height = Math.ceil(Math.max(o.sw * 0.5, o.ratio * o.offsetHeight) + 1) - (fn_ssb.scrollbarTopPos + 10) + 'px';
						
						//console.log("o.offsetHeigh: "+o.offsetHeigh);
						//console.log("fn_ssb.scrollbarTopPos: "+fn_ssb.scrollbarTopPos);
					}
				},
				// arrow scrolling
				arrow_scroll : function () {
					if (fn_ssb.sc !== 0) {
						fn_ssb.asd.scrollTop += 6 * fn_ssb.sc / fn_ssb.asd.ratio;
						fn_ssb.to = setTimeout(fn_ssb.arrow_scroll, fn_ssb.sp);
						fn_ssb.sp = 32;
					}
				},

				/* event binded functions : */
				// scroll on mouse down
				mousedown : function (o, s) {
					if (fn_ssb.sc === 0) {
						// new class name
						o.cont.sb.className = 'ssb_sb ssb_sb_down';
						fn_ssb.asd = o.cont;
						fn_ssb.sc = s;
						fn_ssb.sp = 400;
						fn_ssb.arrow_scroll();
					}
				},
				// on mouseMove binded event
				onmousemove : function(e) {
					if (! e) {
						e = window.event;
					}
					// get vertical mouse position
					fn_ssb.mouseY = e.screenY;

					if (fn_ssb.asd.sg) {
						fn_ssb.asd.scrollTop = fn_ssb.asd.sZ + (fn_ssb.mouseY - fn_ssb.asd.yZ) / fn_ssb.asd.ratio;
					}
				},
				// on mouseUp binded event
				onmouseup : function (e) {
					if (! e) {
						e = window.event;
					}
					document.onselectstart = '';
					fn_ssb.clear();
					
					fn_ssb.asd.sg = false;
				},
				trigger: function(){
					fn_ssb.scrollbar(fn_ssb.getId());
				}
				
			};

			objFn ={
				init: function(){
					// receive the triggered event from td-large-modal-overlay.js
					g.ev.on(g.events.afterLargeModalOverlay, function (event, data) {
						//var obj_id = data.scrollbarInfo.id,
						objFn.modal_id = data.scrollbarInfo.id;
						var	obj_height = data.scrollbarInfo.height,
							obj_status = data.scrollbarInfo.status;	
						
						fn_ssb.parentHeight = obj_height;
						fn_ssb.scrollStatus = obj_status;						

						$el.parent('.'+objFn.parentCSS).css('height', obj_height);
						objFn.modal_height = obj_height;
						//console.log(obj_status);
						
						if(obj_status == 'yes'){
							setTimeout(function(){fn_ssb.refresh(); }, 0);
							
							$el.next('.ssb_st').removeClass('ssb_st_disable');							
							$el.next().next('.ssb_sb').removeClass('ssb_sb_disable');
							$('#'+objFn.modal_id).css('overflow-y','auto');
						}else{
							$el.next('.ssb_st').addClass('ssb_st_disable');								
							$el.next().next('.ssb_sb').addClass('ssb_sb_disable');
							$('#'+objFn.modal_id).css('overflow-y','hidden');
						}
					});		
					
					// receive the triggered event from td-expand.js
					g.ev.on(g.events.afterExpand, function (event, data) {	
						
						var tdExpand_video_player = data.tdExpandInfo.videoplayer,
							tdExpand_height = data.tdExpandInfo.t_h,
							tdExpand_status = data.tdExpandInfo.status,
							current_height = 0,
							cont_max_height = 0,
							$td_modal_header = $('#'+objFn.modal_id).parent('.'+objFn.parentCSS).prev(),
							$td_modal_header_h2 = $td_modal_header.children('h2'),
							td_modal_header_h2_val = 0,
							header_space = 0;
							
						if($td_modal_header_h2.get(0)){
							td_modal_header_h2_val = parseInt($td_modal_header_h2.outerHeight(true));
						}
						
						if($td_modal_header.get(0)){
							header_space = parseInt($td_modal_header.css('marginTop').split('px')[0]) + td_modal_header_h2_val;
						}							

						if($('#'+objFn.modal_id).find(tdExpand_video_player).get(0)){
							current_height = parseInt($('#'+objFn.modal_id).find(tdExpand_video_player).outerHeight(true)) + parseInt(tdExpand_height);
						}else{
							current_height = parseInt(tdExpand_height);
						}
						
						if(window.innerWidth < 768){
							cont_max_height = window.innerHeight;
							cont_max_height = cont_max_height - header_space;
						}else{
							cont_max_height = document.documentElement.clientHeight * 0.85;							
							cont_max_height = cont_max_height - (header_space + 12);					
						}	
						
						if(current_height >= cont_max_height){
							$el.parent('.'+objFn.parentCSS).css('height',parseInt(cont_max_height)+'px');
						}else{
							$el.parent('.'+objFn.parentCSS).css('height',parseInt(current_height)+'px');
						}						

						setTimeout(function(){fn_ssb.refresh(); }, 0);

						if(current_height >= cont_max_height){
							$el.next('.ssb_st').removeClass('ssb_st_disable');							
							$el.next().next('.ssb_sb').removeClass('ssb_sb_disable');
							$('#'+objFn.modal_id).css('overflow-y','auto');
						}else{
							$el.next('.ssb_st').addClass('ssb_st_disable');								
							$el.next().next('.ssb_sb').addClass('ssb_sb_disable');
							$('#'+objFn.modal_id).css('overflow-y','hidden');							
						}
					
					});
					
					fn_ssb.trigger();
				}			
			};
			
			
			$.extend(objFn,fn_ssb);
					
			setTimeout(function(){objFn.init(); }, 500);
			

			

			return objFn;
		
        };

    // PLUGINS DEFINITION
    // ==================
    $.fn.tdScrollbar = g.getPluginDef(tdScrollbar, 'tdScrollbar');
    $.fn.tdScrollbar.defaults = {
		parentCSS: 'custom_scroll_parent_div',
		modalstatus: true
    };
	
    $(".td_rq_scrollbar").tdScrollbar();


}(global));}],
td_rq_checkbox: [function (global) {/* Source: src/js/td-checkbox.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Checkbox
 * Created by daniel.cho@td.com
 * Start date: 2016/10/28
 * @version 1.0
 *
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,

        tdCheckbox = function (element, options) {
            var $el = $(element),
				$checkbox_container = $el.find(".td-form-stacked"),
				//$checkbox_description = $('.td-label-content-wrapper'),			
				$checkbox_input = $checkbox_container.find("input"),
				$checkbox_label = $checkbox_container.find("label"),
				$checkbox = $checkbox_container.find(".td-label-content-wrapper");
				
            init();

            function init() {
				$checkbox.click(checkbox_toggle);	// add checkbox event
				apply_accessibility();
            }
            function checkbox_toggle(ev) {
			   //remove dotted line onclick only
/*
			   $checkbox.click(function(){
					$(this).blur(); 
				});		
*/				
				// if already checked, uncheck it
                var checked = $checkbox_input.attr('class');
                if(checked === undefined) {
                    checkbox_check();
                } else {
                    if ($checkbox_input.attr('class').indexOf('checked') >= 0) {
                        checkbox_uncheck();
                    }
                    else {
                        checkbox_check();
                    }
                }
            }
            function checkbox_check() {
                $checkbox_input.prop('checked', true);
                $checkbox_input.addClass("checked");		// add css class to make the checked state show
                $checkbox_label.addClass("label-checked");	// add css class to make the checked state show
                $checkbox.attr("aria-checked", "true");		  
            }
            function checkbox_uncheck() {
                $checkbox_input.prop('checked', false);
                $checkbox_input.removeClass("checked");		// remove css class to make the checked state not show
                $checkbox_label.removeClass("label-checked");	// remove css class to make the checked state not show
                $checkbox.attr("aria-checked", "false");
				$checkbox.attr('tabindex',0).focus();	
            }
			function apply_accessibility() {
                // make <input> hidden
                $checkbox_input.attr("aria-hidden", "true");
                $checkbox_input.attr("tabindex", -1);

				$checkbox.attr("aria-checked", "false"); 
                $checkbox.attr("tabindex", 0);	// make checkbox <div> get tab focus
                $checkbox.attr("role", "checkbox");	// accessibility: assign checkbox role

                // add keyboard accessibility for checkbox select
                $checkbox.on("keyup", function (event) {
                    if (event.which === 32 || event.which === 13) {
                        checkbox_toggle();
                    }
                });
				//remove dotted line onclick only
				$checkbox.click(function(){
					$(this).trigger('blur'); 
				});
                // prevent spacebar from scrolling page
                $checkbox.on("keydown", function (event) {
                    if (event.which === 32) {
                        event.preventDefault();
                    }
                });
            }			
        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdCheckbox = g.getPluginDef(tdCheckbox, 'tdCheckbox');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdCheckbox.defaults = {};

    $(".td-checkbox").tdCheckbox();
}(global));}],
td_rq_product_grid: [function (global) {/* Source: src/js/td-product-grid.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Product Grid (Credit Card/Account)
 * Created by daniel.cho@td.com
 * - Displaying the list of products (component #27)
 * Updated by daniel.cho@td.com
 * - Made it work with filter tool - it will hide the card(s) adding a'hide' CSS class name to card component #27
 * - Fixed the special offer indicator appears on the top of the card title.
 * - Set dynamic height for image and td-star-ratings
 * - Added callback function for setHeight and setBorder
 * - Made equal height work when image fully loaded.
 * Updated by daniel.cho@td.com (Rate)
 * - new option(cat_type) added and equal height for .rate div added.
 * Updated by daniel.cho@td.com
 * - Made equal height work for compare checkbox section
 *
 * 2018-02-015 by ayeon.chung@td.com
 * - Set equal height for <h2> tag per row
 * - Set h2 padding per row
 * - Removed getEqualHeight_daniel()
 * - Added an extra parameter index to afterCatalogueIndicator to notify the order of the card in a grid
 *
 * 2018-03-14 by ayeon.chung@td.com
 * - added tab interaction (afterTabActiveChanged)
 * - added expand interaction to re-render when the open state of Expand/collapse component gets triggered (afterExpandForOthers)
 * - defect fix on set_h2_padding()
 * - added a parameter card_type to afterCatalogueIndicator event handler, to cover a case with catalogue_cards_account & catalogue_cards_credit components under tabs
 *
 * 2018-03-22 by ayeon.chung@td.com
 * - removed a parameter card_type when trigger afterCatalogueIndicator event, to cover a case with multiple catalogue_cards_account components under tabs
 * - instead of card_type, will check <h2> ID of the each card
 *
 * 2018-08-01 by daniel.cho@td.com
 * - added "revert_equalHeight_range" and "chk_special_offer_type2_existence" functions for special offer indicator type 2
 */


(function (window) {

var $ = window.jQuery,
		$window = $(window),
		g = window.g,
		document = window.document,
		BREAKPOINT_MD_VALUE = g.bpSet('md'),
		BREAKPOINT_SM_VALUE = g.bpSet('sm'),

		tdProductGrid = function (element, options) {

			var $container = $(element),
				settings = $.extend($.fn.tdProductGrid.defaults, $container.metadata(), options),
				cat_type = settings.cat_type,
				TD_CATALOGUE_CARD = ".td_rq_catalogue-card",
				$TD_CATALOGUE_CARD = $container.find(TD_CATALOGUE_CARD),
				$product_h2 = $TD_CATALOGUE_CARD.find("h2"),
			//$product_image = $TD_CATALOGUE_CARD.find(".td-product-info-row > div:first-child > img"),
				$product_image_raing = $TD_CATALOGUE_CARD.find(".td-product-info-row > div:first-child"),
				$product_info = $TD_CATALOGUE_CARD.find(".td-product-info-row .rte"),
				$product_rate = $TD_CATALOGUE_CARD.find(".rate"),
				$product_last_div = $TD_CATALOGUE_CARD.find("> div > div:last-child"),
			//$product_details = $TD_CATALOGUE_CARD.find(".td-product-details"), // only works for Credit Card
				$product_details = $product_last_div.find("> div:first-child"), // works for both Credit Card / Account
				$product_button_div = $product_last_div.find('> div:not(.td-product-details,.td-product-compare,.rte)'),
				$product_compare_div = $product_last_div.find('> div.td-product-compare'),
				arry_rows,
				SPECIAL_OFFER_TYPE2_STATUS = false,
				SPECIAL_OFFER_TYPE2 = ".td-indicator-offer-type2",
				$product_info_content = $TD_CATALOGUE_CARD.find(".td-product-info-row .td-product-info-row-content"),
				$product_details2 = $product_last_div.find("div.td-product-details-2"); // second product details - works for both Credit Card;
			var num_col = 0;

			init();

			function init() {
				// Interation with indicators (Special Offer/Recently Viewed)
				g.ev.on(g.events.afterCatalogueIndicator, function (event, data) {
					event.preventDefault();

					var indicator_status = data.indicator_status,
						$card = data.obj,
						inx_card = data.index, // order of card in a grid which has a status change in indicator
						h2_padding_top = data.h2_padding_top;

					if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
///						if(indicator_status == true) $product_h2.css("height", prod_h2_height + "px");
						var row = parseInt(inx_card / num_col);
						set_h2_padding(arry_rows[row+1], $card, h2_padding_top);

					} else {
						//$product_h2.css({'height':'auto','padding-top':'0px'});
						$card.find('h2').css({'height':'auto','padding-top':h2_padding_top+'px'});
					}
				});

				// tab interaction, interaction when expand
				g.ev.on(g.events.afterTabActiveChanged + ' ' + g.events.afterExpandForOthers, function (event, $element) {
					event.preventDefault();
					setAdjustGrid(setBorder);
				});

				setAdjustGrid(setBorder);

				// wait until images load
				$window.load(function(){
					setAdjustGrid(setBorder);
				});

				// Check setHeight and setBorder again on a resize
				$window.resize(function(){
					setAdjustGrid(setBorder);
				});
				/*
				 setHeight();
				 setBorder();
				 */
				// Check setHeight and setBorder again on a resize
				/*
				 $window.resize(setHeight);
				 $window.resize(setBorder);
				 */
			}

			// set h2 padding to a row where the card belong
			function set_h2_padding(arry, $el, padding) {

//				$el.css({"height":"auto", "padding-top":padding+"px"});
				// set h2 padding only when card_id matches with the id of the object in arry
				if (!arry) return;
				var h2_id = $el.find("h2").attr('id');
				var found = false;
				for (var i=0; i < arry.length; i++) {
					if (found = ($(arry[i]).find("h2").attr('id') == h2_id)) break;
				}
				if (found) {
					$.each(arry,function(i, value){
						$(this).find("h2").css({"height":"auto", "padding-top":padding+"px"});
					});
					equalHeight_range(arry, "h2");
				}
			}

			function setAdjustGrid(callback){
				// Set initial height of all components #27
				setHeight();
				callback();
			}

			function setHeight() {
				if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
					//console.log('AAAAAAAAAAAAAA');
					/*	equal height for all items
					 var prod_h2_height =  getEqualHeight_daniel($product_h2, {full:true}),
					 //prod_image_height =  getEqualHeight_daniel($product_image, {full:true}),
					 prod_info_height =  getEqualHeight_daniel($product_info),
					 prod_details =  getEqualHeight_daniel($product_details),
					 prod_button_div = getEqualHeight_daniel($product_button_div);

					 $product_h2.css("height", prod_h2_height + "px");
					 //$product_image.css("height", prod_image_height + "px");
					 $product_info.css("height", prod_info_height + "px");
					 $product_details.css("height", prod_details + "px");
					 $product_button_div.css("height", prod_button_div + "px");
					 */

					/*
					 var prod_h2_height =  getEqualHeight_daniel($product_h2, {full:true});
					 $product_h2.css("height", prod_h2_height + "px");
					 */

					/* eqaul height for specific row Begin*/
					//var arry_rows = get_stored_rows();
					arry_rows = get_stored_rows();
					
					for(var i=0;i <= arry_rows.length-1;i++){
						equalHeight_range(arry_rows[i],"h2");

						// check the existence of special offer type2
						if(!SPECIAL_OFFER_TYPE2_STATUS) SPECIAL_OFFER_TYPE2_STATUS = chk_special_offer_type2_existence(arry_rows[i]);

						// instead of $product_info
						// equalHeight_range(arry_rows[i],".td-product-info-row .rte");
						if(SPECIAL_OFFER_TYPE2_STATUS){
							revert_equalHeight_range(arry_rows[i],".td-product-info-row .rte");
							equalHeight_range(arry_rows[i],".td-product-info-row .td-product-info-row-content");
						}else{
							equalHeight_range(arry_rows[i],".td-product-info-row .rte");
						}	

						// card image and td-star-ratings
						equalHeight_range(arry_rows[i],".td-product-info-row > div:first-child",true);

						if(cat_type == 'rates'){
							// instead of $product_rate
							equalHeight_range(arry_rows[i],".rate");
							equalHeight_range(arry_rows[i],"> div > div:last-child > div:not(.td-product-compare)");
						}else{

							// instead of $product_details
							equalHeight_range(arry_rows[i],"> div > div:last-child > div:first-child");
							// instead of $product_button_div
							equalHeight_range(arry_rows[i],"> div > div:last-child > div:not(.td-product-details,.td-product-compare,.rte,.td-product-details-2)");
							// instead of $product_details2
							equalHeight_range(arry_rows[i],"> div > div:last-child > div.td-product-details-2");
							
						}
						// instead of $product_compare_div
						equalHeight_range(arry_rows[i],"> div > div:last-child > div.td-product-compare");

					}
					/* eqaul height for specific row End*/

				} else {
					//console.log('BBBBBBBBBBB');
					arry_rows = get_stored_rows();

					$product_h2.css("height", "auto");
					//$product_image.css("height", "auto");
					$product_info_content.css("height","auto");
					$product_image_raing.css("height", "auto");
					$product_info.css("height", "auto");

					if(cat_type == 'rates'){
						$product_rate.css("height", "auto");
					}else{
						$product_details.css("height", "auto");
						$product_details2.css("height", "auto");
					}
					$product_button_div.css("height", "auto");
					$product_compare_div.css("height", "auto");
				}
			}
			
			// Check the existence of special offer type2
			function chk_special_offer_type2_existence(arry){
				var sot2_status = false;
				$.each(arry,function(){
					if(!sot2_status) sot2_status = $(this).find(SPECIAL_OFFER_TYPE2).hasClass('show');
				});
				return sot2_status;
			}

			// apply 'auto' height to each row
			function revert_equalHeight_range(arry,qry_str){
				$.each(arry,function(){
					$(this).find(qry_str).css("height", "auto");
				});
			}
			
			// apply equal height to each row
			function equalHeight_range(arry,qry_str,img_chk){

				if(img_chk == true){
					//$window.load(function(){
					var height = setEqualHeight_range(arry,qry_str);

					$.each(arry,function(){
						$(this).find(qry_str).css("height", height + "px");
					});
					//});
				}else{
					var height = setEqualHeight_range(arry,qry_str);

					$.each(arry,function(){
						$(this).find(qry_str).css("height", height + "px");
					});
				}
			}

			// return equal height for each row
			function setEqualHeight_range(arry,qry_str,options){
				var $docElement,
					height = 0,
					full=false;

				if(typeof options !== 'undefined'){
					if(options.full !== undefined) full = options.full;
				}

				$.each(arry,function(){
					$docElement = $(this).find(qry_str);
					$docElement.css({height: 'auto'});
					height = Math.max(height, $docElement.outerHeight(full));
				});

				return height;
			}

			// return an array that contains all rows
			function get_stored_rows(){
				var	obj_arry = new Array(),
					row = 0,
					col = 0,
					card_idx = 0;

				num_col = 0;

				if(window.innerWidth >= BREAKPOINT_MD_VALUE){ // MD & LG
					num_col = 3;
				}else if (window.innerWidth < BREAKPOINT_MD_VALUE && window.innerWidth >= BREAKPOINT_SM_VALUE) { //SM
					num_col = 2;
				}

				// store jquery objects from each row - BEGIN
				obj_arry[row] = new Array();
				$TD_CATALOGUE_CARD.each(function(index, element){

					// exclude the card that has "display: none"
					if(!$(element).hasClass('hide')){
						//index++;

						// prepare for next row begin
						if(((card_idx % num_col) == 0 ) || (num_col == 0 && card_idx != 0)){
							col = 0;
							row++;
							obj_arry[row] = new Array();
						}
						// prepare for next row end

						card_idx++;

						obj_arry[row][col] = $(element);
						col++;

					}
				});
				// store jquery objects from each row - END

				return obj_arry;
			}

			/*
			 // normal equal height
			 function getEqualHeight_daniel($elements, options) {
			 var $docElement,
			 height = 0,
			 full=false;

			 if(typeof options !== 'undefined'){
			 if(options.full !== undefined) full = options.full;
			 }

			 $elements.each(function () {
			 $docElement = $(this);
			 $docElement.css({height: 'auto'});
			 height = Math.max(height, $docElement.outerHeight(full));
			 });

			 return height;
			 }
			 */

			// set border
			function setBorder(){
				var count =  $container.find(TD_CATALOGUE_CARD+':not(.hide)').length,	//$TD_CATALOGUE_CARD.length,
					column = 1;

				initBorder();
				//renderBorder();

				if(window.innerWidth < BREAKPOINT_SM_VALUE){ // XS
					renderBorder(column);
				}else if (window.innerWidth < BREAKPOINT_MD_VALUE && window.innerWidth >= BREAKPOINT_SM_VALUE) { //SM
					column = 2;
					renderBorder(column);
				}else { // MD & LG
					column = 3;
					renderBorder(column);
				}

				$container.animate({opacity: 1});
			}

			// initialize border
			function initBorder(){
				$TD_CATALOGUE_CARD.find('> div').removeClass('border-bottom-removed-byJS').removeClass('border-right-removed-byJS').removeClass('border-right-added-byJS');
			}

			function renderBorder(col){
				for(var i=0;i <= arry_rows.length-1;i++){
					$.each(arry_rows[i],function(idx){

						// remove border right from last item for each row
						if((idx+1)%col == 0){
							$(this).find('> div').addClass('border-right-removed-byJS');
						}else{
							$(this).find('> div').addClass('border-right-added-byJS');
						}

						// remove border botton from all cards in last row
						if(i == arry_rows.length-1){
							$(this).find('> div').addClass('border-bottom-removed-byJS');
						}
					});
				}
			}
		};


	// PLUGINS DEFINITION
	// ==================
	$.fn.tdProductGrid = g.getPluginDef(tdProductGrid, 'tdProductGrid');

	// DEFAULT SETTINGS
	// ================
	$.fn.tdProductGrid.defaults = {
		cat_type: 'common'
	};

	$(".td_rq_product_grid").tdProductGrid();
}(global));}],
td_rq_filter_tool: [function (global) {/* Source: src/js/td-filter-tool.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Filter Tool - preselecting item(s)
 * Created by daniel.cho@td.com
 * @version 1.0
 *
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,

        tdFilterTool = function (element, options) {
            var $el = $(element),
				$select = $el.find('select.td-select'),
				objFn={};

			objFn ={
				init: function(){
					//console.log(objFn.get_param('ftparm'+1));
		
					$select.each(function(idx) {
						var g_param = objFn.get_param('ftparm'+(idx+1));
						if(typeof g_param !== 'undefined'){
							$(this)[0].options.selectedIndex = g_param -1;
						}
					});
						
				},
				get_param: function(name){
				   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
					  return decodeURIComponent(name[1]);
				}
			}
			
			$.extend(objFn);
						
			objFn.init();

			return objFn;			

        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdFilterTool = g.getPluginDef(tdFilterTool, 'tdFilterTool');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdFilterTool.defaults = {};

    $(".td_rq_filter-tool").tdFilterTool();
}(global));}],
td_rq_sms_text_message: [function (global) {/* Source: src/js/td-sms-text-message.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD SMS Text Message
 * Created by daniel.cho@td.com
 * @version 1.0
 * 
 * Fixed by daniel.cho@td.com
 * 	- focus stuck in inputbox with Tab key - added  "(key != 9 && text == '')" in keydown function to exclude Tab key
 *  - Added 'shiftKey' to exclude special characters
 *  - NumPad added
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,

        tdSMSTextMessage = function (element, options) {
            var $el = $(element),
				$phone_input = $el.find('input[name=your-phone]'),
				$msg_result_success = $el.find('.msg_result_success'),
				$msg_result_error = $el.find('.msg_result_error'),
				objFn={};
 
			objFn ={
				init: function(){
					objFn.phone_formatting($phone_input);
				},
				phone_formatting: function($obj){
					
					$obj.keydown(function(evt){ 
					
					
						var key = evt.keyCode || evt.charCode,
							maxlength = 10;
							
							
							console.log("Key: "+key);

						if(key != 0 && key != 229){
							if (( ((key >= 48 && key <= 57) || (key >= 96 && key <= 105)) && !evt.shiftKey) && objFn.phone_text_trim(this.value).length < maxlength){ // number keys - (key >= 96 && key <= 105) desktop keyboard
								this.value = objFn.phone_masking(this.value);
								
								// message behaviour
								$msg_result_success.css('display','none');
								$msg_result_success.attr('aria-hidden',true);
								$msg_result_error.css('display','none');
								$msg_result_error.attr('aria-hidden',true);
								
							}else{		

								var activeEl = document.activeElement,
									activeElTagName = activeEl ? activeEl.tagName.toLowerCase() : null;								
								
								if ((activeElTagName == "input") && /^(?:text|search|password|tel|url)$/i.test(activeEl.type)) {
									
									// get selected text in inputbox
									var start = this.selectionStart,
										end = this.selectionEnd,
										text = '';
									
									if(key >= 48 && key <= 57){ // select text only for number keys
										text = this.value.slice(start, end);
									}
									
									// return nothing when any keys except backspace key/tab key with no selected text
									if( (key != 8 && text == '' ) && (key != 9 && text == '') ){
										evt.preventDefault();
										return false;	
									}
								}								
/*															
								if(key != 8){ // backspace key
									evt.preventDefault();
									return false;	
								}	
*/								
							}
						}
					});	
					
					// Android
					$obj.keyup(function(evt){ 			
						var key = evt.keyCode || evt.which, // || evt.charCode,
							maxlength = 11;

						if(key == 0 || key == 229){ // only Android returns this number(s) for all keys

							key = objFn.get_keycode_android(this.value);;
							
							if ((key >= 48 && key <= 57) && objFn.phone_text_trim(this.value).length < maxlength){ // number keys - (key >= 96 && key <= 105) desktop keyboard
								this.value = objFn.phone_masking(this.value);
								
							}else{					
								this.value = this.value.substring(0, this.value.length - 1);
								evt.preventDefault();
								return false;	
							}
						}							
							
					});
					
				},
				get_keycode_android: function(str){
					return str.charCodeAt(str.length - 1);
				},
				phone_masking: function(txt){
					var g_input = objFn.phone_text_trim(txt),
						count = parseInt(g_input.length),
						result = "";

					if(count == 1 || count == 2){
						result = '('+g_input;
					}else if(count == 3){
						result = '('+g_input+')';
					}else if(count > 3 && count < 7){
						result = '(' + g_input.substring(0,3) + ') ' + g_input.substring(3, 6);
					}else if(count >= 7){
						result = '(' + g_input.substring(0,3) + ') ' + g_input.substring(3, 6) + '-' +  g_input.substring(6);
					}

					return result;						
				},
				phone_text_trim: function(txt){
					return txt.replace(/\s/g, '').replace(/[\(\)\-']+/g,'')
				}
				
			}
			
			$.extend(objFn);
						
			objFn.init();

			return objFn;			

        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdSMSTextMessage = g.getPluginDef(tdSMSTextMessage, 'tdSMSTextMessage');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdSMSTextMessage.defaults = {};

    $(".td_rq_sms-text-message").tdSMSTextMessage();
}(global));}],
td_rq_anchor_down: [function (global) {/* Source: src/js/td-anchor-down.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Achor Down
 * Created by daniel.cho@td.com
 * @version 1.0
 * - scrolling down to targeted element in a page/TDCT Modal Window
 * 
 * @versoin 1.1
 * - Added a new option 'targetedElInfo' and g.ev.trigger(g.events.afterAnchorDown) to target a specific component sending 'gTargetedElInfo_type' and 'gTargetedElInfo_id' values
 */

(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,

        tdAnchorDown = function (element, options) {
            var $el = $(element),
				settings = $.extend($.fn.tdAnchorDown.defaults, $el.metadata(), options),
				scrollable_area = settings.scrollable_area,
				gID = settings.targetedID,
				gOffset_top = settings.offset_top,
				modal_id = settings.modalID,
				scrolling_speed = settings.scrolling_speed,
				gTargetedElInfo_status = settings.targetedElInfo.status,
				gTargetedElInfo_type = settings.targetedElInfo.type,
				gTargetedElInfo_id = settings.targetedElInfo.id,				
				$header_nav = $(".td-header-nav"), // TDCT Top Nav
				objFn={};
  
			objFn ={
				init: function(){

					objFn.scroll_down($el);
				},
				scroll_down: function($obj){
					$obj.on("click", function(event){
						event.preventDefault();
						
						// Send ID and Type(component type eg, expandCollapse) to targeted component
						if (gTargetedElInfo_status) g.ev.trigger(g.events.afterAnchorDown,[{type:gTargetedElInfo_type,id:gTargetedElInfo_id}]);	
		
						var distance = $('#'+gID).offset().top;
						if(typeof modal_id !== 'undefined'){ // scrolling within a modal window
							scrollable_area = '#'+modal_id+' .td_rq_scrollbar';
							distance = distance - $(scrollable_area).offset().top - gOffset_top;
						}else if($header_nav.get(0) && typeof modal_id === 'undefined') { // scrolling within a page for TDCT Top Nav
							distance = distance - $header_nav.height() - gOffset_top;
						}else{ // scrolling within a page
							distance = distance - gOffset_top;
						}						
						$('#'+gID).attr('tabindex','0');
						$(scrollable_area).animate({ 
							scrollTop: distance
						}, scrolling_speed, function(){
							$('#'+gID).focus(); 
						});
					});
				}
				
			}
			
			$.extend(objFn);
						
			objFn.init();

			return objFn;			

        };


    // PLUGINS DEFINITION
    // ==================
    $.fn.tdAnchorDown = g.getPluginDef(tdAnchorDown, 'tdAnchorDown');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdAnchorDown.defaults = {
		scrollable_area: 'body, html',
		offset_top: 0,
		scrolling_speed: 600,
		targetedElInfo:{
			status: false,
			type: 'expandCollapse',
			id: ''
		}
	};

    $(".td_rq_anchor-down").tdAnchorDown();
}(global));}],
td_rq_dropdown_redirect: [function (global) {/* Source: src/js/td-dropdown-redirect.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Dropdown Redirect
 * Created by danielcho80@gmail.com on 2016-04-19
 * @version 1.0
 * Updated by danielcho80@gmail.com on 2017-02-28
 * @version 1.1
 * - replaced keypress event with keydown event
 * - added a new object (.td_rq_dropdown_redirect.td-select) for TDCT dropdown 
 * - added a new option 'fullUrlValue'
 * - added change event to support iOS/Android devices
 * @version 1.1.1
 * - added 'pageshow' event to force to select the first dropdown item whenever an user views a page. (Fixed for the browser "Back" button)
 */

(function (window) {

var $ = window.jQuery,
        g = window.g,
		td_dropdown_redirect = function(element, option){

			var $el = $(element);
			var settings = $.extend($.fn.tdDropdownRedirect.defaults, $el.metadata(), option),
				fullUrlValue = settings.fullUrlValue,
				click_count = 0,
				mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
			
			var get_targetelement = settings.targetelement;
			
			var filterPullDown = function(e) {
				var seltedIdx = e.currentTarget.options.selectedIndex;
				var pram = e.currentTarget.options[seltedIdx].value;
				var url = window.location.pathname+pram;
				//console.log(url);
				//console.log(click_count);
				
				if(pram != 'default' && click_count > 0){
					if(fullUrlValue == true) {
						document.location.href = pram;
					}else{
						document.location.href = url;
					}
				} 
				
				click_count++;
				
			}
			
			if(mobile){
				$el.on("change",function (e) {
					click_count++;
					filterPullDown(e);
				});			
			}else{
				$el.on("click",function (e) {
					filterPullDown(e);
				});		

				$el.on("keydown",function (e) {
					if(e.which == 13){//Enter key pressed
						click_count++;
						e.preventDefault();
						filterPullDown(e);
					}			
				});				
			}
			
			$(window).on("pageshow", function() { 
				$el.prop('selectedIndex',0);
			});
			
		};
 
    // PLUGINS DEFINITION
    // ==================
    $.fn.tdDropdownRedirect = g.getPluginDef(td_dropdown_redirect, 'tdDropdownRedirect');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdDropdownRedirect.defaults = {
		fullUrlValue: false
    };

    $("select.form-control.auto_redirect, .td_rq_dropdown_redirect.td-select").tdDropdownRedirect();
}(global));}]
}));
