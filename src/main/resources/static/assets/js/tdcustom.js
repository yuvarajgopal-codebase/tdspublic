/********************** DON'T MODIFY THIS DIRECTLY! ***********************/
/*jslint white: true */
(function loadModules(global, modules) {
    'use strict';

	// Store modules and global to global variables to trigger jquery modules manually
	window.modules_ext = modules;
	window.global_ext = global;	
	
    // Loads required modules in the page.
    // First filter the module list, then executes each of the parts.

    var id, funcId, tag, parts,
        doc = global.document,
        modulesToLoad = ["td_rq_common"],
        tags = doc.querySelectorAll('[class^="td_rq"]');

    for (id = 0; id < tags.length; id += 1) {
        tag = tags[id].className.split(" ")[0];
        tag = tag.replace(/-/g, '_'); // replace all '-' with '_';
        if (modulesToLoad.indexOf(tag) === -1) {
            modulesToLoad.push(tag);
        }
    }

    for (id = 0; id < modulesToLoad.length; id += 1) {
        tag = modulesToLoad[id];
        if (modules.hasOwnProperty(tag)) {
            if (global.console) {
                global.console.log(tag);
            }
            parts = modules[tag];
            for (funcId = 0; funcId < parts.length; funcId += 1) {
                parts[funcId](global);
            }
        }
    }
}(this, {
td_rq_bulletin_list: [function (global) {/* Source: src/js/tdam/td-bulletin-list.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,
		tdBulletinList = function (element, options) {
            var $ctaContainer = $(element);
            var $cta_action = $ctaContainer.find(".td-bulletin-heading");
			 setHeight();
            function setHeight() {
                //var height = g.getEqualHeight($cta) + 40;
               
                var cta_action_height = g.getEqualHeight($cta_action);
                if ($window.width() > 750) {
                    
                    $cta_action.css("height", cta_action_height + "px");
                    // $cta.css("height", height + "px");
                } else {
                    
                    $cta_action.css("height", "auto");
                    // $cta.css("height", "auto");
                }
				

            }
 };


    // PLUGINS DEFINITION 
    // ==================
    $.fn.tdBulletinList = g.getPluginDef(tdBulletinList, 'tdBulletinList');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdBulletinList.defaults = {
		sm_cols_per_row: 2,
		md_cols_per_row: 3,
		lg_cols_per_row: 3
	};

    $(".td_rq_bulletin_list").tdBulletinList();
}(global));}],
td_rq_device_swipe: [function (global) {/* Source: src/js/tdam/td-device-swipe.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Number section (Leadership Team on About Us page)
 * Created by Saravana
 * - Displaying the numbers related to the TD
 * - The core code is copied from td-tools-swipe.js (TDCT Component 11.4.2)
 *
 * - swipe for the content
 * - equal height for the content.
 * - accessibility 
 */


(function (window) {

    var $ = window.jQuery,
		$window = $(window),
		document = window.document,
		BREAKPOINT_SM_VALUE = g.bpSet('sm') - 1,
		BREAKPOINT_MD_VALUE = g.bpSet('md') - 1;	

    var tdSwipeable = function (element, options) {

        var $element = $(element),
			$element_css = $element.attr('class'),
			
            hiddenIndex = [], 
		    hiddenContent = [],
            settings = $.extend({}, $.fn.tdSwipeable.defaults, $element.metadata(), options),
			$slicker = $element.children().find(".td-device-swipe-content"),
			slick_content_copy = ".slick-content-copy",
			$slick_content_copy = $element.find(slick_content_copy),
			slick_content_copy_rte = ".rte",
			$slick_content_copy_rte = $element.find(slick_content_copy_rte),
			//win_inner_width = window.innerWidth,
			SLICK_CONTENT_SECOND_ROW = ".slick-content-second-row",
			SLICK_BLOCK_SECOND_ROW = ".slick-block-second-row",
			SLICK_DOTS = ".slick-dots";

        //find unslick name and put in array
        $slicker.slick({
            arrows: settings.arrows, 
            dots: settings.dots,
            infinite: settings.infinite,
            accessibility: settings.accessibility
        });

		$slicker.on('swipe', function(event, slick, direction){
			accessibility();
		});		
		$slicker.on('beforeChange afterChange', function(){
			accessibility();
		});
		
		g.ev.on(g.events.afterTabActiveChanged, function (event, $el) { 
			event.preventDefault();
			
			if ($el.has($element).length > 0){
				
				 if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
					if(window.innerWidth < BREAKPOINT_MD_VALUE) {
						if($element.children('div').length < 2){
							$slick_content_copy.css({height: 'auto'});
							$slick_content_copy_rte.css({height: 'auto'});	

							var str = $slicker.find('.td-device-swipe').attr('class'),
								regex = new RegExp("\\btd-col-sm-12\\b");
							if(regex.test(str)){
								$slicker.find('.td-device-swipe').css('margin-bottom','30px');
							}							
						}else{
							scc_div_rte(scc_div);	
						}
					}else{
						// sc_equalHeight();
						if($element.children('div').length < 2){
							$slicker.find('.td-device-swipe').css('margin-bottom','inherit');
						}
						scc_div_rte(scc_div);					
					}						 			
				}else{
						 
					$slicker.slick("unslick");
					$slicker.slick({
						arrows: settings.arrows,
						dots: settings.dots,
						infinite: settings.infinite,
						accessibility: settings.accessibility,
					});
				
					$slick_content_copy.css({height: 'auto'});
					$slick_content_copy_rte.css({height: 'auto'});
					
					if($element.children('div').length < 2){
						$slicker.find('.td-device-swipe').css('margin-bottom','inherit');
					}
					chk_slick_dots();	
				}
				
				
				// Accessibility
				accessibility();
				
			}
		});
		
		function scc_div_rte(callback){
			//sc_equalHeight('.td-row.td-swipeable-blocks '+slick_content_copy_rte);	
			switch($element.children('div').length){
				case 1:
					sc_equalHeight(' > div:nth-child(1) '+slick_content_copy_rte);	
					break;
				case 2:
					if(window.innerWidth >= BREAKPOINT_MD_VALUE && $element.find(' > div:nth-child(1)').css('float') == 'left'){
						sc_equalHeight(' > div '+slick_content_copy_rte);	
					}else{
						sc_equalHeight(' > div:nth-child(1) '+slick_content_copy_rte);	
						sc_equalHeight(' > div:nth-child(2) '+slick_content_copy_rte);						
					}
					break;
				default:
					sc_equalHeight(' > div:nth-child(1) '+slick_content_copy_rte);	
					sc_equalHeight(' > div:nth-child(2) '+slick_content_copy_rte);					
			}
			callback();
		}			
		function scc_div(){
			//sc_equalHeight(slick_content_copy);
			
			switch($element.children('div').length){
				case 1:
					sc_equalHeight(' > div:nth-child(1) '+slick_content_copy);	
					break;
				case 2:
					if(window.innerWidth >= BREAKPOINT_MD_VALUE && $element.find(' > div:nth-child(1)').css('float') == 'left'){
						sc_equalHeight(' > div '+slick_content_copy);	
					}else{
						sc_equalHeight(' > div:nth-child(1) '+slick_content_copy);	
						sc_equalHeight(' > div:nth-child(2) '+slick_content_copy);								
					}				
					break;
				default:
					sc_equalHeight(' > div:nth-child(1) '+slick_content_copy);	
					sc_equalHeight(' > div:nth-child(2) '+slick_content_copy);					
			}					
		}
        if (window.innerWidth >= BREAKPOINT_MD_VALUE) {
            $slicker.slick('unslick');
			if (window.innerWidth < BREAKPOINT_MD_VALUE) {
				if($element.children('div').length < 2){
					$slick_content_copy.css({height: 'auto'});
					$slick_content_copy_rte.css({height: 'auto'});	
						
					//console.log($slicker.find('.slick-block').attr('class'));	
					var str = $slicker.find('.td-device-swipe').attr('class'),
						regex = new RegExp("\\btd-col-sm-12\\b");
					if(regex.test(str)){
						$slicker.find('.td-device-swipe').css('margin-bottom','30px');
					}		
				}else{
					scc_div_rte(scc_div);	
				}
			}else{
				if($element.children('div').length < 2){
					$slicker.find('.td-device-swipe').css('margin-bottom','inherit');
				}
				scc_div_rte(scc_div);	
			}
        }else{
			$slicker.slick('slickAdd', $element.children().find(SLICK_CONTENT_SECOND_ROW).children(SLICK_BLOCK_SECOND_ROW));
			$slick_content_copy.css({height: 'auto'});
			$slick_content_copy_rte.css({height: 'auto'});

			if($element.children('div').length < 2){
				$slicker.find('.td-device-swipe').css('margin-bottom','inherit');
			}			
			chk_slick_dots();			
		}
		// Accessibility
		accessibility();		
		
        var scrollTop;
        $window.on('resize', function () {
            if (window.innerWidth >= BREAKPOINT_MD_VALUE) {
                $slicker.slick("unslick");
                $slicker.slick('slickRemove', false);
                //$element.children().find(".slick-content").children(SLICK_BLOCK_SECOND_ROW).appendTo(SLICK_CONTENT_SECOND_ROW);
				$element.children().find(".td-device-swipe-content").children(SLICK_BLOCK_SECOND_ROW).appendTo($element.find(SLICK_CONTENT_SECOND_ROW));

				if (window.innerWidth < BREAKPOINT_MD_VALUE) {
					if($element.children('div').length < 2){
						$slick_content_copy.css({height: 'auto'});
						$slick_content_copy_rte.css({height: 'auto'});			

						var str = $slicker.find('.td-device-swipe').attr('class'),
							regex = new RegExp("\\btd-col-sm-12\\b");
						if(regex.test(str)){
							$slicker.find('.td-device-swipe').css('margin-bottom','30px');
						}					
					}else{
						scc_div_rte(scc_div);	
					}
				}else{
					// sc_equalHeight();
					if($element.children('div').length < 2){
						$slicker.find('.td-device-swipe').css('margin-bottom','inherit');
					}
					scc_div_rte(scc_div);					
				}					
            } else {
                if(!$slicker.hasClass('slick-initialized')) {
                    $slicker.slick("unslick");
                    $slicker.slick({
                        arrows: settings.arrows,
                        dots: settings.dots,
                        infinite: settings.infinite,
                        accessibility: settings.accessibility,
                    });
                    $slicker.slick('slickAdd', $element.children().find(SLICK_CONTENT_SECOND_ROW).children(SLICK_BLOCK_SECOND_ROW));
                }
				
				$slick_content_copy.css({height: 'auto'});
				$slick_content_copy_rte.css({height: 'auto'});

				if($element.children('div').length < 2){
					$slicker.find('.td-device-swipe').css('margin-bottom','inherit');
				}
						
				chk_slick_dots();
            }
			
			accessibility();			
			/*
			sc_equalHeight(slick_content_copy);
			sc_equalHeight(slick_content_copy_rte);
			*/			
        });
		
		// Remove margin-top from .slick-list div when there are no dots
		function chk_slick_dots(){
			if(typeof $element.find(SLICK_DOTS).get(0) === 'undefined') {
				$element.find('.slick-list').css('margin-top','0px');
			}
		}
		
		function sc_equalHeight(qry){
			var $obj = $element.find(qry),
            height = g.getEqualHeight($obj);
            $obj.css("height", height+"px");
		}
		
		function accessibility(){
			// accessibility
			if($element.find(".td-device-swipe").get(0)) $element.find(".td-device-swipe").attr('aria-hidden','false');	
			if($element.find(SLICK_BLOCK_SECOND_ROW).get(0)) $element.find(SLICK_BLOCK_SECOND_ROW).attr('aria-hidden','false');	
			if($element.find(SLICK_DOTS).get(0)) {
				$element.find(SLICK_DOTS+" li").attr('aria-hidden','true');	
				$element.find(SLICK_DOTS+" li button").attr('tabindex','-1');	
			}
			
		}
    };
    var setTimeoutConst, setTimeoutConstOut;
    // PLUGINS DEFINITION
    // ==================
    $.fn.tdSwipeable = g.getPluginDef(tdSwipeable, 'tdSwipeable');
    // DEFAULT SETTINGS
    // ================
    $.fn.tdSwipeable.defaults = {
        infinite: false,
        dots: true,
        arrows: false,
        accessibility: false
    };
    $(".td-number-blocks").tdSwipeable();
}(global));}],
td_rq_leadership_grid: [function (global) {/* Source: src/js/tdam/td-leadership-grid.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Leadership Grid (Leadership Team on About Us page)
 * Created by ayeon.chung@td.com
 * - Displaying the list of leadership members
 * - The core code is copied from td-product-grid.js (TDCT Component 27)
 *
 * - dynamic height for image
 * - callback function for setHeight and setBorder
 * - equal height when image fully loaded. 
 */


(function (window) {

var $ = window.jQuery,
        $window = $(window),
        g = window.g,
        document = window.document,
		BREAKPOINT_LG_VALUE = g.bpSet('lg'),
		BREAKPOINT_MD_VALUE = g.bpSet('md'),
		BREAKPOINT_SM_VALUE = g.bpSet('sm'),

        tdLeadershipGrid = function (element, options) {
			
            var $container = $(element),
			settings = $.extend($.fn.tdLeadershipGrid.defaults, $container.metadata(), options),
			LEADERSHIP_CARD = ".leadership-card",
			$LEADERSHIP_CARD = $container.find(LEADERSHIP_CARD),
			$leadership_name = $LEADERSHIP_CARD.find(".leadership-name"),
			$profile_image = $LEADERSHIP_CARD.find(".profile-photo"),
			$leadership_title = $LEADERSHIP_CARD.find(".leadership-info"),
			arr_rows;

            init();

            function init() {


//				$window.load(function(){// wait until images load
                $(document).ready(function(){// will not wait until images load because the image height is already fixed
					setAdjustGrid(setBorder);
				});

                // Check setHeight and setBorder again on a resize
				$window.resize(function(){
					setAdjustGrid(setBorder);
				});

				g.ev.on(g.events.afterTabActiveChanged, function (event, $wrapper) {
					event.preventDefault();
					setAdjustGrid(setBorder);
				});
			}

			function setAdjustGrid(callback){
				// Set initial height of all leadership components
				 setHeight();
				 callback();
			}

            function setHeight() {
                if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
					/* eqaul height for specific row Begin*/
					arr_rows = get_stored_rows();

					for(var i=0;i <= arr_rows.length-1;i++){
						equalHeight_range(arr_rows[i],".leadership-name");
						equalHeight_range(arr_rows[i],".leadership-info.rte");
						equalHeight_range(arr_rows[i],".profile-photo-wrapper");

					}
					/* eqaul height for specific row End*/

                } else {
					arr_rows = get_stored_rows();

					$leadership_name.css("height", "auto");
					$profile_image.css("height", "auto");
				    $leadership_title.css("height", "auto");

                }
            }
			
			// apply equal height row by row
			function equalHeight_range(arry,qry_str){
				var height = setEqualHeight_range(arry,qry_str);

				$.each(arry,function(){
					$(this).find(qry_str).css("height", height + "px");
				});
			}	
			
			// return equal height for each row
			function setEqualHeight_range(arry,qry_str,options){
				var $docElement,
					height = 0,
					full=false;
					
				if(typeof options !== 'undefined'){
					if(options.full !== undefined) full = options.full;
				}
				
				$.each(arry,function(){
					$docElement = $(this).find(qry_str);
					$docElement.css({height: 'auto'});
					height = Math.max(height, $docElement.outerHeight(full));
				});	
				
				return height;
			}
			
			// return an array that contains all rows
			function get_stored_rows(){
				var num_col = 0,
					obj_arry = new Array(),
					row = 0,
					col = 0,
					card_idx = 0;
				if(window.innerWidth >= BREAKPOINT_LG_VALUE) {
					num_col = settings.lg_cols_per_row;
				} else if(window.innerWidth >= BREAKPOINT_MD_VALUE){
					num_col = settings.md_cols_per_row;
				}else if (window.innerWidth >= BREAKPOINT_SM_VALUE) {
					num_col = settings.sm_cols_per_row;
				}
				
				// store jquery objects from each row - BEGIN	
				obj_arry[row] = new Array();
				$LEADERSHIP_CARD.each(function(index, element){

					// prepare for next row begin
					if(((card_idx % num_col) == 0 ) || (num_col == 0 && card_idx != 0)){
						col = 0;
						row++;
						obj_arry[row] = new Array();
					}
					// prepare for next row end

					card_idx++;

					obj_arry[row][col] = $(element);
					col++;
				});		
				// store jquery objects from each row - END
				
				return obj_arry;
			}
			
			// set border
			function setBorder(){	
				var count =  $container.find(LEADERSHIP_CARD).length;	//$LEADERSHIP_CARD.length,

				initBorder();
				//renderBorder();	
				
				if(window.innerWidth < BREAKPOINT_SM_VALUE){ // XS
					renderBorder(1);
				}else if (window.innerWidth < BREAKPOINT_MD_VALUE ) { //SM
					renderBorder(settings.sm_cols_per_row);
				}else if (window.innerWidth < BREAKPOINT_LG_VALUE ) { //MD
					renderBorder(settings.md_cols_per_row);
				}else { // LG
					renderBorder(settings.lg_cols_per_row);
				}					
			}
			
			// initialize border
			function initBorder(){
				$LEADERSHIP_CARD.find('> div').removeClass('border-bottom-removed-byJS').removeClass('border-right-removed-byJS').removeClass('border-right-added-byJS');
			}
			
			function renderBorder(col){
				for(var i=0;i <= arr_rows.length-1;i++){
					$.each(arr_rows[i],function(idx){
						
						// remove border right from last item for each row
						if((idx+1)%col == 0){
							$(this).find('> div').addClass('border-right-removed-byJS');
						}else{
							$(this).find('> div').addClass('border-right-added-byJS');
						}
						
						// remove border botton from all cards in last row
						if(i == arr_rows.length-1){
							$(this).find('> div').addClass('border-bottom-removed-byJS');
						}
					});						
				}	
			}		
        };


    // PLUGINS DEFINITION 
    // ==================
    $.fn.tdLeadershipGrid = g.getPluginDef(tdLeadershipGrid, 'tdLeadershipGrid');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdLeadershipGrid.defaults = {
		sm_cols_per_row: 2,
		md_cols_per_row: 3,
		lg_cols_per_row: 3
	};

    $(".td_rq_leadership_grid").tdLeadershipGrid();
}(global));}],
td_rq_modal_accessible: [function (global) {/* Source: src/js/tdam/td-modal-accessible.js */
"use strict";
/*-----------------------------------------------------------------------------------

 * TD - This code is exclusively the property of TD and all
 * rights therein, including without limitation copyrights  are reserved by TD.
 * Any reproduction, disclosure, distribution or unauthorized use is strictly prohibited.
 -----------------------------------------------------------------------------------*/

/**
 * TD Modal Accessible
 * based on td-large-modal-overlay.js
- will not set aria-hidden="true" to the elements underneath the modal window
 */

(function(window) {

var $ = window.jQuery,
        $window = $(window),
        document = window.document,
        $doc = $(document),
        $previous_focus, // save the element on the page that had focus before the modal opened
        previous_focus_e,

        /**
         * Modal
         */
        modal = function(element, options) {
            var $container = $(element),
                settings = $.extend($.fn.tdModal.defaults, $container.metadata(), options),
                custom_arrangement = settings.custom_arrangement,
                modal_status = settings.show,
                clone_btn_close = settings.clone_btn_close,
                clone_btn_close_float = settings.clone_btn_close_float,
                anchor_status = settings.anchor.status,
                anchor_targetedID = settings.anchor.targetedID,
                anchor_scrolling_speed = settings.anchor.scrolling_speed,
                accessibility_modal_description = settings.accessibility_modal_description,
                td_modal_size_status = settings.size.status,
                td_modal_width = settings.size.width,
                td_modal_height = settings.size.height,
                td_overlay_close = settings.overlayClose,
                td_modal = ".td-modal",
                $modal = $container.find(td_modal),
                td_modal_content = ".td-modal-content",
                $close_button = $modal.find(".close-button"),
                elements_to_hide = [$(".td-header-nav"), $("footer"), $(".td-left-menu"), $(".td-right-menu"), $(".td-header"), $(".td-quick-actions-mobile"), $(".td-sticky-nav"), $(".td-contentarea")], // elements to hide from accessibility when modal is opened
                MODAL_FOCUS_CLASSNAME = 'content_focus_element', // class name of element to put focus on when modal opens
                ID_NAME = 'modal', // id for this component
                MODAL_SHOW_CLASSNAME = "td-modal-show",
                HIDDEN_ELEMENT_CLASSNAME = "td-modal-hidden-element", // apply this class to hide non-modal elements when modal is shown, this marks the element so we know to unhide it when modal is closed
                DATA_ATTR_INITIATED = 'initiated', // data attributed to signify that modal has been initiated
                ANIMATION_DURATION = 200, // fade in-out animation duration
                VIDEO_CLASSNAME = ".td-video-player .video-js",
                td_modal_quoter_item = 'td-modal-quoter-item',
                $item = $modal.find('.' + td_modal_quoter_item),
                breakpoint_sm = g.bpSet('sm') - 1,
                $cloned_btn,
                $tdModalHeading = $container.find(".td-heading"),
            //$tdModalContent = $container.find(".td-modal-body-content"),
                $tdModalContent = $container.find(".td-modal-body-content .rte"),
                mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())),
                mobile_iOS = (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase())),
                $scrollable_area = $container.find('.td_rq_scrollbar'),
                $td_modal_header = $container.find('.td-modal-header'),
                $td_modal_header_h2 = $td_modal_header.children('h2'),
                $td_modal_footer = $container.find('.td-modal-footer'),
                td_rq_expand = '.td_rq_expand';

            // init the modal if it hasn't been already, we know the modal has been initiated if a specific data attribute is set to true
            if ($container.data(DATA_ATTR_INITIATED) != 'true') {
                //setTimeout(function(){init(); }, 0);
                init();
            }


            // apply options
            switch (modal_status) {
                case true:
                    show();
                    break;
                case false:
                    hide();
                    break;
            }

            function init() {
                $container.data(DATA_ATTR_INITIATED, 'true');

                // close button event
                $close_button.click(function(event) {
                    hide();
                });

                setup_accessibility();

                var numOfItems = $item.length;

                if (clone_btn_close) {
                    // clone close button
                    if (numOfItems > 0) clone_close_button(numOfItems);
                }

                // clone the float close button
                if (clone_btn_close_float) clone_close_button_init();
                // custom arrangement: Quoter
                if (custom_arrangement && numOfItems > 0) change_layout(numOfItems);
            }

            // get the height of content div for custom scrollbar
            if($scrollable_area.get(0)) getBodyContHeight();

            function setSize(fixed_size){
                (fixed_size == true) ? $modal.css({'width':td_modal_width+'px','height':td_modal_height+'px'}) : $modal.css({'width':'auto','height':'100%'});
            }

            function getBodyContHeight(){

                var obj = $scrollable_area;
                var obj_id = obj.attr("id");

                var $obj = $('#'+obj_id).find('> div'); //'.modal-content'
                $obj.css('margin-right','1px');

                //console.log("td_modal_size_status:"+td_modal_size_status);

                if(window.innerWidth < 768){

                    // set modal window size
                    if(td_modal_size_status == true) setSize(false);

                    var m_cont_max_height = window.innerHeight;
                    var td_modal_header_h2_val = 0;
                    if($td_modal_header_h2.get(0)){
                        td_modal_header_h2_val = parseInt($td_modal_header_h2.outerHeight(true));
                    }

                    var header_space = parseInt($td_modal_header.css('marginTop').split('px')[0]) + td_modal_header_h2_val;
                    var footer_space = 0;
                    if($td_modal_footer.get(0)){ footer_space = $td_modal_footer.outerHeight(true); }

                    var tdExpand_height = 0;
                    var total_modal_scrollable_area_height = parseInt($obj.css('height').split('px')[0]);
                    m_cont_max_height = m_cont_max_height - header_space - footer_space;

                    if(total_modal_scrollable_area_height > m_cont_max_height){
                        g.ev.trigger(g.events.afterLargeModalOverlay,[{scrollbarInfo:{id:obj_id,height:m_cont_max_height+'px',status:'yes'}}]);
                    }else{
                        g.ev.trigger(g.events.afterLargeModalOverlay,[{scrollbarInfo:{id:obj_id,height:total_modal_scrollable_area_height,status:'no'}}]);
                    }

                }else{
                    // set modal window size
                    if(td_modal_size_status == true) setSize(true);

                    var cont_max_height;
                    if(td_modal_size_status == true){

                        if( parseInt(td_modal_height)  > document.documentElement.clientHeight){
                            $modal.css('height','100%');
                            cont_max_height = document.documentElement.clientHeight * 0.85;
                        }else{
                            $modal.css('height',td_modal_height+'px');
                            cont_max_height = parseInt(td_modal_height);
                        }
                    }else{
                        cont_max_height = document.documentElement.clientHeight * 0.85;
                    }

                    var td_modal_header_h2_val = 0;
                    if($td_modal_header_h2.get(0)){
                        td_modal_header_h2_val = parseInt($td_modal_header_h2.outerHeight(true));
                    }

                    var header_space = parseInt($td_modal_header.css('marginTop').split('px')[0]) + td_modal_header_h2_val;
                    var footer_space = 0;
                    if($td_modal_footer.get(0)){ footer_space = $td_modal_footer.outerHeight(true); }

                    var tdExpand_height = 0;
                    var total_modal_scrollable_area_height = parseInt($obj.css('height').split('px')[0]);


                    cont_max_height = cont_max_height - (header_space + 12) - footer_space; //(header_space + 40);

                    if(total_modal_scrollable_area_height > cont_max_height){
                        g.ev.trigger(g.events.afterLargeModalOverlay,[{scrollbarInfo:{id:obj_id,height:cont_max_height+'px',status:'yes'}}]);
                    }else{
                        g.ev.trigger(g.events.afterLargeModalOverlay,[{scrollbarInfo:{id:obj_id,height:total_modal_scrollable_area_height,status:'no'}}]);
                    }

                }
            }

            function clone_close_button_init() {
                var $cloned_obj = $close_button.clone(true);
                $cloned_obj.addClass('cloned_button');

                $container.append($cloned_obj);
                $cloned_btn = $('.cloned_button');
                $cloned_btn.hide();
            }

            function clone_close_button_float() {
                var modal_content_height = $modal.find(td_modal_content).outerHeight(true),
                    viewport_height = window.innerHeight,
                    $cloned_btn = $('.cloned_button');

                if (viewport_height <= modal_content_height) {
                    $close_button.hide();
                    $cloned_btn.show();
                } else {
                    $close_button.show();
                    $cloned_btn.hide();
                }
            }

            function clone_close_button(n) {
                if (typeof n === "number") {
                    if (n > 4) {
                        var $cloned_obj = $close_button.clone(true);
                        $cloned_obj.css({
                            'top': 'auto',
                            'bottom': '12px'
                        });
                        $cloned_obj.addClass('visible-xs');
                        $close_button.parent().append($cloned_obj);
                    }
                }
            }

            function change_layout(n) {
                if (typeof n === 'number') {
                    switch (n) {
                        case 5:
                        case 6:
                        case 3:
                            $item.removeClass('td-col-sm-3').addClass('td-col-sm-4');
                            break;
                        case 2:
                            $item.eq(0).addClass('td-col-sm-offset-3');
                            break;
                    }
                }
            }

            function show() {
                $container.addClass(MODAL_SHOW_CLASSNAME);
                $container.fadeTo(0, 0);
                $container.fadeTo(ANIMATION_DURATION, 1.0);
                //console.log("==========> show");
                $container.attr("aria-hidden", "false");

                var top_position = 0;

                //Set Modal Heading
                if (settings.modal_heading) {
                    $tdModalHeading.html(settings.modal_heading);
                    settings.modal_heading = '';
                }

                //Set Modal Content
                if (settings.modal_content) {
                    $tdModalContent.html(settings.modal_content);
                    settings.modal_content = '';
                }

                // Check for scroll height setting
                /*
                 if (settings.scroll_height) {
                 $('.td-slim-scroll').slimScroll({
                 height: settings.scroll_height,
                 alwaysVisible: true
                 });
                 }
                 */
                if(window.innerWidth < 768 && td_modal_size_status != true){
                    $container.css('height','100%');
                }else{
                    $container.height($window.height()); // set container height to viewport height (could normally use height: 100vh instead but there is a bug in Safari iOS)
                }

                // ScrollTop if modal height is greater than the viewport
                if ($modal.outerHeight() > $window.height()) {
                    $modal.scrollTop(1);
                }

                $('html, body').css({
                    "overflow": "hidden !important",
                    "position": "fixed"
                }); // disable page scrollbars, prevent iOS overflow scrolling

                // bind window resize event
                $window.on("resize.tdModal", on_window_resize);

                // accessibility
                // save the previous focus on the page before the modal window is opened
                if (mobile_iOS) { // iOS doesn't support 'document.activeElement'
                    $(document).off('click');
                    $(document).click(function(e) {
                        if(e.toElement.tagName != 'A' && e.toElement.tagName != 'BUTTON'){
                            $previous_focus = $(e.toElement).parents('a,button');
                        }else{
                            $previous_focus = $(e.toElement);
                        }
                    });
                } else {

                    $previous_focus = $(document.activeElement);

                }

                //$modal.find("." + MODAL_FOCUS_CLASSNAME).focus(); // put focus into modal

                //$close_button.focus(); // focus stays in close button when modal window loads
                $modal.focus(); // focus stays in modal div when modal window loads

 /*               // hide elements that are underneath the modal
                for (var i = 0; i < elements_to_hide.length; i++) {
                    var element = elements_to_hide[i];

                    // only apply to elements that does not already have aria-hidden=true
                    if (element.attr("aria-hidden") != "true") {
                        element.addClass(HIDDEN_ELEMENT_CLASSNAME); // add a class so we can identify this element later
                        element.attr("aria-hidden", "true");
                    }
                }
*/
                // if content has video, autostart playback
                $modal.find(VIDEO_CLASSNAME).each(function() {
                    var $player = $(this);
                    var player_id = $player.attr('id');
                    var videojs_player = videojs(player_id);

                    if (videojs_player != undefined) {
                        videojs_player.play();
                        // set focus on play button, a slight delay is needed in order for the controls to become visible first, otherwise focus doesn't work
                        setTimeout(function() {
                            $player.find('.vjs-play-control').focus();
                        }, 100);
                    }
                });

                g.ev.trigger(g.events.afterModalShown, [$container]);

                // check if the close button needs to be float
                if (clone_btn_close_float) clone_close_button_float();

                // Anchor down
                if($scrollable_area.get(0) && anchor_status == true && typeof anchor_targetedID !== 'undefined'){
                    scroll_down(anchor_targetedID);
                }else{
                    if($scrollable_area.get(0)){
                        scroll_init();
                    }
                }

            }

            function hide() {

                $container.fadeTo(ANIMATION_DURATION, 0.0, function() {
                    $container.removeClass(MODAL_SHOW_CLASSNAME);
                });
                //console.log("==========> hide");
                $container.attr("aria-hidden", "true").css({
                    "display": "none"
                });

                $('html, body').css({
                    "overflow": "",
                    "position": ""
                }); // re-enable page scrollbars and iOS overflow scrolling

                // unbind window resize event
                $window.off("resize.tdModal");

                // if content has video, stop playback
                $modal.find(VIDEO_CLASSNAME).each(function() {
                    var player_id = $(this).attr('id');
                    var videojs_player = videojs(player_id);

                    if (videojs_player != undefined) {
                        videojs_player.currentTime(0);
                        videojs_player.pause();
                    }
                });

                // accessibility
                // remove aria-hidden true that was previously applied to elements underneath the modal
                for (var i = 0; i < elements_to_hide.length; i++) {
                    var element = elements_to_hide[i];

                    if (element.hasClass(HIDDEN_ELEMENT_CLASSNAME)) {
                        element.removeClass(HIDDEN_ELEMENT_CLASSNAME);
                        element.attr("aria-hidden", "false");
                    }
                }


                if ($previous_focus != undefined) {
                    $previous_focus.focus(); // accessibility restore focus to before modal window opened
                }

                $modal.scrollTop(1); // scroll down slightly, this fixes a bug on iOS where scrolling is frozen if user scrolls up when scroll position is 0

                g.ev.trigger(g.events.afterModalHidden);

            }

            function on_window_resize() {
                // update container height
                //sconsole.log('resize');
                //$container.height($window.height());

                if(window.innerWidth < 768 && td_modal_size_status != true){
                    $container.css('height','100%');
                }else{
                    $container.height($window.height()); // set container height to viewport height (could normally use height: 100vh instead but there is a bug in Safari iOS)
                }

                // check if the close button needs to be float
                if (clone_btn_close_float) clone_close_button_float();
                if($scrollable_area.get(0)) getBodyContHeight();
            }

            /**
             * Anchor down - scrolling down to targeted element
             */
            function scroll_down(gID){
                scroll_init();

                var $targetedObj = $('#'+gID),
                    distance = $targetedObj.offset().top,
                    dis_offset = $scrollable_area.offset().top,
                    distance = distance - dis_offset;

                $targetedObj.attr('tabindex','0');
                $scrollable_area.animate({
                    scrollTop: distance
                }, anchor_scrolling_speed,function(){
                    $targetedObj.focus().css({'outline-width': '0px'});

                });

                // reset - anchor donw settings for other modal window links
                settings.anchor.status = false;
                settings.anchor.targetedID = '';
            }
            function scroll_init(){ // initiated
                var dis_offset = $scrollable_area.offset().top;
                $scrollable_area.scrollTop(0);
            }

            /**
             * Set up accessibility
             */
            function setup_accessibility() {
                //$modal.attr("role", "dialog").attr("tabindex","-1");
                $modal.attr("role", "dialog").attr("tabindex","0");
                //console.log("==========> accessibility");
                $container.attr("aria-hidden", "true");
                $close_button.attr("aria-label", "close");

                // create a screen reader text element that can get focus and be read, this is necessary for VoiceOver as it needs some content to focus on
                $modal.attr('aria-label',accessibility_modal_description);

                /*
                 var $content_focus_element = $('<div tabindex="0" class="' + MODAL_FOCUS_CLASSNAME + ' td-forscreenreader">'+accessibility_modal_description+'</div>');
                 $modal.prepend($content_focus_element);
                 */

                var focusableElementsString = "a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]";

                // trap the focus inside modal window
                $('body').on( "keydown", td_modal, function( event ) {
                    var $this = $(this);

                    if ( event.keyCode == 9) { // tab

                        // get list of all children elements in given object
                        var children = $this.find('*');

                        // get list of focusable items
                        var focusableItems = children.filter(focusableElementsString).filter(':visible');

                        // get currently focused item
                        var focusedItem = $( document.activeElement );

                        // get the number of focusable items
                        var numberOfFocusableItems = focusableItems.length;

                        var focusedItemIndex = focusableItems.index(focusedItem);

                        if ( !event.shiftKey && (focusedItemIndex == numberOfFocusableItems - 1) ){
                            focusableItems.get(0).focus();
                            event.preventDefault();
                        }
                        if ( event.shiftKey && focusedItemIndex == 0 ){
                            focusableItems.get(numberOfFocusableItems - 1).focus();
                            event.preventDefault();
                        }
                    }
                }).on( "keyup", ":not("+td_modal+")", function( event ) {
                    var $this = $(this),
                        $obj_modal = $(td_modal),
                        focusedItem = $( document.activeElement ),
                        in_modal = focusedItem.parents(td_modal).length ? true : false;
                    //$close = $('#js-modal-close');

                    if ( $obj_modal.length && event.keyCode == 9 && in_modal === false ) { // tab
                        $close_button.focus();
                    }

                });

                /*	Commented out due to accessibility issue

                 // create a first & last focusable element in the modal container, when this element gets focus we set the focus back to the modal
                 // this is to prevent tabbing from going outside the modal on to other page elements hidden underneath
                 var $last_focus_element = $('<div tabindex="0" class="focus_element"></div>');
                 $container.append($last_focus_element);
                 $last_focus_element.focus(function(e) {
                 e.stopPropagation();
                 //$modal.focus();
                 $close_button.focus();
                 });

                 var $first_focus_element = $('<div tabindex="0" class="focus_element"></div>');
                 $container.prepend($first_focus_element);
                 $first_focus_element.focus(function(e) {
                 e.stopPropagation();
                 //$modal.focus();
                 //$close_button.focus();

                 $modal.find("[tabindex='0']:visible:not('focus_element'), a:visible, button:visible").last().focus(); // focus on last element that can receive focus
                 });
                 */

                // add escape key to close modal
                $container.keydown(function(e) {
                    if (e.which === 27) {
                        hide();
                    }
                });

                // add click event to outside container to close modal
                $container.click(function(e) {
                    if(td_overlay_close != false) {
                        hide();
                    }
                });

                // prevent container's child click events from propagating
                $container.children().click(function(e) {
                    e.stopPropagation();
                });
            }

        };

    // create unique id to keep track of each instance of the tabs-carousel
    if (!$.tdModalModules) {
        $.tdModalModules = {
            uid: 0
        };
    }

    // PLUGINS DEFINITION
    // ==================
    $.fn.tdModal = g.getPluginDef(modal, 'tdModal');

    // DEFAULT SETTINGS
    // ================
    $.fn.tdModal.defaults = {
        custom_arrangement: true,
        modal_status: false,
        clone_btn_close: false,
        clone_btn_close_float: true,
        anchor:{
            status: false,
            targetedID: '',
            scrolling_speed: 600
        },
        accessibility_modal_description: 'Modal content',
        size:{
            status: false,
            width: 'auto',
            height: '100%'
        },
        overlayClose: true
    };
    setTimeout(function(){
        $(".td-modal-container, .td_rq_modal-accessible").tdModal(); }
    , 1000);


}(global));}]
}));