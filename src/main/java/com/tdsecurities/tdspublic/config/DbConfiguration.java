package com.tdsecurities.tdspublic.config;

import javax.sql.DataSource;
import javax.naming.NamingException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.jndi.JndiTemplate;

// @Configuration
public class DbConfiguration {

private static final Logger logger = LoggerFactory.getLogger(DbConfiguration.class);
    @Autowired
    private Environment env;
    private String tdsDatasourceJndiName = "java:comp/env/jdbc/db";
    
    /*
    @Bean
    public DataSource getDataSource() {
        logger.info("Setting up local datasource");
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("spring.datasource.driverClassName"));
        dataSourceBuilder.url(env.getProperty("spring.datasource.local.url"));
        dataSourceBuilder.username(env.getProperty("spring.datasource.local.username"));
        dataSourceBuilder.password(env.getProperty("spring.datasource.local.password"));
        return dataSourceBuilder.build();
    }
    */

    // @Bean
    public DataSource getAzureDataSource() throws NamingException {


        logger.info("About to connect to DB token service");
	JndiTemplate jndiTemplate = new JndiTemplate();

	DataSource dataSource = (DataSource) jndiTemplate.lookup(tdsDatasourceJndiName);

        logger.info("Got response from DB token service");

        return dataSource;
    }
    
    
}
