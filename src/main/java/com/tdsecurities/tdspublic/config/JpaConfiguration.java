package com.tdsecurities.tdspublic.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.tdsecurities.tdspublic.data")
@EntityScan("com.tdsecurities.tdspublic.data")
public class JpaConfiguration {

}