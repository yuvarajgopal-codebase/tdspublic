package com.tdsecurities.tdspublic.delivery;

import java.util.Optional;

import com.tdsecurities.tdspublic.data.PageInfo;
import com.tdsecurities.tdspublic.data.PageInfoId;
import com.tdsecurities.tdspublic.data.PageInfoRepository;

/*
import com.tdsecurities.common.data.ActivePage;
import com.tdsecurities.common.data.ActivePageRepository;
import com.tdsecurities.common.service.PageService;
*/

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping(path="/tds")
public class PageController {

    private static final Logger logger = LoggerFactory.getLogger(PageController.class);

    /*
    @Autowired
    PageService pageService;

    @Autowired
    ActivePageRepository activePageRepository;
    */

    @Autowired
    PageInfoRepository pageInfoRepository;

    private String DEFAULT_LANG = "en_CA";

    @GetMapping(path="/content/{pageName}")
    public @ResponseBody String getPage(@PathVariable("pageName") String pageName,
        @RequestParam(required = false) String language) {

        /* if (!pageName.equals("Leadership")) {
            logger.error("Could not find page {}", pageName);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        */

        if (pageName == null) {
            logger.error("No page identifier was provided");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (language == null) {
            logger.info("No language provided. Using default language.");
            language = DEFAULT_LANG;
        }

    // try {


        logger.info("Processing request for page {}", pageName);
        Optional<PageInfo> pageInfoOptional = pageInfoRepository.findById(
            new PageInfoId(pageName, 1));
            
        if (!pageInfoOptional.isPresent()) {
            logger.warn("Cound not find Page {} with Version {}", pageName, 1);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        PageInfo pageInfo = pageInfoOptional.get();
        logger.info("Found Page {}", pageInfo.getId());

        return pageInfo.toString();

        /*
        Optional<ActivePage> activePageOptional = activePageRepository.findById(pageName);
        if (!activePageOptional.isPresent()) {
            logger.error("Could not find page {}", pageName);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        */


            /*ActivePage activePage = activePageOptional.get();

            String pageHtml = pageService.getPageContent(activePage.getPageId(), 
                activePage.getLiveVersion(), language);

            if (pageHtml == null) {
                logger.error("Error generating page content");
                throw new Exception("Error generating page content");
            }

            return pageHtml;
            */

            /*
                Template pageTemplate = freeMarkerConfigurer.getConfiguration().getTemplate("page/Template1.ftl");

                Map<String, Object> params = new HashMap<>();
                params.put("name", pageName);

                StringWriter pageStringWriter = new StringWriter();
                pageTemplate.process(params, pageStringWriter);
                
                return pageStringWriter.toString();
            */

            /*
        } catch (Exception e) {
            logger.error("Failed to load page", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        */
    }
    
}