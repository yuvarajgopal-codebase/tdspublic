package com.tdsecurities.tdspublic.support;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HealthStatusController {


    @GetMapping(path="/ping")
    public ResponseEntity<?> performHealthCheck() {
        return new ResponseEntity<>("success", HttpStatus.OK);
    }
}