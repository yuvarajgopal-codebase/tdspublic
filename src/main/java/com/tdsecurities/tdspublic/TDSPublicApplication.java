package com.tdsecurities.tdspublic;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;

/**
 * Used to run application as a SpringBoot war.
 * Uncomment annotations to use.
 */

@SpringBootApplication(scanBasePackages={"com.tdsecurities.tdspublic", "com.tdsecurities.common"})
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class TDSPublicApplication extends SpringBootServletInitializer  {

    public static void main(String args[]) {
        SpringApplication.run(TDSPublicApplication.class, args);
        System.out.println("Started TD Securities Public Application");
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(TDSPublicApplication.class);
    }
}
