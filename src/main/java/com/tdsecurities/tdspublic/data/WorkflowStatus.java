package com.tdsecurities.tdspublic.data;

public enum WorkflowStatus {
    Draft,
    Submitted,
    Stage,
    Published,
    UnPublished
}