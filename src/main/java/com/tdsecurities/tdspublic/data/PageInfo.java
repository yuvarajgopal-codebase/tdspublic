package com.tdsecurities.tdspublic.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "tdspw_page_info")
@IdClass(PageInfoId.class)
public class PageInfo {

    @Id
    private String id;

    @Id
    private long version;

    @Column(name ="template_id")
    private String templateId;

    @Column(name = "template_version")
    private long templateVersion;

    // private String title;

    private String notes;

    @Enumerated(EnumType.STRING)
    private WorkflowStatus status;

    // @Column(name = "analytics_tag")
    // private String analyticsTag;

    private boolean active;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "updated_by")
    private String updatedBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public long getTemplateVersion() {
        return templateVersion;
    }

    public void setTemplateVersion(long templateVersion) {
        this.templateVersion = templateVersion;
    }
    
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public WorkflowStatus getStatus() {
        return status;
    }

    public void setStatus(WorkflowStatus status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "PageInfo [active=" + active + ", createdAt=" + createdAt + ", createdBy=" + createdBy + ", id=" + id
                + ", notes=" + notes + ", status=" + status + ", templateId=" + templateId + ", templateVersion="
                + templateVersion + ", updatedAt=" + updatedAt + ", updatedBy=" + updatedBy + ", version=" + version
                + "]";
    }

    /*

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnalyticsTag() {
        return analyticsTag;
    }

    public void setAnalyticsTag(String analyticsTag) {
        this.analyticsTag = analyticsTag;
    }
    */


    

}