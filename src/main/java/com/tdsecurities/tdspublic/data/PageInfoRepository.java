package com.tdsecurities.tdspublic.data;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageInfoRepository extends CrudRepository<PageInfo, PageInfoId> {

    List<PageInfo> findAll();

}