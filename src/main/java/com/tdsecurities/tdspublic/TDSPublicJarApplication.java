package com.tdsecurities.tdspublic;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.Controller;

/**
 * Used to run application as a SpringBoot jar.
 * Uncomment annotations to use.
 */
/*
@Controller
@SpringBootApplication
*/
public class TDSPublicJarApplication {

    public static void main(String args[]) {
        SpringApplication.run(TDSPublicJarApplication.class, args);
        System.out.println("Started TD Securities Public Application");
    }
}