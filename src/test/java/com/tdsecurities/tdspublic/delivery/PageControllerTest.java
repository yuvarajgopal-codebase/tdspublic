package com.tdsecurities.tdspublic.delivery;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Optional;

import com.tdsecurities.tdspublic.data.PageInfo;
import com.tdsecurities.tdspublic.data.PageInfoRepository;

/*
import com.tdsecurities.common.data.ActivePage;
import com.tdsecurities.common.data.ActivePageRepository;
import com.tdsecurities.common.service.PageService;
*/

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(PageController.class)
public class PageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    /*
    @MockBean
    private PageService pageServiceMock;

    @MockBean
    private ActivePageRepository activePageRepositoryMock;
    */

    @MockBean
    private PageInfoRepository pageInfoRepositoryMock;

    /*
    @Test
    public void testSuccessfulPage() throws Exception {

        ActivePage activePageMock = Mockito.mock(ActivePage.class);
        when(activePageMock.getPageId()).thenReturn("Leadership");
        when(activePageMock.getLiveVersion()).thenReturn(1L);

        Optional<ActivePage> optionalPage = Optional.of(activePageMock);        
        when(activePageRepositoryMock.findById("Leadership")).thenReturn(optionalPage);

        when(pageServiceMock.getPageContent(anyString(), anyLong(), anyString())).thenReturn("Test Page");

        this.mockMvc.perform(get("/tds/content/Leadership")).andExpect(status().isOk());
    }

    @Test
    public void testPageNotFound() throws Exception {
        Optional<ActivePage> optionalPage = Optional.empty();
        when(activePageRepositoryMock.findById(anyString())).thenReturn(optionalPage);

        this.mockMvc.perform(get("/tds/content/Hello")).andExpect(status().isNotFound());
    }*/

    @Test
    public void testSuccessfulPage() throws Exception {

        PageInfo pageInfo = new PageInfo();
        pageInfo.setId("Leadership");
        
        Optional<PageInfo> optionalPage = Optional.of(pageInfo);        
        when(pageInfoRepositoryMock.findById(any())).thenReturn(optionalPage);

        this.mockMvc.perform(get("/tds/content/Leadership")).andExpect(status().isOk());
    }

    @Test
    public void testPageNotFound() throws Exception {

        Optional<PageInfo> optionalPage = Optional.empty();
        when(pageInfoRepositoryMock.findById(any())).thenReturn(optionalPage);

        this.mockMvc.perform(get("/tds/content/Hello")).andExpect(status().isNotFound());
    }


    public void testFailedPage() throws Exception {
    }

}